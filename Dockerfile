FROM nextcloud:latest

EXPOSE 80/tcp

ADD ./files/nextcloud-data.tar.gz /var/www/html/data/
