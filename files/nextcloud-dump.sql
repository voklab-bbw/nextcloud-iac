-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: nextcloud
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `nextcloud`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `nextcloud` /*!40100 DEFAULT CHARACTER SET latin1 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `nextcloud`;

--
-- Table structure for table `oc_accounts`
--

DROP TABLE IF EXISTS `oc_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_accounts` (
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_accounts`
--

LOCK TABLES `oc_accounts` WRITE;
/*!40000 ALTER TABLE `oc_accounts` DISABLE KEYS */;
INSERT INTO `oc_accounts` VALUES ('admin','{\"displayname\":{\"value\":\"admin\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('disableduser','{\"displayname\":{\"value\":\"disableduser\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('testuser','{\"displayname\":{\"value\":\"testuser\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('testuser2','{\"displayname\":{\"value\":\"testuser2\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('testuser3','{\"displayname\":{\"value\":\"testuser3\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('vok','{\"displayname\":{\"value\":\"vok\",\"scope\":\"contacts\"},\"address\":{\"value\":\"\",\"scope\":\"private\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":\"kevin.vo@sunrise.net\",\"scope\":\"private\",\"verified\":\"1\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}');
/*!40000 ALTER TABLE `oc_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_activity`
--

DROP TABLE IF EXISTS `oc_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_activity` (
  `activity_id` bigint NOT NULL AUTO_INCREMENT,
  `timestamp` int NOT NULL DEFAULT '0',
  `priority` int NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `affecteduser` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `app` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `subjectparams` longtext COLLATE utf8mb4_bin NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `messageparams` longtext COLLATE utf8mb4_bin,
  `file` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `link` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `object_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `object_id` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`),
  KEY `activity_user_time` (`affecteduser`,`timestamp`),
  KEY `activity_filter_by` (`affecteduser`,`user`,`timestamp`),
  KEY `activity_filter` (`affecteduser`,`type`,`app`,`timestamp`),
  KEY `activity_object` (`object_type`,`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_activity`
--

LOCK TABLES `oc_activity` WRITE;
/*!40000 ALTER TABLE `oc_activity` DISABLE KEYS */;
INSERT INTO `oc_activity` VALUES (1,1678194775,30,'file_created','admin','admin','files','created_self','[{\"3\":\"\\/bbw-logo.png\"}]','','[]','/bbw-logo.png','http://localhost/index.php/apps/files/?dir=/','files',3),(2,1678194775,30,'file_created','admin','admin','files','created_self','[{\"4\":\"\\/Photos\"}]','','[]','/Photos','http://localhost/index.php/apps/files/?dir=/','files',4),(3,1678194775,30,'file_created','admin','admin','files','created_self','[{\"5\":\"\\/Photos\\/Coast.jpg\"}]','','[]','/Photos/Coast.jpg','http://localhost/index.php/apps/files/?dir=/Photos','files',5),(4,1678194775,30,'file_created','admin','admin','files','created_self','[{\"6\":\"\\/Photos\\/Hummingbird.jpg\"}]','','[]','/Photos/Hummingbird.jpg','http://localhost/index.php/apps/files/?dir=/Photos','files',6),(5,1678194775,30,'file_created','admin','admin','files','created_self','[{\"7\":\"\\/Photos\\/Nut.jpg\"}]','','[]','/Photos/Nut.jpg','http://localhost/index.php/apps/files/?dir=/Photos','files',7),(6,1678194775,30,'file_created','admin','admin','files','created_self','[{\"8\":\"\\/Nextcloud.mp4\"}]','','[]','/Nextcloud.mp4','http://localhost/index.php/apps/files/?dir=/','files',8),(7,1678194775,30,'file_created','admin','admin','files','created_self','[{\"9\":\"\\/Nextcloud Manual.pdf\"}]','','[]','/Nextcloud Manual.pdf','http://localhost/index.php/apps/files/?dir=/','files',9),(8,1678194775,30,'file_created','admin','admin','files','created_self','[{\"10\":\"\\/Documents\"}]','','[]','/Documents','http://localhost/index.php/apps/files/?dir=/','files',10),(9,1678194775,30,'file_created','admin','admin','files','created_self','[{\"11\":\"\\/Documents\\/About.odt\"}]','','[]','/Documents/About.odt','http://localhost/index.php/apps/files/?dir=/Documents','files',11),(10,1678194775,30,'file_created','admin','admin','files','created_self','[{\"12\":\"\\/Documents\\/About.txt\"}]','','[]','/Documents/About.txt','http://localhost/index.php/apps/files/?dir=/Documents','files',12),(11,1678194775,30,'calendar','admin','admin','dav','calendar_add_self','{\"actor\":\"admin\",\"calendar\":{\"id\":1,\"uri\":\"personal\",\"name\":\"Personal\"}}','','[]','','','calendar',1),(12,1678194776,30,'calendar','admin','admin','dav','calendar_add_self','{\"actor\":\"admin\",\"calendar\":{\"id\":2,\"uri\":\"admincal\",\"name\":\"admincal\"}}','','[]','','','calendar',2),(13,1678351486,30,'file_created','admin','admin','files','created_self','[{\"144\":\"\\/Test\"}]','','[]','/Test','http://fair-coral.maas/index.php/apps/files/?dir=/','files',144),(14,1678351646,30,'file_created','admin','admin','files','created_self','[{\"145\":\"\\/Test\\/10.1_ContainerVirtualisierung.pdf\"}]','','[]','/Test/10.1_ContainerVirtualisierung.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',145),(15,1678351646,30,'file_created','admin','admin','files','created_self','[{\"146\":\"\\/Test\\/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf\"}]','','[]','/Test/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',146),(16,1678351647,30,'file_created','admin','admin','files','created_self','[{\"147\":\"\\/Test\\/10.3_Container_oder_VM_Pros_und_Kontras.pdf\"}]','','[]','/Test/10.3_Container_oder_VM_Pros_und_Kontras.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',147),(17,1678351647,30,'file_created','admin','admin','files','created_self','[{\"148\":\"\\/Test\\/10.4_CV_Versus_VM.docx\"}]','','[]','/Test/10.4_CV_Versus_VM.docx','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',148),(18,1678351647,30,'file_created','admin','admin','files','created_self','[{\"149\":\"\\/Test\\/10.5_Docker Einstieg - CLI-Befehle.pdf\"}]','','[]','/Test/10.5_Docker Einstieg - CLI-Befehle.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',149),(19,1678351647,30,'file_created','admin','admin','files','created_self','[{\"150\":\"\\/Test\\/docker-compose.yml\"}]','','[]','/Test/docker-compose.yml','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',150),(20,1678352135,30,'file_created','admin','admin','files','created_self','[{\"152\":\"\\/Testfile.txt\"}]','','[]','/Testfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',152),(21,1678352141,30,'file_changed','admin','admin','files','changed_self','[{\"152\":\"\\/Testfile.txt\"}]','','[]','/Testfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',152),(22,1678352217,30,'file_created','admin','admin','files','created_self','[{\"159\":\"\\/m129_bandbreitenberechnung.pdf\"}]','','[]','/m129_bandbreitenberechnung.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',159),(23,1678352218,30,'file_created','admin','admin','files','created_self','[{\"160\":\"\\/m129_bandbreitenberechnung-lsg.pdf\"}]','','[]','/m129_bandbreitenberechnung-lsg.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',160),(24,1678352368,30,'calendar','admin','system','dav','calendar_add','{\"actor\":\"admin\",\"calendar\":{\"id\":3,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',3),(25,1678356423,30,'file_created','admin','admin','files','created_self','[{\"193\":\"\\/deletedfile.txt\"}]','','[]','/deletedfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',193),(26,1678356431,30,'file_deleted','admin','admin','files','deleted_self','[{\"193\":\"\\/deletedfile.txt\"}]','','[]','/deletedfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',193),(27,1678358159,30,'file_created','vok','vok','files','created_self','[{\"237\":\"\\/bbw-logo.png\"}]','','[]','/bbw-logo.png','http://fair-coral.maas/index.php/apps/files/?dir=/','files',237),(28,1678358159,30,'file_created','vok','vok','files','created_self','[{\"238\":\"\\/Photos\"}]','','[]','/Photos','http://fair-coral.maas/index.php/apps/files/?dir=/','files',238),(29,1678358159,30,'file_created','vok','vok','files','created_self','[{\"239\":\"\\/Photos\\/Coast.jpg\"}]','','[]','/Photos/Coast.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',239),(30,1678358159,30,'file_created','vok','vok','files','created_self','[{\"240\":\"\\/Photos\\/Hummingbird.jpg\"}]','','[]','/Photos/Hummingbird.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',240),(31,1678358159,30,'file_created','vok','vok','files','created_self','[{\"241\":\"\\/Photos\\/Nut.jpg\"}]','','[]','/Photos/Nut.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',241),(32,1678358159,30,'file_created','vok','vok','files','created_self','[{\"242\":\"\\/Nextcloud.mp4\"}]','','[]','/Nextcloud.mp4','http://fair-coral.maas/index.php/apps/files/?dir=/','files',242),(33,1678358159,30,'file_created','vok','vok','files','created_self','[{\"243\":\"\\/Nextcloud Manual.pdf\"}]','','[]','/Nextcloud Manual.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',243),(34,1678358159,30,'file_created','vok','vok','files','created_self','[{\"244\":\"\\/Documents\"}]','','[]','/Documents','http://fair-coral.maas/index.php/apps/files/?dir=/','files',244),(35,1678358159,30,'file_created','vok','vok','files','created_self','[{\"245\":\"\\/Documents\\/About.odt\"}]','','[]','/Documents/About.odt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',245),(36,1678358160,30,'file_created','vok','vok','files','created_self','[{\"246\":\"\\/Documents\\/About.txt\"}]','','[]','/Documents/About.txt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',246),(37,1678358160,30,'calendar','vok','vok','dav','calendar_add_self','{\"actor\":\"vok\",\"calendar\":{\"id\":4,\"uri\":\"personal\",\"name\":\"Personal\"}}','','[]','','','calendar',4),(38,1678358180,30,'file_created','vok','vok','files','created_self','[{\"251\":\"\\/testfile.txt\"}]','','[]','/testfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',251),(39,1678358187,30,'file_created','vok','vok','files','created_self','[{\"253\":\"\\/testfile2.txt\"}]','','[]','/testfile2.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',253),(40,1678358226,30,'personal_settings','vok','vok','settings','email_changed_self','[]','','[]','','','',0),(41,1678358227,30,'personal_settings','vok','vok','settings','email_changed_self','[]','','[]','','','',0),(42,1678359111,30,'file_created','vok','vok','files','created_self','[{\"331\":\"\\/!!!IMPORTANT!!!\"}]','','[]','/!!!IMPORTANT!!!','http://fair-coral.maas/index.php/apps/files/?dir=/','files',331),(43,1678359119,30,'file_created','vok','vok','files','created_self','[{\"332\":\"\\/!!!IMPORTANT!!!\\/WIN_20211028_15_17_04_Pro.jpg\"}]','','[]','/!!!IMPORTANT!!!/WIN_20211028_15_17_04_Pro.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/%21%21%21IMPORTANT%21%21%21','files',332),(44,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"343\":\"\\/bbw-logo.png\"}]','','[]','/bbw-logo.png','http://fair-coral.maas/index.php/apps/files/?dir=/','files',343),(45,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"344\":\"\\/Photos\"}]','','[]','/Photos','http://fair-coral.maas/index.php/apps/files/?dir=/','files',344),(46,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"345\":\"\\/Photos\\/Coast.jpg\"}]','','[]','/Photos/Coast.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',345),(47,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"346\":\"\\/Photos\\/Hummingbird.jpg\"}]','','[]','/Photos/Hummingbird.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',346),(48,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"347\":\"\\/Photos\\/Nut.jpg\"}]','','[]','/Photos/Nut.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',347),(49,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"348\":\"\\/Nextcloud.mp4\"}]','','[]','/Nextcloud.mp4','http://fair-coral.maas/index.php/apps/files/?dir=/','files',348),(50,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"349\":\"\\/Nextcloud Manual.pdf\"}]','','[]','/Nextcloud Manual.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',349),(51,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"350\":\"\\/Documents\"}]','','[]','/Documents','http://fair-coral.maas/index.php/apps/files/?dir=/','files',350),(52,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"351\":\"\\/Documents\\/About.odt\"}]','','[]','/Documents/About.odt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',351),(53,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"352\":\"\\/Documents\\/About.txt\"}]','','[]','/Documents/About.txt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',352),(54,1678956926,30,'calendar','testuser2','testuser2','dav','calendar_add_self','{\"actor\":\"testuser2\",\"calendar\":{\"id\":5,\"uri\":\"personal\",\"name\":\"Personal\"}}','','[]','','','calendar',5),(55,1678956975,30,'file_created','testuser2','testuser2','files','created_self','[{\"357\":\"\\/Subnetting Table.xlsx\"}]','','[]','/Subnetting Table.xlsx','http://fair-coral.maas/index.php/apps/files/?dir=/','files',357),(56,1678956996,30,'file_created','testuser2','testuser2','files','created_self','[{\"359\":\"\\/05_subnetting.pdf\"}]','','[]','/05_subnetting.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',359),(57,1678956996,30,'file_created','testuser2','testuser2','files','created_self','[{\"360\":\"\\/Subnetting (Post-Routing).pkt\"}]','','[]','/Subnetting (Post-Routing).pkt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',360),(58,1678956997,30,'file_created','testuser2','testuser2','files','created_self','[{\"361\":\"\\/Subnetting.docx\"}]','','[]','/Subnetting.docx','http://fair-coral.maas/index.php/apps/files/?dir=/','files',361),(59,1678956997,30,'file_created','testuser2','testuser2','files','created_self','[{\"363\":\"\\/Subnetting.pdf\"}]','','[]','/Subnetting.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',363),(60,1678956998,30,'file_created','testuser2','testuser2','files','created_self','[{\"364\":\"\\/Subnetting.pkt\"}]','','[]','/Subnetting.pkt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',364),(61,1681985340,30,'calendar','admin','admin','dav','calendar_add_self','{\"actor\":\"admin\",\"calendar\":{\"id\":6,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',6),(62,1681985340,30,'calendar','testuser2','testuser2','dav','calendar_add_self','{\"actor\":\"testuser2\",\"calendar\":{\"id\":7,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',7),(63,1681985340,30,'calendar','vok','vok','dav','calendar_add_self','{\"actor\":\"vok\",\"calendar\":{\"id\":8,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',8);
/*!40000 ALTER TABLE `oc_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_activity_mq`
--

DROP TABLE IF EXISTS `oc_activity_mq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_activity_mq` (
  `mail_id` bigint NOT NULL AUTO_INCREMENT,
  `amq_timestamp` int NOT NULL DEFAULT '0',
  `amq_latest_send` int NOT NULL DEFAULT '0',
  `amq_type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amq_affecteduser` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `amq_appid` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `amq_subject` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amq_subjectparams` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `object_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `object_id` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`mail_id`),
  KEY `amp_user` (`amq_affecteduser`),
  KEY `amp_latest_send_time` (`amq_latest_send`),
  KEY `amp_timestamp_time` (`amq_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_activity_mq`
--

LOCK TABLES `oc_activity_mq` WRITE;
/*!40000 ALTER TABLE `oc_activity_mq` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_activity_mq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_addressbookchanges`
--

DROP TABLE IF EXISTS `oc_addressbookchanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_addressbookchanges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `addressbookid` bigint NOT NULL,
  `operation` smallint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `addressbookid_synctoken` (`addressbookid`,`synctoken`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_addressbookchanges`
--

LOCK TABLES `oc_addressbookchanges` WRITE;
/*!40000 ALTER TABLE `oc_addressbookchanges` DISABLE KEYS */;
INSERT INTO `oc_addressbookchanges` VALUES (1,'Database:vok.vcf',1,3,1),(2,'Database:testuser.vcf',2,3,1),(3,'Database:testuser2.vcf',3,3,1),(4,'Database:testuser2.vcf',4,3,2),(5,'Database:testuser.vcf',5,3,2),(6,'Database:testuser3.vcf',6,3,1),(7,'Database:testuser3.vcf',7,3,2),(8,'Database:disableduser.vcf',8,3,1),(9,'Database:disableduser.vcf',9,3,2),(10,'Database:admin.vcf',10,3,1),(11,'Database:admin.vcf',11,3,2),(12,'Database:admin.vcf',12,3,2),(13,'Database:admin.vcf',13,3,2),(14,'Database:vok.vcf',14,3,2),(15,'Database:vok.vcf',15,3,2),(16,'Database:vok.vcf',16,3,2),(17,'Database:vok.vcf',17,3,2),(18,'Database:vok.vcf',18,3,2),(19,'Database:vok.vcf',19,3,2),(20,'Database:vok.vcf',20,3,2),(21,'Database:vok.vcf',21,3,2),(22,'Database:vok.vcf',22,3,2);
/*!40000 ALTER TABLE `oc_addressbookchanges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_addressbooks`
--

DROP TABLE IF EXISTS `oc_addressbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_addressbooks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `addressbook_index` (`principaluri`,`uri`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_addressbooks`
--

LOCK TABLES `oc_addressbooks` WRITE;
/*!40000 ALTER TABLE `oc_addressbooks` DISABLE KEYS */;
INSERT INTO `oc_addressbooks` VALUES (1,'principals/users/admin','Contacts','contacts',NULL,1),(2,'principals/users/admin','admincard','admincard',NULL,1),(3,'principals/system/system','system','system','System addressbook which holds all users of this instance',23),(4,'principals/users/vok','Contacts','contacts',NULL,1),(5,'principals/users/testuser2','Contacts','contacts',NULL,1);
/*!40000 ALTER TABLE `oc_addressbooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_appconfig`
--

DROP TABLE IF EXISTS `oc_appconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_appconfig` (
  `appid` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configkey` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configvalue` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`appid`,`configkey`),
  KEY `appconfig_config_key_index` (`configkey`),
  KEY `appconfig_appid_key` (`appid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_appconfig`
--

LOCK TABLES `oc_appconfig` WRITE;
/*!40000 ALTER TABLE `oc_appconfig` DISABLE KEYS */;
INSERT INTO `oc_appconfig` VALUES ('accessibility','enabled','yes'),('accessibility','installed_version','1.6.0'),('accessibility','types',''),('activity','enabled','yes'),('activity','installed_version','2.13.4'),('activity','types','filesystem'),('backgroundjob','lastjob','82'),('cloud_federation_api','enabled','yes'),('cloud_federation_api','installed_version','1.3.0'),('cloud_federation_api','types','filesystem'),('comments','enabled','yes'),('comments','installed_version','1.10.0'),('comments','types','logging'),('contactsinteraction','enabled','yes'),('contactsinteraction','installed_version','1.1.0'),('contactsinteraction','types','dav'),('core','backgroundjobs_mode','cron'),('core','enterpriseLogoChecked','yes'),('core','installedat','1678194770.1388'),('core','lastcron','1682686614'),('core','lastupdateResult','{\"version\":\"13.0.12.1\",\"versionstring\":\"Nextcloud 13.0.12\",\"url\":\"https:\\/\\/download.nextcloud.com\\/server\\/releases\\/nextcloud-13.0.12.zip\",\"web\":\"https:\\/\\/docs.nextcloud.com\\/server\\/13\\/admin_manual\\/maintenance\\/upgrade.html\",\"autoupdater\":\"1\",\"eol\":\"1\"}'),('core','lastupdatedat','0'),('core','moveavatarsdone','yes'),('core','oc.integritycheck.checker','[]'),('core','previewsCleanedUp','1'),('core','public_files','files_sharing/public.php'),('core','public_webdav','dav/appinfo/v1/publicwebdav.php'),('core','scss.variables','81ed5474f262cf7e7b7290f9a8cb0f1a'),('core','theming.variables','e06163e2b33a3c3c3e83e3a3e06955fa'),('core','vendor','nextcloud'),('dashboard','enabled','yes'),('dashboard','installed_version','7.0.0'),('dashboard','types',''),('dav','buildCalendarReminderIndex','yes'),('dav','buildCalendarSearchIndex','yes'),('dav','chunks_migrated','1'),('dav','enabled','yes'),('dav','installed_version','1.16.2'),('dav','regeneratedBirthdayCalendarsForYearFix','yes'),('dav','types','filesystem'),('federatedfilesharing','enabled','yes'),('federatedfilesharing','installed_version','1.10.2'),('federatedfilesharing','types',''),('federation','enabled','yes'),('federation','installed_version','1.10.1'),('federation','types','authentication'),('files','cronjob_scan_files','500'),('files','enabled','yes'),('files','installed_version','1.15.0'),('files','types','filesystem'),('files_pdfviewer','enabled','yes'),('files_pdfviewer','installed_version','2.0.2'),('files_pdfviewer','types',''),('files_rightclick','enabled','yes'),('files_rightclick','installed_version','0.17.0'),('files_rightclick','types',''),('files_sharing','enabled','yes'),('files_sharing','installed_version','1.12.2'),('files_sharing','types','filesystem'),('files_texteditor','enabled','no'),('files_texteditor','installed_version','2.8.0'),('files_texteditor','types',''),('files_trashbin','enabled','yes'),('files_trashbin','installed_version','1.10.1'),('files_trashbin','types','filesystem,dav'),('files_versions','enabled','yes'),('files_versions','installed_version','1.13.0'),('files_versions','types','filesystem,dav'),('files_videoplayer','enabled','yes'),('files_videoplayer','installed_version','1.9.0'),('files_videoplayer','types',''),('firstrunwizard','enabled','yes'),('firstrunwizard','installed_version','2.9.0'),('firstrunwizard','types','logging'),('gallery','enabled','no'),('gallery','installed_version','18.4.0'),('gallery','types',''),('logreader','enabled','yes'),('logreader','installed_version','2.5.0'),('logreader','types',''),('lookup_server_connector','enabled','yes'),('lookup_server_connector','installed_version','1.8.0'),('lookup_server_connector','types','authentication'),('nextcloud_announcements','enabled','yes'),('nextcloud_announcements','installed_version','1.9.0'),('nextcloud_announcements','pub_date','Thu, 24 Oct 2019 00:00:00 +0200'),('nextcloud_announcements','types','logging'),('notifications','enabled','yes'),('notifications','installed_version','2.8.0'),('notifications','types','logging'),('oauth2','enabled','yes'),('oauth2','installed_version','1.8.0'),('oauth2','types','authentication'),('password_policy','enabled','yes'),('password_policy','installed_version','1.10.1'),('password_policy','types','authentication'),('photos','enabled','yes'),('photos','installed_version','1.2.3'),('photos','types',''),('privacy','enabled','yes'),('privacy','installed_version','1.4.0'),('privacy','types',''),('provisioning_api','enabled','yes'),('provisioning_api','installed_version','1.10.0'),('provisioning_api','types','prevent_group_restriction'),('recommendations','enabled','yes'),('recommendations','installed_version','0.8.0'),('recommendations','types',''),('serverinfo','cached_count_filecache','616'),('serverinfo','cached_count_storages','8'),('serverinfo','enabled','yes'),('serverinfo','installed_version','1.10.0'),('serverinfo','types',''),('settings','enabled','yes'),('settings','installed_version','1.2.0'),('settings','types',''),('sharebymail','enabled','yes'),('sharebymail','installed_version','1.10.0'),('sharebymail','types','filesystem'),('support','SwitchUpdaterServerHasRun','yes'),('support','enabled','yes'),('support','installed_version','1.3.0'),('support','types','session'),('survey_client','enabled','yes'),('survey_client','installed_version','1.8.0'),('survey_client','types',''),('systemtags','enabled','yes'),('systemtags','installed_version','1.10.0'),('systemtags','types','logging'),('text','enabled','yes'),('text','installed_version','3.1.0'),('text','types','dav'),('theming','cachebuster','1'),('theming','color','#000000'),('theming','enabled','yes'),('theming','installed_version','1.11.0'),('theming','types','logging'),('twofactor_backupcodes','enabled','yes'),('twofactor_backupcodes','installed_version','1.9.0'),('twofactor_backupcodes','types',''),('updatenotification','core','13.0.12.1'),('updatenotification','enabled','no'),('updatenotification','installed_version','1.3.0'),('updatenotification','types',''),('updatenotification','update_check_errors','0'),('user_status','enabled','yes'),('user_status','installed_version','1.0.1'),('user_status','types',''),('viewer','enabled','yes'),('viewer','installed_version','1.4.0'),('viewer','types',''),('weather_status','enabled','yes'),('weather_status','installed_version','1.0.0'),('weather_status','types',''),('workflowengine','enabled','yes'),('workflowengine','installed_version','2.2.1'),('workflowengine','types','filesystem');
/*!40000 ALTER TABLE `oc_appconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_authtoken`
--

DROP TABLE IF EXISTS `oc_authtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_authtoken` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `login_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `password` longtext COLLATE utf8mb4_bin,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` smallint unsigned DEFAULT '0',
  `remember` smallint unsigned DEFAULT '0',
  `last_activity` int unsigned DEFAULT '0',
  `last_check` int unsigned DEFAULT '0',
  `scope` longtext COLLATE utf8mb4_bin,
  `expires` int unsigned DEFAULT NULL,
  `private_key` longtext COLLATE utf8mb4_bin,
  `public_key` longtext COLLATE utf8mb4_bin,
  `version` smallint unsigned NOT NULL DEFAULT '1',
  `password_invalid` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `authtoken_token_index` (`token`),
  KEY `authtoken_last_activity_index` (`last_activity`),
  KEY `authtoken_uid_index` (`uid`),
  KEY `authtoken_version_index` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_authtoken`
--

LOCK TABLES `oc_authtoken` WRITE;
/*!40000 ALTER TABLE `oc_authtoken` DISABLE KEYS */;
INSERT INTO `oc_authtoken` VALUES (8,'vok','vok','PO4XURwIHNrBJonyzSrIn+CGKqO9ZDWjCTxHQPVPq4xmh5uzenzgJcypvR6xi8N4HQZZvfIEH3OlUeAxoSveir7uTrMq3SOztTj9vIeqcrNUYyWT8x+QJzWEPSVGX0qwkCEbHgpQR92vn3ZJ7WePFIByfT3awgfUWYBlj1sxcEf0Z/VHLlcAO4jGUgmihjTEKDXNoyGk7OyJtgeM/r1z9ZvrJOo4WuCnAlS6XPAPMNgzAEKDHiBRtwmh6KTSFUB59VFR7LbBdnpyTKbtUgnRz8Eq8x1BIFlx5+9QOVNq/uES60ErNgcfjTX6GR5GonTg+g88bUNgMXAxhPhv6M0peg==','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 Edg/112.0.1722.58','ffc586bc024535783081a534670880f2392489af4e788a003130e719b0e0408077d00782c32e82201797e7afd1d20c9c2dd1b19effa9a40eead7b2be1fb5fe4f',0,1,1682686782,1682686512,NULL,NULL,'c3a2957b22318c4116aebc9f693ed05f4f6e0790f44de2453a2a7ce45852a087392ded68719b326a3c84c5d837f42dc4e7200b6cf0b2ee941ffa160fbb4be8960a6ef720df770c2b5d28e527f8475596752d4ab029041b3c97bf665add517a720fa35b26f001da688236163eb4f7815f4a13300da1de26ba2642d3c5248ddc9f1bae3563fb77879340c8615b807d3b53c51c669fb81389969afd0121748457cc94332855439dc01de916c2c0231f962284bf11d246ac74c99909825d3f4ac46a82a5539a460d3d3b5fbd2e21e80188d4636f91324d3957e60f4f8acfe0f45b78dfd7705c3209a91df255c90700e2f641ab6948289fe93daa330660ceb4465de48294693290adc3c1f52de6712ed2893563d4bfb869b98559fdd7542707177b4389ca62d591066a84eed91ba07c1980ee67a55699aa0f29d6d423b324be44c475ec27aa7d30f8671670903e46db15909c2346584ad93c01aa2787eda35ebef00356e3cca22b94133a4b2a2a9f9cad3d50334c6f2b2e9624eee155320f9956889f0049be03e66c606212294093f6a97eb8f8259952be4a1463b27df7f0b2b362146759eef5eaa6fd4c35b15cc86b5c6de6e2beabbb547fb5daa5b01a1c42e462c4ed5ff6e5bc6d5184879e55ef201c8020ba1a496fb9f676f5e85139cfae353d4a4013ce7ccc56d2dee0e9cdbfea0922bdf1e5bce9ac3920128ce84e4beffd776a0989047499d601d4e7433a8936fc60c6f8287c54c874a2614f988e5b23c5872e95faa045ca1b5bd488183ecef135745d2ef7153d7da4c765a21b44c2182afac374bfbcb234c93e2b3f37fba836d888de9090dce12f63b9838c2a126ebd97df16ecf20b6c256b2da10da969098e14e1b89739dda84dcba0c5a8d67501dcdf41f0eb25cbbd81069a90a32064c8f8f97e6d7a4a501430678a3e258e119acd8f7816953062fd097fea882009b503932803612af1c9b04b31e9de44a02f967381598db58cbe68f571ba742d1dee25a6554e34dc9bb8a0924d8dd934cd211a3352bc979d720e5fa47bdcded6b69ba4a1546d509d55d19d7ac85e5e162a3e38225914af1ab3043c31f6902493f739da926edc2a8cf69878818ec0584f85a47d0937beb312ce326b41aa7b3ecd2eb3cfd753cb76837177fde980e5f1cee56c5899a20aa140004d41e95b8413f4a6470e183be02578c204ed16d80999f28bb10ddd381bdbd5daf29c429e769dff592996679fae63c65f5443cb94b2a847e4d8ed216092e4da946ce7acdc7717d5a6847b88ac2906cbd27f9eb4ff6f9c95e2367adcff06b144dd61a089a92ee13dafcd754638a9191a96550b4754e550ce920c700d7d826dff2ff16255897acaf26ce46affd5d6a93ee9ed19d9c06034621b64451be7cb65c3db039cee519b6000aae4c05d9021690a89065ba120a1eaf1b9b83103b2297f05200500b0af03d78619aceacaec038dc2f1bafefa93383c31b33e25616b8938adc271d0ad3f22fa4df7b98c95fd26070c55603ff5015f0738fa8201b9183b2f498a8a35f5fc9f1e0c1ef8436668dbc52222a3f6591400166d6c8f92c9013977900d80106e4a18888ec76d04465cb1d803d1e15b2e9684b1552a93ee57a78d2396ce7bccf8300998b26278f9b39af3fd2500e46b642d8024a09b5304856191849479ae306bde15dafa8d235c88968ad0942d1153fc89db9be139f872a45bf19479e52057002f772030e7ea54b693279729e723f7505d096e11150c7275a34280947af54f0d753139fc04698fd3b78f49b9d19ff84026fb03cec1e52e90590f1c0ab2b71ea00f6edb98f4659b62118f549c236c1afe75bafbed31c389bf8caf7964ff6d5ffda9ebd724c96a3780b4f06753991bd00c5486f2408ced29fe44fb2f0902298cbd599dd24a6d731d68b30528a0dc6b933a2ba33464c4468158b46d77ff7e447826d93cb41dcd94c4291dcb70b0d9df6e3508d68e23aeae1d026af395f0064db3b7a662db663c9c88b85105c4a9476db2c3a01d2922c8899c745aabe801d9328e0b5fb5d81a2dcfe1fc0e20873c0ba441a5c61d41ddc2ce2e2122c698281bc0eca6591a997491c4a5b033f92fcd7520aa639a23be26ef759469ebce37c6c20e8fbe2c1db278f332e34936f622947f1bc37e8849ade71667bd6ed5bfd0151d0b39d57bb48fde1e5c506cb813d76f2cd35ff8d3fbc725594c97fca7e71f1087d24b39f18e63fece60e9f9b4937922df64aaf54b9c254f28199d69b1c7b44beff880e14322a940ad4720fa57f53100c89d073ff80f18104c57230144606626a7f4c3d06523cc27d80260836509c3c353d7e1126d7efabd29b84068e9ed31f4b9c5b9b6a9f4248e9984488b012d89379139a6983a6b397586ec3ef4313c236b6a7628ab15c8f4ca96b8cec71ef67c|5001d51b550d2209862a8dfeef7c355b|0603843efc4a4e25fe93d724d048c724e891cf2f7c75d0c707074dbf8592fe6e971273d941a97433072bda3ec5c6cab4216ecf6937363a7276294f043503c880|2','-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA12enRrFVZTLqrR9x0gDx\nZaLMzPfF4NyJxnICd6lEcv5DWZFTtC0dPVygk80bFA4VAi6IKB5k0wy/qGcRiFXX\n0wrr+wa7O1Y+M7YOPjXlnvQCGYjmsBKHd6/mLXusPTzFQKAgS8vM+wa/ZCwJS9M8\nyR1CcT3N6oATqDP0AnyzG0yBCvTEmYaYH3Q+HxydALve66ZaRNeMVWrrrluiydS4\nKYITtFCqkAHqjstfhfD5GM1gNDeSwMAXvNaw1jkGbRO3PHaaV6V0QxAvOD3wlQlC\nw2+t1+aMruHcDBjmFGWic9CWGDODZ4g4o1UQ+MI7AdilCzXwWH0RD7b/4tru2glC\n5QIDAQAB\n-----END PUBLIC KEY-----\n',2,0);
/*!40000 ALTER TABLE `oc_authtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_bruteforce_attempts`
--

DROP TABLE IF EXISTS `oc_bruteforce_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_bruteforce_attempts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `occurred` int unsigned NOT NULL DEFAULT '0',
  `ip` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `subnet` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `metadata` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `bruteforce_attempts_ip` (`ip`),
  KEY `bruteforce_attempts_subnet` (`subnet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_bruteforce_attempts`
--

LOCK TABLES `oc_bruteforce_attempts` WRITE;
/*!40000 ALTER TABLE `oc_bruteforce_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_bruteforce_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_invitations`
--

DROP TABLE IF EXISTS `oc_calendar_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_invitations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `recurrenceid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `attendee` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `organizer` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sequence` bigint unsigned DEFAULT NULL,
  `token` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `expiration` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_invitation_tokens` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_invitations`
--

LOCK TABLES `oc_calendar_invitations` WRITE;
/*!40000 ALTER TABLE `oc_calendar_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_reminders`
--

DROP TABLE IF EXISTS `oc_calendar_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_reminders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `calendar_id` bigint NOT NULL,
  `object_id` bigint NOT NULL,
  `is_recurring` smallint DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `recurrence_id` bigint unsigned DEFAULT NULL,
  `is_recurrence_exception` smallint NOT NULL,
  `event_hash` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `alarm_hash` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `is_relative` smallint NOT NULL,
  `notification_date` bigint unsigned NOT NULL,
  `is_repeat_based` smallint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_reminder_objid` (`object_id`),
  KEY `calendar_reminder_uidrec` (`uid`,`recurrence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_reminders`
--

LOCK TABLES `oc_calendar_reminders` WRITE;
/*!40000 ALTER TABLE `oc_calendar_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_resources`
--

DROP TABLE IF EXISTS `oc_calendar_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_resources` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `backend_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `resource_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `group_restrictions` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_resources_bkdrsc` (`backend_id`,`resource_id`),
  KEY `calendar_resources_email` (`email`),
  KEY `calendar_resources_name` (`displayname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_resources`
--

LOCK TABLES `oc_calendar_resources` WRITE;
/*!40000 ALTER TABLE `oc_calendar_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_resources_md`
--

DROP TABLE IF EXISTS `oc_calendar_resources_md`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_resources_md` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` bigint unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `value` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_resources_md_idk` (`resource_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_resources_md`
--

LOCK TABLES `oc_calendar_resources_md` WRITE;
/*!40000 ALTER TABLE `oc_calendar_resources_md` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_resources_md` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_rooms`
--

DROP TABLE IF EXISTS `oc_calendar_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_rooms` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `backend_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `resource_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `group_restrictions` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_rooms_bkdrsc` (`backend_id`,`resource_id`),
  KEY `calendar_rooms_email` (`email`),
  KEY `calendar_rooms_name` (`displayname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_rooms`
--

LOCK TABLES `oc_calendar_rooms` WRITE;
/*!40000 ALTER TABLE `oc_calendar_rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_rooms_md`
--

DROP TABLE IF EXISTS `oc_calendar_rooms_md`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_rooms_md` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `room_id` bigint unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `value` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_rooms_md_idk` (`room_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_rooms_md`
--

LOCK TABLES `oc_calendar_rooms_md` WRITE;
/*!40000 ALTER TABLE `oc_calendar_rooms_md` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_rooms_md` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarchanges`
--

DROP TABLE IF EXISTS `oc_calendarchanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarchanges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `calendarid` bigint NOT NULL,
  `operation` smallint NOT NULL,
  `calendartype` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `calid_type_synctoken` (`calendarid`,`calendartype`,`synctoken`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarchanges`
--

LOCK TABLES `oc_calendarchanges` WRITE;
/*!40000 ALTER TABLE `oc_calendarchanges` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarchanges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarobjects`
--

DROP TABLE IF EXISTS `oc_calendarobjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarobjects` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `calendardata` longblob,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendarid` bigint unsigned NOT NULL,
  `lastmodified` int unsigned DEFAULT NULL,
  `etag` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  `componenttype` varchar(8) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstoccurence` bigint unsigned DEFAULT NULL,
  `lastoccurence` bigint unsigned DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `classification` int DEFAULT '0',
  `calendartype` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `calobjects_index` (`calendarid`,`calendartype`,`uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarobjects`
--

LOCK TABLES `oc_calendarobjects` WRITE;
/*!40000 ALTER TABLE `oc_calendarobjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarobjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarobjects_props`
--

DROP TABLE IF EXISTS `oc_calendarobjects_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarobjects_props` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `calendarid` bigint NOT NULL DEFAULT '0',
  `objectid` bigint unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `parameter` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendartype` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `calendarobject_index` (`objectid`,`calendartype`),
  KEY `calendarobject_name_index` (`name`,`calendartype`),
  KEY `calendarobject_value_index` (`value`,`calendartype`),
  KEY `calendarobject_calid_index` (`calendarid`,`calendartype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarobjects_props`
--

LOCK TABLES `oc_calendarobjects_props` WRITE;
/*!40000 ALTER TABLE `oc_calendarobjects_props` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarobjects_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendars`
--

DROP TABLE IF EXISTS `oc_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendars` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendarorder` int unsigned NOT NULL DEFAULT '0',
  `calendarcolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `timezone` longtext COLLATE utf8mb4_bin,
  `components` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `transparent` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `calendars_index` (`principaluri`,`uri`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendars`
--

LOCK TABLES `oc_calendars` WRITE;
/*!40000 ALTER TABLE `oc_calendars` DISABLE KEYS */;
INSERT INTO `oc_calendars` VALUES (1,'principals/users/admin','Personal','personal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(2,'principals/users/admin','admincal','admincal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(3,'principals/system/system','Contact birthdays','contact_birthdays',1,NULL,0,'#FFFFCA',NULL,'VEVENT',0),(4,'principals/users/vok','Personal','personal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(5,'principals/users/testuser2','Personal','personal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(6,'principals/users/admin','Contact birthdays','contact_birthdays',1,NULL,0,'#E9D859',NULL,'VEVENT',0),(7,'principals/users/testuser2','Contact birthdays','contact_birthdays',1,NULL,0,'#E9D859',NULL,'VEVENT',0),(8,'principals/users/vok','Contact birthdays','contact_birthdays',1,NULL,0,'#E9D859',NULL,'VEVENT',0);
/*!40000 ALTER TABLE `oc_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarsubscriptions`
--

DROP TABLE IF EXISTS `oc_calendarsubscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarsubscriptions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `refreshrate` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendarorder` int unsigned NOT NULL DEFAULT '0',
  `calendarcolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `striptodos` smallint DEFAULT NULL,
  `stripalarms` smallint DEFAULT NULL,
  `stripattachments` smallint DEFAULT NULL,
  `lastmodified` int unsigned DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `source` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `calsub_index` (`principaluri`,`uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarsubscriptions`
--

LOCK TABLES `oc_calendarsubscriptions` WRITE;
/*!40000 ALTER TABLE `oc_calendarsubscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarsubscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_cards`
--

DROP TABLE IF EXISTS `oc_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_cards` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `addressbookid` bigint NOT NULL DEFAULT '0',
  `carddata` longblob,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastmodified` bigint unsigned DEFAULT NULL,
  `etag` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  `uid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_abid` (`addressbookid`),
  KEY `cards_abiduri` (`addressbookid`,`uri`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_cards`
--

LOCK TABLES `oc_cards` WRITE;
/*!40000 ALTER TABLE `oc_cards` DISABLE KEYS */;
INSERT INTO `oc_cards` VALUES (1,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:vok\r\nFN:vok\r\nN:vok;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOzd2XJb17Y/5sGeki3baiyrl6zWVn/+e\r\n ZdUHiOVSlXeJZXnSOUueQIDoEiLtChvi7Ityg3pbVEmGgLIhfY5Z7uRLIoAJrDG913uU8ceJhe\r\n BNX9zjDmn/o///Vk/AAAAgEqbLl0AAAAAMHwCAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAA\r\n AAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAAC\r\n ABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEB\r\n AAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAA\r\n AAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAk\r\n IAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAA\r\n AAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAA\r\n AASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABI\r\n QAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAA\r\n AAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAAB\r\n AAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACA\r\n gAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAA\r\n AAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAAS\r\n EAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAA\r\n AAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAA\r\n AAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAk\r\n IAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAA\r\n AAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAA\r\n gAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABA\r\n QAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAA\r\n AACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJ\r\n CAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAA\r\n AAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAA\r\n IAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQ\r\n EAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAA\r\n AAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAAC\r\n QgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAA\r\n AAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAA\r\n AABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAE\r\n hAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAA\r\n AAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAA\r\n EACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAI\r\n CAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAA\r\n AAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAAB\r\n IQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAA\r\n AAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAA\r\n AAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAAC\r\n QgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAA\r\n AAAAggdnSBQBU0f/6vx2PU6fnSpdR1PqjVvxf/+d26TJgIv3P/8uH8T/+p0Olyyju//m/X8T/9\r\n /++LF0GQGXoAAAYgnqtWbqE4j69PB/vve9rBvZrZibi5q2F0mWMhUbdZynAIHkzAxgCL60RMzN\r\n Tcfu2RQzs1/UbC7G46BVt40k7tre7pcsAqBTfLgBDsL3djY0n7dJlFHf33mLpEmDi+Lt5paaTC\r\n mDgBAAAQ+Ll1RgA7NfsrPb/iIhutx9LDZ+hAIPmrQxgSJYazeh2+6XLKGp6eiru3LWbCW/rxmc\r\n LsbDg9ezxejte7vRKlwFQOb5hAIbk5U4vvn5sDEA7M7w9fy+vOEcFYDgEAABD5CU24tKluXj/i\r\n K8b+DuzsxGffa79v9Ppx/IDn50Aw+CNDGCIHiw1Y2/PGIAxAPh7n32u/T8iYm21Fa1W7s9NgGH\r\n xLQMwRK1WP9ZWW6XLKE5bM/y9e/cPlS5hLNS+2C1dAkBlCQAAhswYQMTFi8YA4E3m5l4dAJhds\r\n 9mL1YdCU4Bh8TYGMGRfrjSj1cp9mvX09JQuAHiDz28uxvz8VOkyiltZbkW3W7oKgOoSAAAM2d7\r\n eq5fa7JwDAK8nIHulXtP+DzBMAgCAEWjUvdRevDgXRz7wtQN/ND8/pf0/InZedGP9katTAYbJm\r\n xjACDz6qh0vXxoD0AUAf3bz1kLMzWn/X2o0o+/wf4ChEgAAjECvF+61Dm3O8Ff8XbxSq/mMBBg\r\n 2AQDAiNRdbRUXLszFBx/66oH/ND8/Fdeua//f2tqLpxud0mUAVJ63MIAR+cc/OvHPX3Ifb20MA\r\n H7v1m3t/xGuSwUYFQEAwAg1Gl5y72l3hv+i/f+VuvZ/gJEQAACMkCuuIi5cnDcGABGxsKD9PyJ\r\n i81knnm/ulS4DIAVvYAAj9P13e/Hjj150dQHAq/b/2Vnt/3b/AUZHAAAwYmZdtT1DRMTde4dKl\r\n zAWfCYCjI4AAGDE3AYQcf7CfHz0ka8g8lpcnIqr1+ZLl1HcxpN2bG/nPhwVYJS8fQGM2E8/deP\r\n 771x3pQuAzG7fWdT+HxE17f8AIyUAACjAYYDan8lNABbR7fZjyc0oACMlAAAooF5vRq/XL11GU\r\n efOz8XRozOly4CRO3RoKq5c1f7/eL0dL3d6pcsASEUAAFDAr//sxZMnxgDu3HUFGvncvrMYMzP\r\n a/x3+BzB6AgCAQhwGaAyAnLT/R3Q6/Vh+IAAAGDUBAEAhS41mdLvGAIwBkMnhw1Nx+Yr2/7XVV\r\n rRauT//AEoQAAAUsrvbj/VH7dJlFGc3lEy0/79S0wEFUIQAAKAgtwEIAMjF8x7RbPZi9WGrdBk\r\n AKQkAAApaWW5Fp5O7Dfbsubk4dswYANX33vvT8ell7f8ry63odktXAZCTAACgoHa7bycs7IqSw\r\n 5272v8jdD4BlCQAACjMLKwAgBw85xE7L7rOPgEoSAAAUNjaaiuazV7pMoo6c3Yujh83BkB1vX9\r\n kOi5dmitdRnFLjWb0c089ARQlAAAorNsN92GH3VGq7c7dxZie1v5fq/msAyhJAAAwBupeigUAV\r\n JrnO2Jray+ebnRKlwGQmgAAYAw8Xm/Hzovcx2KfPjMXJ04YA6B6jnwwHRcvav9v1AWdAKUJAAD\r\n GQL//ajY2uzt37ZJSPdr/X9HpBFCeAABgTNgd0yZNNXmuIzafdeL55l7pMgDSEwAAjIknTzqxt\r\n ZX7BdkYAFXzwYfTceGC9n+7/wDjQQAAMEZ0AdgtpVru3dP+H+GzDWBcCAAAxoiXZAEA1eJ5jth\r\n 40o7t7dyHnAKMCwEAwBjZfLYXz5/nHgM4dXouPv7YGACT76OPpuP8hfnSZRRX0/4PMDYEAABjp\r\n v7FbukSirNrShV4jiO63b4bTgDGiAAAYMwYA4i4e/9Q6RLgwO7e8xw/Xm/Hy51e6TIA+BcBAMC\r\n Y2drqxtONdukyivrkk9k4edIYAJPr6NGZOHfe6f8CTYDxIgAAGEN1L826AJho2v8jOp1+LD/wW\r\n QYwTgQAAGOoUW9Gr9cvXUZRFlBMMs9vxNpqK1qt3J9jAONGAAAwhnZe9OLrx7nHAE6enI1PTs2\r\n WLgP27dixmTh7Tvt/zYGmAGNHAAAwpszO2kVlMt2777ltNnux+rBVugwA/kAAADCmHiw1Y28vd\r\n /vsnbsWUkwewVXEynIrut3SVQDwRwIAgDHVbPbjq7XcO2jGAJg0x4/PxOkz2v/rNe3/AONIAAA\r\n wxowB2E1lsmj/j9h50Y31R7nPMAEYVwIAgDH25Uoz2u3cYwACACaJ5zViqdGMfu6PLYCxJQAAG\r\n GOdzqsQILOPP56NU6eNATD+Pv54Jk6d1v5fq+X+zAIYZwIAgDFX9zJtV5WJ4DmN2Nrai6cbndJ\r\n lAPAaAgCAMffVWit++61XuoyiLKyYBHfvHypdQnHOLQEYbwIAgDHX60UsP8j9Un3ixGycPmMMg\r\n PF18uRMfPKJZ1THEsB4EwAATAAv1boAGG/3/sPu/+azTjzf3CtdBgBvIAAAmABfP27Hr//sli6\r\n jqDt3BQCML8+noBJgEggAACbEUiP3y/WJE7Nx5qwWa8bPqdOzcfKkZ9P8P8D4EwAATIhabbd0C\r\n cUZA2AceS4jNp60Y3s7d5cSwCQQAABMiO++3Yuffso9X6vNmnEkAIioaf8HmAgCAIAJkr3F9vh\r\n xYwCMl9NnZuPEidzPZLfbTz+iBDApBAAAE6T+hTEAu62ME89jxOP1drzc6ZUuA4C3IAAAmCA//\r\n tiNZ993SpdRlAUX48TzqDMJYJIIAAAmTParto4dm42z53K3XDMezpydjePHcz+LnU4/lh/k/kw\r\n CmCQCAIAJ06jvRq/XL11GUXfvHSpdAsS9+57DtdVWtFq5P48AJokAAGDC/PJLLzY2jAFAaXfuL\r\n pQuobiac0kAJooAAGACZR8DOHp0Js6dnytdBomdOz8Xx47lbv9vNnux+rBVugwA9kEAADCBluq\r\n 70e3mbrvVBUBJnr+IleVWdLulqwBgPwQAABPot9/68Xi9XbqMou7ctQCjHM9fRL2m/R9g0ggAA\r\n CZU9pfvo0dn4vwFYwCM3vkLc3H06EzpMoraedGN9Ue5Q0iASSQAAJhQK8ut6HRyjwHc04ZNAZ6\r\n 7iKVGM/q5P34AJpIAAGBCtVr9WFvNfQCXNmxK8NxF1JIfRAowqQQAABMs+xVcH340ExcvGgNgd\r\n C5enIsPP8rd/r+1tRdPk19FCjCpBAAAE2z1YSuazV7pMoqyG8so3bvveWvU7f4DTCoBAMAE63Z\r\n fnQWQmQCAUbp9x/NW1/4PMLEEAAATrlE3BmAMgFH49NO5+ODD3O3/m8868Xxzr3QZALwjAQDAh\r\n Hv0VTtevsw9BnDXqeyMgOfM7j/ApBMAAEy4fj9iKXkXgDEARkH7v/l/gEknAACogOwv5R98OBO\r\n XLhkDYHguX5mPIx/kbv/feNKO7e1u6TIAOAABAEAFfPNNJ/2LufZshsnzFVHT/g8w8QQAABWR/\r\n TBA7dkMy9RUxO3bC6XLKKrb7cdSQwAAMOkEAAAVYQxgJj791BgAg3f5yny8fyR3+//j9Xa83Ml\r\n 92ChAFQgAACri2fd78cMPua/n0qbNMNy777nKHjACVIUAAKBCGjVjADBIU1MRt27nfq46nX4sP\r\n xAAAFSBAACgQrLf0X3kg5m4fGW+dBlUyNVr8/Hee7lfl9ZWW9Fq9UuXAcAA5P5GA6iYn3/uxrd\r\n PO6XLKMoYAIPkeYqofZG7swigSgQAABWT/jaA2wsxNVW6Cqpgelr7f7PZi9WHrdJlADAgAgCAi\r\n qnXm9Hr5W3Xff/ITHx62RgAB3f12nwcPpz7VWlluRXdbukqABiU3N9qABX04tde/OPrdukyitK\r\n 2zSB4jiLqyQ8WBagaAQBABWW/sssYAAel/T9i50U31h/lDhMBqkYAAFBBD5aasbeXewzAbQAcx\r\n LXr83HoUO7XpKVGM/p5P0YAKin3NxtARe3u9tPv3Gnf5iDu3T9UuoTiasmvFQWoIgEAQEVln92\r\n 9fWfRGADvZGYm4uathdJlFLW1tRdPN3JfKQpQRQIAgIpaWW5Gu523f/e996bjylVjAOzf9RsLs\r\n biY+xUp+zkiAFWV+9sNoMI6nYiHX+Z+iTcGwLvw3ETUtf8DVJIAAKDCsr/E37ptDID9mZmJ+Px\r\n m7vb/zWedeL65V7oMAIZAAABQYWurrdjd7ZUuo5j33puOq9eMAfD2bnym/T97cAhQZbm/4QAqr\r\n teLWH6Q+2VeOzf74Xkx/w9QZQIAgIrLvpt36/ZiTPu24y3Mzmr/33jSju3tbukyABgSr0QAFff\r\n 143a8+DXvC/3hw24D4O3c+GwhFhZyvxrVkgeGAFWX+1sOIIF+P2KpkfulXls3b+P+fxwqXUJR3\r\n W4//WcFQNUJAAASyL6rZwyAvzM396oDILPH6+14uZP30FCADLwOASTw7dNO/Pxz3mu9Dh92GwB\r\n v9vnNxZifz31npMP/AKpPAACQRPaXe2MAvEn256PT6ae/MQQgAwEAQBL1L3ZLl1CUMQBeZ24u4\r\n vqN3O3/a6utaLX6pcsAYMi8CgEk8cMP3dh81ildRjGHDk3HtevGAPizm7e0/9eSB4QAWQgAABK\r\n pJz8M8O693Ke889eyt/83m71YfdgqXQYAIyAAAEgk+zkAN28tGAPgd+bnp9K3/68st6LbLV0FA\r\n KPgNQggke3tbmw8aZcuo5hDh6bTL/b4vZu3FmJuLnf7f72m/R8gCwEAQDK19GMAudu9+b1793M\r\n /DzsvurH+KG8oCJCNAAAgmaVGM7rdvKd937y1EDMzpatgHCwsTMW167k7QpYazejn/TgASEcAA\r\n JDMy51efP04747f4uJ0+kUfr9y6vRCzs7nb/7N3BAFkIwAASMhtALnbvnkl+60QW1t78XQj79W\r\n gABkJAAASWn7QjL29vH2/xgBYXJyKq9fmS5dRVPZbQQAyEgAAJNRq9WNtNe+934uLbgPI7tbtx\r\n fTt/9k7gQAyEgAAJJV9988YQG7Zf/+bzzrxfHOvdBkAjJgAACCpL1ea0Wr1SpdRjDGAvLT/2/0\r\n HyEoAAJDU3l7EynLeMYCFhem48ZkxgIxu31mMmZnc7f/ZO4AAshIAACTWqO+WLqGo7G3gWd27n\r\n /v3vvGkHdvb3dJlAFCAAAAgsUdftePly7xjAJ/fXIjZ2dJVMEqHDk3F5Su52/9r2v8B0hIAACT\r\n W60U8WMq7GDAGkM+du7nb/7vdfiw18v7NA2QnAABIrlEzBkAe2X/fj9fb8XInb9cPQHYCAIDk/\r\n vGPTvzzl7zzwJ99bgwgi8OHp+LTy7nb/x3+B5CbAACAaCRuCV5YmI7PPjcGkEH29v9Opx/LD/L\r\n +rQMgAAAgIurJxwDu3M3dFp5F9vb/tdVWtFr90mUAUJAAAID4/ru9+PHHvdJlFPP5zUVjABX33\r\n vvT6dv/a1/kDvoAEAAA8C+ZZ4Pn56eMAVTcnbuLMT2dt/2/2ezF6sNW6TIAKEwAAEBERNST7w7\r\n eu3+odAkM0b37udv/V5Zb0c171icA/yIAACAiIn76qRvffdspXUYxNz5biLm50lUwDO8fmY6LF\r\n 3P/crOf8wHAKwIAAP5Lo553kTA/PxWf38y9S1xVd+/lbv/fedGN9Uft0mUAMAYEAAD8l3q9Gb1\r\n e3lPCs58SX1XZf69LjWb08/5ZA/BvBAAA/Jdf/9mLJ0+MAVAdRz6YjgsXcv9Sa7W8B3wC8HsCA\r\n AB+J/NhgHNzU3HzVu7d4qrJ3v6/tbUXTzfyhnoA/J4AAIDfWWo0o9vN2y+cvV28au4l/31mvt4\r\n TgD8TAADwO7u7/dQHhl2/sRDz83l3jKvkgw+n49z53O3/de3/APwbAQAAf5L5yrBXYwALpctgA\r\n O4lb//ffNaJ55t7pcsAYIwIAAD4k5XlVnQ6xgCYbPfuHypdQlF2/wH4IwEAAH/Sbvdj9WGrdBn\r\n FXLtuDGDSHT06k7793/w/AH8kAADgL9XS3wZgDGCSZe/i2HjSju3tbukyABgzAgAA/tLaait2d\r\n 3ulyygm+wJy0mX//dW0/wPwFwQAAPylbjdiZTnvIuLa9YVYWDAGMImOHZuJs+fytv93u/1YauT\r\n 92wXg9QQAALxW5kPEjAFMruy7/4/X2/FyJ2/3DgCvJwAA4LUer7dj50XeOeLsC8lJlf335vA/A\r\n F5HAADAa/X7kbqV2BjA5Dl+fCbOnM3b/t/p9GP5Qd6/WQDeTAAAwBtlPkxsdnYqbt02BjBJsu/\r\n +r622otXqly4DgDElAADgjZ5udGJra690GcXcvXeodAnsw737uQOAzNd3AvD3BAAA/K3MM8VXr\r\n 83H4qIxgElw4sRMnDqdt/2/2ezF6sNW6TIAGGMCAAD+VuYA4NUYQO5d5UmRffd/ZbkV3bxndgL\r\n wFgQAAPytzWd78fx55jGA3AvLSXH3fu5xjXpN+z8AbyYAAOCt1BPPFl+5agxg3J08OROffDJbu\r\n oxidl50Y/1Ru3QZAIw5AQAAb8UYgC6AcZZ993+p0Yy+w/8B+BsCAADeytZWN55u5N1hNAYw3rL\r\n /fjJf1wnA2xMAAPDWMi8yrl6bj0OHjAGMo09OzcbJk3nb/7e29uLpRqd0GQBMAAEAAG9tqdGMX\r\n i9nn/HMjDGAcZV99z/zeA4A+yMAAOCt7bzoxdePjQEwXrL/XuqJO3MA2B8BAAD7knm38cpVYwD\r\n j5vSZ2fj447zt/5vPOvF8M+8VnQDsjwAAgH15sNSMvb28YwC37+TebR43dv/zBnIA7J8AAIB9a\r\n Tb78dVaq3QZxWRfcI6bO3dz/z4yd+QAsH8CAAD2LfOi4/KV+Th82BjAODhzdjZOnMjb/r/xpB3\r\n b293SZQAwQQQAAOzblyvNaLV6pcsowhjA+MjejZH5Wk4A3o0AAIB963QivlwxBkBZmX8P3W4/l\r\n hoCAAD2RwAAwDvJPAbw6WVjAKWdPTcbx47lbf9/vN6Olzs5u3AAeHcCAADeyVdrrfjtt5wLkJm\r\n ZqfSHz5V2996h0iUUlTmAA+DdCQAAeCe9XsTyg7yLEAFAWZnb/zudfuq/PQDenQAAgHeW+Q7yy\r\n 1fm4733fY2WcP7CXBw9OlO6jGLWVlvRavVLlwHABPLmAsA7+/pxO379Z85ryKanp+L27YXSZaR\r\n 0L/Huf0RE7Yvd0iUAMKEEAAAcSOaTyDO3oZeUefyi2ezF6sO8N3AAcDACAAAOpFbLuxv56WVjA\r\n KN28eJcfPhR3vb/leVWdHM23QAwAN5aADiQ777di59+2itdRhHT024DGLXsXRf1xIEbAAcnAAD\r\n gwDJfSZZ9QTpqmQOXnRfdWH/ULl0GABNMAADAgdUTH0p26dJcvH/E1+koXLo0Fx98mLf9f6nRj\r\n L7D/wE4AG8sABzYjz9249n3ndJlFGEMYHSyd1vUEl+7CcBgCAAAGIh64sVJ9oXpqGQOWra29uL\r\n pRs6QDYDBEQAAMBCN+m70ejn7ky9eNAYwbJevzMeRD/K2/2c+ZwOAwfG2AsBA/PJLLzaS7lBOT\r\n 0/pAhiy7D/fzB02AAyOAACAgcm8SMm+QB2mqamI27cXSpdRzOazTjzfzHnVJgCDJQAAYGCW6rv\r\n R7eYcA7hwYS6OfOBrdRguX5mP94/kbf/PHKwBMFjeVAAYmN9+68fj9Zz3lLsNYHiyd1eY/wdgU\r\n AQAAAxUvbZbuoRisi9Uh2FqKuL2nbw/140n7dje7pYuA4CKEAAAMFDLD1rR6eQdA/jgQ1+tg3T\r\n l6ny8917en2lN+z8AA5T3GxWAoWi3+7G22ipdRhHGAAYvc1dFt9uPpYYAAIDBEQAAMHC1L/KOA\r\n dxLvGAdtOnp3O3/j9fb8XKnV7oMACpEAADAwK0+bEWzmXPhcuHivDGAAbl6bT4OH877s3T4HwC\r\n DlvdbFYCh6XYjVpZzjgFE6AIYlMzt/51OP5YfCAAAGCwBAABD4TYADmJ6OuLW7bw/x7XVVrRaO\r\n Q/TBGB4BAAADMX6o3bsvMh5fdn5C/Px0Ue+Yg/i2vX5OHQo788w8zkaAAxP3m9WAIaq3494sJS\r\n 3hVkXwMHcvSpFjwEAAB5wSURBVHeodAnFNJu9WH2Yd4QGgOERAAAwNJkPMcu8gD2omZmIm7cWS\r\n pdRzMpyK7o5m2cAGDIBAABD8803ndjezrmSOXd+Lo4enSldxkS6dn0hdft/5vMzABiuvN+uAIx\r\n Eo553MXPnbt5d7IPIPD6x86Ib64/apcsAoKIEAAAMlTEA9iN7+/9Soxl9h/8DMCQCAACG6tn3e\r\n /HDD3ulyyjCGMD+3fhsIRYX876e1Gp5AzMAhi/vNywAI1NPfKVZ5nb2d5H557W1tRdPNzqlywC\r\n gwgQAAAxd7jGAvAva/Zqdjfj8Zt72/8x/JwCMhgAAgKH7+edufPs0587m2XNzceyYMYC3ceOzh\r\n VhYyPtqUtf+D8CQ5f2WBWCkaomvNtMF8HYy/5w2n3Xi+WbOszIAGB0BAAAjsdRoRq+X83jzzAv\r\n bt/Wq/T/vz8nuPwCjIAAAYCRe/NqLf3yd837zM2fn4vhxYwBv8tnnCzE/P1W6jGLM/wMwCgIAA\r\n EYm8yJHF8Cb3bt/qHQJxWw8acf2drd0GQAkIAAAYGSWGs3Y2zMGwO/Nzb06ADCrmvZ/AEZEAAD\r\n AyDSb/Xj0Vat0GUWcPjMXJ04YA/grN28tpm3/73b7sdQQAAAwGgIAAEbKGAB/lPnn8ni9HS93e\r\n qXLACAJAQAAI7Wy3Ix2O+cYwJ27eRe6rzM/PxXXb+Rt/88ciAEwegIAAEaq04n4ciXnoscYwJ/\r\n dvLUQc3M52/87nX4sP8j5twBAGQIAAEYu865n5nb3v5L557G22opWK2c3DABlCAAAGLm11Vbs7\r\n uace8684P2j+fmpuHY9b/t/7Yvd0iUAkIwAAICR6/UibevzqdNz8fHHxgAiIm7dztv+32z2YvV\r\n hzhsxAChHAABAEfXEd5/rAngl889hZbkV3W7pKgDIRgAAQBGP19vx4tecK6C79w+VLqG4hYXc7\r\n f/1mvZ/AEZPAABAMUuNnF0An3wyGydP5h4DuH1nMWZnc7b/77zoxvqjdukyAEhIAABAMbXMYwD\r\n JuwAyt/8vNZrRd/g/AAUIAAAo5tunnfj5573SZRSReQG8uDgVV67Oly6jmMzBFwBlCQAAKKpRz\r\n 7kYOnlyNj45NVu6jCIyt/9vbe3F041O6TIASEoAAEBR9cR3oWftAsj63x2RN/ACYDwIAAAo6oc\r\n furH5LOeO6J27+RbChw7lbv/PfP0lAOUJAAAoLuuiKOMYwO07izEzk7P9f/NZJ55v5jzzAoDxI\r\n AAAoLjMbdHZ2uGz/ff+u6xBFwDjQwAAQHHb293YeJLzXvRMC+LDh6fi8pW87f+Zgy4AxoMAAIC\r\n xkPVqtI8/no1Tp3OMAdy5m7f9f+NJO7a3u6XLACA5AQAAY2Gp0Yxut1+6jCKydAFk+e/8K1kDL\r\n gDGiwAAgLHwcqcXXz82BlBV770/HZ9eztn+3+32Y6khAACgPAEAAGMj6yFpJ07Mxukz1R4DuHN\r\n 3Maanc7b/P15vx8udXukyAEAAAMD4WH7QjE7HGEAVVf2/700c/gfAuBAAADA2Wq1+fLXWKl1GE\r\n XfuVneB/P6R6bh0aa50GUV0Ov1YfiAAAGA8CAAAGCu1L3ZLl1DEiROzceZsNccAMrf/r622otX\r\n K2dUCwPgRAAAwVlYftqLVyjkvXdU2+ar+d72NrIEWAONJAADAWNnbi1hZNgZQFUc+mI6LF3O2/\r\n zebvVh9mPNZBmA8CQAAGDv1Ws5d0+PHqzcGcPde3vb/leVWdLulqwCA/yYAAGDsrD9qx8uXxgC\r\n q4F7F/nv2I2uQBcD4EgAAMHZ6vYgHSzlPTq9SAPDBh9Nx7nzO9v+dF91Yf9QuXQYA/I4AAICx1\r\n Ei6e3rs2GycPVeNMYB7idv/lxrN6Dv8H4AxIwAAYCz94x+d+OcvOQeo7947VLqEgahSN8N+1Wo\r\n 5O1gAGG8CAADGVr2ecxFVhYXzRx9Nx/kL86XLKGJray+ebnRKlwEAfyIAAGBsNeo5xwCOHp2Z+\r\n Nn5KoQY76qRNLgCYPwJAAAYW99/txc//rhXuowiJn0BXZUxhndR1/4PwJgSAAAw1upf5OwCuHN\r\n 3cgOAKnQwvKvNZ514vpkztAJg/AkAABhrWdupjx6difMXJnMRfe/+5IYXB2X3H4BxJgAAYKz99\r\n FM3vvs254Fq9yZ0DGDSxxcOImtgBcBkEAAAMPayHgY4iWMAJ07MxJmzk9m5cFAbT9qxvZ3z6ko\r\n AJoMAAICxV683o9frly5j5D78aCYuX5msq/Qyt//XtP8DMOYEAACMvV//2Ytvvsk5BvAf/2OyF\r\n tT3/iPn6f/dbj+WGgIAAMabAACAidCo5RwDuH1nMWZmSlfxdj45NRsnT86WLqOIx+vteLnTK10\r\n GALyRAACAibDUaEa3m28M4NCh6fj85kLpMt7K7duTUecwOPwPgEkgAABgIuzu9uPRV+3SZRRxf\r\n 0La6ifx0MJB6HT6sfxAAADA+BMAADAxst4GcOOzhZifnypdxhudODETp07nPP1/bbUVrVa+7hQ\r\n AJo8AAICJsbLcinY730Jrbm4qbt4a7/b623dy7v5HRNS+yBlMATB5BAAATIx2ux9rq63SZRQx7\r\n tfr3Uo6/99s9mL1Yc5nEoDJIwAAYKJk3W29dn0hFhfHcwzg/SPTcfZczvb/leVWdLulqwCAtyM\r\n AAGCirK22Ync333Vrs7PjOwZw69ZCTE+PZzgxbPWk11MCMJkEAABMlG43YmU554nr4zpnf/PWe\r\n NY1bDsvurH+KOfNFABMJgEAABOnXssZAFy7Pn63AczNRVy+Ml+6jCKWGs3o5zuTEoAJJgAAYOI\r\n 8Xm/Hzot8g9dzc1Px+c3xGgO4dn0h5ubGK5QYlVrSIAqAySUAAGDi9Puvdl8zGrcxgHE9l2DYt\r\n rb24ulGp3QZALAvAgAAJlLW3dfrN+ZjZqZ0Ff/ts89yBgCNes7nD4DJJgAAYCI93ejE1tZe6TJ\r\n GbmFhOq5dH49F97nzc/H+kTFKI0Yo6zkUAEw2AQAAEyvrLuy4tN2P23kEo7L5rBPPN/OFTwBMP\r\n gEAABMr6y7szTFZeGdt/8/63AEw+QQAAEys55t78fx5vp3Y94/MxMWLc4VrmI6z58rWUErWzhM\r\n AJp8AAICJVv9it3QJRXxeeAwg6+7/xpN2bG/nu4ISgGoQAAAw0bLuxt68VfY6wM8+zxkAZL19A\r\n oBqEAAAMNG2trrxdKNduoyRO3lyNo4dK3MC//R0xNVr80X+3SV1u/1YaggAAJhcAgAAJl7WXdn\r\n PCh0GePHiXCwu5nuFeLzejpc7vdJlAMA7y/ftDUDlLDWa0ev1S5cxcp8XasO/kbT9P+u4CQDVI\r\n QAAYOLtvOjF14/zjQF8enk+5uenRv7vvXEjXwDQ6fRj+YEAAIDJJgAAoBIy3s0+OzsV166Pdhb\r\n /yAfTcfpMvuv/1lZb0Wrl6zIBoFoEAABUwvKDZuzt5Vug3RjxdXzXr+fb/Y+IqCW9bhKAahEAA\r\n FAJzWY/vlprlS5j5D4bdQBwI9/p/81mL1Yf5nu2AKgeAQAAlZFxDOCDD2fi1OnZkf37riXsAFh\r\n ZbkW3W7oKADg4AQAAlfHlSjNarXzXtI3qUL5z5+fi8OF8rw71mvZ/AKoh37c4AJW1txfx5Uq+V\r\n u1RXct3I2H7/86Lbqw/ynfDBADVJAAAoFIy3tV+8eLcSK4DvJbw+r+lRjP6+c6WBKCiBAAAVMp\r\n Xa6347bdcYwAzM1Nx5epwd+cXFqbi/Pl81//VEp4rAUB1CQAAqJReL+LBUr5F27CvA7xydT5mZ\r\n obfZTBOtrb24ulGp3QZADAwAgAAKifjGMC168PtALiesP0/43MEQLUJAAConK8ft+PXf+a6t+3\r\n 48dk4fnxmaP/8YQcM4yjjtZIAVJsAAIBKqifcvR3WIX3Hjs3E8eOzQ/lnj6vNZ514vrlXugwAG\r\n CgBAACV1Kjnu7t9WNf0Xb1m9x8AqkAAAEAlffftXvz0U64d3E8vz8fUEM7pM/8PANUgAACgsrI\r\n t4hYXp+PChcFf1Xf5Sq4OgI0n7djeznWGBAA5CAAAqKz6F/nGAAbdrn/23GwcPpzrdaGm/R+Ai\r\n sr1jQ5AKj/+2I3vv8t1j/vV64Nt1792LVf7f7fbj6WGAACAahIAAFBp2cYALlyYi/n5wR0EkO0\r\n AwMfr7Xi50ytdBgAMhQAAgEpr1Hej1+uXLmNkZmamBjazPzMTcfFSrgAgW2AEQC4CAAAq7Zdfe\r\n vHkSa4xgGsD2rW/9Ol8zM0N4VqBMdXp9GP5gQAAgOoSAABQedl2da9cHUwAMKh/zqRYW21Fq5W\r\n nWwSAfAQAAFTeUn03ut08C7tTp+fivfcP/hV/NVkAUEt4awQAuQgAAKi8337rx+P1dukyRuqgi\r\n /e5uYiz5+YGVM34azZ7sfqwVboMABgqAQAAKdRruXZ3D9q+/+nl+ZiZyTP/v7Lcim63dBUAMFw\r\n CAABSWH7Qik4nzxjAQQOAK1cXBlTJZMgWEAGQkwAAgBTa7X6sreZp8T5+fDY++ujdv+avDOgqw\r\n Umw86Ib649yjYgAkJMAAIA0sh3y9q67+PPzU3H6zOyAqxlfS41m9PM0hwCQmAAAgDRWH7ai2ey\r\n VLmNkLr/jLv6lT+dSzf/XarmuiQQgLwEAAGl0u68Oe8viXW8C+PRynvb/ra29eLrRKV0GAIyEA\r\n ACAVDId9vbhRzNx7NjMvv//Ms3/N+p2/wHIQwAAQCrrj9qx8yLPfW/7vQ1gdjbizNm5IVUzfur\r\n a/wFIRAAAQCr9fsSDpTyLvv2eA3Dx0nzMzuaY/9981onnm3ulywCAkREAAJBOpl3f/bbzZ5r/z\r\n /QcAECEAACAhJ486cT2do4xgA8+3N85AJcu5Wn/N/8PQDYCAABSatTzHAb4tmMAU1MR5y/kCAA\r\n 2nrTThEAA8J8EAACklKn9+23b+k+fmY2FhRyvBrVEv38A+E85vuUB4A82n+3FDz/kOADu08tvt\r\n 6t/6dMc8//dbj+WGgIAAPIRAACQVv2LHGMAx47Nxkcf/f1X/sWLOdr/H6+34+VOr3QZADByAgA\r\n A0sp0CNzb7O5fupSjAyDT7x0A/p0AAIC0fv65G98+7ZQuYyT+LgD44MPp+PCjt78tYFJ1Ov1Yf\r\n iAAACAnAQAAqdVqOcYA/u56vwtJTv9fW21Fq9UvXQYAFCEAACC1pUYzer3qLwhPfjIbi4tTr/2\r\n /Z2n/ryU59wEA/ooAAIDUXvzai3983S5dxtBNT0+9cQzgQoIDAJvNXqw+bJUuAwCKEQAAkF49y\r\n Z3wF18zBjA9HXH6TPUDgJXlVnS7pasAgHIEAACk92CpGXt71R8DuPiaNv/TZ2Zjbu714wFVUU9\r\n y3gMAvI4AAID0ms1+PPqq+q3h587NxdRfrPPPX6j+/P/Oi26sP6r+qAcAvIkAAAAix93w8/NTc\r\n frM7J/+93Pn/vy/Vc1Soxn96jd5AMAbCQAAICJWlpvRbld/hXjh4p93+zN0ANSSnPMAAG8iAAC\r\n AiOh0Ir5cqf4i8cKF3x/2NzcX8fHHM4WqGY2trb14utEpXQYAFCcAAIB/yTAGcP4PAcDZs3MxP\r\n V3tAwAz/F4B4G0IAADgX9ZWW7G72ytdxlAdPz4TCwv/veA/e7761/9lueYRAP6OAAAA/qXXe3U\r\n lYJVNT0/9rgvgzJlqBwCbzzrxfHOvdBkAMBYEAADwbzK0i58799+L/rNnq30DgN1/APhvAgAA+\r\n DeP19vx4tdu6TKG6uy/AoDZ2YiPT1Y7AMgQ6ADA2xIAAMAfLDWqvWg8869d/1On52JmproHAG4\r\n 8acf2drXDHADYDwEAAPxB1e+MP358NhYWpuL06Wrv/lf99wgA+yUAAIA/+PZpJ37+udoHx50+P\r\n VvpAKDb7Ve+kwMA9ksAAAB/oeqz46fOzMWpCt8A8Hi9HS93qn2lIwDslwAAAP5C/Yvd0iUM1al\r\n Ts3HqVHU7AKoe4ADAuxAAAMBf+OGHbjz7vlO6jKG5fmM+Dh+u5mtAp9OP5QcCAAD4o2p+8wPAA\r\n FR5F/nYseru/q+ttqLV6pcuAwDGjgAAAF6jXqv2GEBV1So+vgEA70oAAACv8csvvdh40i5dBvv\r\n QbPZi9WGrdBkAMJYEAADwBu6Snywry63odktXAQDjSQAAAG+w1GhGt2uefFIY2wCA1xMAAMAbv\r\n NzpxdePjQFMgp0X3Vh/5HcFAK8jAACAv1E3BjARlhrN6GvWAIDXEgAAwN9YftCMTsfKctw5rwE\r\n A3kwAAAB/o9Xqx1drTpYfZ1tbe/F0o1O6DAAYawIAAHgL7pYfb4263X8A+DsCAAB4C6sPW9Fq9\r\n UqXwWs4pwEA/p4AAADewt7eqzvmGT+bzzrxfHOvdBkAMPYEAADwltwxP57s/gPA2xEAAMBbevR\r\n VO16+NAYwbsz/A8DbEQAAwFvq9yMeLFlsjpONJ+3Y3u6WLgMAJoIAAAD2oe42gLFS0/4PAG9NA\r\n AAA+/DNN5345y92nMdBt9uPpYYAAADelgAAAPapbuZ8LDxeb8fLHWcyAMDbEgAAwD416sYAxoH\r\n D/wBgfwQAALBP33+3Fz/+6N75kjqdfiw/EAAAwH4IAADgHTgMsKy11Va0Wv3SZQDARBEAAMA70\r\n H5eVk0AAwD7JgAAgHfw00/d+O7bTukyUmo2e7H6sFW6DACYOAIAAHhH9Zpd6BJWllvRdRMjAOy\r\n bAAAA3lG93oxezxz6qAleAODdCAAA4B29+LUX33xjDGCUdl50Y/1Ru3QZADCRBAAAcAANu9Ejt\r\n dRoRl/TBQC8EwEAABzAUqMZ3a4V6ajUam5fAIB3JQAAgAPY3e3Ho6+0pI/C1tZePN0wcgEA70o\r\n AAAAH1KgbAxiFRt3uPwAchAAAAA5oZbkV7bYxgGGra/8HgAMRAADAAbXb/Xj4pcXpMG0+68Tzz\r\n b3SZQDARBMAAMAAaE8fLrv/AHBwAgAAGIC11Vbs7vZKl1FZAhYAODgBAAAMQLcbsbJskToMG0/\r\n asb3dLV0GAEw8AQAADIg29eGo+bkCwEAIAABgQB6vt+PFr3aqB6nb7cdSQwAAAIMgAACAAen3I\r\n x4sWawO0uP1drzccbYCAAyCAAAABki7+mA5/A8ABkcAAAAD9HSjE1tb7qsfhE6nH8sPBAAAMCg\r\n CAAAYMLvWg7G22opWq1+6DACoDAEAAAyY2wAGo/bFbukSAKBSBAAAMGDPN/di81mndBkTrdnsx\r\n erDVukyAKBSBAAAMATGAA5mZbkVXTcqAsBACQAAYAiMARxMvab9HwAGTQAAAEOwvd2Npxvt0mV\r\n MpJ0X3Vh/5GcHAIMmAAD4/9u7t96YojAAw8t/lviXeiROleACNwTFTDudcVtJIyrWHvo+zy/4s\r\n u72u9cBJjmwC+CPHB+txs7l/wDw1wkAADDJ8dFqbLe+ZG9LOAGAOQQAAJjk/Mt2vDyzlf02Pnz\r\n YjDevvaAAADMIAAAwkcsAb8frCQAwjwAAABOdnqzGZuMYwO8STABgHgEAACZarXbj2dP1vsf4L\r\n 7x7eznev9vsewwAuLMEAACYzF/t32OdAGAuAQAAJnv8aDXW6+2+x/jnOf8PAHMJAAAw2WYzxuN\r\n HjgH8yutXF+Pjx6t9jwEAd5oAAAALODz4vu8R/mkHtv8DwHQCAAAs4Pmzi/Htm2MAN7m62o3jI\r\n wEAAGYTAABgAdvtGCfHPnJvcvbiYnw9F0cAYDYBAAAW4pK7m1kXAFiGAAAAC3l5djE+f3LR3XW\r\n Xl7txeiIAAMASBAAAWNChv90/efpkPdbr3b7HAIAEAQAAFnR06DWA6w4eWg8AWMq9B/ffyu4AA\r\n ABwx9kBAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIA\r\n AAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECA\r\n AAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAg\r\n AAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAA\r\n BAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAA\r\n AQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAA\r\n AECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAA\r\n ABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAA\r\n AAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEDAD9vZqXwrTO92AAAAAElFTkSuQ\r\n mCC\r\nCLOUD:vok@fair-coral.maas\r\nEND:VCARD\r\n','Database:vok.vcf',1678358259,'748bcd238e92b751faebc36c2b1815b0',22470,'vok'),(2,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:testuser\r\nFN:testuser\r\nN:testuser;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAZtElEQVR4nOzbPUocUBhG4WsYhRBIYWljiqwpW3AD7\r\n sbeLmuxmcpOkBjSBAfBGeJksgIt/OESz/Os4K0P37e3vDndDQAAAOBd+zB7AAAAAPD2BAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAA\r\n AAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAA\r\n AACBAAAAAAIEAAAAAAgQAAAAACAgMXsAQA836eDr+PL4cnsGQBjjDHuNpfj6vfZ7BkAPMIFAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAELGYPAOD\r\n 5HrarcXt/MXsGr+Dj/vE4WBzOnjHNar0cu92f2TN4ofXDz9kTAHiCAADwH9tsf43r2/PZM3gFR\r\n 5+/pQPAj9X3sf17N3sGALxrXgAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAA\r\n BAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAA\r\n gQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAA\r\n AAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAICAveXN6\r\n W72CAAAAOBtuQAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAA\r\n AACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAA\r\n AAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAA\r\n AAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAA\r\n AAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAA\r\n QAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAI\r\n EAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAAC\r\n BAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAA\r\n gQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAA\r\n IAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAA\r\n AAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAA\r\n AAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAA\r\n AAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAA\r\n AAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAA\r\n BAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAA\r\n gQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAA4F87diAAAAAAIMjfepALIwBgQAAAAADAgAAAAACAAQEAAAAAAwIAAAAABgQAAAAAD\r\n ARyyy5ISToc2wAAAABJRU5ErkJggg==\r\nCLOUD:testuser@fair-coral.maas\r\nEND:VCARD\r\n','Database:testuser.vcf',1678353689,'fa794aa5f15801c74f72ef860fca262d',9428,'testuser'),(3,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:testuser2\r\nFN:testuser2\r\nN:testuser2;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAZt0lEQVR4nOzbv0keQACH4VM0YCPoAjY2KbOMK7iA2\r\n 6S3cwRniIUByQImCoaIRMU/XybQwigH3/s8E/y6O17uVo4PzhcDAAAAWGqrswcAAAAAH08AAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAA\r\n AAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAA\r\n AACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAA\r\n AAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAA\r\n AAAIEAAAAAAgAABAAAAAAIEAAAAAAhYmz0AgLfb2v00vuxvz54BMMYY4+rH/Tj5+nv2DABe4AU\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAASszR4\r\n AwNvdXz+NX99uZ8/gHWzurI+N7e6xfHl6N54fFrNn8J9ufj7OngDAK7o3DYAl8PfiaXw//DN7B\r\n u/g895mOgCcHV2Ph5vn2TMAYKn5AgAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAASsH\r\n B+cL2aPAAAAAD6WFwAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIA\r\n AAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECA\r\n AAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAg\r\n AAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAA\r\n BAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAA\r\n AQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAA\r\n AECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAA\r\n ABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAA\r\n AAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQI\r\n AAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEC\r\n AAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAA\r\n AECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAwL927EAAAAAAQJC/9SAXRgAMCAAAAAAYEAAAAAAwIAAAAABgQAAAAADAgAAAA\r\n ACAgQBSHi37UFl+ZQAAAABJRU5ErkJggg==\r\nCLOUD:testuser2@fair-coral.maas\r\nEND:VCARD\r\n','Database:testuser2.vcf',1678353673,'3ab9c2e4f4315785a044f3ad3f23e772',9436,'testuser2'),(4,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:testuser3\r\nFN:testuser3\r\nN:testuser3;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAZuElEQVR4nOzbPU4VUBRG0Qt52pCQQOxtnAA942AKT\r\n oDZ0NMZZmFFooWVNQFCQ3yYyI/wHAEWCLmRvdYIvnrnnLX9s6PVAAAAAF619dkDAAAAgJcnAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIA\r\n AAAABAgAAAAAECAAAAAAAABAgAAAAAELGYPAODpPrx9Nz5u786eATDGGOP7zcU4uPw8ewYAj3A\r\n BAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABi9k\r\n DAHi65f3N+PLrZPYMnsH7N1tje7Exe8Y0365Px93qYfYM/tH57+XsCQD8hQAA8B+7uL8ahz+OZ\r\n 8/gGext7qQDwKfl1/Hz4Xb2DAB41bwAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAA\r\n AAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQI\r\n AAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEC\r\n AAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAA\r\n AECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n Wv7Z0er2SMAAACAl+UCAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAA\r\n gQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAA\r\n IAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAA\r\n AAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAA\r\n AAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAA\r\n AAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAA\r\n AAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAA\r\n BAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAA\r\n gQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAIA/7diBAAAAAIAgf+tBLowAgAEBAAAAAAMCAAAAAAYEAAAAAAwIAAAAABgQA\r\n AAAADAQ2KMtzY+ltekAAAAASUVORK5CYII=\r\nCLOUD:testuser3@fair-coral.maas\r\nEND:VCARD\r\n','Database:testuser3.vcf',1678353743,'1668c9ab3128a660dae9ccdc26469848',9436,'testuser3'),(5,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:disableduser\r\nFN:disableduser\r\nN:disableduser;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOzdabDd9Z3f+e//nHPPuat0dSW0IBBCb\r\n GLxwo6NV7BDu+3udtuxk3S7nO7KpNvJJJWqzEzN43k0VamkJlMz1Q+mk47jsbudjjPtdtsG08Q\r\n N2GYxmzGbQWAQiwRC0pV097POA0BCZsfS/f3P//d6qaiLBVX+2A90rt76/f7/4v/c+z8PAgAAA\r\n Ki0WuoBAAAAwMknAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAAC\r\n QAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABA\r\n QAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAA\r\n AADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAM\r\n iAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAA\r\n AAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAA\r\n EAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAY\r\n EAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAA\r\n AAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAAD\r\n IgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAA\r\n AAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAA\r\n AABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAG\r\n RAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAA\r\n AAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAA\r\n CADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAM\r\n CAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAA\r\n AAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAAB\r\n kQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAA\r\n AAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAA\r\n AgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACAD\r\n AgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAA\r\n AAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAA\r\n JABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAE\r\n BAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAA\r\n AAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAA\r\n yIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgA\r\n AAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAA\r\n AQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABAB\r\n gQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAA\r\n AAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAA\r\n MiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIA\r\n AAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAA\r\n AAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAA\r\n ZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQA\r\n AAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAA\r\n AIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgA\r\n wIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgA\r\n AAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGWikHgBAeVw99em4d\r\n OJjqWdwEg0G/ehFL/qDfgyi/9KPQS/6L//c0R+DbvQGvegMVqI9WHnpa3852oOVWBksR2fQfvk\r\n /v/T3K/2lWOzPx1J/PnrRTf0/EwB4HQIAAGSkKGrRiFpEcfL+O1b6y7HUn4/F/lws9udjsXckF\r\n vpzsdSfj4X+XCz25+Jw90AsDxZP3ggA4DUEAADghGrVRqNVG43p2PCm/167vxJHegfjSO9gHO4\r\n diCO92Ze+dl/6+250VmkxAORBAAAAkmjWWrGhtiU2jGx53X++2JuL2d6LcaCzNw50n4/93Ze+t\r\n gcrq7wUAKpBAAAASmm8PhXj9anY2txx3M/P9Q7Fge7zL/3Vef7o3/ejl2gpAAwHAQAAGCpT9em\r\n Yqk/H9tbOoz/XG3Tjxc6eeL6zO57vPB17O7tjrjebcCUAlI8AAAAMvXrRiM3NbbG5ue3ozy325\r\n o7GgOc7T8cL7ac9VwCArAkAAEAljdenYkf9wtgxemFERPQHvdjXeS6eae+Kp9u7Ym/7KdcGAMi\r\n KAAAAZKFW1I+eErg8ro3OoB3PtX8Zz6y8FAQOdPemnggAJ5UAAABkaaRoxvbWzqPPEljszcUz7\r\n cdj98qj8eTKw7EyWEq8EABOLAEAACBeujJw3tjFcd7YxdEf9GJP+8l4fOXBeGL5gVjoH0k9DwB\r\n +bQIAAMCvqBX1OK11dpzWOjs+OvXb8ULn2Xhi5YF4fPmBONw7kHoeALwrAgAAwJsoitrRZwdcP\r\n fXpONB9IR5f/nn8YukeMQCAoSIAAAC8A+sbm2L95CfjyslPxp72U/HI0l2xa/n+aA9WUk8DgDc\r\n lAAAAvEunNrfHqc3t8ZE1vxO/XH4wHl66O55p70o9CwBelwAAAPBrGimacd7YJXHe2CUx15uNX\r\n yzdGw8v3eWKAAClIgAAAJxAU/V1cfnktXHZxMdjd/ux+NnCj+Lp9mOpZwGAAAAAcDIURS22t3b\r\n G9tbOONjdF/cv/CgeWbonutFJPQ2ATAkAAAAn2UxjY3x87efjA1OfigcX74yfL94W8/1DqWcBk\r\n BkBAABglYzWxuOyyY/HJRMfiSdWHop7F26OFzrPpJ4FQCYEAACAVVYr6nHO6HvjnNH3xlMrv4g\r\n 7528UAgA46QQAAICEXnlOgBAAwMkmAAAAlIAQAMDJJgAAAJTIq0PAHXM/iH3dZ1NPAqAiBAAAg\r\n BLa3toZZzTPjUeX74ufzH0/FvpHUk8CYMgJAAAAJVUUtdg5dmmcNfqeuGf+7+KehZujF93UswA\r\n YUrXUAwAAeHMjRTOumrouvnzK/xrnjV6Seg4AQ0oAAAAYElP16bhu+h/FF2f+ZWwe2ZZ6DgBDR\r\n gAAABgym5vb4ovr/2V8cu0/iFYxlnoOAEPCMwAAAIbU+WOXxRnN8+LWue/EY8s/Sz0HgJJzAgA\r\n AYIiN16fiN6Z/P35r+g9jorYm9RwASkwAAACogDNHL4gvbfhf4j1jV6WeAkBJCQAAABXRqo3Gx\r\n 9d+Pj4/889iur4h9RwASkYAAAComK3NHfF7G/51vHf86tRTACgRAQAAoIIaxUh8bM1n4zPTf+B\r\n NAQBEhAAAAFBpO0YvjN/f8D/F1uZZqacAkJgAAABQcZP1tfG5dX8UV01eFxFF6jkAJCIAAABko\r\n ChqccXkJ+Lvz/zzmKxNp54DQAICAABARk5tbo/f3/Cv48zWBamnALDKBAAAgMy0amPxmel/HJd\r\n NXJN6CgCrSAAAAMhQUdTig1Ofit9Y+/tRj0bqOQCsAgEAACBj5469P76w/n+Mydra1FMAOMkEA\r\n ACAzG0cOS3+4fp/FVtGzkg9BYCTSAAAACDG61PxuZmvxIVjV6SeAsBJIgAAABAREfWiEdeu/UJ\r\n 8cPI3U08B4CQQAAAAOM5lkx+Pa9d8ISKK1FMAOIEEAAAAXuPC8Svi09NfjlrUU08B4AQRAAAAe\r\n F1njV4UvzvzR9EsWqmnAHACCAAAALyhrc0d8fmZfxZjtcnUUwD4NQkAAAC8qVNGtsYXZ/5FrKn\r\n PpJ4CwK9BAAAA4C2tbayPvz/zz2NtfX3qKQC8SwIAAABvy2R9bXxu5isxVV+XegoA74IAAADA2\r\n zZVn47Pr/tKTNamU08B4B1qpB4AAGVx65HvxFxvNvWMk6ZeNKKIWtSKWtSjHkVRj9orP4p6tIr\r\n RGCma0ayNRrMYjWbRipGiFc3aaLSK0RirTUSt8Eo4ItY0ZuLzM1+Jbx38k1joH0k9B4C3SQAAg\r\n Jc9vfJoHOztSz2j1EaL8Zior4nx2lSM1SZj4uWva+rrYm1jfaytr4/R2njqmayCtY318bmXI8B\r\n Sfz71HADeBgEAAHjblgeLsdxdjAPx/Bv+OyNFM9bU18fa+kysra+P6cYpsaGxJWYam6JVG13Ft\r\n Zxs6xqnxOdmvhL/7cCfxPJgMfUcAN6CAAAAnFCdQTsOdPfGge7e1/yzydp0bBjZEusbm2NDY0u\r\n cMrI11tU3RFF4LNGwWt/YFJ+b+eP41sE/ifZgJfUcAN6EAAAArJr5/qGYXzkUT608cvTnmsVob\r\n Bk5I7Y0t8fmkW2xaWSbkwJDZsPIqfGZ6T+Iv5r90xhEP/UcAN6AAAAAJNUeLMfu9qOxu/3o0Z9\r\n b39gcpzXPjm2tc2PryI5o1loJF/J2nNY6Oz659otx4+Fvpp4CwBsQAACA0jnQfT4OdJ+P+xd/H\r\n EXU4tTmmbGteU5sa50bGxtbXRkoqZ1jl8bh3sG4c/7G1FMAeB0CAABQaoPox3PtJ+K59hNx+/w\r\n NMVqMx9mj74mzR98bpzXP8mrCkrly8pNxpHcwHlm6O/UUAH6FAAAADJXlwWI8uHRnPLh0Z7SKs\r\n Thr9KI4e/S9cXrz7KgXvrUpg2vWfD7meofi2fbjqacA8Co+JQGAobUyWIqHl+6Kh5fuimYxGue\r\n NXRIXjV0Rp4xsTT0ta/WiEZ+e/nL81wP/dxzs7Us9B4CXuUAHAFRCe7AcDyzeFn9x4N/Hn+//P\r\n +LnCz+Jlf5S6lnZatXG4ndm/odoFWOppwDwMgEAAKic/d09cfPct+NP9/1vccOhP48XOs+knpS\r\n lqfq6+NT0l1LPAOBlrgAAAJXVj148tnxfPLZ8X2xtnhWXjH8ktrd2eovAKtrWOjeumrwu7pj/Q\r\n eopANkTAACALLzyJoF19VPi4omPxs6xS6JRjKSelYXLJ66Jve2nYnf70dRTALImfwMAWZntvRg\r\n /PPKt+OqL/3v8fPH26A26qSdVXlHU4rrp34vJ2nTqKQBZEwAAgCwt9ufi5iP/X3xt/7+JXyzdE\r\n 4NBP/WkShutjcen1305alFPPQUgWwIAAJC1ud5s3Hj4m/GN/f8uHl9+IPWcSts0cnp8dM1nU88\r\n AyJYAAAAQEQd7++L7h74W3zrwJ3Gg+0LqOZX1nvGr4tzRi1PPAMiSAAAA8Cp7Ok/GN/b/u/jRk\r\n b+Jlf5y6jmV9PE1vxsTtTWpZwBkRwAAAHiNQdy3eGv8v/v/TTy6dF/qMZXTqo3FJ9Z+MfUMgOw\r\n IAAAAb2CxPxc/OPzn8dcH/0PM9w6nnlMpZ7TOi/eMXZV6BkBWBAAAgLewu/1ofH3/v3Ua4AS7e\r\n uozsaY+k3oGQDYEAACAt6E9WI4fHP7zuP7Q12O5v5h6TiU0a634e2v/UeoZANkQAAAA3oFdy/f\r\n H1/f/23hmZVfqKZVwanN7XDrxsdQzALIgAAAAvEOL/bn4q9k/jbvnf5h6SiVcNXldzNQ3pp4BU\r\n HkCAADAuzKI2+avj+/OftXrAn9N9aIR16z9QuoZAJUnAAAA/Bp+ufJQfPPAv4/9nb2ppwy1U5v\r\n b4/yxy1LPAKg0AQAA4Nd0uHcg/vLA/xVPrjySespQu3ryN6NZjKaeAVBZAgAAwAnQjU78zex/i\r\n gcX70w9ZWiN16fig5O/kXoGQGUJAAAAJ8wgfnjkW3HH3A9SDxla7xn/QJzS2Jp6BkAlCQAAACf\r\n YTxduir89/F+iP+ilnjJ0iqIWH1/zudQzACpJAAAAOAkeWbo7vnfoa9EbdFNPGTqbm9viwrErU\r\n s8AqBwBAADgJHly5eG4/tDXRYB34YNTvxnNopV6BkClCAAAACfRL1ceiusPfd11gHdorDYRl01\r\n ck3oGQKUIAAAAJ5kI8O68b+JDMVabTD0DoDIEAACAVfDEyoPxg8N/EYNBP/WUoTFSNOPKiU+kn\r\n gFQGQIAAMAq2bV8f/x47nupZwyVC8evjKn6utQzACpBAAAAWEX3Ld4a9y/8OPWMoVEvGvGByet\r\n SzwCoBAEAAGCV3TL31/HE8oOpZwyN80YvjvWNzalnAAw9AQAAIIEbDn0j9rZ3p54xFIqiFh+c/\r\n FTqGQBDTwAAAEigF9343qH/HAu9I6mnDIUzRy+IzSPbUs8AGGoCAABAIov9Oa8HfAcum7gm9QS\r\n AoSYAAAAktKfzZNw2d33qGUPhzNb5MV3fkHoGwNASAAAAErt38ZZ4fPmB1DNKryhqccnEx1LPA\r\n BhaAgAAQAn87eFvxqHu/tQzSm/n2CUxXptKPQNgKAkAAAAl0Bm048bDfxGDQT/1lFJrFCPx/vE\r\n PpZ4BMJQEAACAkni+83Tcs3BL6hml957xD8RI0Uw9A2DoCAAAACVyx/wPYn9nb+oZpdaqjcVFY\r\n 1elngEwdAQAAIAS6Ucvbjz8zegNuqmnlNr7Jz4chW9lAd4Rv2oCAJTM/u6e+On8TalnlNpUfTp\r\n 2tC5IPQNgqAgAAAAldPfC38XB7r7UM0rtonHXAADeCQEAAKCEBtGPW458O/WMUtvWPCem6utSz\r\n wAYGgIAAEBJPdPeFbuWf556RmkVRS0uGrsy9QyAoSEAAACU2I+OfCc6g3bqGaV1/thlEVGkngE\r\n wFAQAAIASm+8f9kDANzFZXxtnts5PPQNgKAgAAAAld9/CrTHXm009o7Q8DBDg7REAAABKrh+9u\r\n GP+xtQzSmt787yYrK1NPQOg9AQAAIAh8MjSPV4L+AaKohYXjF2eegZA6QkAAABDYRB3zN2QekR\r\n pnTt2ceoJAKUnAAAADInHVx6IfZ1nU88opZnGxljf2Jx6BkCpCQAAAEPkdqcA3tA5o+9LPQGg1\r\n AQAAIAhsrv9aOzv7Ek9o5QEAIA3JwAAAAyZexZuST2hlNY1TokNjVNTzwAoLQEAAGDIPLb8s5j\r\n rHUo9o5TOdQoA4A0JAAAAQ2YQ/fjZwo9Szyils0ffm3oCQGkJAAAAQ+jBpTtipb+UekbpTDc2x\r\n CmNralnAJSSAAAAMIQ6g3Y8uHRn6hmldI5TAACvSwAAABhSDy7eEYNBP/WM0tne2pl6AkApCQA\r\n AAEPqcO9APNf+ZeoZpbNh5NSYqK1JPQOgdAQAAIAh5hrA6zujdV7qCQClIwAAAAyxx5cfiOX+Y\r\n uoZpXOGawAAryEAAAAMsX704pGlu1PPKJ1tzXMiokg9A6BUBAAAgCH30KJrAL+qVRuLLSPbUs8\r\n AKBUBAABgyB3s7Yv9nT2pZ5SOawAAxxMAAAAq4LHl+1NPKB2vAwQ4ngAAAFABjy3/LPWE0jmlc\r\n WqMFuOpZwCUhgAAAFABR3oHY1/n2dQzSqUoarG1uSP1DIDSEAAAACpil2sAr3Fq88zUEwBKQwA\r\n AAKgIzwF4rS0j21NPACgNAQAAoCLmerPxYue51DNKZePI1qhHI/UMgFIQAAAAKuSplV+knlAqt\r\n aIem0ZOTz0DoBQEAACAChEAXstzAABeIgAAAFTI3s7uWOkvpZ5RKgIAwEsEAACAShnE0+1dqUe\r\n UypaRM1JPACgFAQAAoGKeWnkk9YRSadXGYn1jc+oZAMkJAAAAFbN75dHUE0pn88i21BMAkhMAA\r\n AAqZrE/Fwe7+1LPKJVTGqemngCQnAAAAFBBz7V/mXpCqWwYEQAABAAAgAra034y9YRSWd/Ykno\r\n CQHICAABABTkBcLxWbTSm6utSzwBISgAAAKig+f6hmOsdSj2jVDwHAMidAAAAUFGuARxvg2sAQ\r\n OYEAACAitrjGsBxPAgQyJ0AAABQUS90nk09oVRcAQByJwAAAFTU/u7e6A96qWeUxpr6uhgpmql\r\n nACQjAAAAVFQ/enGw+0LqGaVRFLWYaWxKPQMgGQEAAKDC9nWeSz2hVNbWN6SeAJCMAAAAUGH7u\r\n gLAq03X16eeAJCMAAAAUGEvOgFwnOmGEwBAvgQAAIAKe7HzXAwG/dQzSmONEwBAxgQAAIAK60Y\r\n njvRmU88oDVcAgJwJAAAAFXewty/1hNIYr095FSCQLQEAAKDiDnVfTD2hVFwDAHIlAAAAVNxs1\r\n wmAV3MNAMiVAAAAUHGzvf2pJ5SKNwEAuRIAAAAqzgmA47kCAORKAAAAqLjF/lys9JdTzyiNydq\r\n a1BMAkhAAAAAycKjnQYCvGK9PpZ4AkIQAAACQgbneodQTSmPCCQAgUwIAAEAG5nqzqSeUxnhtM\r\n vUEgCQEAACADDgBcEytqMdoMZ56BsCqEwAAADIwLwAcZ6LuGgCQHwEAACADc30B4NU8BwDIkQA\r\n AAJABVwCON17zJgAgPwIAAEAGFvtz0Rt0U88ojQmvAgQyJAAAAGRisT+XekJpOAEA5EgAAADIx\r\n GJ/PvWE0vAqQCBHAgAAQCaW+gupJ5RGsxhNPQFg1QkAAACZWBYAjmrWBAAgPwIAAEAmnAA4plm\r\n 0Uk8AWHUCAABAJgSAY1wBAHIkAAAAZMIVgGNcAQByJAAAAGTCCYBjXAEAciQAAABkQgA4pl40o\r\n h6N1DMAVpUAAACQic6gnXpCqbgGAORGAAAAyERnsJJ6Qqm4BgDkRgAAAMiEEwDH8yYAIDcCAAB\r\n AJpwAOF6jGEk9AWBVCQAAAJloOwFwnFpRTz0BYFUJAAAA2RhEd9BJPaI06iEAAHkRAAAAMiIAH\r\n FPzrTCQGb/qAQBkpN1fTj2hNIrCt8JAXvyqBwCQkX70U08oDScAgNz4VQ8AICMCwDH1opF6AsC\r\n qEgAAADLSH/RSTyiNwrfCQGb8qgcAkJGBEwBH1TwDAMiMX/UAADLiCsAxXgMI5EYAAADISH8gA\r\n LyiKAQAIC8CAABARlwBOKaIIvUEgFUlAAAAZMRDAI8Z+P8CyIwAAACQkUEMUk8oDc9DAHIjAAA\r\n AZKTm3vtRAgCQGwEAACAjhW//juq5AgBkxicAAEBGar79O8oDEYHc+AQAAMhIrfDt3ys8EBHIj\r\n U8AAICMOAFwTC8EACAvPgEAADJSeAjgUYOBKwBAXgQAAICMOAFwjLcAALnxCQAAkBEB4BgBAMi\r\n NTwAAgIzUXAE4qj/opp4AsKoEAACAjDSLVuoJpdEVAIDMCAAAABkZKZqpJ5RGZ7CSegLAqhIAA\r\n AAyUY+GKwCv0hYAgMwIAAAAmfCn/8dr95dTTwBYVQIAAEAmRmru/7+iP+hFNzqpZwCsKgEAACA\r\n THgB4TGfQTj0BYNUJAAAAmXAF4Jj2wPF/ID8CAABAJkacADhqxf1/IEMCAABAJsZqE6knlIY3A\r\n AA5EgAAADIxVptMPaE0XAEAciQAAABkwgmAY7wCEMiRAAAAkAknAI5ZcQIAyJAAAACQiXEB4Kj\r\n l/kLqCQCrTgAAAMiEEwDHLPTnUk8AWHUCAABAJjwD4JjF3pHUEwBWnQAAAJAJVwCOcQIAyJEAA\r\n ACQgXo0olUbSz2jNBYFACBDAgAAQAbW1NelnlAqAgCQIwEAACADUwLAUZ1BOzqDduoZAKtOAAA\r\n AyIATAMcs9vzpP5AnAQAAIANOABzjAYBArgQAAIAMOAFwjPv/QK4EAACADDgBcMxi70jqCQBJC\r\n AAAABkQAI6Z6x9KPQEgCQEAAKDi6tGIydqa1DNK41B3f+oJAEkIAAAAFbeusTGKwrd9rzjcO5h\r\n 6AkASPgkAACpuprEx9YRSOdI7kHoCQBICAABAxc00NqWeUBpL/YVoD1ZSzwBIQgAAAKi4dU4AH\r\n HW460//gXwJAAAAFecEwDGHHf8HMiYAAABUWhHT9fWpR5SGAADkTAAAAKiw6fr6qBeN1DNKQwA\r\n AciYAAABU2DQ+BZAAABhASURBVMaR01NPKBXPAAByJgAAAFTYppHTUk8oFScAgJwJAAAAFbbJC\r\n YCjVvpLsdA/knoGQDICAABAZRWxoXFq6hGlcaD7fOoJAEkJAAAAFTVTPyWatVbqGaWxv7Mn9QS\r\n ApAQAAICK2tTclnpCqezv7k09ASApAQAAoKI2NTwA8NUEACB3AgAAQEVtaW5PPaE0BoN+7O8IA\r\n EDeBAAAgApqFqOxobEl9YzSONKbjW50Us8ASEoAAACooK3NHVEUvtV7heP/AAIAAEAlndY8K/W\r\n EUhEAAAQAAIBKEgCO5xWAAAIAAEDluP//Wvu6z6WeAJCcAAAAUDGnNc9y//9V5nuHY643m3oGQ\r\n HI+GQAAKmZb85zUE0plb2d36gkApSAAAABUzJmjF6SeUCp720+lngBQCgIAAECFrG9sian6utQ\r\n zSmVv56nUEwBKQQAAAKiQHS1/+v9q3UEn9nU8ABAgQgAAAKiUHaMXpp5QKvs6z8Yg+qlnAJSCA\r\n AAAUBHjtanY2Niaekap7Gk/mXoCQGkIAAAAFXFm63yv//sVe9z/BzjKJwQAQEWcPfre1BNKxxs\r\n AAI4RAAAAKmCsNhmnN89OPaNUDnRfiJXBUuoZAKUhAAAAVMC5o++PWlFPPaNUnl55NPUEgFIRA\r\n AAAKmDn2CWpJ5TObgEA4DgCAADAkFtbXx+bRk5PPaNUuoNOPNf+ZeoZAKUiAAAADLmdY5emnlA\r\n 6z7V/Gb3opp4BUCoCAADAkDtv9OLUE0rH8X+A1xIAAACG2OnNc2K6sSH1jNLZvfKL1BMASkcAA\r\n AAYYu8bvzr1hNKZ683GbO/F1DMASkcAAAAYUpO1tbG9tTP1jNLZvfJY6gkApSQAAAAMqfeMfyB\r\n qRT31jNJx/B/g9QkAAABDqIhaXDh2ReoZpdMddOLpthMAAK9HAAAAGELnjL4vxutTqWeUztMrj\r\n 0Vn0E49A6CUBAAAgCF06cRHU08opceW7089AaC0BAAAgCGzvbkzThnZmnpG6XQHnXhy5aHUMwB\r\n KSwAAABgyl01em3pCKTn+D/DmBAAAgCGytXlWnNrcnnpGKTn+D/DmBAAAgCFy+cQ1qSeUkuP/A\r\n G9NAAAAGBKbRk6Pba1zU88oJcf/Ad6aAAAAMCQ+OPmp1BNKy/F/gLcmAAAADIHtzZ1xeuuc1DN\r\n KqTNoO/4P8DYIAAAApVfE1Ws+k3pEae1aut/xf4C3QQAAACi5i8auiPWNTalnlNZDS3emngAwF\r\n AQAAIASa8RIXDn591LPKK2D3X2xt7M79QyAoSAAAACU2GWT18REfU3qGaX14OIdqScADA0BAAC\r\n gpKbrG+LSiY+lnlFavUE3Hlm6O/UMgKEhAAAAlNS1a78Q9aKRekZpPbH8YKwMllLPABgaAgAAQ\r\n AldOHZFbG3uSD2j1B708D+Ad0QAAAAombHaZFw99enUM0rtcPdAPNt+PPUMgKEiAAAAlMxHpn4\r\n 7RmvjqWeUmj/9B3jnBAAAgBI5q3VRnDd2ceoZpdbur8QDi7enngEwdAQAAICSmKytjWvXfiH1j\r\n NJ7eOmn0R4sp54BMHQEAACAkrhu+vcc/X8L/UEv7l24NfUMgKEkAAAAlMDlE9d66v/bsGv55zH\r\n fP5R6BsBQEgAAABLbNHJ6XDn5ydQzhsI9CzenngAwtAQAAICEWsVYfGrtl6JW1FNPKb2nVx6L/\r\n d09qWcADC0BAAAgkSJq8enpL8eaxkzqKUPh3oVbUk8AGGoCAABAIh9d89k4rXV26hlDYX9nTzz\r\n dfiz1DIChJgAAACRw0diV8d7xD6SeMTTuWvhh6gkAQ08AAABYZVtGtsdH13w29Yyh8WLnudi1f\r\n H/qGQBDTwAAAFhF6+qnxGfW/UHUi0bqKUPjtrnrU08AqAQBAABglUzW1sZnZ/4oxmoTqacMjT3\r\n tJ2N3+9HUMwAqQQAAAFgFo8V4/O7MH8dUfTr1lKHyk7nvp54AUBkCAADASTZSNOOzM/801jVOS\r\n T1lqDy58kjs7TyVegZAZQgAAAAnUT0a8VvTfxgbR05LPWWoDAb9uH3uhtQzACrF02cAAE6SkaI\r\n Zv73un8TW5o7UU4bOY8v3x/7untQzACpFAAAAOAmaxWh8dt0/jc3NbamnDJ3eoBt3zP8g9QyAy\r\n hEAAABOsNFiPD4388exYeTU1FOG0n0Lt8bh3oHUMwAqRwAAADiBJmpr4ndn/jhmGhtTTxlKc73\r\n Z+On8TalnAFSSAAAAcIKsb2yJ3173hzFVX5d6ytC69ch3ohud1DMAKkkAAAA4Ac5sXRDXrf29a\r\n NZaqacMradWfhFPrDyYegZAZQkAAAC/pkvGPxpXT/1mFIU3LL9b3UEnbjny7dQzACpNAAAAeJe\r\n KqMU1az4fF45fkXrK0Lt34RYP/gM4yQQAAIB3YbI2HZ+a/lJsaZ6ResrQO9I9GHfN//fUMwAqT\r\n wAAAHiHdrQujE+s/WKM1sZTT6mEm+e+Hb3opp4BUHkCAADA21SLenxo6tPx/okPp55SGQ8v3hV\r\n PrTySegZAFgQAAIC3YW19fXxq+kuxceS01FMqY643G7fMefAfwGoRAAAA3lQRF49/OK6aui5Gi\r\n mbqMZUxGPTjxsP/JTqDduopANkQAAAA3sBMfWN8Yu0/iM3NbamnVM79iz+J59pPpJ4BkBUBAAD\r\n gVxRRi8snronLJ6+NeuHbpRPtYHdf/GTu+6lnAGTHJxoAwKtsbZ4VH536ndgwsiX1lErqD3px4\r\n 6G/8NR/gAQEAACAiFhTn4kPT/1WnDV6UeoplXbXwg9jX/fZ1DMAsiQAAABZaxatuHzyE/H+8Q8\r\n 57n+S7Wk/FT+dvyn1DIBs+ZQDALJUi3pcOH5lXDnxiRivT6WeU3mLvbn4/qGvxSD6qacAZEsAA\r\n ACy8spv/C+b+HhM1adTz8lCf9CL6w9/Ixb7c6mnAGRNAAAAslCPRlw0flVcOvGxmKyvTT0nK7f\r\n NXe+VfwAlIAAAAJXWLEbjorEr4+KJj8REfU3qOdl5fPmBuHfxltQzAAgBAACoqJn6xnjfxIdj5\r\n 9glMVI0U8/J0mz3xfjbw99MPQOAlwkAAEClnNm6IN4//qE4vXVO6ilZ6wza8b3Zr0Zn0E49BYC\r\n XCQAAwNCbrm+InWOXxvljl8ZUfV3qOdkbDPpx0+G/jIO9famnAPAqAgAAMJRaxVicO3ZxnD96a\r\n Wxubks9h1e5bf762LV8f+oZAPwKAQAAGBrNohXbW+fHOaPvi+2tnVEvfCtTNj9fvD3uWbg59Qw\r\n AXodPTQCg1CZqa2JH64LYMXpRnNY8y2/6S+zJ5Yfj5iN/lXoGAG/AJygAUDJFbGxsjW2tc2NH6\r\n 8LYNHJaFEUt9SjewgudZ+L6Q1+PiEHqKQC8AQEAAEhufWNznNY8O05vnh1bmzuiVRtLPYl34Ej\r\n 3YHxn9s+iG53UUwB4EwIAALCq6tGIjSNbY9PIttg8si1Oa54V4/Wp1LN4l5b7i/Ht2T+Npf586\r\n ikAvAUBAAA4iYpY39gUm0ZOf/k3/KfH+sbmqBX11MM4Adr9lfib2T+LQ739qacA8DYIAADACVD\r\n EuvqGmGlsOvrX+samWNfY6KF9FdUZtOOvZ/9D7O3sTj0FgLfJJzIA8La0irFYU5+JNfV1x39tr\r\n I/p+nq/0c9IZ9CO78z+WeztPJV6CgDvgE9qAMhUI0aiWRuNZtGKsdpEjNUmY7w2+dqv9amYqk1\r\n Hs9ZKPZkS6Aza8Tez/ymeaz+RegoA75AAAAAvu2Lyk7E8WEo94x2rvfKjqEURtagV9aM/VxS1a\r\n EQjRmqtGCla0Sxe+dr0aj3ese6gE9+d/Wo823489RQA3gUBAABedu7Y+1NPgNJ65Tf/z7R3pZ4\r\n CwLsk/QMA8Ka6g058b/Y/x9Ptx1JPAeDX4AQAAABvaKW/HN899FV3/gEqQAAAAOB1zfcOx1/P/\r\n sc40N2begoAJ4AAAADAa8x2X4y/Ovj/xHz/UOopAJwgAgAAAMfZ294d35n9j7EyhG/FAOCNCQA\r\n AABz15Moj8f3Zr0UvuqmnAHCCCQAAAERExMOLd8VNR/5rRAxSTwHgJBAAAAAyNxj047b5G+Keh\r\n b9LPQWAk0gAAADI2HJ/MW449I14uv1Y6ikAnGQCAABApvZ39sZ3D301jvQOpp4CwCoQAAAAMvT\r\n Y0s/ipsN/Gd3opJ4CwCoRAAAAMvLSff/r456Fm1NPAWCVCQAAAJlY7i/G9Ye+Hs+0d6WeAkACA\r\n gAAQAaeWdkVNx7+Ziz0j6SeAkAiAgAAQIX1Bt24fe6GuHfxltRTAEhMAAAAqKgD3RfihkPfiAP\r\n dvamnAFACAgAAQAXdv/Dj+PHc96IX3dRTACgJAQAAoEIWekfipsN/Gbvbj6aeAkDJCAAAABXx6\r\n NJ9ccuRb8fyYDH1FABKSAAAABhyh7sH4odH/pvX+wHwpgQAAIAh1Rt0496FW+Kn8ze56w/AWxI\r\n AAACG0J72k/HDw9+Kg719qacAMCQEAACAIbLcX4yfzH0vHlr6aeopAAwZAQAAYAj0B714aOmuu\r\n H3ueg/5A+BdEQAAAEruyeWH48dz343Z3ouppwAwxAQAAICSer79dPx47ruxp/Nk6ikAVIAAAAB\r\n QMoe7B+L2+RviseWfpZ4CQIUIAAAAJbHcX4yfzt8U9y/+JAbRTz0HgIoRAAAAElvoHYn7Fm6Nn\r\n y/eFt3opJ4DQEUJAAAAicz1ZuPuhZvjocU7ox+91HMAqDgBAABglc12X4y7F34Yv1i611F/AFa\r\n NAAAAsEr2d/bEXQv/f/t2syPFeYBh9EuuOTeSG8ouziKKUbCDwfzZZgAP4n+Gme7qrqosbE3iy\r\n EqIDS7gOUdqfaXqzVvLflr1p3F79/etpwAQJAAAALxHyzqP+/uvxvWLz8aD6fbWcwAIEwAAAN6\r\n DN/PrcePyr+OLi7+M8+XV1nMAQAAAAHiXHk3fjOsXfx63d9e93w/AB0UAAAD4laZlP27tro3rF\r\n 5+NZ8fHW88BgJ8lAAAA/ALLOo9vp6/HrcvPx93dl2Mex60nAcB/JQAAAPwfvp++Gzd3n4+vL6+\r\n N3Xqx9RwAeGsCAADA//Dy+HTc2l0bNy//Nl7Nz7aeAwC/iAAAAPAznh1Px/3dP8ad3RfjyfFk6\r\n zkA8KsJAAAA44d3+h9N98e9/Y1xd//lOJtfbD0JAN4pAQAAyNovu/Ht/ua4t78xvtl/NaZ1t/U\r\n kAHhvBAAAIGNej+P0cDJOpjvjwXRnPJruj3UsW88CgN+EAAAAfLKWdR5PDg/HyXRnnEx3x6Pp/\r\n jiOw9azAGATAgAA8MmY1+N4enw8Tqa74+H+7nh4uDcO67T1LAD4IAgAAMBHaV2X8WJ+Op4cHoz\r\n vDw/Gk8OD8eTwcCxj3noaAHyQBAAA4KNwNr8Yp4eTcXr47ur07z4AvD0BAAD4oJzPr8bz4+nV5\r\n 9mP53693HoaAHzUBAAA4Dc3r8dxNr8cL+enP/mx//x4OqZ1v/U8APgkCQAAwDt3XA/jbH45Xs/\r\n Pr87X84tx9uP5Znm99UQAyBEAAIC3sqzzuFzejMvlfFws51fnv19fLufjbH45LpazrecCAP9BA\r\n ACAj9yyzmMZy1jWZaw/XF3dW9dlLD+5N4/jehyHdT+mdT8Oy/7qelr347BOY1p2//p+3Y/dcjE\r\n ulvMxrbutHxUA+BV+98fHf1i3HgEAAAC8X7/fegAAAADw/gkAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQI\r\n AAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEC\r\n AAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAA\r\n AECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAH/BM5DXVlq2TI+AAAAAElFTkSuQmCC\r\nCLOUD:disableduser@fair-coral.maas\r\nEND:VCARD\r\n','Database:disableduser.vcf',1678353836,'e854666b7b50503a88b923483f330504',20307,'disableduser'),(6,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:admin\r\nFN:admin\r\nN:admin;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc\r\n 4AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAPKElEQVR4nO2de7BV1X3HP9w5wzAMw9yxxmEMUWqId\r\n WgSFa2PHXQbqA8kbQyPELUGY2KTmlhC2DRj12S73GTVIWxib1TSFoyPBBNMajRRQQ3iTnBrCKE\r\n EE0sNUqSUUHrL3DLMnTvMnZv+8VuHezj3nHv2Xud1k57vzJkLa+/fWr+1f3u9fq89Lk7935Idf\r\n cB+YDewFXgu8JIjOehHIE79e4AwJ9kQ0Au8CewEtgAvBF4yUA8v7cC4nAIoxyCwGVgdeMmPXSp\r\n wFEAlHAO+ZXnZ34D6WoKuOukLwIeAJE795+PUn94AnlwxGbgDeCtO/fvi1J/cRl4yo14BlOIa4\r\n PU49T/VwDpd0AV8HvhFnPqXtJmXmmikAAAmAOvi1O9pcL0umAa8Eqf+wnYzMhoaLYAi/jpO/XV\r\n NqjsPCsDGOPU/2m5GqqFZAgD4VJz6f9vE+rOiC9gQp/6sdjNSCc0UAMDKOPWvbHIbWVAcCae1m\r\n 5FyNFsAXcBDceqPb3I7WXAmsLrdTJSj2QIAmA58rgXtZMFtcepf0G4mStEKAQCsGCOjAOCudjN\r\n QilYJYArw5y1qqxbmx6l/eruZKKIAfC3DfV3IQ7wSOMOxrRuB71UofzUjD0WMR/b4VyHnjrwoA\r\n POBf3KgbTjG5bk5Tv0CMp+vRjqSB33AHwReMpSTrhovpwFrgFsdyJ8MvGRBI/ioF7kEUESc+vO\r\n Bf3Yg/aPAS950aXMUXr4OfCYn2YHAS85uJB+ucFoDAi95EviBA+l5Lu3VwAogr0r8rDj1Xaavh\r\n qOeRfibDjQNPwgFXnIct5fBdS1rKOoRwD4Hmu462hsNv3agmdRwLhxQjwBc3ub+OtobDe9woGk\r\n WL7lQjwDmOdD01dHeaLjOgeZYw7lwgJMArOXrLx1IXaatWrx8HHhvTrK+wEuONpoXF+QWQJz65\r\n wHPAxNzkg4Cv8zbXg1e5gP/6EC6q5F81INCnPpXZbkP0SZeDXyM/IcwgNcqeS3EqT8NOdlmxQR\r\n EwbcAOQ27YKsjXcNRoHXMfL9K+SdojFdEHjzZ4vaqolXKuBOIy8hYwPbASxo6FdaDVgngkXodu\r\n BqINe1moBStEEA/sLIF7WTBjsBLnmg3E6VohQBU4CUHW9BOLQwCt7ebiXK47Gby4LnAS/6+yW1\r\n kxYrAS0bdflqnsivyVBp4yZIs98WpvwYYYQhqtgBONLn+PMjCyxXAx3PWm0kAwELgrPLCZk9BN\r\n 8Spn1dX3yzcF6d+3hNz09GKNWB1nPpTWtBOLYzH7dTcVLRCAJOAL7WgnSzw4tS/od1MlKJV54D\r\n bxpBX2op2M1CKVglgAnBTi9qqBc8qFMcECsBHMt57JjAHuAE3wS0CHqhQ/m3gX3LUUwDOQZRxr\r\n v7/C4EvO9I2FIXAS57Kcf9a69r3NBW2VDVwWZz64wMvOWU7GHjJHmBPzroAvmJtAQ+RfzvtM0Y\r\n EkPtNtoeZa4G8AXHjyW84qcXLY8BfOZBe3Eg+6oGrW8oeYL0D6Tku7dXgZT35R1B3nPrNchDIh\r\n XoW4WcdaJq1E3rOgeZ3XgAu6uW8Zsys+C8HmkpuKblVJzm8viuuU/UIYJoDTbM8Ed7tQHM8Y1k\r\n t1AzNjVN/EuLcPAL1COBmB5reOtqrCOti6HK6PVyh7DcO9dyY4Z75VHnWrm4p19tK88Jlu1kL9\r\n 5LfzfBglbQGLo7DX4hTf0a1i1YPdm+167n2z3HqdwG3AffnobM4Bux1oKvGy2SkY3c4kO/IWT4\r\n aJiKZApYEXnLKZsAGim9ADrEVMc7maqiFAjAVmG3/uuCpwEtGnLqtW8wHc9QzEZl3ZyPpCVzw2\r\n cBL1la6EKf+r8kwr1fBG0CKWN9mkuGkXqB1LiEbq5R/sIU8gGRaqRSpU8TjuPMzw/4yo1XKuF4\r\n gj8qjmfhBDQ+NRxEhtQStEsCaMZTLx4x2MfCSfbTQh6kVAtgPjBXD/OOBl2RZaBUtcl9vhQBuH\r\n yNv/xFgWZYbrRtNS+KJmy2ALwde8qMmt5EFQ8CNebzzAi/5Gi3wIW2mAB4LvGSs2II/GXjJSw5\r\n 0tyDbyqahWQJYj3g9txuDwJLASx5xIQ68pB+xfTRtFDdaACeApYGX3N6ogOw6cBiYY402zrBRm\r\n HOBrzaEq1PxRCMFsA24yM6d7cZ64I9dMzmWI/CSwcBLliM28Ubosw4BiwMvWdwI18SXgVWBl2x\r\n uQF31YAB4wvLyRjMaCLzkpTj134dECS0H8qa+2Qv0AOuLO8O8AhhEAu12AwlyqjyQs45GoZ/hx\r\n K0vAs/Y6aKpCLxkEDmofctqQa9H1CkzGGkjOYSMmG3As4GXbC+vb1wO2+gJuyg1FFafnzdtQH+\r\n 5d8VYgTW+FAIvaVZIbgcddNBBBx100EEHHXTQQQf1YJyOzC/sv48jR/seHaqmpHPRkZmP6FCe1\r\n aH6u1Hu+4kOVdV4XR2ZTUjAx2zgXB2quMp9FwM361BlsoS1A11IsIKPGB9+BiQ6MlUdiepED/B\r\n Jaqt231/j+kHEynUa8K5R7ptEE1ziG4mCDlVRZ9EHrNWRWQzMQjSLAOjInAugQ1XRdU9HZgIwq\r\n EM1WKO9qTpUJ9W5lu4c4LgO1Qilno7MZETBtV+H6qRjrw7V7fZ6JV6mIc5bTvlJdWSmIoJ9U4d\r\n qoOzaFCTa/U0dqhNl16r2RUdmku3HER2qU8yiIxK32inpEzpUO3VkZgDfZTjV2FnAR3So9unI9\r\n CCxXYuQiJM5yNRydkld3cDb9tpDyJu9G1iFvMF3I9rM6Yh2c54OVb+OzP8inglLEG3iJcAGHar\r\n I1vs2cD7ilHuhDtVSHZnTkJxEQ4gx5lzEx2eODtWHdWQSYGnp9Koj8zRwH/Ly3Y94cHQjhqXLb\r\n F+368iMBx5G8p7usX+fAabpUC3RkVmIJCQp9mXA9uW4jszfII7MuxH19W4dqptt+1cVdGRutfx\r\n MQiw/T+lQ7bRlGwGlQ/WUJfgQki/0A/a6ApbrUM2zD7sidKh2AOfryPxWh+p8W9d/AJfrUB20/\r\n 9+ApCFei7zBFwGX6lAN2bcr0ZHZpUNVLUdoD7ClREhnICrzrCPhMmB1sX4dmTuALyLBgAoY0KG\r\n 6yF47Hfg5YgsBSYHj61Dtt9cfBW7VkXkECYt9lw7VgI5MF/CqjsyfImnTvt0F/KH9vQcRwsU6M\r\n t06Mhcg08pJjzYdqmeA0+0wBdg8ygOphaPAQvt2ASxl2AuhAKzUoRqy7Q7YTt5SqSIdmQKSnf0\r\n rJbweQd7arDhc1peXGNbv/wUlDl06VL3IiC6it6wvy21fBmxfrteR6bL9WYCMlIXAMwUdqrvLO\r\n nM/IvFXgGklu6QiuhmOkMwTXlqORbZTX9SR2YNMH/9grw3pUJVnWNxL9aCQM4He8jnb0nygwv2\r\n VUN5e8eEV699f4f5iYMiNyBT0tu3L08BaHapBHZl5yFR7v47MLmCjDtVjOjLvAX5VySa8BZmrj\r\n wOv6VCdX/pD5t7iFFXL8F7Re9kOxUM6VIuAdyJvzDyGPzHSZRfgUnQjo6YSjlE55qu8rLy/WXd\r\n 7vRXunQqn9GUxw32ZC6yxI2KXDtW1wNlI/5bqyHwO+B/gHZUEcDUyb24HZtr5DtvYZOStrxQXd\r\n RyZnkqtW9dU6dBU4N+Kw9KuOfcgLt1FlH//awEyp4+A3ckd0JEp/2BQaYr6I5RsSe0aUWu7W8R\r\n zlHjV2V1N0e1mCvBWWV/uRjYms7DBjDpUgzpULyOL/YWIq8vCgo5MMT5gIrLb6Aau1aE6piOzC\r\n njR3jOEuOutt9fKH8KgjsxLwMM6Mg8j01TF7Lo6VAd0ZHYAG3VkHrJt38XwvHoIuMUu7DuRl+I\r\n q4PJRHtIy4FEdmbuRXdDNyO6mGIz3fcDoyAwhu5w7yR6QcRewyR4A9yAPVtbDUB2y/f6ujsw62\r\n 5cVSGDGj4FuHZmvI+n+pyDT+2ft7mpzF/Dv9vdzS3ihDtVh+6C+ajt2OXJYW6lDVfR224AYm0u\r\n xGPgp8uC77P9LA6lLnbUWIHPlHGTHc6cOVXENWGrr6EdSKfwG2REVzwLL7bVtlg/s2zUX2Uxcj\r\n SzAdyK7I3SoHrd9uQIZaZ9Gdjl7gAOMDCPqxWZ50aE6qkN1qRXEDy3PbzEcdHgLIuBiX5bpUD1\r\n gz0VXAK9b3qYBf6ZD9YKtd6nTBxz+v8GO0n8tqjzsvP8isE6H6jv11N3slGW/L1gJ/FBH5lJkt\r\n MxC1CF1Z2DsjIAc0JGZiagi9ldTy3TQQQcd/A7h5BpgVa3XAU/oUPWXlJ+BnA92A9N1qFwCHZx\r\n htZyX6FC12/m3KSg9Cd+E7J1nl92zENmzz0T25zWhI9OjI+P8fXkdGWOVgSAHujH1/cdGolQAc\r\n 4FHkIiQUswBNgEvkD338gXU95Wi9zJG8vk0G+MAdGQmIie7S4GtOlTvtuVdwH8jp8szkangG1a\r\n Z9CPEvDgT0bOsQk6Vq5BR8zKin/+Gjsx5yOl1OqJnWmWNOhOQTyOut38PIxrGjyL68q3IabcHO\r\n ZUuQxR82wCjQ3VCR+Y2YLsO1clvAujI3IRMmXtL6r8DsUi5ZPpqGooj4EpgmzWl9evIFJVWFwD\r\n 7dKiOIg/vw7Z8EWKY2YpMT5uQk2EBOZL32mvbrYlwC/LdmQW2fItV8k1A1BObLC87EeXVYeAnD\r\n Ks6piMqjWX2fg/RqWB5Kp/urrVlpfUXGNbijhkUT8JzESZBpprrEMvUbKDa4veoDlUxO8hjOjL\r\n LEBPdyzoyfcAOHapf6sg8iHhaFPMzfMfamD+DpLGcAcwtXWR1ZHoRNe4uuxZMQsykA/b6vWRfF\r\n 0bUP5ZQFMA1yNQB8gYuQwRwNdU/vlCuSTxIhfTsyA5qQEfm8yVl70SmtAeAoxkezhtlxpajZM8\r\n R1DdWHz5AQUfmLMT0eMiWbQM2WJ33+4HXqtCWW5+qGWfGI8aH0oiRVxnOWJUlrChvNEypvWJMf\r\n LCtGgrIQnoy0ZBd2HYAX0AsYrVcTWphL+LGcTJFjI7MLNxz8pTjGCMNRLlSxrQTXciDfr6s/Fn\r\n ENlBenhUnGE5RuQ64xx7oih4FD1I5Z1sRA1RJclcBv6LE8qUj8zHcEgq2BV0MR/GVYjNidC6dO\r\n /sYNlzvZWQ2kX0MTycPAw/qyNxn598e4BUdmdcRg806Wz6IZJkqxzeBL1k9fD8jU531Mxyv+wA\r\n wUUfmP3Vkfgb8CWJc6Rul/jGD/wPvjOyyoyJ2YQAAAABJRU5ErkJggg==\r\nCLOUD:admin@fair-coral.maas\r\nEND:VCARD\r\n','Database:admin.vcf',1678356476,'9d26a70cf7994d9788508ecc4607cf21',5669,'admin');
/*!40000 ALTER TABLE `oc_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_cards_properties`
--

DROP TABLE IF EXISTS `oc_cards_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_cards_properties` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `addressbookid` bigint NOT NULL DEFAULT '0',
  `cardid` bigint unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `preferred` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `card_contactid_index` (`cardid`),
  KEY `card_name_index` (`name`),
  KEY `card_value_index` (`value`),
  KEY `cards_prop_abid` (`addressbookid`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_cards_properties`
--

LOCK TABLES `oc_cards_properties` WRITE;
/*!40000 ALTER TABLE `oc_cards_properties` DISABLE KEYS */;
INSERT INTO `oc_cards_properties` VALUES (13,3,3,'UID','testuser2',0),(14,3,3,'FN','testuser2',0),(15,3,3,'N','testuser2;;;;',0),(16,3,3,'CLOUD','testuser2@fair-coral.maas',0),(17,3,2,'UID','testuser',0),(18,3,2,'FN','testuser',0),(19,3,2,'N','testuser;;;;',0),(20,3,2,'CLOUD','testuser@fair-coral.maas',0),(25,3,4,'UID','testuser3',0),(26,3,4,'FN','testuser3',0),(27,3,4,'N','testuser3;;;;',0),(28,3,4,'CLOUD','testuser3@fair-coral.maas',0),(33,3,5,'UID','disableduser',0),(34,3,5,'FN','disableduser',0),(35,3,5,'N','disableduser;;;;',0),(36,3,5,'CLOUD','disableduser@fair-coral.maas',0),(49,3,6,'UID','admin',0),(50,3,6,'FN','admin',0),(51,3,6,'N','admin;;;;',0),(52,3,6,'CLOUD','admin@fair-coral.maas',0),(93,3,1,'UID','vok',0),(94,3,1,'FN','vok',0),(95,3,1,'N','vok;;;;',0),(96,3,1,'CLOUD','vok@fair-coral.maas',0);
/*!40000 ALTER TABLE `oc_cards_properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_collres_accesscache`
--

DROP TABLE IF EXISTS `oc_collres_accesscache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_collres_accesscache` (
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `collection_id` bigint NOT NULL DEFAULT '0',
  `resource_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `resource_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `access` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`user_id`,`collection_id`,`resource_type`,`resource_id`),
  KEY `collres_user_res` (`user_id`,`resource_type`,`resource_id`),
  KEY `collres_user_coll` (`user_id`,`collection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_collres_accesscache`
--

LOCK TABLES `oc_collres_accesscache` WRITE;
/*!40000 ALTER TABLE `oc_collres_accesscache` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_collres_accesscache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_collres_collections`
--

DROP TABLE IF EXISTS `oc_collres_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_collres_collections` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_collres_collections`
--

LOCK TABLES `oc_collres_collections` WRITE;
/*!40000 ALTER TABLE `oc_collres_collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_collres_collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_collres_resources`
--

DROP TABLE IF EXISTS `oc_collres_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_collres_resources` (
  `collection_id` bigint NOT NULL,
  `resource_type` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `resource_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`collection_id`,`resource_type`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_collres_resources`
--

LOCK TABLES `oc_collres_resources` WRITE;
/*!40000 ALTER TABLE `oc_collres_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_collres_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_comments`
--

DROP TABLE IF EXISTS `oc_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_comments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint unsigned NOT NULL DEFAULT '0',
  `topmost_parent_id` bigint unsigned NOT NULL DEFAULT '0',
  `children_count` int unsigned NOT NULL DEFAULT '0',
  `actor_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `actor_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `message` longtext COLLATE utf8mb4_bin,
  `verb` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `creation_timestamp` datetime DEFAULT NULL,
  `latest_child_timestamp` datetime DEFAULT NULL,
  `object_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `object_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `reference_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_parent_id_index` (`parent_id`),
  KEY `comments_topmost_parent_id_idx` (`topmost_parent_id`),
  KEY `comments_object_index` (`object_type`,`object_id`,`creation_timestamp`),
  KEY `comments_actor_index` (`actor_type`,`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_comments`
--

LOCK TABLES `oc_comments` WRITE;
/*!40000 ALTER TABLE `oc_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_comments_read_markers`
--

DROP TABLE IF EXISTS `oc_comments_read_markers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_comments_read_markers` (
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `marker_datetime` datetime DEFAULT NULL,
  `object_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `object_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`,`object_type`,`object_id`),
  KEY `comments_marker_object_index` (`object_type`,`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_comments_read_markers`
--

LOCK TABLES `oc_comments_read_markers` WRITE;
/*!40000 ALTER TABLE `oc_comments_read_markers` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_comments_read_markers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_dav_cal_proxy`
--

DROP TABLE IF EXISTS `oc_dav_cal_proxy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_dav_cal_proxy` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `proxy_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `permissions` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dav_cal_proxy_uidx` (`owner_id`,`proxy_id`,`permissions`),
  KEY `dav_cal_proxy_ioid` (`owner_id`),
  KEY `dav_cal_proxy_ipid` (`proxy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_dav_cal_proxy`
--

LOCK TABLES `oc_dav_cal_proxy` WRITE;
/*!40000 ALTER TABLE `oc_dav_cal_proxy` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_dav_cal_proxy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_dav_shares`
--

DROP TABLE IF EXISTS `oc_dav_shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_dav_shares` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `access` smallint DEFAULT NULL,
  `resourceid` bigint unsigned NOT NULL,
  `publicuri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dav_shares_index` (`principaluri`,`resourceid`,`type`,`publicuri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_dav_shares`
--

LOCK TABLES `oc_dav_shares` WRITE;
/*!40000 ALTER TABLE `oc_dav_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_dav_shares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_direct_edit`
--

DROP TABLE IF EXISTS `oc_direct_edit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_direct_edit` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `editor_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `file_id` bigint NOT NULL,
  `user_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `share_id` bigint DEFAULT NULL,
  `timestamp` bigint unsigned NOT NULL,
  `accessed` tinyint(1) DEFAULT '0',
  `file_path` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4D5AFECA5F37A13B` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_direct_edit`
--

LOCK TABLES `oc_direct_edit` WRITE;
/*!40000 ALTER TABLE `oc_direct_edit` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_direct_edit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_directlink`
--

DROP TABLE IF EXISTS `oc_directlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_directlink` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `file_id` bigint unsigned NOT NULL,
  `token` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `expiration` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `directlink_token_idx` (`token`),
  KEY `directlink_expiration_idx` (`expiration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_directlink`
--

LOCK TABLES `oc_directlink` WRITE;
/*!40000 ALTER TABLE `oc_directlink` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_directlink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_federated_reshares`
--

DROP TABLE IF EXISTS `oc_federated_reshares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_federated_reshares` (
  `share_id` bigint NOT NULL,
  `remote_id` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT 'share ID at the remote server',
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_federated_reshares`
--

LOCK TABLES `oc_federated_reshares` WRITE;
/*!40000 ALTER TABLE `oc_federated_reshares` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_federated_reshares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_file_locks`
--

DROP TABLE IF EXISTS `oc_file_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_file_locks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `lock` int NOT NULL DEFAULT '0',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `ttl` int NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lock_key_index` (`key`),
  KEY `lock_ttl_index` (`ttl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_file_locks`
--

LOCK TABLES `oc_file_locks` WRITE;
/*!40000 ALTER TABLE `oc_file_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_file_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_filecache`
--

DROP TABLE IF EXISTS `oc_filecache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_filecache` (
  `fileid` bigint NOT NULL AUTO_INCREMENT,
  `storage` bigint NOT NULL DEFAULT '0',
  `path` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `path_hash` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `parent` bigint NOT NULL DEFAULT '0',
  `name` varchar(250) COLLATE utf8mb4_bin DEFAULT NULL,
  `mimetype` bigint NOT NULL DEFAULT '0',
  `mimepart` bigint NOT NULL DEFAULT '0',
  `size` bigint NOT NULL DEFAULT '0',
  `mtime` bigint NOT NULL DEFAULT '0',
  `storage_mtime` bigint NOT NULL DEFAULT '0',
  `encrypted` int NOT NULL DEFAULT '0',
  `unencrypted_size` bigint NOT NULL DEFAULT '0',
  `etag` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `permissions` int DEFAULT '0',
  `checksum` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`fileid`),
  UNIQUE KEY `fs_storage_path_hash` (`storage`,`path_hash`),
  KEY `fs_parent_name_hash` (`parent`,`name`),
  KEY `fs_storage_mimetype` (`storage`,`mimetype`),
  KEY `fs_storage_mimepart` (`storage`,`mimepart`),
  KEY `fs_storage_size` (`storage`,`size`,`fileid`),
  KEY `fs_mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=1123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_filecache`
--

LOCK TABLES `oc_filecache` WRITE;
/*!40000 ALTER TABLE `oc_filecache` DISABLE KEYS */;
INSERT INTO `oc_filecache` VALUES (1,1,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,9905229,1681984278,1681984278,0,0,'64410b1667762',23,''),(2,1,'files','45b963397aa40d4a0063e0d85e4fe7a1',1,'files',2,1,9905227,1678356431,1678356431,0,0,'6409afcfb0665',31,''),(3,1,'files/bbw-logo.png','7fb0f51046121e4c805f9aeaa14b3093',2,'bbw-logo.png',4,3,6878,1678194775,1678194775,0,0,'972426fd6bed6b70f53d0d27754adfe9',27,''),(4,1,'files/Photos','d01bb67e7b71dd49fd06bad922f521c9',2,'Photos',2,1,2360011,1678194775,1678194775,0,0,'6407385779d97',31,''),(5,1,'files/Photos/Coast.jpg','a6fe87299d78b207e9b7aba0f0cb8a0a',4,'Coast.jpg',5,3,819766,1678194775,1678194775,0,0,'1596c03ef5ec36933baae96da91cb612',27,''),(6,1,'files/Photos/Hummingbird.jpg','e077463269c404ae0f6f8ea7f2d7a326',4,'Hummingbird.jpg',5,3,585219,1678194775,1678194775,0,0,'d32a8581eec3b7d5b5731d974865aa98',27,''),(7,1,'files/Photos/Nut.jpg','aa8daeb975e1d39412954fd5cd41adb4',4,'Nut.jpg',5,3,955026,1678194775,1678194775,0,0,'256e6896a2f199bbb350840437da5569',27,''),(8,1,'files/Nextcloud.mp4','77a79c09b93e57cba23c11eb0e6048a6',2,'Nextcloud.mp4',7,6,462413,1678194775,1678194775,0,0,'9063e610529ab31e9336f34f622a1517',27,''),(9,1,'files/Nextcloud Manual.pdf','2bc58a43566a8edde804a4a97a9c7469',2,'Nextcloud Manual.pdf',9,8,4540242,1678194775,1678194775,0,0,'109de5b17a51edf1af106c125dfaa018',27,''),(10,1,'files/Documents','0ad78ba05b6961d92f7970b2b3922eca',2,'Documents',2,1,78496,1678194775,1678194775,0,0,'64073857cdc2c',31,''),(11,1,'files/Documents/About.odt','b2ee7d56df9f34a0195d4b611376e885',10,'About.odt',10,8,77422,1678194775,1678194775,0,0,'e912fc845a9fc17c736c911a57984880',27,''),(12,1,'files/Documents/About.txt','9da7b739e7a65d74793b2881da521169',10,'About.txt',12,11,1074,1678194775,1678194775,0,0,'eab457e5fd52af56158222024013d85c',27,''),(13,2,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,8556983,1678956997,1678352236,0,0,'6412d9c5736b6',23,''),(14,2,'appdata_ocp2ck7yo4uj','511e1603a617f66d86c36ae02930e2d1',13,'appdata_ocp2ck7yo4uj',2,1,8556983,1678956997,1678358224,0,0,'6412d9c5736b6',31,''),(15,2,'appdata_ocp2ck7yo4uj/js','493274afae6aa7897fd145345985624b',14,'js',2,1,2662994,1678351427,1678351366,0,0,'64099c4369691',31,''),(16,2,'appdata_ocp2ck7yo4uj/js/core','f14d31b7798843b58fe4e295bde30d57',15,'core',2,1,355676,1678351366,1678351366,0,0,'64099c06dba3f',31,''),(17,2,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js','579d4b039f255abe4dbed89444d56310',16,'merged-template-prepend.js',13,8,148486,1678351188,1678351188,0,0,'e20b2006015acddb2a586fc6dde240ad',27,''),(18,2,'appdata_ocp2ck7yo4uj/preview','b9b868100017c68bd74bd35b4838053c',14,'preview',2,1,4492361,1678956928,1678956927,0,0,'6412d9802992e',31,''),(19,2,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.deps','49447c4a958dfb13087fc02658d2161d',16,'merged-template-prepend.js.deps',14,8,1180,1678351188,1678351188,0,0,'9a7cb82c69ffe85bfeeb7957f9660027',27,''),(20,2,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.gzip','996d183e420339a81267d7d48a42842b',16,'merged-template-prepend.js.gzip',15,8,40626,1678351188,1678351188,0,0,'8316dc498fcf2c472ca491b867a4788a',27,''),(21,2,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js','2d6f839fff5c210df1345235e03f12b7',16,'merged-share-backend.js',13,8,105817,1678351356,1678351356,0,0,'9a1c152a7627532ec07e6796780263bd',27,''),(22,2,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.deps','dced322d42d4338d1a49cb39c9f9e53a',16,'merged-share-backend.js.deps',14,8,692,1678351356,1678351356,0,0,'ac79ea18a2974d2a70f1070f558df70f',27,''),(23,2,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.gzip','64df495640c2992fa06f955533ba6a75',16,'merged-share-backend.js.gzip',15,8,22728,1678351356,1678351356,0,0,'49e8e81b93fd7d61f9247f74b1ee4058',27,''),(24,2,'appdata_ocp2ck7yo4uj/js/core/merged-login.js','1286373dbb5f6e81fb13b4f5c0843496',16,'merged-login.js',13,8,7563,1678351356,1678351356,0,0,'9d10b1932b8ae4f3bec30e57a07cbd7d',27,''),(25,2,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.deps','3b52fc8139d5575cb46dffc48d35120b',16,'merged-login.js.deps',14,8,247,1678351356,1678351356,0,0,'5be01bc7f4dd83f5602cf809aa5b40c0',27,''),(26,2,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.gzip','b654b22875e4d115d122da548cb98865',16,'merged-login.js.gzip',15,8,2276,1678351356,1678351356,0,0,'62121c928d6be1ebfbfd523778ee536d',27,''),(27,2,'appdata_ocp2ck7yo4uj/css','c7ebe2c99c887f1959d7328bdce2dcb8',14,'css',2,1,190031,1678358962,1678352252,0,0,'6409b9b291a9f',31,''),(28,2,'appdata_ocp2ck7yo4uj/css/theming','b7c0b45ef82251f3f095726b639e914b',27,'theming',2,1,1163,1678358268,1678358268,0,0,'6409b6fc9ce89',31,''),(29,2,'appdata_ocp2ck7yo4uj/theming','1a917cff90046d01d35343c92b270d5b',14,'theming',2,1,2705,1678956997,1678358756,0,0,'6412d9c5736b6',31,''),(33,1,'cache','0fea6a13c52b4d4725368f24b045ca84',1,'cache',2,1,0,1681984278,1681984278,0,0,'64410b1662a05',31,''),(34,2,'appdata_ocp2ck7yo4uj/avatar','91d888474bf0e119311f6ea5fc91c88b',14,'avatar',2,1,65286,1678956927,1678353832,0,0,'6412d97f862ff',31,''),(35,2,'appdata_ocp2ck7yo4uj/avatar/admin','1fb69ff79d77a83aba1b7ef55d030cbb',34,'admin',2,1,17066,1678356482,1678356482,0,0,'6409b002e75a2',31,''),(36,2,'appdata_ocp2ck7yo4uj/js/notifications','b1b946b5fe6500cf3bfc4d9ba1503c65',15,'notifications',2,1,26448,1678351364,1678351364,0,0,'64099c04a4d22',31,''),(37,2,'appdata_ocp2ck7yo4uj/js/notifications/merged.js','7f97a755d5335843011a8c28e0dc4578',36,'merged.js',13,8,20888,1678351364,1678351364,0,0,'8cf96d09e60eb13b892ca6ed2e00a8ec',27,''),(38,2,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.deps','0d71ebc93bb58de39f0db5e2fba9c879',36,'merged.js.deps',14,8,306,1678351364,1678351364,0,0,'1c8fb6519db06cd44b07e1174d699d4a',27,''),(39,2,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.gzip','3e6adf3e96590e9411ded0cf69a9cbeb',36,'merged.js.gzip',15,8,5254,1678351364,1678351364,0,0,'ee1349753da533d50688648d51ab0338',27,''),(40,2,'appdata_ocp2ck7yo4uj/js/files','3eb7143fc18697e4e2511eb3adbc241c',15,'files',2,1,420072,1678351364,1678351364,0,0,'64099c04f15e8',31,''),(41,2,'appdata_ocp2ck7yo4uj/js/files/merged-index.js','ea21876dfbd6b920e81f7bc213706376',40,'merged-index.js',13,8,337930,1678351364,1678351364,0,0,'22f43eac4706f53793b6108578982653',27,''),(42,2,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.deps','adfa388cbf45f6b392d7960797e5880e',40,'merged-index.js.deps',14,8,1957,1678351364,1678351364,0,0,'7bdd0aec4eb1ebfc227b7445c0b61f7f',27,''),(43,2,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.gzip','b5b4e4222a3423e2acb28c7699cd86b6',40,'merged-index.js.gzip',15,8,80185,1678351364,1678351364,0,0,'c69e42953b2fef9b2b2481feefd7532f',27,''),(44,2,'appdata_ocp2ck7yo4uj/js/activity','a9f39bf5207544df2d5b895a84618a1e',15,'activity',2,1,21189,1678351365,1678351365,0,0,'64099c053c4df',31,''),(45,2,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js','5b53ceef539881b19ca36f7752013cfb',44,'activity-sidebar.js',13,8,16380,1678351365,1678351365,0,0,'4718ff67f87346fdd6447587ff498ea3',27,''),(46,2,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.deps','fb3cfe9931d0a1786203ab7ccddec138',44,'activity-sidebar.js.deps',14,8,458,1678351365,1678351365,0,0,'e83fd081d410c9077b2f1502f36e8dd6',27,''),(47,2,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.gzip','ab64a435f0684089954a1503a4915c2f',44,'activity-sidebar.js.gzip',15,8,4351,1678351365,1678351365,0,0,'13539329f95ddac9d0edb616efd9f4b5',27,''),(48,2,'appdata_ocp2ck7yo4uj/js/comments','e4648049e6995efacee6f80096521231',15,'comments',2,1,80480,1678351365,1678351365,0,0,'64099c057ed18',31,''),(49,2,'appdata_ocp2ck7yo4uj/js/comments/merged.js','406aebeee235fd67c35f4ebeb199dfe9',48,'merged.js',13,8,62511,1678351365,1678351365,0,0,'122e12d85b1fbf617030560bc635b8e4',27,''),(50,2,'appdata_ocp2ck7yo4uj/js/comments/merged.js.deps','18af4dd92bc976d39f038edb58b42c61',48,'merged.js.deps',14,8,788,1678351365,1678351365,0,0,'3d760584163ba5053579614f47c76a83',27,''),(51,2,'appdata_ocp2ck7yo4uj/js/comments/merged.js.gzip','0647ad3df0fb06da8883d8d7cb01b546',48,'merged.js.gzip',15,8,17181,1678351365,1678351365,0,0,'fd8eb6aef0d23660b35cdcb35fba4328',27,''),(52,2,'appdata_ocp2ck7yo4uj/js/files_sharing','7676e75f6ea45d19eb1827a880982349',15,'files_sharing',2,1,19382,1678351365,1678351365,0,0,'64099c05ba5f2',31,''),(53,2,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js','253b8ab876fa052002ac07f2240a1656',52,'additionalScripts.js',13,8,14539,1678351365,1678351365,0,0,'bd836a3b3a58ca8d781327c3b7f0f66d',27,''),(54,2,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.deps','342d284054df0c2482eac265157e4431',52,'additionalScripts.js.deps',14,8,316,1678351365,1678351365,0,0,'0678d24e0e8237781227d07aed31daea',27,''),(55,2,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.gzip','51d06c1265f8fff99a85a72ac1cf63d4',52,'additionalScripts.js.gzip',15,8,4527,1678351365,1678351365,0,0,'141f6968a945046b63653e36a05fb3e1',27,''),(56,2,'appdata_ocp2ck7yo4uj/js/files_texteditor','8e34386727f9050bc69e0087ca33fd6a',15,'files_texteditor',2,1,839303,1678351366,1678351365,0,0,'64099c061f9ab',31,''),(57,2,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js','ee0652870e1e2acecbadd6d595ab596e',56,'merged.js',13,8,699810,1678351365,1678351365,0,0,'369a0fb2e75e06a8a21c0914db0dff31',27,''),(58,2,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.deps','000a73daa8b6451f2642c89504a07157',56,'merged.js.deps',14,8,346,1678351365,1678351365,0,0,'56323a79016a7f0f216f3baaf970c033',27,''),(59,2,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.gzip','f23ada379b79c7c7cab74dbb34067beb',56,'merged.js.gzip',15,8,139147,1678351366,1678351366,0,0,'de8a5530286366639ae84f30adfff4af',27,''),(60,2,'appdata_ocp2ck7yo4uj/js/files_versions','80e7253edd9ea7831366f098c11d692a',15,'files_versions',2,1,16695,1678351366,1678351366,0,0,'64099c065e126',31,''),(61,2,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js','9f3977c48c9a7a5bee51eb406a30ab06',60,'merged.js',13,8,12719,1678351366,1678351366,0,0,'ee1cdc4736f00a0cf5e8e4f0d8450b6d',27,''),(62,2,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.deps','dde041654f1d7b4eebc3a5c5e5c1d384',60,'merged.js.deps',14,8,394,1678351366,1678351366,0,0,'17a5e750f416f979e1d92c79158e0127',27,''),(63,2,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.gzip','ceff3f6abe8b8b5f8268c4188be76a80',60,'merged.js.gzip',15,8,3582,1678351366,1678351366,0,0,'698c0dc68028b1310a5358647dc0d734',27,''),(64,2,'appdata_ocp2ck7yo4uj/js/gallery','0868b0a242e983dabdfec4727a60e7b9',15,'gallery',2,1,859739,1678351427,1678351427,0,0,'64099c4369691',31,''),(65,2,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js','f82849093986c955349f665498cef5fa',64,'scripts-for-file-app.js',13,8,227059,1678351366,1678351366,0,0,'76f83fd272da623f4ac3c36bca2e71fc',27,''),(66,2,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.deps','8cee64de4107b037e19a6e0001274792',64,'scripts-for-file-app.js.deps',14,8,796,1678351366,1678351366,0,0,'46be51c28a2e30d78bbd262e605b4bd1',27,''),(67,2,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.gzip','4aa0f263d2fc5e9829ce1349ab3e2fbd',64,'scripts-for-file-app.js.gzip',15,8,54659,1678351366,1678351366,0,0,'0bc9932bfcabf2786a450fe4bf675522',27,''),(68,2,'appdata_ocp2ck7yo4uj/js/core/merged.js','bf362a7fdbb6f0c38deadb41e68acde7',16,'merged.js',13,8,20224,1678351366,1678351366,0,0,'8f708cd26297318c05f38ee1432c098f',27,''),(69,2,'appdata_ocp2ck7yo4uj/js/core/merged.js.deps','0672ccdef2d9384a411dd0895964d57b',16,'merged.js.deps',14,8,472,1678351366,1678351366,0,0,'87afe98da0cc24906db3f0f304831173',27,''),(70,2,'appdata_ocp2ck7yo4uj/js/core/merged.js.gzip','3e19032edbb0a5d54a4d25606c53c3c8',16,'merged.js.gzip',15,8,5365,1678351366,1678351366,0,0,'31ef8556e4269056493caad90755405d',27,''),(71,2,'appdata_ocp2ck7yo4uj/js/systemtags','ee888bbc00ce11acc6ae76273ea221c3',15,'systemtags',2,1,24010,1678351367,1678351367,0,0,'64099c0723690',31,''),(72,2,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js','01377a4b3140da5070416af63e7d8c4c',71,'merged.js',13,8,18223,1678351367,1678351367,0,0,'17d1a37a2da2319ff75fabcb9db66266',27,''),(73,2,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.deps','b0e572499441370a48b9f9765d801f8e',71,'merged.js.deps',14,8,459,1678351367,1678351367,0,0,'6834d82ce7962b4188232fdf05b02946',27,''),(74,2,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.gzip','d8d4c0de28d2f5c53f1f945631fa5152',71,'merged.js.gzip',15,8,5328,1678351367,1678351367,0,0,'45b38bf400228f2a878747907268a181',27,''),(75,2,'appdata_ocp2ck7yo4uj/css/core','e5da66b8fc55196b7f2a80eba5b06ea6',27,'core',2,1,129494,1678358701,1678358701,0,0,'6409b8ad65b48',31,''),(88,2,'appdata_ocp2ck7yo4uj/css/files','c22100d5f13d55aac69ac2c832874652',27,'files',2,1,27898,1678358962,1678358962,0,0,'6409b9b291a9f',31,''),(92,2,'appdata_ocp2ck7yo4uj/css/files_trashbin','5f32bdfb01ceaf60105bd7ab5ef25a2d',27,'files_trashbin',2,1,642,1678358700,1678358700,0,0,'6409b8ac59fd9',31,''),(96,2,'appdata_ocp2ck7yo4uj/css/comments','19602c40a64f0290173a3ebf1dfa08f6',27,'comments',2,1,1523,1678358700,1678358700,0,0,'6409b8ac95c26',31,''),(100,2,'appdata_ocp2ck7yo4uj/css/files_sharing','80ca69a7ea85a44940833be82f45fe5e',27,'files_sharing',2,1,3412,1678358700,1678358700,0,0,'6409b8acd76fc',31,''),(104,2,'appdata_ocp2ck7yo4uj/css/files_texteditor','4ebecdcef2c2d8caa84532be9b7f3701',27,'files_texteditor',2,1,5799,1678358701,1678358701,0,0,'6409b8ad2a82e',31,''),(114,2,'appdata_ocp2ck7yo4uj/preview/3','cc5565c4ba6fd56871cbbe502dfe977c',18,'3',2,1,13632,1678356467,1678356467,0,0,'6409aff3ad578',31,''),(116,2,'appdata_ocp2ck7yo4uj/preview/3/193-96-max.png','56da6505a652ce6ec813851f00831d3a',114,'193-96-max.png',4,3,6148,1678351374,1678351374,0,0,'85be394f5f032347d6e8027ac9701705',27,''),(118,2,'appdata_ocp2ck7yo4uj/preview/3/64-64-crop.png','2beb2205135ee5e848582beea9a83e2a',114,'64-64-crop.png',4,3,2669,1678351374,1678351374,0,0,'4f8f1a105e6a031415e761f3db696025',27,''),(119,2,'appdata_ocp2ck7yo4uj/preview/5','eaca83211066f046a8313d0843f8f854',18,'5',2,1,335671,1678351426,1678351426,0,0,'64099c42513af',31,''),(120,2,'appdata_ocp2ck7yo4uj/preview/6','825339ba134f3c814c79a812ec5d826a',18,'6',2,1,595528,1678351426,1678351426,0,0,'64099c42646ed',31,''),(121,2,'appdata_ocp2ck7yo4uj/preview/7','803e2a3dffd0cdf960192b097016e362',18,'7',2,1,815617,1678351426,1678351426,0,0,'64099c424b3f3',31,''),(122,2,'appdata_ocp2ck7yo4uj/preview/5/1100-734-max.jpg','9263bb56a9b788110fa9893a71d48566',119,'1100-734-max.jpg',5,3,308351,1678351398,1678351398,0,0,'c606799c80a202f0f774971650051873',27,''),(123,2,'appdata_ocp2ck7yo4uj/preview/6/2000-1333-max.jpg','f934b86c92bf5aa5b42031f2f87f5ad4',120,'2000-1333-max.jpg',5,3,570843,1678351398,1678351398,0,0,'1c50de7acc3f8a946f9ec940cf6dd9cc',27,''),(124,2,'appdata_ocp2ck7yo4uj/preview/7/2000-1333-max.jpg','0be546cd23b5453d595aa086facd9409',121,'2000-1333-max.jpg',5,3,789147,1678351398,1678351398,0,0,'31df4942ffe9736d2ddaa36eac790133',27,''),(125,2,'appdata_ocp2ck7yo4uj/preview/5/64-64-crop.jpg','0540a52b35796b32c2efc19f39d3dbb0',119,'64-64-crop.jpg',5,3,2543,1678351398,1678351398,0,0,'97e4dc1325e79f8852a7f70e308315cf',27,''),(126,2,'appdata_ocp2ck7yo4uj/preview/6/64-64-crop.jpg','ae9b59d54212644239c4b8823bee2d1d',120,'64-64-crop.jpg',5,3,3420,1678351398,1678351398,0,0,'c2f920c346bccaea075030e977ba833b',27,''),(127,2,'appdata_ocp2ck7yo4uj/preview/7/64-64-crop.jpg','7a7f631972b2911536a530b3786f2f39',121,'64-64-crop.jpg',5,3,2929,1678351398,1678351398,0,0,'8510b854f355bf2be16e4ce4b0e087ca',27,''),(130,2,'appdata_ocp2ck7yo4uj/preview/12','a2d7ac5cb617daa0d638f3fa78cbbd51',18,'12',2,1,180145,1678351426,1678351426,0,0,'64099c42a29ef',31,''),(131,2,'appdata_ocp2ck7yo4uj/preview/12/4096-4096-max.png','96a2825d9dd0f53d9edd6e78d4b13f59',130,'4096-4096-max.png',4,3,153221,1678351406,1678351406,0,0,'d772fd1734b86350af642f60a785d9d3',27,''),(132,2,'appdata_ocp2ck7yo4uj/preview/12/64-64-crop.png','ad96ec8ee7f3c392ed81f564e110fd6f',130,'64-64-crop.png',4,3,4748,1678351406,1678351406,0,0,'1865a84c6e496606731b41df3ef8798a',27,''),(133,2,'appdata_ocp2ck7yo4uj/preview/7/256-256-crop.jpg','21fa823be8e84c0851bf52f9808645eb',121,'256-256-crop.jpg',5,3,23541,1678351426,1678351426,0,0,'eea70dab3e79892caed35a841c71912e',27,''),(134,2,'appdata_ocp2ck7yo4uj/preview/5/256-256-crop.jpg','856c67cb75dbd07fb50abeace7c52b97',119,'256-256-crop.jpg',5,3,24777,1678351426,1678351426,0,0,'b87caf0df726e15d976653b703e04a16',27,''),(135,2,'appdata_ocp2ck7yo4uj/preview/6/256-256-crop.jpg','3d70421864eda45bdf9ca5029d09ff9c',120,'256-256-crop.jpg',5,3,21265,1678351426,1678351426,0,0,'ff1a9a8327d68494833cd665492cfeda',27,''),(136,2,'appdata_ocp2ck7yo4uj/preview/3/96-96-crop.png','bbd949a06059ebc98eab286a0a5e0f3d',114,'96-96-crop.png',4,3,3679,1678351426,1678351426,0,0,'e8608f721186d203a97d3f61b967a7de',27,''),(137,2,'appdata_ocp2ck7yo4uj/preview/12/256-256-crop.png','80986e4e6dbfe180dd06dfd6de914e1e',130,'256-256-crop.png',4,3,22176,1678351426,1678351426,0,0,'cd89db7343678c936555931f449c8362',27,''),(138,2,'appdata_ocp2ck7yo4uj/js/gallery/merged.js','fdc2b62e3b85db6ebc8c9fd2be985c0e',64,'merged.js',13,8,451474,1678351427,1678351427,0,0,'de319d4155b06f5e478f7f3904d0e320',27,''),(139,2,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.deps','195df1a1ff873a95bbbe8f3accf90d45',64,'merged.js.deps',14,8,2069,1678351427,1678351427,0,0,'f9c929e4e1e16278d0e3f7075470a4a6',27,''),(140,2,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.gzip','86129e9991d4a6f79317be5e30ec6f3b',64,'merged.js.gzip',15,8,123682,1678351427,1678351427,0,0,'db78c615decb6a6b11c1fc481314cb05',27,''),(144,1,'files/Test','56c303da11d07c61688947bb3296276a',2,'Test',2,1,2190811,1678351647,1678351647,0,0,'64099d1feb055',31,''),(145,1,'files/Test/10.1_ContainerVirtualisierung.pdf','d865080338acdc012d25bb242b7b25ea',144,'10.1_ContainerVirtualisierung.pdf',9,8,110769,1678351646,1678351646,0,0,'ef0d11062b0789404dafe0bb6aa6bbb6',27,''),(146,1,'files/Test/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf','81af18266b5aec175e8251f990177f32',144,'10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf',9,8,307530,1678351646,1678351646,0,0,'9cce7ce6a8e45186fd51be7ce262fc88',27,''),(147,1,'files/Test/10.3_Container_oder_VM_Pros_und_Kontras.pdf','77f9d70222dbb272c1062128f5a7ef58',144,'10.3_Container_oder_VM_Pros_und_Kontras.pdf',9,8,1266125,1678351647,1678351647,0,0,'c7ee593e13da6558f8b0ad2d7b3cd818',27,''),(148,1,'files/Test/10.4_CV_Versus_VM.docx','66fe42e6e6d9b94bd7393f4d15c691df',144,'10.4_CV_Versus_VM.docx',18,8,47076,1678351647,1678351647,0,0,'6630f112a5192a3cd1ac9b78cc6ac33c',27,''),(149,1,'files/Test/10.5_Docker Einstieg - CLI-Befehle.pdf','f8d47cef2d136cd900b087ed86528ff6',144,'10.5_Docker Einstieg - CLI-Befehle.pdf',9,8,458836,1678351647,1678351647,0,0,'8b5a443fd1b61f6b17b6ee4e0ea8df36',27,''),(150,1,'files/Test/docker-compose.yml','8592a5cf0fcd129b3e6b5dad6b57afbc',144,'docker-compose.yml',19,8,475,1678351647,1678351647,0,0,'c66811eb4d0f2288d2987a6d8256c19e',27,''),(152,1,'files/Testfile.txt','1724763b6e378096e6bc77a84f84ba06',2,'Testfile.txt',12,11,9,1678352141,1678352141,0,0,'09a1c99817b4fe994cb78e50d1e69b79',27,''),(154,1,'files_versions','9692aae50022f45f1098646939b287b1',1,'files_versions',2,1,1,1678352141,1678352141,0,0,'64099f0d12f5c',31,''),(155,1,'files_versions/Testfile.txt.v1678352135','66b38ad6af1c8f42b26dd08dbf78ab16',154,'Testfile.txt.v1678352135',12,11,1,1678352141,1678352141,0,0,'af59dd808d2376ea3f1183e9ee07e5c6',27,''),(156,2,'appdata_ocp2ck7yo4uj/preview/152','d3564f308ae3aa82990f73f8a55fd41e',18,'152',2,1,30985,1678457879,1678457879,0,0,'640b3c1782aa1',31,''),(157,2,'appdata_ocp2ck7yo4uj/preview/152/4096-4096-max.png','e993f925a7782bf3037be3a606399ccd',156,'4096-4096-max.png',4,3,26048,1678352141,1678352141,0,0,'146fbdac137757811dc6e041952930e2',27,''),(158,2,'appdata_ocp2ck7yo4uj/preview/152/64-64-crop.png','082f6736b1f106ad596c51a2489663b6',156,'64-64-crop.png',4,3,644,1678352141,1678352141,0,0,'313910f831e5d23d495ae3e97f2bd82a',27,''),(159,1,'files/m129_bandbreitenberechnung.pdf','9bb0b1c54fe00704b9d0ac508976895e',2,'m129_bandbreitenberechnung.pdf',9,8,161663,1678352217,1678352217,0,0,'cb458eae1a9bc6e28e99197151978050',27,''),(160,1,'files/m129_bandbreitenberechnung-lsg.pdf','df131c44fd2d73cd29505fda7a3524d1',2,'m129_bandbreitenberechnung-lsg.pdf',9,8,104704,1678352218,1678352218,0,0,'f6650da8a37a9e13191aa18163bcab58',27,''),(161,2,'files_external','c270928b685e7946199afdfb898d27ea',13,'files_external',2,1,0,1678352236,1678352236,0,0,'64099f6cef560',31,''),(162,2,'appdata_ocp2ck7yo4uj/preview/152/256-256-crop.png','e43ed5fc69ef670c38697cdb7d776f79',156,'256-256-crop.png',4,3,3896,1678352237,1678352237,0,0,'4fcdfecf7258cfc92990beecde404732',27,''),(163,2,'appdata_ocp2ck7yo4uj/appstore','2345d9a5592e7a2e8d79bf2223c8e348',14,'appstore',2,1,1087363,1678457923,1678352241,0,0,'640b3c4398298',31,''),(164,2,'appdata_ocp2ck7yo4uj/appstore/apps.json','aea0a87bc9c12b12737ee801593ccba8',163,'apps.json',20,8,1087363,1678457923,1678457923,0,0,'32572096144682a63fba401729a04653',27,''),(165,2,'appdata_ocp2ck7yo4uj/css/settings','2fd09612cf55a6d85d4729e9fa0f7a9e',27,'settings',2,1,20100,1678358698,1678358698,0,0,'6409b8aa3a871',31,''),(169,2,'appdata_ocp2ck7yo4uj/avatar/vok','0a0486437d7af5fe0661d5870080b6fe',34,'vok',2,1,17536,1678358217,1678358217,0,0,'6409b6c958fcc',31,''),(170,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.png','1c5e17f3562bf0026ff2433bc8359af1',169,'avatar.png',4,3,14672,1678352368,1678352368,0,0,'5e7317446f7ca30a43bf906dc382830a',27,''),(171,2,'appdata_ocp2ck7yo4uj/avatar/vok/generated','92d0578812637bcd106a74c51bd2c0cd',169,'generated',14,8,0,1678352368,1678352368,0,0,'c81b737665ebd870f489378c561e3907',27,''),(172,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.40.png','d709c254c65347b092c445a6e5dbaaa3',169,'avatar.40.png',4,3,458,1678352369,1678352369,0,0,'8e687f814173036489e31beeb8b9cfbf',27,''),(173,2,'appdata_ocp2ck7yo4uj/avatar/testuser','35512c74e396de5a72f211830497ab48',34,'testuser',2,1,5695,1678352389,1678352389,0,0,'6409a00556fe5',31,''),(174,2,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.png','344c673f2655b9feace05731093b597a',173,'avatar.png',4,3,5477,1678352388,1678352388,0,0,'9cc3a2c1253c5445587071e8ef3c7b04',27,''),(175,2,'appdata_ocp2ck7yo4uj/avatar/testuser/generated','62fd23db4be5617c3426ec24aa711f91',173,'generated',14,8,0,1678352388,1678352388,0,0,'3f5bed1bb547512efbab8bd8df4700f5',27,''),(176,2,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.40.png','1165c2f5e3965d783a95654d53e4147c',173,'avatar.40.png',4,3,218,1678352389,1678352389,0,0,'8b000c6e9818e4478cd5a842b0da47f3',27,''),(177,2,'appdata_ocp2ck7yo4uj/avatar/testuser2','638d7e7ca3dce9c416dada796c49c1ed',34,'testuser2',2,1,5910,1678956927,1678956927,0,0,'6412d97f862ff',31,''),(178,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.png','a3783e8054fcad0ccf5744275f000992',177,'avatar.png',4,3,5487,1678352409,1678352409,0,0,'e0a7231e74cd76ebaee9b1a9ad8080fe',27,''),(179,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/generated','27d667766eef561885e247be37aeaa76',177,'generated',14,8,0,1678352409,1678352409,0,0,'6eb1a2c062b1ee736c393328bd7e2652',27,''),(180,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.40.png','8a34e8e5a8ce95f5ead5eddf1e5abfe2',177,'avatar.40.png',4,3,218,1678352410,1678352410,0,0,'0dbfe30f10ecba1c7b3fd7280cd689db',27,''),(181,2,'appdata_ocp2ck7yo4uj/dav-photocache','d30c65c6c0c584cdd932ec953747fc71',14,'dav-photocache',2,1,52222,1678457885,1678457885,0,0,'640b3c1d3642f',31,''),(184,2,'appdata_ocp2ck7yo4uj/avatar/testuser3','d086ba37569c9161e11461068373ee41',34,'testuser3',2,1,5706,1678353724,1678353724,0,0,'6409a53c42b97',31,''),(185,2,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.png','05d1d04fde756a5ccfe6c591210c7cf1',184,'avatar.png',4,3,5487,1678353723,1678353723,0,0,'7fdc7822a2babcb7b5c1e072931a12bd',27,''),(186,2,'appdata_ocp2ck7yo4uj/avatar/testuser3/generated','c68c06a199b0826020e178163f42cf75',184,'generated',14,8,0,1678353723,1678353723,0,0,'7bc0c312f74c568932c106817e7ff121',27,''),(187,2,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.40.png','ccce2d61522438868b4e0d107682f425',184,'avatar.40.png',4,3,219,1678353724,1678353724,0,0,'1fef0d8a25d0319b97e43a0744bd465e',27,''),(189,2,'appdata_ocp2ck7yo4uj/avatar/disableduser','6830f17f340ba5039fbbae652584970f',34,'disableduser',2,1,13373,1678353833,1678353833,0,0,'6409a5a947d3d',31,''),(190,2,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.png','ecb540d8c6611cb24a2fe7a69867fb31',189,'avatar.png',4,3,12924,1678353832,1678353832,0,0,'95abc1852407b4700b14e866e530d950',27,''),(191,2,'appdata_ocp2ck7yo4uj/avatar/disableduser/generated','0b4666d8647679ba143b11759a36c07f',189,'generated',14,8,0,1678353832,1678353832,0,0,'7f35922cb7556e35a6c50b33a8f1d1dc',27,''),(192,2,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.40.png','d6e11c84318f70f5e74c5d1ec67c1a61',189,'avatar.40.png',4,3,449,1678353833,1678353833,0,0,'b3f2e2f5f3350e540db4e25f37430a76',27,''),(193,1,'files_trashbin/files/deletedfile.txt.d1678356431','98c2b27aefac656d26d415cde28742e0',196,'deletedfile.txt.d1678356431',14,8,1,1678356423,1678356423,0,0,'d80f2479b1f5e9995f04a51b09a72b83',27,''),(195,1,'files_trashbin','fb66dca5f27af6f15c1d1d81e6f8d28b',1,'files_trashbin',2,1,1,1678356431,1678356431,0,0,'6409afcfa757f',31,''),(196,1,'files_trashbin/files','3014a771cbe30761f2e9ff3272110dbf',195,'files',2,1,1,1678356431,1678356431,0,0,'6409afcfa757f',31,''),(197,1,'files_trashbin/versions','c639d144d3f1014051e14a98beac5705',195,'versions',2,1,0,1678356431,1678356431,0,0,'6409afcf89a4d',31,''),(198,1,'files_trashbin/keys','9195c7cfc1b867f229ac78cc1b6a0be3',195,'keys',2,1,0,1678356431,1678356431,0,0,'6409afcf914de',31,''),(199,2,'appdata_ocp2ck7yo4uj/preview/193','461e154eff9f19485709a164ac7546df',18,'193',2,1,0,1678356445,1678356445,0,0,'6409afdda426f',31,''),(200,2,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de','bbbf0016034ecc5a6096c8b52da6b8b7',181,'7641eb079d4bafa5e2024031c65e28de',2,1,6925,1678356448,1678356448,0,0,'6409afe0530f7',31,''),(201,2,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.png','75e1798c3f63cfc444cd65c58a550396',200,'photo.png',4,3,6662,1678356448,1678356448,0,0,'20c33d2234ea37d72ca38bf3111dfe76',27,''),(204,2,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f','23789bb835b044e302311e35484613a6',181,'7a6e71f6fabbda1da4a96ba84be4329f',2,1,6927,1678356448,1678356448,0,0,'6409afe059ab6',31,''),(205,2,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.png','695384511c2fa6076a55b047624a7fd9',204,'photo.png',4,3,6661,1678356448,1678356448,0,0,'64e9a293d835c49190e4719a0f89ecdc',27,''),(206,2,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766','1619c8ef2c9355e72c8b5bf03aeaeecc',181,'274857af206af2cd22deae7c56f80766',2,1,6922,1678356448,1678356448,0,0,'6409afe0650a2',31,''),(207,2,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.png','c22bc875ab14a2f3b737b1c87bec25df',206,'photo.png',4,3,6658,1678356448,1678356448,0,0,'2eed36fe45b22d01eada2811cd0b3633',27,''),(208,2,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.32.png','6141d37c71dd6fb7888d80b871c78c13',200,'photo.32.png',4,3,263,1678356448,1678356448,0,0,'412f1b12bd00165d56a0166429749d8b',27,''),(210,2,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.32.png','c5c4f3df014addc24a28b254941e8b46',204,'photo.32.png',4,3,266,1678356448,1678356448,0,0,'59b2f19cea4d45c9fae129bffaf31e7c',27,''),(211,2,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72','141f595693cdf6f12110583a7998ba56',181,'7f3e57a2397831bc58cb185e0becdc72',2,1,14912,1678356448,1678356448,0,0,'6409afe07f443',31,''),(212,2,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.32.png','cc6bc3653657a63f28ac9ad9968055e1',206,'photo.32.png',4,3,264,1678356448,1678356448,0,0,'972a572b0a5c72e1b32560c6a283d9d5',27,''),(213,2,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.png','6a4bff6c7c59d57e1e88f93db2059b40',211,'photo.png',4,3,14490,1678356448,1678356448,0,0,'3943cdc30ab2f320c4ebd9b4ea9f5da4',27,''),(214,2,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.32.png','a21e27f03088f448ee3fcd45b61158d5',211,'photo.32.png',4,3,422,1678356448,1678356448,0,0,'adfa6ccef4fa3c0ff9d67fe43ee378e4',27,''),(225,2,'appdata_ocp2ck7yo4uj/preview/3/32-32-crop.png','089ced233440fe37d36fb62dac5497c0',114,'32-32-crop.png',4,3,1136,1678356467,1678356467,0,0,'0d3d6dd76c6506db30276aae0b8e615a',27,''),(227,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.png','d8dd45159872dc1cb96d8f1d5e3764d6',35,'avatar.png',4,3,3958,1678356476,1678356476,0,0,'18be4d8cd34807df71d8440b263d09ca',27,''),(231,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.40.png','4519fa97dd131e66e97ae8eb7202e4d6',35,'avatar.40.png',4,3,1464,1678356477,1678356477,0,0,'1931144bcedc5df2a494283f1886b4a9',27,''),(232,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.182.png','392718da6f4eea3f15b499074c74f5ea',35,'avatar.182.png',4,3,10549,1678356477,1678356477,0,0,'ea78b9fd42a93d007809e5e04622e552',27,''),(233,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.32.png','f091d9d8c6eb1c6f6f27a4b1b4a29b70',35,'avatar.32.png',4,3,1095,1678356482,1678356482,0,0,'2f4ac614d8ca044c8bf1f3ff25167c82',27,''),(234,7,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,8717771,1678359119,1678358159,0,0,'6409ba4f48a7c',23,''),(235,7,'cache','0fea6a13c52b4d4725368f24b045ca84',234,'cache',2,1,0,1678358158,1678358158,0,0,'6409b68ee3f7d',31,''),(236,7,'files','45b963397aa40d4a0063e0d85e4fe7a1',234,'files',2,1,8717771,1678359119,1678359111,0,0,'6409ba4f48a7c',31,''),(237,7,'files/bbw-logo.png','7fb0f51046121e4c805f9aeaa14b3093',236,'bbw-logo.png',4,3,6878,1678358159,1678358159,0,0,'484ee96f0c045bb6f0941c216056ec31',27,''),(238,7,'files/Photos','d01bb67e7b71dd49fd06bad922f521c9',236,'Photos',2,1,2360011,1678358159,1678358159,0,0,'6409b68f7a659',31,''),(239,7,'files/Photos/Coast.jpg','a6fe87299d78b207e9b7aba0f0cb8a0a',238,'Coast.jpg',5,3,819766,1678358159,1678358159,0,0,'7f07cfa245d7a2c4a09896aaa4a78c9f',27,''),(240,7,'files/Photos/Hummingbird.jpg','e077463269c404ae0f6f8ea7f2d7a326',238,'Hummingbird.jpg',5,3,585219,1678358159,1678358159,0,0,'8ef6f24b2e602c7a4555de6f20dfa173',27,''),(241,7,'files/Photos/Nut.jpg','aa8daeb975e1d39412954fd5cd41adb4',238,'Nut.jpg',5,3,955026,1678358159,1678358159,0,0,'631741074b2a805c25deeb198b968d6e',27,''),(242,7,'files/Nextcloud.mp4','77a79c09b93e57cba23c11eb0e6048a6',236,'Nextcloud.mp4',7,6,462413,1678358159,1678358159,0,0,'300c4d282e4b4eb0a8922a1907c7f715',27,''),(243,7,'files/Nextcloud Manual.pdf','2bc58a43566a8edde804a4a97a9c7469',236,'Nextcloud Manual.pdf',9,8,4540242,1678358159,1678358159,0,0,'b29a2e654ce8ea8ecd32fd60efe5c589',27,''),(244,7,'files/Documents','0ad78ba05b6961d92f7970b2b3922eca',236,'Documents',2,1,78496,1678358160,1678358160,0,0,'6409b6900d783',31,''),(245,7,'files/Documents/About.odt','b2ee7d56df9f34a0195d4b611376e885',244,'About.odt',10,8,77422,1678358159,1678358159,0,0,'3d88bfa35567e4ce4dfd645816333ef9',27,''),(246,7,'files/Documents/About.txt','9da7b739e7a65d74793b2881da521169',244,'About.txt',12,11,1074,1678358160,1678358160,0,0,'04562c6ec48108348c279641cab91d36',27,''),(247,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.32.png','19097930e38bfc9c4f8bd42e72127c39',169,'avatar.32.png',4,3,391,1678358160,1678358160,0,0,'e71517f26ce97e1fe56fce127697f219',27,''),(248,2,'appdata_ocp2ck7yo4uj/preview/237','2659a3cdbcb2a96b3b3cdbf0a5df6f7f',18,'237',2,1,12496,1678358965,1678358965,0,0,'6409b9b5d5cf5',31,''),(249,2,'appdata_ocp2ck7yo4uj/preview/237/193-96-max.png','50d08586a69e0184430ad8cc773a0c95',248,'193-96-max.png',4,3,6148,1678358161,1678358161,0,0,'51266f6d1ca3c428dde8312651da4bfa',27,''),(250,2,'appdata_ocp2ck7yo4uj/preview/237/64-64-crop.png','9368924db841411cae5010b37e353748',248,'64-64-crop.png',4,3,2669,1678358161,1678358161,0,0,'69da871175186bac898e8407633148ed',27,''),(251,7,'files/testfile.txt','cd075662fbab9b754c348f29c20c8571',236,'testfile.txt',12,11,1,1678358180,1678358180,0,0,'09fa80b27a41f88d6b8bb46916c0867d',27,''),(252,2,'appdata_ocp2ck7yo4uj/preview/251','decab0cb33d2a303801bc42cf78f89cb',18,'251',2,1,0,1678358181,1678358181,0,0,'6409b6a553a6b',31,''),(253,7,'files/testfile2.txt','c5f6c5296e18f9ced7fb289925ff0752',236,'testfile2.txt',12,11,1,1678358187,1678358187,0,0,'bf8c980b853092c2fdeadce01522cb85',27,''),(254,2,'appdata_ocp2ck7yo4uj/preview/253','5c622a885af00ec11d18377abac472d4',18,'253',2,1,0,1678358188,1678358188,0,0,'6409b6ac11cf6',31,''),(255,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.182.png','9bee4d8d9dc2e2f87b9f92344499726a',169,'avatar.182.png',4,3,2015,1678358217,1678358217,0,0,'518d9b3db9955bbe3eb6aa72ea8cefb8',27,''),(256,2,'appdata_ocp2ck7yo4uj/identityproof','2d4281eebef45968390785218dc56c63',14,'identityproof',2,1,4021,1678358224,1678358224,0,0,'6409b6d0bccff',31,''),(257,2,'appdata_ocp2ck7yo4uj/identityproof/user-vok','645643f49f95bb6a22d54366a668d7a2',256,'user-vok',2,1,4021,1678358224,1678358224,0,0,'6409b6d0bccff',31,''),(258,2,'appdata_ocp2ck7yo4uj/identityproof/user-vok/private','0eb8ad1208a413e19deb14433329e20e',257,'private',14,8,3570,1678358224,1678358224,0,0,'9502d6278776d5ced454aeedaaaaf01c',27,''),(259,2,'appdata_ocp2ck7yo4uj/identityproof/user-vok/public','a004b7c46da2c2254f89de1e50172352',257,'public',14,8,451,1678358224,1678358224,0,0,'02fbce812942da9a85f811c95e10b243',27,''),(268,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css','78bff418cb5b8be898905e26105f439d',75,'89a5-dd39-server.css',16,11,100134,1678358268,1678358268,0,0,'586be42b71e881e76f165e342ed84c27',27,''),(269,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.deps','baa462ce2528e7565d9e8e94861d584e',75,'89a5-dd39-server.css.deps',14,8,772,1678358268,1678358268,0,0,'f2578b7376527e11ff6decae1be137c9',27,''),(270,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.gzip','88dd674bc368509aa3ea198c2f4626f9',75,'89a5-dd39-server.css.gzip',15,8,15888,1678358268,1678358268,0,0,'3971f0dedcc495100e354ac8e77d7a24',27,''),(271,2,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css','684553ec9bd093db235f659c57b15af8',28,'2bf5-dd39-theming.css',16,11,724,1678358268,1678358268,0,0,'d768235dab8080bda78988c9a95f3c2d',27,''),(272,2,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.deps','666a810bd01910cf9caa1734d6bbca52',28,'2bf5-dd39-theming.css.deps',14,8,132,1678358268,1678358268,0,0,'5fe767ab48e48f2d1060ac3e703b785d',27,''),(273,2,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.gzip','b61b61c25ad60bb4846e5b819a0e4baa',28,'2bf5-dd39-theming.css.gzip',15,8,307,1678358268,1678358268,0,0,'075a03ee07ce7f78602c1934e12fa816',27,''),(274,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css','1dbfa0f3f5c9cfd5492550c33ffa95ff',75,'89a5-dd39-jquery-ui-fixes.css',16,11,4102,1678358697,1678358697,0,0,'43597846dcf2b569df15d68b01defc49',27,''),(275,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.deps','635838ae2dacee8a4eaacc0163b9ae97',75,'89a5-dd39-jquery-ui-fixes.css.deps',14,8,131,1678358697,1678358697,0,0,'e853db3d6a11ffcc31670ae108a36276',27,''),(276,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.gzip','31f45e6cbf6c6109a5c543594268b0f7',75,'89a5-dd39-jquery-ui-fixes.css.gzip',15,8,846,1678358697,1678358697,0,0,'492275de2e3a7b38160fe217d460de05',27,''),(277,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css','72958c1e1ceabf47c101dc39f746f375',75,'89a5-dd39-share.css',16,11,2685,1678358697,1678358697,0,0,'bb8092c7df6aac42684436d0d2fc48fb',27,''),(278,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.deps','5e1996bb860233f579b5cc0685c018c8',75,'89a5-dd39-share.css.deps',14,8,121,1678358697,1678358697,0,0,'27959d71aa020b7f07aa4b19ce69e565',27,''),(279,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.gzip','05f6e2f8484349624c1cbb5c85ae1de5',75,'89a5-dd39-share.css.gzip',15,8,957,1678358697,1678358697,0,0,'335bfa703f8d2a4ac435309a6c6ac4a2',27,''),(280,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css','86681b8bc226a647a798fea77f821f8b',75,'89a5-dd39-jquery.ocdialog.css',16,11,1253,1678358697,1678358697,0,0,'1fca2a34000b177db54cb6e25b64c2ca',27,''),(281,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.deps','f5564a3bc71307439ccf0e75917842ec',75,'89a5-dd39-jquery.ocdialog.css.deps',14,8,131,1678358697,1678358697,0,0,'4214ce481031d57878145b447dc6f4f5',27,''),(282,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.gzip','3d9bffd046bcb0bd6f922550fd1728e7',75,'89a5-dd39-jquery.ocdialog.css.gzip',15,8,560,1678358697,1678358697,0,0,'f426e96192eb0ab7a329118cdcf388c9',27,''),(283,2,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css','c32a464f00dea1e419cb2153dea8d226',165,'89a5-dd39-settings.css',16,11,15629,1678358698,1678358698,0,0,'416da9a531bb1347c9937099cb6a74a1',27,''),(284,2,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.deps','70ca02cf7791d1bf0ab2ee6e6190a08d',165,'89a5-dd39-settings.css.deps',14,8,128,1678358698,1678358698,0,0,'4bdb0e0af5ff0a06628e61b30b7536dc',27,''),(285,2,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.gzip','7509ce07ce4d63163a95c2d37570e5db',165,'89a5-dd39-settings.css.gzip',15,8,4343,1678358698,1678358698,0,0,'670c7bdf2e161c9007ad6fa151fd14ee',27,''),(286,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css','a7455aea2749eb5babf9882432e43052',88,'4fda-dd39-merged.css',16,11,18493,1678358700,1678358700,0,0,'ec6b5e2bc38d56188e0e3d97a5fb7264',27,''),(287,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.deps','014a08cbc68d833047d7ed9ef00582d5',88,'4fda-dd39-merged.css.deps',14,8,397,1678358700,1678358700,0,0,'558889ca166a01d91b9220882e386872',27,''),(288,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.gzip','7417c522a6b92d18106617a0fe0cd93c',88,'4fda-dd39-merged.css.gzip',15,8,4220,1678358700,1678358700,0,0,'75b7b86bdfd89f247a1b1757d171037b',27,''),(289,2,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css','cead350d6af9fb0d4b0acf5c57433d0a',92,'1980-dd39-trash.css',16,11,344,1678358700,1678358700,0,0,'5c79c16cb5211c5fe2d623ba0a8c7c7e',27,''),(290,2,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.deps','ddc57b10ad9af48a3ed703e6db4d325d',92,'1980-dd39-trash.css.deps',14,8,137,1678358700,1678358700,0,0,'469659a800067a06cd226ef84357096e',27,''),(291,2,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.gzip','cd58401fa37b93e6d8ece58f6793ad07',92,'1980-dd39-trash.css.gzip',15,8,161,1678358700,1678358700,0,0,'e1f4d1cb3fb71611590d8f0a61ff39d2',27,''),(292,2,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css','315c9a71ed6d9bfc55b67efa3639823c',96,'1980-dd39-autocomplete.css',16,11,964,1678358700,1678358700,0,0,'8a4a5386a4a2db011e136787350e0c3b',27,''),(293,2,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.deps','25caf287d9b6f3709e7fb0f6909daef3',96,'1980-dd39-autocomplete.css.deps',14,8,138,1678358700,1678358700,0,0,'5a45300ef8a72b3ed8041a3f052aff61',27,''),(294,2,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.gzip','4bcfbe4d23bea15b19cc1be6de2ab207',96,'1980-dd39-autocomplete.css.gzip',15,8,421,1678358700,1678358700,0,0,'725bb4db97aff789dbbbee99829da203',27,''),(295,2,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css','bb2bcb3b5216a5bf08870b98ab8108a8',100,'35c3-dd39-mergedAdditionalStyles.css',16,11,2303,1678358700,1678358700,0,0,'9bf47207e8cef093612daf37cde95f26',27,''),(296,2,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.deps','3d2dd721c9177b66fcb27c9ce23724b5',100,'35c3-dd39-mergedAdditionalStyles.css.deps',14,8,316,1678358700,1678358700,0,0,'a063f04225dde33fbd003b11208af35e',27,''),(297,2,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.gzip','d27a4c0564ba11157d932a143b719725',100,'35c3-dd39-mergedAdditionalStyles.css.gzip',15,8,793,1678358700,1678358700,0,0,'f4a78b449c9e5f71c3702a74ee1adc34',27,''),(298,2,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css','9e3066d1300a64bbe4c1bb0a1152a98e',104,'21f4-dd39-merged.css',16,11,4148,1678358701,1678358701,0,0,'61ad3fca826f0199be37f420628b18cc',27,''),(299,2,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.deps','bc43f6c94909d91c8c7b59e21fa67f61',104,'21f4-dd39-merged.css.deps',14,8,389,1678358701,1678358701,0,0,'77ff19a618e3af717c431a9ee36365dd',27,''),(300,2,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.gzip','ccb5610cf876f5e2261d71e2f0e41b28',104,'21f4-dd39-merged.css.gzip',15,8,1262,1678358701,1678358701,0,0,'e7dca8f19b2de327482ac7b24f7024bd',27,''),(301,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css','3dd40571f6f29d0a0c946dce61469748',75,'89a5-dd39-systemtags.css',16,11,1403,1678358701,1678358701,0,0,'93b3e68f642c4d98af181634281cd40f',27,''),(302,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.deps','54f3b5f5c223467ca699ffe87c8678d1',75,'89a5-dd39-systemtags.css.deps',14,8,126,1678358701,1678358701,0,0,'57e594124e7bce772a4a6683e25521d4',27,''),(303,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.gzip','ca3d590ed9b309e4099c628c9ba37bfc',75,'89a5-dd39-systemtags.css.gzip',15,8,385,1678358701,1678358701,0,0,'f48e1a27f56a9891c0b1f7fe546bf2e7',27,''),(304,2,'appdata_ocp2ck7yo4uj/theming/1','d20433a493075cc0414e07c5c31de2bf',29,'1',2,1,2705,1678956997,1678956997,0,0,'6412d9c5736b6',31,''),(305,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_video.svg','b2db7a7e6ab13f9ea09a02bd98fa0039',304,'icon-core-filetypes_video.svg',17,3,277,1678358756,1678358756,0,0,'62f2213a9716b94c50f6b459ffc907bf',27,''),(306,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_folder.svg','9c31a0be115de8ea2d86812cb7491591',304,'icon-core-filetypes_folder.svg',17,3,255,1678358756,1678358756,0,0,'5183852be10933eccfc2b38e046a7555',27,''),(307,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_image.svg','ab48ac8e035ddb84f1d34cb43e486f7f',304,'icon-core-filetypes_image.svg',17,3,352,1678358756,1678358756,0,0,'47d86fb3b31ce1db2cf6186b25b92bd2',27,''),(308,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_text.svg','2aaf405bd7368b937bb2172e801b9be5',304,'icon-core-filetypes_text.svg',17,3,295,1678358756,1678358756,0,0,'ef538bc49774f7aeb3a5818337dc732d',27,''),(309,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_application-pdf.svg','1c363d4f97b09c9974eabdf1b1e79195',304,'icon-core-filetypes_application-pdf.svg',17,3,676,1678358756,1678358756,0,0,'ddde1587ba031ca724d51e53f89be219',27,''),(310,2,'appdata_ocp2ck7yo4uj/preview/240','66538cbe3c339e6ff7cd619d5e10bb67',18,'240',2,1,595528,1678358963,1678358963,0,0,'6409b9b3e4e39',31,''),(311,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-document.svg','d5896aaefcf6842432895d5fa19c41b1',304,'icon-core-filetypes_x-office-document.svg',17,3,295,1678358960,1678358960,0,0,'91a8d7bbba00fb12fee92618de11e5f3',27,''),(312,2,'appdata_ocp2ck7yo4uj/preview/240/2000-1333-max.jpg','109640fa4bd202334f7dbbde430c14c2',310,'2000-1333-max.jpg',5,3,570843,1678358961,1678358961,0,0,'2f55898222d8046ea5115cd835059c53',27,''),(313,2,'appdata_ocp2ck7yo4uj/preview/239','6ea1f2e4fabd223bb8b6ad53d41a7fb1',18,'239',2,1,335671,1678358963,1678358963,0,0,'6409b9b3bf6df',31,''),(314,2,'appdata_ocp2ck7yo4uj/preview/240/64-64-crop.jpg','0c9a17ebcebc388d5f438daa79585b05',310,'64-64-crop.jpg',5,3,3420,1678358961,1678358961,0,0,'b5c2c4486dc43637d47dcb8bf33e23e6',27,''),(315,2,'appdata_ocp2ck7yo4uj/preview/239/1100-734-max.jpg','34f53b48ebf376f90df62b71d4ddae26',313,'1100-734-max.jpg',5,3,308351,1678358961,1678358961,0,0,'8c2a04a6bbb92cf0a48d8c53f2ea70c0',27,''),(316,2,'appdata_ocp2ck7yo4uj/preview/246','8734b661d9ef2e41975596cbb7aea946',18,'246',2,1,180145,1678358966,1678358966,0,0,'6409b9b6bb240',31,''),(317,2,'appdata_ocp2ck7yo4uj/preview/241','23fce7333eee9a2ebfdabc3ce0221a0f',18,'241',2,1,815617,1678358964,1678358964,0,0,'6409b9b419874',31,''),(318,2,'appdata_ocp2ck7yo4uj/preview/239/64-64-crop.jpg','65a3deb5ecae8673330bf52779ca6387',313,'64-64-crop.jpg',5,3,2543,1678358961,1678358961,0,0,'bc1cee47be4da62affd00daed325bb84',27,''),(319,2,'appdata_ocp2ck7yo4uj/preview/241/2000-1333-max.jpg','377a6f6a22649ef4fa2e7802496a4aaa',317,'2000-1333-max.jpg',5,3,789147,1678358961,1678358961,0,0,'eb37655d36b1cc1b9186761be9188481',27,''),(320,2,'appdata_ocp2ck7yo4uj/preview/241/64-64-crop.jpg','38014ff76499410a401d139c0a37b5f2',317,'64-64-crop.jpg',5,3,2929,1678358962,1678358962,0,0,'a252554979d0b2c8371f0f06a5f76df8',27,''),(321,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css','8d9996c9ec1c3019ac4ab8ba503e4f1d',88,'4fda-dd39-upload.css',16,11,3643,1678358962,1678358962,0,0,'60a82c308a11dc13af8b3ce096808b11',27,''),(322,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.deps','4dc0211ca2cfd0dc15fa3b1c519d8443',88,'4fda-dd39-upload.css.deps',14,8,129,1678358962,1678358962,0,0,'61340eb196e87520e9b2cc842baf015a',27,''),(323,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.gzip','8f74774bd8232922b5c77877921908e9',88,'4fda-dd39-upload.css.gzip',15,8,1016,1678358962,1678358962,0,0,'7c0ed9653d6725f8b0f88b8384ad0606',27,''),(324,2,'appdata_ocp2ck7yo4uj/preview/246/4096-4096-max.png','f01f0d365843fde49e45c08290be1516',316,'4096-4096-max.png',4,3,153221,1678358962,1678358962,0,0,'d97b8651465a5f7eabf90746827165de',27,''),(325,2,'appdata_ocp2ck7yo4uj/preview/246/64-64-crop.png','7fe853813378f83155fba05db04dbaf3',316,'64-64-crop.png',4,3,4748,1678358963,1678358963,0,0,'2a79285654c19434ec6635efe6ab6b07',27,''),(326,2,'appdata_ocp2ck7yo4uj/preview/239/256-256-crop.jpg','886334752446814dcc2b231022778ee9',313,'256-256-crop.jpg',5,3,24777,1678358963,1678358963,0,0,'36b46ef9c63da4bfc0ecef0ab4dec834',27,''),(327,2,'appdata_ocp2ck7yo4uj/preview/240/256-256-crop.jpg','bbd758a5e04a2ed32cec068ffe7a9cbc',310,'256-256-crop.jpg',5,3,21265,1678358963,1678358963,0,0,'716d8e297c3d90d1af7acb41d2d027f4',27,''),(328,2,'appdata_ocp2ck7yo4uj/preview/241/256-256-crop.jpg','98d8c52e04c5bbe4db451b129c85eef4',317,'256-256-crop.jpg',5,3,23541,1678358964,1678358964,0,0,'a5477ce2498c2dd829ea50e3672bbb8d',27,''),(329,2,'appdata_ocp2ck7yo4uj/preview/237/96-96-crop.png','234d24f23ec4cb27e343e4657a87b498',248,'96-96-crop.png',4,3,3679,1678358965,1678358965,0,0,'ce078b7590ce9327d9219bc54064a971',27,''),(330,2,'appdata_ocp2ck7yo4uj/preview/246/256-256-crop.png','4871a26802ed1eb1e25d671f5b1718c6',316,'256-256-crop.png',4,3,22176,1678358966,1678358966,0,0,'fa62b69ad1bc393b22c2b6e857a8320c',27,''),(331,7,'files/!!!IMPORTANT!!!','1d2426dce41e886063fa2165e3cecdc2',236,'!!!IMPORTANT!!!',2,1,1269729,1678359119,1678359119,0,0,'6409ba4f48a7c',31,''),(332,7,'files/!!!IMPORTANT!!!/WIN_20211028_15_17_04_Pro.jpg','27803d8e661a2b1a686e6878458c589c',331,'WIN_20211028_15_17_04_Pro.jpg',5,3,1269729,1678359119,1678359119,0,0,'4b2337404aed1f64d0f21a1615bc20c9',27,''),(333,2,'appdata_ocp2ck7yo4uj/preview/332','b7c45cd7a53996abde18890823dc48ad',18,'332',2,1,572509,1678359120,1678359120,0,0,'6409ba501bbac',31,''),(334,2,'appdata_ocp2ck7yo4uj/preview/332/2560-1440-max.jpg','e223ae728a3547ba402cb5e883945301',333,'2560-1440-max.jpg',5,3,570066,1678359119,1678359119,0,0,'2421b841c11d59b605c265f7ceb29076',27,''),(335,2,'appdata_ocp2ck7yo4uj/preview/332/64-64-crop.jpg','f79b7f498e60cee0ce0053afdb7cec0a',333,'64-64-crop.jpg',5,3,2443,1678359120,1678359120,0,0,'e268566f81751459edd72930d0d64b1c',27,''),(336,2,'appdata_ocp2ck7yo4uj/preview/152/32-32-crop.png','7a9f0a0a8785feade277994e00380778',156,'32-32-crop.png',4,3,397,1678457879,1678457879,0,0,'d09932a7c96580b208ad0b4c6217cb8f',27,''),(337,2,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e','3ef75959df1ffce11a451390f7801a6a',181,'43c2025c19f23a73a880300144252f7e',2,1,16536,1678457885,1678457885,0,0,'640b3c1d3642f',31,''),(338,2,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.png','3e07f6fba42704c88db57c8943598eff',337,'photo.png',4,3,16074,1678457885,1678457885,0,0,'f7ce973f35bae41edeccfb3b9e201103',27,''),(339,2,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.32.png','f144ba0fbdec0447f39f03e4a8939d44',337,'photo.32.png',4,3,462,1678457885,1678457885,0,0,'0c99f45b9d8e432f5237ab047803f2fb',27,''),(340,5,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,9760893,1678956998,1678956925,0,0,'6412d9c60e176',23,''),(341,5,'cache','0fea6a13c52b4d4725368f24b045ca84',340,'cache',2,1,0,1678956925,1678956925,0,0,'6412d97d47e70',31,''),(342,5,'files','45b963397aa40d4a0063e0d85e4fe7a1',340,'files',2,1,9760893,1678956998,1678956998,0,0,'6412d9c60e176',31,''),(343,5,'files/bbw-logo.png','7fb0f51046121e4c805f9aeaa14b3093',342,'bbw-logo.png',4,3,6878,1678956925,1678956925,0,0,'d030f135d10da9754adecdd230cd68cb',27,''),(344,5,'files/Photos','d01bb67e7b71dd49fd06bad922f521c9',342,'Photos',2,1,2360011,1678956926,1678956925,0,0,'6412d97e03380',31,''),(345,5,'files/Photos/Coast.jpg','a6fe87299d78b207e9b7aba0f0cb8a0a',344,'Coast.jpg',5,3,819766,1678956925,1678956925,0,0,'9be4aa90b8a3a954bdfef483d1b2efda',27,''),(346,5,'files/Photos/Hummingbird.jpg','e077463269c404ae0f6f8ea7f2d7a326',344,'Hummingbird.jpg',5,3,585219,1678956925,1678956925,0,0,'c62a3d14746c4e33456cb52897b85f1f',27,''),(347,5,'files/Photos/Nut.jpg','aa8daeb975e1d39412954fd5cd41adb4',344,'Nut.jpg',5,3,955026,1678956926,1678956926,0,0,'15754b0cf56425ee523406a9699eda0b',27,''),(348,5,'files/Nextcloud.mp4','77a79c09b93e57cba23c11eb0e6048a6',342,'Nextcloud.mp4',7,6,462413,1678956926,1678956926,0,0,'aa8f6755a83b040e05119156b3a39faa',27,''),(349,5,'files/Nextcloud Manual.pdf','2bc58a43566a8edde804a4a97a9c7469',342,'Nextcloud Manual.pdf',9,8,4540242,1678956926,1678956926,0,0,'7e1bc0ad65ea057cea6cb4f54e9d74a4',27,''),(350,5,'files/Documents','0ad78ba05b6961d92f7970b2b3922eca',342,'Documents',2,1,78496,1678956926,1678956926,0,0,'6412d97ec4b0e',31,''),(351,5,'files/Documents/About.odt','b2ee7d56df9f34a0195d4b611376e885',350,'About.odt',10,8,77422,1678956926,1678956926,0,0,'d3274d1e8b2f2b6ad76b2a7d9376f172',27,''),(352,5,'files/Documents/About.txt','9da7b739e7a65d74793b2881da521169',350,'About.txt',12,11,1074,1678956926,1678956926,0,0,'091156346d6abf56a46da91345a1a334',27,''),(353,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.32.png','92509fe245c48ba6da7d02a8d6b041fa',177,'avatar.32.png',4,3,205,1678956927,1678956927,0,0,'9b477c10177e72ef4259f672575bd675',27,''),(354,2,'appdata_ocp2ck7yo4uj/preview/343','c173e600f50af55367d655c4b562ad62',18,'343',2,1,8817,1678956928,1678956928,0,0,'6412d9802992e',31,''),(355,2,'appdata_ocp2ck7yo4uj/preview/343/193-96-max.png','80cbfcbdf7d4e4f6479db76cd03b7f53',354,'193-96-max.png',4,3,6148,1678956928,1678956928,0,0,'3cea4c5b1b3136be0dca594292c6d6d5',27,''),(356,2,'appdata_ocp2ck7yo4uj/preview/343/64-64-crop.png','cf421d8bcfb182c5e22efabc2ca3c89e',354,'64-64-crop.png',4,3,2669,1678956928,1678956928,0,0,'d76c63e49685a85e0da58b033ab6129f',27,''),(357,5,'files/Subnetting Table.xlsx','588e4af1f3c4fbd43f0d53235e1f7159',342,'Subnetting Table.xlsx',21,8,11500,1678956975,1678956975,0,0,'1d9eb30271f8ccce83ed21af5fbecd7e',27,''),(358,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-spreadsheet.svg','254c6deb7b6882a179b786c4aadf5747',304,'icon-core-filetypes_x-office-spreadsheet.svg',17,3,327,1678956975,1678956975,0,0,'894d7b3114004e1a15a6f59e98513cda',27,''),(359,5,'files/05_subnetting.pdf','57747b9630ad46da935fd8dd69cd80cb',342,'05_subnetting.pdf',9,8,210523,1678956996,1678956996,0,0,'a0849c064b57128e9227bed9ab021834',27,''),(360,5,'files/Subnetting (Post-Routing).pkt','c8ebce43c09b83c4bfba3c02d315d540',342,'Subnetting (Post-Routing).pkt',14,8,68687,1678956996,1678956996,0,0,'293da1e9fe3d66a189f7d13399fe4184',27,''),(361,5,'files/Subnetting.docx','d04d3a524299087873a0f390ae4ccccd',342,'Subnetting.docx',18,8,1270172,1678956997,1678956997,0,0,'e76068ffb37ef25aae98adc7f28264b4',27,''),(362,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_file.svg','501c0881d7a4d79f1d7f7ed3c42e1b57',304,'icon-core-filetypes_file.svg',17,3,228,1678956997,1678956997,0,0,'6da28f891acb5e8105408c22c193f4b5',27,''),(363,5,'files/Subnetting.pdf','25b7737e814d7e898606c1fb48220cbf',342,'Subnetting.pdf',9,8,683310,1678956997,1678956997,0,0,'c8bb25b9c09127dc8c9d880bbd4db2a1',27,''),(364,5,'files/Subnetting.pkt','9680faa84e0ac8e8ed90d59fc7934060',342,'Subnetting.pkt',14,8,68661,1678956998,1678956998,0,0,'ad1acca5f9740ddc04426eedc011eebb',27,''),(365,8,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,37697316,1681985165,1681984653,0,0,'64410e8da0250',23,''),(366,8,'appdata_oc5ev4nda8xn','aadf862f9d9aa9893360280d6b549eb9',365,'appdata_oc5ev4nda8xn',2,1,36241,1682686203,1682686203,0,0,'64410c96bc171',31,''),(393,8,'files_external','c270928b685e7946199afdfb898d27ea',365,'files_external',2,1,261889,1681985164,1681983708,0,0,'64410e8da0250',31,''),(563,8,'appdata_oc5ev4nda8xn/js','18f5fbb7894cc66f1f93168d89e094de',366,'js',2,1,0,1681984662,1681984536,0,0,'64410c96bc171',31,''),(564,8,'appdata_oc5ev4nda8xn/js/core','3858020e95639eaf58aaeea02b3522f9',563,'core',2,1,0,1682686188,1682686188,0,0,'64410c95e31ec',31,''),(566,8,'appdata_oc5ev4nda8xn/preview','148964b704a264ffd6f55b095ee03766',366,'preview',2,1,36241,1681984546,1681984545,0,0,'64410c2211108',31,''),(569,8,'appdata_oc5ev4nda8xn/avatar','24c2918bc2d8caf49c791dec052908d3',366,'avatar',2,1,0,1682686198,1682686198,0,0,'64410c156ae31',31,''),(570,8,'appdata_oc5ev4nda8xn/avatar/admin','0bf14e48db6f85f60ec1c27a930aa77d',569,'admin',2,1,0,1681984533,1681984533,0,0,'64410c156604d',31,''),(574,8,'appdata_oc5ev4nda8xn/js/notifications','b45656d799e2f661691010e2183ac950',563,'notifications',2,1,0,1681984662,1681984662,0,0,'64410c968ec7f',31,''),(578,8,'appdata_oc5ev4nda8xn/js/files','15379d7a281e64810b318b92ab85412b',563,'files',2,1,0,1682686230,1682686230,0,0,'64410c96060d9',31,''),(582,8,'appdata_oc5ev4nda8xn/js/activity','dd3bdc73fc335ef7833b57742b2d69ab',563,'activity',2,1,0,1682686195,1682686195,0,0,'64410c958af63',31,''),(586,8,'appdata_oc5ev4nda8xn/js/comments','0f1fa1f621f4eca369f6ddf0a4b29344',563,'comments',2,1,0,1681984661,1681984661,0,0,'64410c95a25ad',31,''),(590,8,'appdata_oc5ev4nda8xn/js/files_sharing','c826960e92f79ff8c857f9c21b9e4817',563,'files_sharing',2,1,0,1681984662,1681984662,0,0,'64410c961ca9d',31,''),(594,8,'appdata_oc5ev4nda8xn/js/files_texteditor','560bf8dd6f0f64ee8a0f0f283677f9b5',563,'files_texteditor',2,1,0,1681984662,1681984662,0,0,'64410c96369da',31,''),(598,8,'appdata_oc5ev4nda8xn/js/files_versions','1be92f2992ecd0ec2aed95df6114d5db',563,'files_versions',2,1,0,1681984662,1681984662,0,0,'64410c9652375',31,''),(602,8,'appdata_oc5ev4nda8xn/js/gallery','59662866997a59ea5291db86640e4c34',563,'gallery',2,1,0,1681984662,1681984662,0,0,'64410c966e2fb',31,''),(609,8,'appdata_oc5ev4nda8xn/js/systemtags','59f965b05051e27b09d2cec1601af10d',563,'systemtags',2,1,0,1681984662,1681984662,0,0,'64410c96bc171',31,''),(613,8,'appdata_oc5ev4nda8xn/css','c550d16cdd4a96489355578284c2274b',366,'css',2,1,0,1682686234,1682686234,0,0,'64410c95709f2',31,''),(614,8,'appdata_oc5ev4nda8xn/css/core','df1a093f82a2be03b117935cb372861e',613,'core',2,1,0,1682686196,1682686196,0,0,'64410c94ef1f6',31,''),(627,8,'appdata_oc5ev4nda8xn/css/files','cde22d9c186456e3caa58ce82acd0a4e',613,'files',2,1,0,1682686230,1682686230,0,0,'64410c9513d3d',31,''),(631,8,'appdata_oc5ev4nda8xn/css/files_trashbin','6b23a907a55f3c8db1ab0012fc29c857',613,'files_trashbin',2,1,0,1681984661,1681984661,0,0,'64410c9559bad',31,''),(635,8,'appdata_oc5ev4nda8xn/css/comments','c8ccaac21c271ff5547850578c34d5cc',613,'comments',2,1,0,1681984660,1681984660,0,0,'64410c946d289',31,''),(639,8,'appdata_oc5ev4nda8xn/css/files_sharing','a61ece2cad2b35fc6d7ce6acf3f50966',613,'files_sharing',2,1,0,1682686230,1682686230,0,0,'64410c952b859',31,''),(643,8,'appdata_oc5ev4nda8xn/css/files_texteditor','457a831184b979282e4f1d9ef5dc5ccf',613,'files_texteditor',2,1,0,1681984661,1681984661,0,0,'64410c95420a1',31,''),(650,8,'appdata_oc5ev4nda8xn/css/theming','36b8448a48f37054e0530d24fae7a1bc',613,'theming',2,1,0,1682686188,1682686188,0,0,'64410c95709f2',31,''),(654,8,'appdata_oc5ev4nda8xn/preview/3','22b0c9c784c1a95cf68b7d2037c817a2',566,'3',2,1,9562,1681984545,1681984545,0,0,'64410c2151838',31,''),(655,8,'appdata_oc5ev4nda8xn/preview/152','1f618c7eb33c79de17c0125e17fc74f2',566,'152',2,1,26679,1681984546,1681984545,0,0,'64410c2211108',31,''),(656,8,'appdata_oc5ev4nda8xn/preview/3/193-96-max.png','f97fffa88b6c93ff53b6b6a2e45b84fe',654,'193-96-max.png',4,3,6127,1681984545,1681984545,0,0,'be3e2f4f8a1bf17c485dad6e65f68670',27,''),(657,8,'appdata_oc5ev4nda8xn/preview/152/4096-4096-max.png','2408f5928b06787d97c16918be4b57de',655,'4096-4096-max.png',4,3,26057,1681984545,1681984545,0,0,'72a9ae03fc796b38cf59bc5aa1acb276',27,''),(658,8,'appdata_oc5ev4nda8xn/preview/3/64-64-crop.png','0be2261332bf87e1c0a671ea46df2ab0',654,'64-64-crop.png',4,3,3435,1681984545,1681984545,0,0,'8c0c6fa4f1f1bf83798c21d221144f24',27,''),(659,8,'appdata_oc5ev4nda8xn/preview/152/64-64-crop.png','334d38e558f19f719b5e409d380ecad8',655,'64-64-crop.png',4,3,622,1681984546,1681984546,0,0,'d47c24bd5ce69448d3a88f74e4a1805e',27,''),(660,8,'appdata_oc5ev4nda8xn/css/icons','068260a98eeb3f3f055cd1220707c9b0',613,'icons',2,1,0,1682686196,1682686196,0,0,'64410c8b42c66',31,''),(661,8,'appdata_oc5ev4nda8xn/appstore','798bc0fb714493dc4858ed7f58af4505',366,'appstore',2,1,0,1681984659,1681984659,0,0,'64410c9311097',31,''),(662,8,'appdata_oc5ev4nda8xn/appstore/apps.json','8073e805b6a8e4de305b58a3b9229c96',661,'apps.json',20,8,1935481,1681985299,1681985299,0,0,'7c4b8d5ceee5ed47f25d2eef56d4421d',27,''),(663,8,'appdata_oc5ev4nda8xn/dav-photocache','090cf76cb5e4c0e9bed207e1453ac995',366,'dav-photocache',2,1,0,1682686209,1682686209,0,0,'64410d005a78a',31,''),(664,8,'appdata_oc5ev4nda8xn/text','38928fe0dfdf43b1b68c5f06e82ea65e',366,'text',2,1,0,1681984938,1681984938,0,0,'64410daa3a952',31,''),(665,8,'appdata_oc5ev4nda8xn/text/documents','77de8713833008a9ef8ebe86d80736e1',664,'documents',2,1,0,1681984938,1681984938,0,0,'64410daa4253a',31,''),(666,8,'index.html','eacf331f0ffc35d4b482f1d15a887d3b',365,'index.html',29,11,0,1681983708,1681983708,0,0,'cca66da160d074a50b7d9f1fb19b78af',27,''),(667,8,'admin','21232f297a57a5a743894a0e4a801fc3',365,'admin',2,1,9905229,1681985164,1681983708,0,0,'64410e8da0250',31,''),(668,8,'vok','96a752ab58e6a44badcdfc33ae8acb0a',365,'vok',2,1,8717771,1681985164,1681983708,0,0,'64410e8da0250',31,''),(669,8,'appdata_ocp2ck7yo4uj','511e1603a617f66d86c36ae02930e2d1',365,'appdata_ocp2ck7yo4uj',2,1,8556983,1681985165,1681983708,0,0,'64410e8da0250',31,''),(670,8,'testuser2','58dd024d49e1d1b83a5d307f09f32734',365,'testuser2',2,1,9760893,1681985165,1681983708,0,0,'64410e8da0250',31,''),(671,8,'.ocdata','a840ac417b1f143f29a22c7261756dad',365,'.ocdata',14,8,0,1681984526,1681984526,0,0,'4781b6bebd8270241f6500efd4a6028d',27,''),(672,8,'nextcloud.log','edbc35c4c40bdf29f0494207fbbf5aeb',365,'nextcloud.log',14,8,458310,1681985137,1681985137,0,0,'9ebe1ab583f3ce14095b0ea51797fa00',27,''),(673,8,'admin/files_versions','6450f18c15be102af37ec1718d428e0b',667,'files_versions',2,1,1,1681985164,1681983708,0,0,'64410e8da0250',31,''),(674,8,'admin/files_trashbin','a72459d4cc20d1f997eaa57f8044fc12',667,'files_trashbin',2,1,1,1681985164,1681983708,0,0,'64410e8da0250',31,''),(675,8,'admin/cache','a5a866a7252a49dd4bfbe22664d16245',667,'cache',2,1,0,1681983708,1681983708,0,0,'64410e8c55456',31,''),(676,8,'admin/files','16cdbbebf30085594dd7a9532a5ecc5e',667,'files',2,1,9905227,1681985164,1681983708,0,0,'64410e8da0250',31,''),(677,8,'admin/files_versions/Testfile.txt.v1678352135','a4047ac4bef4485caddb7b4c629f06c0',673,'Testfile.txt.v1678352135',12,11,1,1681983708,1681983708,0,0,'12c241a4c224831c8b16dc5b55705b7e',27,''),(678,8,'admin/files_trashbin/keys','97944b60ec9948f713738985efccd2be',674,'keys',2,1,0,1681983708,1681983708,0,0,'64410e8c5c03e',31,''),(679,8,'admin/files_trashbin/versions','d47d1b0d8f6afecc12dc8e02fc41655a',674,'versions',2,1,0,1681983708,1681983708,0,0,'64410e8c5c083',31,''),(680,8,'admin/files_trashbin/files','5ff61c230393becfa6af205f09cba358',674,'files',2,1,1,1681985164,1681983708,0,0,'64410e8da0250',31,''),(681,8,'admin/files_trashbin/files/deletedfile.txt.d1678356431','193fffb1c3bb7efee66df95f1588b0e3',680,'deletedfile.txt.d1678356431',14,8,1,1681983708,1681983708,0,0,'7792300717cda2683601ac894a8ba3e7',27,''),(682,8,'admin/files/Nextcloud Manual.pdf','4478603ed9f1b460437fca367632a1b1',676,'Nextcloud Manual.pdf',9,8,4540242,1681983708,1681983708,0,0,'60888a3bcfc003d7dd3074eed878a0ed',27,''),(683,8,'admin/files/Documents','f3228bb96d44c0d5c5de7928d373cb0c',676,'Documents',2,1,78496,1681985164,1681983708,0,0,'64410e8da0250',31,''),(684,8,'admin/files/m129_bandbreitenberechnung.pdf','47b03ea181deaa896f36a89a6a8ee230',676,'m129_bandbreitenberechnung.pdf',9,8,161663,1681983708,1681983708,0,0,'dd0dc7c991e3b74b8cfbf7bb1dbbd93f',27,''),(685,8,'admin/files/Test','8fc6b96d5b26788aaa1b0b5fcdfa5536',676,'Test',2,1,2190811,1681985164,1681983708,0,0,'64410e8da0250',31,''),(686,8,'admin/files/Photos','3fe593e31bd535ec607aa5939d770823',676,'Photos',2,1,2360011,1681985164,1681983708,0,0,'64410e8da0250',31,''),(687,8,'admin/files/Testfile.txt','f0ef3770415318c0a33eb2301b55c749',676,'Testfile.txt',12,11,9,1681983708,1681983708,0,0,'31ea3518f3ac30696161621a8126b334',27,''),(688,8,'admin/files/bbw-logo.png','349c3cc41505fb3328abf23605130d3e',676,'bbw-logo.png',4,3,6878,1681983708,1681983708,0,0,'062c198ff07f5d6dc6ec85daf1a8b017',27,''),(689,8,'admin/files/m129_bandbreitenberechnung-lsg.pdf','a16619b4f3724076881bd2f655095ba8',676,'m129_bandbreitenberechnung-lsg.pdf',9,8,104704,1681983708,1681983708,0,0,'8fd92604bd8faea76f85099300b113a6',27,''),(690,8,'admin/files/Nextcloud.mp4','b41d2ad31f10c1f04a8c2126abe8608a',676,'Nextcloud.mp4',7,6,462413,1681983708,1681983708,0,0,'bc666ce1af1827cd8df4d9ff22423dc7',27,''),(691,8,'admin/files/Documents/About.odt','91a5b1e7629b6dfe7dd53750bf64885c',683,'About.odt',10,8,77422,1681983708,1681983708,0,0,'e49ce22f135d99dca6b20c0687ce88df',27,''),(692,8,'admin/files/Documents/About.txt','983746326dcab80a19bf1ab9d2f4d83f',683,'About.txt',12,11,1074,1681983708,1681983708,0,0,'6e6a7740d28bab91848085ea0b8b07f5',27,''),(693,8,'admin/files/Test/docker-compose.yml','57a7170bb5b2c9a05d5cdca7d7e7630d',685,'docker-compose.yml',19,8,475,1681983708,1681983708,0,0,'501fa86d2c543dac4264576bb27968a7',27,''),(694,8,'admin/files/Test/10.1_ContainerVirtualisierung.pdf','dfb08fb68216df6d58764ea033b2878e',685,'10.1_ContainerVirtualisierung.pdf',9,8,110769,1681983708,1681983708,0,0,'d01c2612064118dcc7806c7a1beacc62',27,''),(695,8,'admin/files/Test/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf','26beedcee2fb3159b9fb26252d7702f1',685,'10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf',9,8,307530,1681983708,1681983708,0,0,'faeec77087b4440941074288c1cd7e9a',27,''),(696,8,'admin/files/Test/10.4_CV_Versus_VM.docx','581bffb29cbf2d8ec526a5283d667b08',685,'10.4_CV_Versus_VM.docx',18,8,47076,1681983708,1681983708,0,0,'509ec99172ff446998b8cad41843126c',27,''),(697,8,'admin/files/Test/10.5_Docker Einstieg - CLI-Befehle.pdf','65cafd9f976e38d62f45bc78e98093bd',685,'10.5_Docker Einstieg - CLI-Befehle.pdf',9,8,458836,1681983708,1681983708,0,0,'5a4a8b7abf4a8ab5d9ad6aa0e5ec9bc4',27,''),(698,8,'admin/files/Test/10.3_Container_oder_VM_Pros_und_Kontras.pdf','1226f1e3a9db339871a8590e236a9409',685,'10.3_Container_oder_VM_Pros_und_Kontras.pdf',9,8,1266125,1681983708,1681983708,0,0,'b55f04d269001207b1a84aa490438d5a',27,''),(699,8,'admin/files/Photos/Nut.jpg','4bce7b916362e3d9134f74339bced071',686,'Nut.jpg',5,3,955026,1681983708,1681983708,0,0,'a598c8ce15d0bd383abf169a2e1e37c9',27,''),(700,8,'admin/files/Photos/Coast.jpg','5f502ce9e69ad43ae074def8c74313f9',686,'Coast.jpg',5,3,819766,1681983708,1681983708,0,0,'083873a3f99baebaff1945f87c430f91',27,''),(701,8,'admin/files/Photos/Hummingbird.jpg','1db9c9307506296c2d9f31b8db14ea5f',686,'Hummingbird.jpg',5,3,585219,1681983708,1681983708,0,0,'22aec2fe406842539326ad0ff493806c',27,''),(702,8,'vok/cache','8063c4cb8080d99f737a81bbbdc5fd67',668,'cache',2,1,0,1681983708,1681983708,0,0,'64410e8c7a5dc',31,''),(703,8,'vok/files','f391312ad3c7157e5cbddc9ac7989765',668,'files',2,1,8717771,1681985164,1681983708,0,0,'64410e8da0250',31,''),(704,8,'vok/files/Nextcloud Manual.pdf','7e0d11a6277bc6c736f78ad221902793',703,'Nextcloud Manual.pdf',9,8,4540242,1681983708,1681983708,0,0,'a7cc85fc300caf961dac86b2f9d6aa81',27,''),(705,8,'vok/files/Documents','eb2ffab66210d758ee687ade3c4bb0af',703,'Documents',2,1,78496,1681985164,1681983708,0,0,'64410e8da0250',31,''),(706,8,'vok/files/testfile2.txt','251a6dc70cbb2af8a82aa36a1951212d',703,'testfile2.txt',12,11,1,1681983708,1681983708,0,0,'3d6f326b3837892e6f3dbcf18d85f21a',27,''),(707,8,'vok/files/!!!IMPORTANT!!!','5bc9bd2d0eb2886f2bff6af1914f2c5f',703,'!!!IMPORTANT!!!',2,1,1269729,1681985164,1681983708,0,0,'64410e8da0250',31,''),(708,8,'vok/files/testfile.txt','44ad54883f2cf6d0956f6201cf11bf4c',703,'testfile.txt',12,11,1,1681983708,1681983708,0,0,'e0da4f3a53744a8bbc0d19696fcebc74',27,''),(709,8,'vok/files/Photos','a8534ea801b1a8398de9758c147f7d8f',703,'Photos',2,1,2360011,1681985164,1681983708,0,0,'64410e8da0250',31,''),(710,8,'vok/files/bbw-logo.png','101efd34f7a3d75e41457c52599006ae',703,'bbw-logo.png',4,3,6878,1681983708,1681983708,0,0,'9c60d75d71db724501f4698ed808528e',27,''),(711,8,'vok/files/Nextcloud.mp4','347927c51f31cb1429de7123a3ba3c8f',703,'Nextcloud.mp4',7,6,462413,1681983708,1681983708,0,0,'847320cf3f47a981e4135938a82c4785',27,''),(712,8,'vok/files/Documents/About.odt','55a0d26dd7c949ba80d3cfe5837f5a4c',705,'About.odt',10,8,77422,1681983708,1681983708,0,0,'e031ef879f96736a62711804eaee16d5',27,''),(713,8,'vok/files/Documents/About.txt','07429a32dcd512d72abfd29e2c03cd48',705,'About.txt',12,11,1074,1681983708,1681983708,0,0,'984e0b92fd4bbfece1a0da9f39deb308',27,''),(714,8,'vok/files/!!!IMPORTANT!!!/WIN_20211028_15_17_04_Pro.jpg','dcf2abded1e0192af7d6e89d7819db52',707,'WIN_20211028_15_17_04_Pro.jpg',5,3,1269729,1681983708,1681983708,0,0,'5538c6bac37e0053e00429d4d6df965d',27,''),(715,8,'vok/files/Photos/Nut.jpg','422bd4f2a46ebeb0b74e7136c587e188',709,'Nut.jpg',5,3,955026,1681983708,1681983708,0,0,'ced358ce14f484336b9960e851a21cc1',27,''),(716,8,'vok/files/Photos/Coast.jpg','3c962a5e6e6a560127f7ccbb43caf47f',709,'Coast.jpg',5,3,819766,1681983708,1681983708,0,0,'08971bc8bd8a06e81e3c05d054486cfe',27,''),(717,8,'vok/files/Photos/Hummingbird.jpg','743080a2b293f84a6f9f4b419b5b392e',709,'Hummingbird.jpg',5,3,585219,1681983708,1681983708,0,0,'90018d645a8872e4e98b733458926ed0',27,''),(718,8,'files_external/rootcerts.crt','96f0eb1b0b28e846975c98dbeb5e734d',393,'rootcerts.crt',14,8,261889,1681983708,1681983708,0,0,'d2865039249584a0b440c9f93166f32e',27,''),(719,8,'appdata_ocp2ck7yo4uj/avatar','91d888474bf0e119311f6ea5fc91c88b',669,'avatar',2,1,65286,1681985164,1681983708,0,0,'64410e8da0250',31,''),(720,8,'appdata_ocp2ck7yo4uj/theming','1a917cff90046d01d35343c92b270d5b',669,'theming',2,1,2705,1681985164,1681983708,0,0,'64410e8da0250',31,''),(721,8,'appdata_ocp2ck7yo4uj/css','c7ebe2c99c887f1959d7328bdce2dcb8',669,'css',2,1,190031,1681985164,1681983708,0,0,'64410e8da0250',31,''),(722,8,'appdata_ocp2ck7yo4uj/appstore','2345d9a5592e7a2e8d79bf2223c8e348',669,'appstore',2,1,1087363,1681985164,1681983708,0,0,'64410e8da0250',31,''),(723,8,'appdata_ocp2ck7yo4uj/identityproof','2d4281eebef45968390785218dc56c63',669,'identityproof',2,1,4021,1681985164,1681983708,0,0,'64410e8da0250',31,''),(724,8,'appdata_ocp2ck7yo4uj/js','493274afae6aa7897fd145345985624b',669,'js',2,1,2662994,1681985165,1681983708,0,0,'64410e8da0250',31,''),(725,8,'appdata_ocp2ck7yo4uj/preview','b9b868100017c68bd74bd35b4838053c',669,'preview',2,1,4492361,1681985165,1681983708,0,0,'64410e8da0250',31,''),(726,8,'appdata_ocp2ck7yo4uj/dav-photocache','d30c65c6c0c584cdd932ec953747fc71',669,'dav-photocache',2,1,52222,1681985165,1681983708,0,0,'64410e8da0250',31,''),(727,8,'appdata_ocp2ck7yo4uj/avatar/admin','1fb69ff79d77a83aba1b7ef55d030cbb',719,'admin',2,1,17066,1681985164,1681983708,0,0,'64410e8da0250',31,''),(728,8,'appdata_ocp2ck7yo4uj/avatar/testuser','35512c74e396de5a72f211830497ab48',719,'testuser',2,1,5695,1681985164,1681983708,0,0,'64410e8da0250',31,''),(729,8,'appdata_ocp2ck7yo4uj/avatar/disableduser','6830f17f340ba5039fbbae652584970f',719,'disableduser',2,1,13373,1681985164,1681983708,0,0,'64410e8da0250',31,''),(730,8,'appdata_ocp2ck7yo4uj/avatar/vok','0a0486437d7af5fe0661d5870080b6fe',719,'vok',2,1,17536,1681985164,1681983708,0,0,'64410e8da0250',31,''),(731,8,'appdata_ocp2ck7yo4uj/avatar/testuser3','d086ba37569c9161e11461068373ee41',719,'testuser3',2,1,5706,1681985164,1681983708,0,0,'64410e8da0250',31,''),(732,8,'appdata_ocp2ck7yo4uj/avatar/testuser2','638d7e7ca3dce9c416dada796c49c1ed',719,'testuser2',2,1,5910,1681985164,1681983708,0,0,'64410e8da0250',31,''),(733,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.40.png','4519fa97dd131e66e97ae8eb7202e4d6',727,'avatar.40.png',4,3,1464,1681983708,1681983708,0,0,'968d4b42d15f425c05c4ebfddfbb5074',27,''),(734,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.182.png','392718da6f4eea3f15b499074c74f5ea',727,'avatar.182.png',4,3,10549,1681983708,1681983708,0,0,'db6bcc09b23e48d4355e8294814e95b6',27,''),(735,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.32.png','f091d9d8c6eb1c6f6f27a4b1b4a29b70',727,'avatar.32.png',4,3,1095,1681983708,1681983708,0,0,'ec384187a1a00bf1525aac903d750e85',27,''),(736,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.png','d8dd45159872dc1cb96d8f1d5e3764d6',727,'avatar.png',4,3,3958,1681983708,1681983708,0,0,'51120603c7b76879505b6afcc73afa6e',27,''),(737,8,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.40.png','1165c2f5e3965d783a95654d53e4147c',728,'avatar.40.png',4,3,218,1681983708,1681983708,0,0,'b7f5be06f34512f397a878716b6eb790',27,''),(738,8,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.png','344c673f2655b9feace05731093b597a',728,'avatar.png',4,3,5477,1681983708,1681983708,0,0,'155025b18df391d13e3291749a527ef4',27,''),(739,8,'appdata_ocp2ck7yo4uj/avatar/testuser/generated','62fd23db4be5617c3426ec24aa711f91',728,'generated',14,8,0,1681983708,1681983708,0,0,'5a86f820d357c7e6b938dda9e1a38bf7',27,''),(740,8,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.40.png','d6e11c84318f70f5e74c5d1ec67c1a61',729,'avatar.40.png',4,3,449,1681983708,1681983708,0,0,'16a9c6c3e9605a990b2d9f0a82b7eab8',27,''),(741,8,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.png','ecb540d8c6611cb24a2fe7a69867fb31',729,'avatar.png',4,3,12924,1681983708,1681983708,0,0,'fe47ecd9cce1e9395fbe4dacf7876206',27,''),(742,8,'appdata_ocp2ck7yo4uj/avatar/disableduser/generated','0b4666d8647679ba143b11759a36c07f',729,'generated',14,8,0,1681983708,1681983708,0,0,'20dfae903309f107ca93d2a7b0743814',27,''),(743,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.40.png','d709c254c65347b092c445a6e5dbaaa3',730,'avatar.40.png',4,3,458,1681983708,1681983708,0,0,'27a615b50d51b060bb05e8a09590066a',27,''),(744,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.182.png','9bee4d8d9dc2e2f87b9f92344499726a',730,'avatar.182.png',4,3,2015,1681983708,1681983708,0,0,'1d03806154dcbaa8c1e9e4268448ebf2',27,''),(745,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.32.png','19097930e38bfc9c4f8bd42e72127c39',730,'avatar.32.png',4,3,391,1681983708,1681983708,0,0,'a21d90139198057211552e0c2af431d5',27,''),(746,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.png','1c5e17f3562bf0026ff2433bc8359af1',730,'avatar.png',4,3,14672,1681983708,1681983708,0,0,'e919f3fdcea1879b62f84f8a5c77cee0',27,''),(747,8,'appdata_ocp2ck7yo4uj/avatar/vok/generated','92d0578812637bcd106a74c51bd2c0cd',730,'generated',14,8,0,1681983708,1681983708,0,0,'1a59819f2e510ad13cc1411b9c774e54',27,''),(748,8,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.40.png','ccce2d61522438868b4e0d107682f425',731,'avatar.40.png',4,3,219,1681983708,1681983708,0,0,'a54df512ae8ebafdebd9380401d7eaf1',27,''),(749,8,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.png','05d1d04fde756a5ccfe6c591210c7cf1',731,'avatar.png',4,3,5487,1681983708,1681983708,0,0,'6a5ca215471111aa2fda785d257c62e0',27,''),(750,8,'appdata_ocp2ck7yo4uj/avatar/testuser3/generated','c68c06a199b0826020e178163f42cf75',731,'generated',14,8,0,1681983708,1681983708,0,0,'1e89787f0188098655c04f2218972182',27,''),(751,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.40.png','8a34e8e5a8ce95f5ead5eddf1e5abfe2',732,'avatar.40.png',4,3,218,1681983708,1681983708,0,0,'60b93b1a35c2925ae48daaf87727a156',27,''),(752,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.32.png','92509fe245c48ba6da7d02a8d6b041fa',732,'avatar.32.png',4,3,205,1681983708,1681983708,0,0,'5542dec688b21a0d7665f98687f31009',27,''),(753,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.png','a3783e8054fcad0ccf5744275f000992',732,'avatar.png',4,3,5487,1681983708,1681983708,0,0,'c95fa0e4104a47ff5bcf6f3d422f08bf',27,''),(754,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/generated','27d667766eef561885e247be37aeaa76',732,'generated',14,8,0,1681983708,1681983708,0,0,'6a679867e67a80709a9de78653b250ac',27,''),(755,8,'appdata_ocp2ck7yo4uj/theming/1','d20433a493075cc0414e07c5c31de2bf',720,'1',2,1,2705,1681985164,1681983708,0,0,'64410e8da0250',31,''),(756,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-spreadsheet.svg','254c6deb7b6882a179b786c4aadf5747',755,'icon-core-filetypes_x-office-spreadsheet.svg',17,3,327,1681983708,1681983708,0,0,'c50f50df610afbd348a8bd937f38f031',27,''),(757,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_folder.svg','9c31a0be115de8ea2d86812cb7491591',755,'icon-core-filetypes_folder.svg',17,3,255,1681983708,1681983708,0,0,'eec4ee5618a6ed7c7a003d9a9f6c5f4c',27,''),(758,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_text.svg','2aaf405bd7368b937bb2172e801b9be5',755,'icon-core-filetypes_text.svg',17,3,295,1681983708,1681983708,0,0,'8fb6bdf9c1f2d757a52007e426fdc028',27,''),(759,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_application-pdf.svg','1c363d4f97b09c9974eabdf1b1e79195',755,'icon-core-filetypes_application-pdf.svg',17,3,676,1681983708,1681983708,0,0,'466511c749b9da0f990c28ffc153c84e',27,''),(760,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_video.svg','b2db7a7e6ab13f9ea09a02bd98fa0039',755,'icon-core-filetypes_video.svg',17,3,277,1681983708,1681983708,0,0,'f97b81b8c55ab977386cbd2ceb08c036',27,''),(761,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-document.svg','d5896aaefcf6842432895d5fa19c41b1',755,'icon-core-filetypes_x-office-document.svg',17,3,295,1681983708,1681983708,0,0,'ea92ef468ae2caa83bad87704e59d950',27,''),(762,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_image.svg','ab48ac8e035ddb84f1d34cb43e486f7f',755,'icon-core-filetypes_image.svg',17,3,352,1681983708,1681983708,0,0,'0155978b34f0457d3f08b4720f71bb42',27,''),(763,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_file.svg','501c0881d7a4d79f1d7f7ed3c42e1b57',755,'icon-core-filetypes_file.svg',17,3,228,1681983708,1681983708,0,0,'39bb05305cc59dda37bb20d93737aa0e',27,''),(764,8,'appdata_ocp2ck7yo4uj/css/theming','b7c0b45ef82251f3f095726b639e914b',721,'theming',2,1,1163,1681985164,1681983708,0,0,'64410e8da0250',31,''),(765,8,'appdata_ocp2ck7yo4uj/css/settings','2fd09612cf55a6d85d4729e9fa0f7a9e',721,'settings',2,1,20100,1681985164,1681983708,0,0,'64410e8da0250',31,''),(766,8,'appdata_ocp2ck7yo4uj/css/comments','19602c40a64f0290173a3ebf1dfa08f6',721,'comments',2,1,1523,1681985164,1681983708,0,0,'64410e8da0250',31,''),(767,8,'appdata_ocp2ck7yo4uj/css/files_texteditor','4ebecdcef2c2d8caa84532be9b7f3701',721,'files_texteditor',2,1,5799,1681985164,1681983708,0,0,'64410e8da0250',31,''),(768,8,'appdata_ocp2ck7yo4uj/css/files_trashbin','5f32bdfb01ceaf60105bd7ab5ef25a2d',721,'files_trashbin',2,1,642,1681985164,1681983708,0,0,'64410e8da0250',31,''),(769,8,'appdata_ocp2ck7yo4uj/css/files','c22100d5f13d55aac69ac2c832874652',721,'files',2,1,27898,1681985164,1681983708,0,0,'64410e8da0250',31,''),(770,8,'appdata_ocp2ck7yo4uj/css/core','e5da66b8fc55196b7f2a80eba5b06ea6',721,'core',2,1,129494,1681985164,1681983708,0,0,'64410e8da0250',31,''),(771,8,'appdata_ocp2ck7yo4uj/css/files_sharing','80ca69a7ea85a44940833be82f45fe5e',721,'files_sharing',2,1,3412,1681985164,1681983708,0,0,'64410e8da0250',31,''),(772,8,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.gzip','b61b61c25ad60bb4846e5b819a0e4baa',764,'2bf5-dd39-theming.css.gzip',15,8,307,1681983708,1681983708,0,0,'84ac0f53aa788863f46b918f293bf33b',27,''),(773,8,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css','684553ec9bd093db235f659c57b15af8',764,'2bf5-dd39-theming.css',16,11,724,1681983708,1681983708,0,0,'4418a52945046db14d04da977682762f',27,''),(774,8,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.deps','666a810bd01910cf9caa1734d6bbca52',764,'2bf5-dd39-theming.css.deps',14,8,132,1681983708,1681983708,0,0,'46a5d3586356df9a71c02aaa6e1c0e28',27,''),(775,8,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css','c32a464f00dea1e419cb2153dea8d226',765,'89a5-dd39-settings.css',16,11,15629,1681983708,1681983708,0,0,'d4d57f7d08a6e13c16c8078c3475fe46',27,''),(776,8,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.gzip','7509ce07ce4d63163a95c2d37570e5db',765,'89a5-dd39-settings.css.gzip',15,8,4343,1681983708,1681983708,0,0,'e035226adae353a53faa323013a80d72',27,''),(777,8,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.deps','70ca02cf7791d1bf0ab2ee6e6190a08d',765,'89a5-dd39-settings.css.deps',14,8,128,1681983708,1681983708,0,0,'b02f5d15152c347ca770639043d899b8',27,''),(778,8,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.gzip','4bcfbe4d23bea15b19cc1be6de2ab207',766,'1980-dd39-autocomplete.css.gzip',15,8,421,1681983708,1681983708,0,0,'80bd98bd354e06364722a1d374848f51',27,''),(779,8,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css','315c9a71ed6d9bfc55b67efa3639823c',766,'1980-dd39-autocomplete.css',16,11,964,1681983708,1681983708,0,0,'33d63a1e2a284aaee16a1c998eb85386',27,''),(780,8,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.deps','25caf287d9b6f3709e7fb0f6909daef3',766,'1980-dd39-autocomplete.css.deps',14,8,138,1681983708,1681983708,0,0,'ed234b35b6eb463c974073e977f13d79',27,''),(781,8,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.deps','bc43f6c94909d91c8c7b59e21fa67f61',767,'21f4-dd39-merged.css.deps',14,8,389,1681983708,1681983708,0,0,'0560f413137cfa8285eab9a6377467ea',27,''),(782,8,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.gzip','ccb5610cf876f5e2261d71e2f0e41b28',767,'21f4-dd39-merged.css.gzip',15,8,1262,1681983708,1681983708,0,0,'d3bcf809f4c26238e078fbe72eee0304',27,''),(783,8,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css','9e3066d1300a64bbe4c1bb0a1152a98e',767,'21f4-dd39-merged.css',16,11,4148,1681983708,1681983708,0,0,'a81e3885db0eb625961a194d1f5df8c5',27,''),(784,8,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.deps','ddc57b10ad9af48a3ed703e6db4d325d',768,'1980-dd39-trash.css.deps',14,8,137,1681983708,1681983708,0,0,'698e934ba7729511359b31041ddd94b2',27,''),(785,8,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css','cead350d6af9fb0d4b0acf5c57433d0a',768,'1980-dd39-trash.css',16,11,344,1681983708,1681983708,0,0,'52a966adec18b6f29454b8bb7d25e02e',27,''),(786,8,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.gzip','cd58401fa37b93e6d8ece58f6793ad07',768,'1980-dd39-trash.css.gzip',15,8,161,1681983708,1681983708,0,0,'bb8aa99b2edc038451e0532dea21c766',27,''),(787,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css','a7455aea2749eb5babf9882432e43052',769,'4fda-dd39-merged.css',16,11,18493,1681983708,1681983708,0,0,'d6a36ab92195db37dc17821af519e185',27,''),(788,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.gzip','8f74774bd8232922b5c77877921908e9',769,'4fda-dd39-upload.css.gzip',15,8,1016,1681983708,1681983708,0,0,'16b91d542388d318d8b35d016e6c055b',27,''),(789,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.gzip','7417c522a6b92d18106617a0fe0cd93c',769,'4fda-dd39-merged.css.gzip',15,8,4220,1681983708,1681983708,0,0,'952fa27f4a8b68e69a71432355a5484c',27,''),(790,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css','8d9996c9ec1c3019ac4ab8ba503e4f1d',769,'4fda-dd39-upload.css',16,11,3643,1681983708,1681983708,0,0,'7c117134ce7e3ac84dbbe7d023031ecc',27,''),(791,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.deps','4dc0211ca2cfd0dc15fa3b1c519d8443',769,'4fda-dd39-upload.css.deps',14,8,129,1681983708,1681983708,0,0,'cdea3a31df655a3116ba70835ae1577e',27,''),(792,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.deps','014a08cbc68d833047d7ed9ef00582d5',769,'4fda-dd39-merged.css.deps',14,8,397,1681983708,1681983708,0,0,'a6e92ed35af52bab5edce2d223711820',27,''),(793,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css','72958c1e1ceabf47c101dc39f746f375',770,'89a5-dd39-share.css',16,11,2685,1681983708,1681983708,0,0,'163718f6a7a4665b7ce65c9015ee48b2',27,''),(794,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.gzip','ca3d590ed9b309e4099c628c9ba37bfc',770,'89a5-dd39-systemtags.css.gzip',15,8,385,1681983708,1681983708,0,0,'38bc64ddbe1b5030803d725d945dab9e',27,''),(795,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.deps','54f3b5f5c223467ca699ffe87c8678d1',770,'89a5-dd39-systemtags.css.deps',14,8,126,1681983708,1681983708,0,0,'dd5eba060c138ac15f9850812c4dd87b',27,''),(796,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.deps','635838ae2dacee8a4eaacc0163b9ae97',770,'89a5-dd39-jquery-ui-fixes.css.deps',14,8,131,1681983708,1681983708,0,0,'66489741de15f934fb92b92fe739793a',27,''),(797,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.gzip','31f45e6cbf6c6109a5c543594268b0f7',770,'89a5-dd39-jquery-ui-fixes.css.gzip',15,8,846,1681983708,1681983708,0,0,'ef1403bc7d8348ee793833217e5f3e98',27,''),(798,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css','78bff418cb5b8be898905e26105f439d',770,'89a5-dd39-server.css',16,11,100134,1681983708,1681983708,0,0,'2c290a5e421556c10aca5fcae08ece5c',27,''),(799,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.deps','f5564a3bc71307439ccf0e75917842ec',770,'89a5-dd39-jquery.ocdialog.css.deps',14,8,131,1681983708,1681983708,0,0,'ae6914d61fe60f85ce46bc84a1e8558a',27,''),(800,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css','1dbfa0f3f5c9cfd5492550c33ffa95ff',770,'89a5-dd39-jquery-ui-fixes.css',16,11,4102,1681983708,1681983708,0,0,'b7e712873cb3c248883bd5effde0ffeb',27,''),(801,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.deps','baa462ce2528e7565d9e8e94861d584e',770,'89a5-dd39-server.css.deps',14,8,772,1681983708,1681983708,0,0,'4f52467be182db4c877d208f7c52d677',27,''),(802,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css','3dd40571f6f29d0a0c946dce61469748',770,'89a5-dd39-systemtags.css',16,11,1403,1681983708,1681983708,0,0,'f35dc1c385ab2e2ff413a683b2a8d637',27,''),(803,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css','86681b8bc226a647a798fea77f821f8b',770,'89a5-dd39-jquery.ocdialog.css',16,11,1253,1681983708,1681983708,0,0,'06dc477e9334d804a7518924c9cd7504',27,''),(804,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.gzip','05f6e2f8484349624c1cbb5c85ae1de5',770,'89a5-dd39-share.css.gzip',15,8,957,1681983708,1681983708,0,0,'28a0cc367a6940f84354776341957691',27,''),(805,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.gzip','88dd674bc368509aa3ea198c2f4626f9',770,'89a5-dd39-server.css.gzip',15,8,15888,1681983708,1681983708,0,0,'486be3def804c36e40f1a71432769d20',27,''),(806,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.deps','5e1996bb860233f579b5cc0685c018c8',770,'89a5-dd39-share.css.deps',14,8,121,1681983708,1681983708,0,0,'452d9f473a847e4cdea95178fe7061c1',27,''),(807,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.gzip','3d9bffd046bcb0bd6f922550fd1728e7',770,'89a5-dd39-jquery.ocdialog.css.gzip',15,8,560,1681983708,1681983708,0,0,'c5362d2a7fd8d31dab9b1f2b38072647',27,''),(808,8,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.deps','3d2dd721c9177b66fcb27c9ce23724b5',771,'35c3-dd39-mergedAdditionalStyles.css.deps',14,8,316,1681983708,1681983708,0,0,'db0ee15658a4938aea029388a30c2be3',27,''),(809,8,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css','bb2bcb3b5216a5bf08870b98ab8108a8',771,'35c3-dd39-mergedAdditionalStyles.css',16,11,2303,1681983708,1681983708,0,0,'30851a75ad9867e8a581bb8c72c6cff7',27,''),(810,8,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.gzip','d27a4c0564ba11157d932a143b719725',771,'35c3-dd39-mergedAdditionalStyles.css.gzip',15,8,793,1681983708,1681983708,0,0,'e414120fab01e82886e11c5b21370965',27,''),(811,8,'appdata_ocp2ck7yo4uj/appstore/apps.json','aea0a87bc9c12b12737ee801593ccba8',722,'apps.json',20,8,1087363,1681983708,1681983708,0,0,'cb612b341afb9606453a91945e05fdb9',27,''),(812,8,'appdata_ocp2ck7yo4uj/identityproof/user-vok','645643f49f95bb6a22d54366a668d7a2',723,'user-vok',2,1,4021,1681985164,1681983708,0,0,'64410e8da0250',31,''),(813,8,'appdata_ocp2ck7yo4uj/identityproof/user-vok/public','a004b7c46da2c2254f89de1e50172352',812,'public',14,8,451,1681983708,1681983708,0,0,'ce4e5c289871d5034db972bd6577ae78',27,''),(814,8,'appdata_ocp2ck7yo4uj/identityproof/user-vok/private','0eb8ad1208a413e19deb14433329e20e',812,'private',14,8,3570,1681983708,1681983708,0,0,'3a8ee4531d6afde9cd7501fe6af014e3',27,''),(815,8,'appdata_ocp2ck7yo4uj/js/comments','e4648049e6995efacee6f80096521231',724,'comments',2,1,80480,1681985164,1681983708,0,0,'64410e8da0250',31,''),(816,8,'appdata_ocp2ck7yo4uj/js/files_texteditor','8e34386727f9050bc69e0087ca33fd6a',724,'files_texteditor',2,1,839303,1681985165,1681983708,0,0,'64410e8da0250',31,''),(817,8,'appdata_ocp2ck7yo4uj/js/files_versions','80e7253edd9ea7831366f098c11d692a',724,'files_versions',2,1,16695,1681985165,1681983708,0,0,'64410e8da0250',31,''),(818,8,'appdata_ocp2ck7yo4uj/js/systemtags','ee888bbc00ce11acc6ae76273ea221c3',724,'systemtags',2,1,24010,1681985165,1681983708,0,0,'64410e8da0250',31,''),(819,8,'appdata_ocp2ck7yo4uj/js/notifications','b1b946b5fe6500cf3bfc4d9ba1503c65',724,'notifications',2,1,26448,1681985165,1681983708,0,0,'64410e8da0250',31,''),(820,8,'appdata_ocp2ck7yo4uj/js/activity','a9f39bf5207544df2d5b895a84618a1e',724,'activity',2,1,21189,1681985165,1681983708,0,0,'64410e8da0250',31,''),(821,8,'appdata_ocp2ck7yo4uj/js/gallery','0868b0a242e983dabdfec4727a60e7b9',724,'gallery',2,1,859739,1681985165,1681983708,0,0,'64410e8da0250',31,''),(822,8,'appdata_ocp2ck7yo4uj/js/files','3eb7143fc18697e4e2511eb3adbc241c',724,'files',2,1,420072,1681985165,1681983708,0,0,'64410e8da0250',31,''),(823,8,'appdata_ocp2ck7yo4uj/js/core','f14d31b7798843b58fe4e295bde30d57',724,'core',2,1,355676,1681985165,1681983708,0,0,'64410e8da0250',31,''),(824,8,'appdata_ocp2ck7yo4uj/js/files_sharing','7676e75f6ea45d19eb1827a880982349',724,'files_sharing',2,1,19382,1681985165,1681983708,0,0,'64410e8da0250',31,''),(825,8,'appdata_ocp2ck7yo4uj/js/comments/merged.js.deps','18af4dd92bc976d39f038edb58b42c61',815,'merged.js.deps',14,8,788,1681983708,1681983708,0,0,'4860d3566008a788bdc3362ffdc4f3f2',27,''),(826,8,'appdata_ocp2ck7yo4uj/js/comments/merged.js','406aebeee235fd67c35f4ebeb199dfe9',815,'merged.js',13,8,62511,1681983708,1681983708,0,0,'d57b7412e06ceba28371c04504cb2f5d',27,''),(827,8,'appdata_ocp2ck7yo4uj/js/comments/merged.js.gzip','0647ad3df0fb06da8883d8d7cb01b546',815,'merged.js.gzip',15,8,17181,1681983708,1681983708,0,0,'543397379c45056c6bfa3022a51948f8',27,''),(828,8,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.deps','000a73daa8b6451f2642c89504a07157',816,'merged.js.deps',14,8,346,1681983708,1681983708,0,0,'8ae4607e6afa41a201988418e291f506',27,''),(829,8,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js','ee0652870e1e2acecbadd6d595ab596e',816,'merged.js',13,8,699810,1681983708,1681983708,0,0,'4b9dd86383610e3fe6583963ccfa98b2',27,''),(830,8,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.gzip','f23ada379b79c7c7cab74dbb34067beb',816,'merged.js.gzip',15,8,139147,1681983708,1681983708,0,0,'4a1bddaaaa42b1e80e410415ec8cf425',27,''),(831,8,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.deps','dde041654f1d7b4eebc3a5c5e5c1d384',817,'merged.js.deps',14,8,394,1681983708,1681983708,0,0,'348f33812909dc37e4fb529e61130399',27,''),(832,8,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js','9f3977c48c9a7a5bee51eb406a30ab06',817,'merged.js',13,8,12719,1681983708,1681983708,0,0,'f163cef37a40b61ce485f0fd4d0d65ec',27,''),(833,8,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.gzip','ceff3f6abe8b8b5f8268c4188be76a80',817,'merged.js.gzip',15,8,3582,1681983708,1681983708,0,0,'8db3640a75d4cff46d62bfc980a28373',27,''),(834,8,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.deps','b0e572499441370a48b9f9765d801f8e',818,'merged.js.deps',14,8,459,1681983708,1681983708,0,0,'9b2689ff764b9923363d1571b2a60203',27,''),(835,8,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js','01377a4b3140da5070416af63e7d8c4c',818,'merged.js',13,8,18223,1681983708,1681983708,0,0,'9aac570363557b07c029782ec1baa2db',27,''),(836,8,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.gzip','d8d4c0de28d2f5c53f1f945631fa5152',818,'merged.js.gzip',15,8,5328,1681983708,1681983708,0,0,'a888400d853ba8ff35d002589cf1beaa',27,''),(837,8,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.deps','0d71ebc93bb58de39f0db5e2fba9c879',819,'merged.js.deps',14,8,306,1681983708,1681983708,0,0,'6d5f91633daca43ff8c70f16c4e3a80f',27,''),(838,8,'appdata_ocp2ck7yo4uj/js/notifications/merged.js','7f97a755d5335843011a8c28e0dc4578',819,'merged.js',13,8,20888,1681983708,1681983708,0,0,'8290013901bd214bca8a5699d009b6d1',27,''),(839,8,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.gzip','3e6adf3e96590e9411ded0cf69a9cbeb',819,'merged.js.gzip',15,8,5254,1681983708,1681983708,0,0,'a6662c6812534b26cee3e5c1e99a8665',27,''),(840,8,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.gzip','ab64a435f0684089954a1503a4915c2f',820,'activity-sidebar.js.gzip',15,8,4351,1681983708,1681983708,0,0,'afe0aaea9b1db65d94c8a5c4146f3d88',27,''),(841,8,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js','5b53ceef539881b19ca36f7752013cfb',820,'activity-sidebar.js',13,8,16380,1681983708,1681983708,0,0,'b0846456484cbfeaf0f8f4c4b68625d3',27,''),(842,8,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.deps','fb3cfe9931d0a1786203ab7ccddec138',820,'activity-sidebar.js.deps',14,8,458,1681983708,1681983708,0,0,'db4b0cc7ee48c01ae77848071fa45c5d',27,''),(843,8,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.deps','195df1a1ff873a95bbbe8f3accf90d45',821,'merged.js.deps',14,8,2069,1681983708,1681983708,0,0,'4e2fe96c87d69d75aceafbef1c1cdd20',27,''),(844,8,'appdata_ocp2ck7yo4uj/js/gallery/merged.js','fdc2b62e3b85db6ebc8c9fd2be985c0e',821,'merged.js',13,8,451474,1681983708,1681983708,0,0,'9135e01ee25d7c14cddd69d12056f136',27,''),(845,8,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js','f82849093986c955349f665498cef5fa',821,'scripts-for-file-app.js',13,8,227059,1681983708,1681983708,0,0,'2a4356b5152d8d75d502bc7b64f13586',27,''),(846,8,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.gzip','86129e9991d4a6f79317be5e30ec6f3b',821,'merged.js.gzip',15,8,123682,1681983708,1681983708,0,0,'99fe62608099bb9e49e175cb60197088',27,''),(847,8,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.gzip','4aa0f263d2fc5e9829ce1349ab3e2fbd',821,'scripts-for-file-app.js.gzip',15,8,54659,1681983708,1681983708,0,0,'9db09a1540b2d255888006230e5e6fe2',27,''),(848,8,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.deps','8cee64de4107b037e19a6e0001274792',821,'scripts-for-file-app.js.deps',14,8,796,1681983708,1681983708,0,0,'5247e104d425e3f8616bb4e243909e0a',27,''),(849,8,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.deps','adfa388cbf45f6b392d7960797e5880e',822,'merged-index.js.deps',14,8,1957,1681983708,1681983708,0,0,'54616e5d7fa98db1e0b34f469be5cc53',27,''),(850,8,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.gzip','b5b4e4222a3423e2acb28c7699cd86b6',822,'merged-index.js.gzip',15,8,80185,1681983708,1681983708,0,0,'cf0d426641ccb13cc0ff83941b875766',27,''),(851,8,'appdata_ocp2ck7yo4uj/js/files/merged-index.js','ea21876dfbd6b920e81f7bc213706376',822,'merged-index.js',13,8,337930,1681983708,1681983708,0,0,'43062eb275ccf42d6cb61efd570cdbe4',27,''),(852,8,'appdata_ocp2ck7yo4uj/js/core/merged.js.deps','0672ccdef2d9384a411dd0895964d57b',823,'merged.js.deps',14,8,472,1681983708,1681983708,0,0,'4b0c8f19e2e1e2ebda8ea132f398889c',27,''),(853,8,'appdata_ocp2ck7yo4uj/js/core/merged.js','bf362a7fdbb6f0c38deadb41e68acde7',823,'merged.js',13,8,20224,1681983708,1681983708,0,0,'93072fbf8ef71a5eb6de2cac94ea12b4',27,''),(854,8,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.deps','3b52fc8139d5575cb46dffc48d35120b',823,'merged-login.js.deps',14,8,247,1681983708,1681983708,0,0,'a99ef95a112ce6485d425a177112add7',27,''),(855,8,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.gzip','996d183e420339a81267d7d48a42842b',823,'merged-template-prepend.js.gzip',15,8,40626,1681983708,1681983708,0,0,'57cec5b58911f5cbe11ebef25df1d902',27,''),(856,8,'appdata_ocp2ck7yo4uj/js/core/merged-login.js','1286373dbb5f6e81fb13b4f5c0843496',823,'merged-login.js',13,8,7563,1681983708,1681983708,0,0,'a1e7096f98060cd5b8ba4501cea210d9',27,''),(857,8,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.gzip','b654b22875e4d115d122da548cb98865',823,'merged-login.js.gzip',15,8,2276,1681983708,1681983708,0,0,'c1a2b8ff23a6ddd69958e4340e6c87ec',27,''),(858,8,'appdata_ocp2ck7yo4uj/js/core/merged.js.gzip','3e19032edbb0a5d54a4d25606c53c3c8',823,'merged.js.gzip',15,8,5365,1681983708,1681983708,0,0,'4259bcf0fa5456ba4f2e027ddc51714e',27,''),(859,8,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.gzip','64df495640c2992fa06f955533ba6a75',823,'merged-share-backend.js.gzip',15,8,22728,1681983708,1681983708,0,0,'5b907fe37d7c80ed0a33450d084710c9',27,''),(860,8,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.deps','49447c4a958dfb13087fc02658d2161d',823,'merged-template-prepend.js.deps',14,8,1180,1681983708,1681983708,0,0,'1f18c84ffc4d26cee0929cda6c31cedd',27,''),(861,8,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js','579d4b039f255abe4dbed89444d56310',823,'merged-template-prepend.js',13,8,148486,1681983708,1681983708,0,0,'5de3cf132d62952dc696a76a1497c248',27,''),(862,8,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.deps','dced322d42d4338d1a49cb39c9f9e53a',823,'merged-share-backend.js.deps',14,8,692,1681983708,1681983708,0,0,'16e8057c9a56b1153950a6c880c4df8e',27,''),(863,8,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js','2d6f839fff5c210df1345235e03f12b7',823,'merged-share-backend.js',13,8,105817,1681983708,1681983708,0,0,'f17ec2761f35652b048a9431fc015f0b',27,''),(864,8,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js','253b8ab876fa052002ac07f2240a1656',824,'additionalScripts.js',13,8,14539,1681983708,1681983708,0,0,'a9dc84676d0c3d809597ced0a657c3b9',27,''),(865,8,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.deps','342d284054df0c2482eac265157e4431',824,'additionalScripts.js.deps',14,8,316,1681983708,1681983708,0,0,'bfe81404cfab6db9e3eb5f7000295d66',27,''),(866,8,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.gzip','51d06c1265f8fff99a85a72ac1cf63d4',824,'additionalScripts.js.gzip',15,8,4527,1681983708,1681983708,0,0,'d3c51e905b1bfac8b5c4b82feda8afff',27,''),(867,8,'appdata_ocp2ck7yo4uj/preview/343','c173e600f50af55367d655c4b562ad62',725,'343',2,1,8817,1681985165,1681983708,0,0,'64410e8da0250',31,''),(868,8,'appdata_ocp2ck7yo4uj/preview/251','decab0cb33d2a303801bc42cf78f89cb',725,'251',2,1,0,1681983708,1681983708,0,0,'64410e8d296b9',31,''),(869,8,'appdata_ocp2ck7yo4uj/preview/246','8734b661d9ef2e41975596cbb7aea946',725,'246',2,1,180145,1681985165,1681983708,0,0,'64410e8da0250',31,''),(870,8,'appdata_ocp2ck7yo4uj/preview/237','2659a3cdbcb2a96b3b3cdbf0a5df6f7f',725,'237',2,1,12496,1681985165,1681983708,0,0,'64410e8da0250',31,''),(871,8,'appdata_ocp2ck7yo4uj/preview/6','825339ba134f3c814c79a812ec5d826a',725,'6',2,1,595528,1681985165,1681983708,0,0,'64410e8da0250',31,''),(872,8,'appdata_ocp2ck7yo4uj/preview/241','23fce7333eee9a2ebfdabc3ce0221a0f',725,'241',2,1,815617,1681985165,1681983708,0,0,'64410e8da0250',31,''),(873,8,'appdata_ocp2ck7yo4uj/preview/12','a2d7ac5cb617daa0d638f3fa78cbbd51',725,'12',2,1,180145,1681985165,1681983708,0,0,'64410e8da0250',31,''),(874,8,'appdata_ocp2ck7yo4uj/preview/239','6ea1f2e4fabd223bb8b6ad53d41a7fb1',725,'239',2,1,335671,1681985165,1681983708,0,0,'64410e8da0250',31,''),(875,8,'appdata_ocp2ck7yo4uj/preview/193','461e154eff9f19485709a164ac7546df',725,'193',2,1,0,1681983708,1681983708,0,0,'64410e8d29867',31,''),(876,8,'appdata_ocp2ck7yo4uj/preview/240','66538cbe3c339e6ff7cd619d5e10bb67',725,'240',2,1,595528,1681985165,1681983708,0,0,'64410e8da0250',31,''),(877,8,'appdata_ocp2ck7yo4uj/preview/253','5c622a885af00ec11d18377abac472d4',725,'253',2,1,0,1681983708,1681983708,0,0,'64410e8d298e0',31,''),(878,8,'appdata_ocp2ck7yo4uj/preview/3','cc5565c4ba6fd56871cbbe502dfe977c',725,'3',2,1,13632,1681985165,1681983708,0,0,'64410e8da0250',31,''),(879,8,'appdata_ocp2ck7yo4uj/preview/5','eaca83211066f046a8313d0843f8f854',725,'5',2,1,335671,1681985165,1681983708,0,0,'64410e8da0250',31,''),(880,8,'appdata_ocp2ck7yo4uj/preview/152','d3564f308ae3aa82990f73f8a55fd41e',725,'152',2,1,30985,1681985165,1681983708,0,0,'64410e8da0250',31,''),(881,8,'appdata_ocp2ck7yo4uj/preview/332','b7c45cd7a53996abde18890823dc48ad',725,'332',2,1,572509,1681985165,1681983708,0,0,'64410e8da0250',31,''),(882,8,'appdata_ocp2ck7yo4uj/preview/7','803e2a3dffd0cdf960192b097016e362',725,'7',2,1,815617,1681985165,1681983708,0,0,'64410e8da0250',31,''),(883,8,'appdata_ocp2ck7yo4uj/preview/343/193-96-max.png','80cbfcbdf7d4e4f6479db76cd03b7f53',867,'193-96-max.png',4,3,6148,1681983708,1681983708,0,0,'1080401b863dbc239cbfdbb244065b00',27,''),(884,8,'appdata_ocp2ck7yo4uj/preview/343/64-64-crop.png','cf421d8bcfb182c5e22efabc2ca3c89e',867,'64-64-crop.png',4,3,2669,1681983708,1681983708,0,0,'01af5f5d9932f4d0f9db974a759a515a',27,''),(885,8,'appdata_ocp2ck7yo4uj/preview/246/4096-4096-max.png','f01f0d365843fde49e45c08290be1516',869,'4096-4096-max.png',4,3,153221,1681983708,1681983708,0,0,'327e66431006a544e86079e30950408d',27,''),(886,8,'appdata_ocp2ck7yo4uj/preview/246/64-64-crop.png','7fe853813378f83155fba05db04dbaf3',869,'64-64-crop.png',4,3,4748,1681983708,1681983708,0,0,'65e4901fcf3086c98d829252ac63f987',27,''),(887,8,'appdata_ocp2ck7yo4uj/preview/246/256-256-crop.png','4871a26802ed1eb1e25d671f5b1718c6',869,'256-256-crop.png',4,3,22176,1681983708,1681983708,0,0,'36d989dbc90cf0bba9e0a45fcce6d7ad',27,''),(888,8,'appdata_ocp2ck7yo4uj/preview/237/193-96-max.png','50d08586a69e0184430ad8cc773a0c95',870,'193-96-max.png',4,3,6148,1681983708,1681983708,0,0,'6f75754116db11d808317d19f2f093ca',27,''),(889,8,'appdata_ocp2ck7yo4uj/preview/237/64-64-crop.png','9368924db841411cae5010b37e353748',870,'64-64-crop.png',4,3,2669,1681983708,1681983708,0,0,'f2b35437b7f301bb23f4c9beeb042600',27,''),(890,8,'appdata_ocp2ck7yo4uj/preview/237/96-96-crop.png','234d24f23ec4cb27e343e4657a87b498',870,'96-96-crop.png',4,3,3679,1681983708,1681983708,0,0,'c716f6f097bc3d8988c5012404cf3af5',27,''),(891,8,'appdata_ocp2ck7yo4uj/preview/6/64-64-crop.jpg','ae9b59d54212644239c4b8823bee2d1d',871,'64-64-crop.jpg',5,3,3420,1681983708,1681983708,0,0,'1d92f358a97f3e2004d95208070e2502',27,''),(892,8,'appdata_ocp2ck7yo4uj/preview/6/256-256-crop.jpg','3d70421864eda45bdf9ca5029d09ff9c',871,'256-256-crop.jpg',5,3,21265,1681983708,1681983708,0,0,'f4f17177c70774cc4d2b65862594fc94',27,''),(893,8,'appdata_ocp2ck7yo4uj/preview/6/2000-1333-max.jpg','f934b86c92bf5aa5b42031f2f87f5ad4',871,'2000-1333-max.jpg',5,3,570843,1681983708,1681983708,0,0,'cee53563abeb7d006f2eff4d5c524728',27,''),(894,8,'appdata_ocp2ck7yo4uj/preview/241/64-64-crop.jpg','38014ff76499410a401d139c0a37b5f2',872,'64-64-crop.jpg',5,3,2929,1681983708,1681983708,0,0,'bce024d1af1d43ba6f727e8884c56102',27,''),(895,8,'appdata_ocp2ck7yo4uj/preview/241/256-256-crop.jpg','98d8c52e04c5bbe4db451b129c85eef4',872,'256-256-crop.jpg',5,3,23541,1681983708,1681983708,0,0,'c18ca791caaedb298a15d401c546b700',27,''),(896,8,'appdata_ocp2ck7yo4uj/preview/241/2000-1333-max.jpg','377a6f6a22649ef4fa2e7802496a4aaa',872,'2000-1333-max.jpg',5,3,789147,1681983708,1681983708,0,0,'a4e129c0edff9ac7e7db18500cadbe5c',27,''),(897,8,'appdata_ocp2ck7yo4uj/preview/12/4096-4096-max.png','96a2825d9dd0f53d9edd6e78d4b13f59',873,'4096-4096-max.png',4,3,153221,1681983708,1681983708,0,0,'e0c79a023867b015fd14b9282828f34f',27,''),(898,8,'appdata_ocp2ck7yo4uj/preview/12/64-64-crop.png','ad96ec8ee7f3c392ed81f564e110fd6f',873,'64-64-crop.png',4,3,4748,1681983708,1681983708,0,0,'df26082603d0cb35c3a6aff7216a45a2',27,''),(899,8,'appdata_ocp2ck7yo4uj/preview/12/256-256-crop.png','80986e4e6dbfe180dd06dfd6de914e1e',873,'256-256-crop.png',4,3,22176,1681983708,1681983708,0,0,'f05650a2a2bc6b9433071347c10aeb20',27,''),(900,8,'appdata_ocp2ck7yo4uj/preview/239/64-64-crop.jpg','65a3deb5ecae8673330bf52779ca6387',874,'64-64-crop.jpg',5,3,2543,1681983708,1681983708,0,0,'fc6b821d428346bac5d11b1c3056f5e4',27,''),(901,8,'appdata_ocp2ck7yo4uj/preview/239/1100-734-max.jpg','34f53b48ebf376f90df62b71d4ddae26',874,'1100-734-max.jpg',5,3,308351,1681983708,1681983708,0,0,'fbc2b216bbaa1dc770d0602116a81956',27,''),(902,8,'appdata_ocp2ck7yo4uj/preview/239/256-256-crop.jpg','886334752446814dcc2b231022778ee9',874,'256-256-crop.jpg',5,3,24777,1681983708,1681983708,0,0,'4f58770a5fb1dc99d3bd0873d9c788b2',27,''),(903,8,'appdata_ocp2ck7yo4uj/preview/240/64-64-crop.jpg','0c9a17ebcebc388d5f438daa79585b05',876,'64-64-crop.jpg',5,3,3420,1681983708,1681983708,0,0,'ad14745e7440370c4509ab69f040028b',27,''),(904,8,'appdata_ocp2ck7yo4uj/preview/240/256-256-crop.jpg','bbd758a5e04a2ed32cec068ffe7a9cbc',876,'256-256-crop.jpg',5,3,21265,1681983708,1681983708,0,0,'9d5c3ae21ca781785533e0043f4a7142',27,''),(905,8,'appdata_ocp2ck7yo4uj/preview/240/2000-1333-max.jpg','109640fa4bd202334f7dbbde430c14c2',876,'2000-1333-max.jpg',5,3,570843,1681983708,1681983708,0,0,'a706d5e40775568289feb3ec85d2c0a0',27,''),(906,8,'appdata_ocp2ck7yo4uj/preview/3/32-32-crop.png','089ced233440fe37d36fb62dac5497c0',878,'32-32-crop.png',4,3,1136,1681983708,1681983708,0,0,'6a7bba77da59de098c0d6c5f3c68a699',27,''),(907,8,'appdata_ocp2ck7yo4uj/preview/3/193-96-max.png','56da6505a652ce6ec813851f00831d3a',878,'193-96-max.png',4,3,6148,1681983708,1681983708,0,0,'32601d57d6faf77a247bc10bd56f1904',27,''),(908,8,'appdata_ocp2ck7yo4uj/preview/3/64-64-crop.png','2beb2205135ee5e848582beea9a83e2a',878,'64-64-crop.png',4,3,2669,1681983708,1681983708,0,0,'4e207381d4ee77353099a4683dc06c12',27,''),(909,8,'appdata_ocp2ck7yo4uj/preview/3/96-96-crop.png','bbd949a06059ebc98eab286a0a5e0f3d',878,'96-96-crop.png',4,3,3679,1681983708,1681983708,0,0,'4e0e2e967e4a0eaef4a5f781cfea1c39',27,''),(910,8,'appdata_ocp2ck7yo4uj/preview/5/64-64-crop.jpg','0540a52b35796b32c2efc19f39d3dbb0',879,'64-64-crop.jpg',5,3,2543,1681983708,1681983708,0,0,'87336cca7b1b27303f17ac4744f8035f',27,''),(911,8,'appdata_ocp2ck7yo4uj/preview/5/1100-734-max.jpg','9263bb56a9b788110fa9893a71d48566',879,'1100-734-max.jpg',5,3,308351,1681983708,1681983708,0,0,'e0c125ecb0ca4d299f1ec3cf67b90166',27,''),(912,8,'appdata_ocp2ck7yo4uj/preview/5/256-256-crop.jpg','856c67cb75dbd07fb50abeace7c52b97',879,'256-256-crop.jpg',5,3,24777,1681983708,1681983708,0,0,'e009dc0667fafa22473d52468da3fdf0',27,''),(913,8,'appdata_ocp2ck7yo4uj/preview/152/4096-4096-max.png','e993f925a7782bf3037be3a606399ccd',880,'4096-4096-max.png',4,3,26048,1681983708,1681983708,0,0,'964936254cb21a9e679af95963acecf5',27,''),(914,8,'appdata_ocp2ck7yo4uj/preview/152/32-32-crop.png','7a9f0a0a8785feade277994e00380778',880,'32-32-crop.png',4,3,397,1681983708,1681983708,0,0,'9038a8dd938ae5e90eb75be325368303',27,''),(915,8,'appdata_ocp2ck7yo4uj/preview/152/64-64-crop.png','082f6736b1f106ad596c51a2489663b6',880,'64-64-crop.png',4,3,644,1681983708,1681983708,0,0,'e0c56277c760d2a15e5088d9f024d44c',27,''),(916,8,'appdata_ocp2ck7yo4uj/preview/152/256-256-crop.png','e43ed5fc69ef670c38697cdb7d776f79',880,'256-256-crop.png',4,3,3896,1681983708,1681983708,0,0,'bb9f50c82e1cebab89be48d7c25cf3d2',27,''),(917,8,'appdata_ocp2ck7yo4uj/preview/332/64-64-crop.jpg','f79b7f498e60cee0ce0053afdb7cec0a',881,'64-64-crop.jpg',5,3,2443,1681983708,1681983708,0,0,'b7ad885e47c91a03b6478f607800fea9',27,''),(918,8,'appdata_ocp2ck7yo4uj/preview/332/2560-1440-max.jpg','e223ae728a3547ba402cb5e883945301',881,'2560-1440-max.jpg',5,3,570066,1681983708,1681983708,0,0,'63aa729f525721e2c2e641b35e660113',27,''),(919,8,'appdata_ocp2ck7yo4uj/preview/7/64-64-crop.jpg','7a7f631972b2911536a530b3786f2f39',882,'64-64-crop.jpg',5,3,2929,1681983708,1681983708,0,0,'eb99107d367a3886c2c883c0996fee09',27,''),(920,8,'appdata_ocp2ck7yo4uj/preview/7/256-256-crop.jpg','21fa823be8e84c0851bf52f9808645eb',882,'256-256-crop.jpg',5,3,23541,1681983708,1681983708,0,0,'3ab40093b0aadfdfe4d88e51926849cf',27,''),(921,8,'appdata_ocp2ck7yo4uj/preview/7/2000-1333-max.jpg','0be546cd23b5453d595aa086facd9409',882,'2000-1333-max.jpg',5,3,789147,1681983708,1681983708,0,0,'05a3a5042de436c516b4696bd1200071',27,''),(922,8,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e','3ef75959df1ffce11a451390f7801a6a',726,'43c2025c19f23a73a880300144252f7e',2,1,16536,1681985165,1681983708,0,0,'64410e8da0250',31,''),(923,8,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72','141f595693cdf6f12110583a7998ba56',726,'7f3e57a2397831bc58cb185e0becdc72',2,1,14912,1681985165,1681983708,0,0,'64410e8da0250',31,''),(924,8,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f','23789bb835b044e302311e35484613a6',726,'7a6e71f6fabbda1da4a96ba84be4329f',2,1,6927,1681985165,1681983708,0,0,'64410e8da0250',31,''),(925,8,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de','bbbf0016034ecc5a6096c8b52da6b8b7',726,'7641eb079d4bafa5e2024031c65e28de',2,1,6925,1681985165,1681983708,0,0,'64410e8da0250',31,''),(926,8,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766','1619c8ef2c9355e72c8b5bf03aeaeecc',726,'274857af206af2cd22deae7c56f80766',2,1,6922,1681985165,1681983708,0,0,'64410e8da0250',31,''),(927,8,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.png','3e07f6fba42704c88db57c8943598eff',922,'photo.png',4,3,16074,1681983708,1681983708,0,0,'099d213b47f65ab3ef4e80f9be325a16',27,''),(928,8,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.32.png','f144ba0fbdec0447f39f03e4a8939d44',922,'photo.32.png',4,3,462,1681983708,1681983708,0,0,'f0dddef36ddbfb69a889c74198f75054',27,''),(929,8,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.png','6a4bff6c7c59d57e1e88f93db2059b40',923,'photo.png',4,3,14490,1681983708,1681983708,0,0,'bb40ce9115363e8cb29daf00b5455584',27,''),(930,8,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.32.png','a21e27f03088f448ee3fcd45b61158d5',923,'photo.32.png',4,3,422,1681983708,1681983708,0,0,'a1ffebbb7d4908f8af8b279a0eb234ef',27,''),(931,8,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.png','695384511c2fa6076a55b047624a7fd9',924,'photo.png',4,3,6661,1681983708,1681983708,0,0,'23dc05f01a1f7a4d80b962e1513ed2ec',27,''),(932,8,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.32.png','c5c4f3df014addc24a28b254941e8b46',924,'photo.32.png',4,3,266,1681983708,1681983708,0,0,'c3acc4c68a7cd3c705c4d6b0d7b04997',27,''),(933,8,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.png','75e1798c3f63cfc444cd65c58a550396',925,'photo.png',4,3,6662,1681983708,1681983708,0,0,'c83d0932bc4d95d15cabff4f3a49771e',27,''),(934,8,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.32.png','6141d37c71dd6fb7888d80b871c78c13',925,'photo.32.png',4,3,263,1681983708,1681983708,0,0,'2b6f9c4feba1753f9b3517b67a1510f8',27,''),(935,8,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.png','c22bc875ab14a2f3b737b1c87bec25df',926,'photo.png',4,3,6658,1681983708,1681983708,0,0,'1e965cadddc9a61337b7ffd227d93ae4',27,''),(936,8,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.32.png','cc6bc3653657a63f28ac9ad9968055e1',926,'photo.32.png',4,3,264,1681983708,1681983708,0,0,'6adee0a336152a12b2c80ffda9983aec',27,''),(937,8,'testuser2/cache','25e025547959d130da07135e7b1daea8',670,'cache',2,1,0,1681983708,1681983708,0,0,'64410e8d8b2f2',31,''),(938,8,'testuser2/files','c34aaebb1c5efff95e3799f0a64394e4',670,'files',2,1,9760893,1681985165,1681983708,0,0,'64410e8da0250',31,''),(939,8,'testuser2/files/Nextcloud Manual.pdf','c61e503f3a8ca531ea4e58216715efd7',938,'Nextcloud Manual.pdf',9,8,4540242,1681983708,1681983708,0,0,'7f920cc5649c6747cb4dbf9857baf46e',27,''),(940,8,'testuser2/files/Documents','3922f16317c8b4d2932440f8f523249b',938,'Documents',2,1,78496,1681985165,1681983708,0,0,'64410e8da0250',31,''),(941,8,'testuser2/files/Subnetting (Post-Routing).pkt','534946c7bb9befafe322cc8169db34ba',938,'Subnetting (Post-Routing).pkt',14,8,68687,1681983708,1681983708,0,0,'1025024c0e022a578bea4cdb82b9a8c5',27,''),(942,8,'testuser2/files/Subnetting.pkt','9e2d9cbc356d08ff3c7925b5df8837d6',938,'Subnetting.pkt',14,8,68661,1681983708,1681983708,0,0,'b55b5bf61520579368facad0f0334673',27,''),(943,8,'testuser2/files/Subnetting.pdf','8c2098393b505bd06d69ac5517ed4bd8',938,'Subnetting.pdf',9,8,683310,1681983708,1681983708,0,0,'f15e181a1fcee8fb0f676dbd1a65fd54',27,''),(944,8,'testuser2/files/Subnetting.docx','ea980273753172e2743d5dfceb20418e',938,'Subnetting.docx',18,8,1270172,1681983708,1681983708,0,0,'9223108dae41e3d4284b852da28c9557',27,''),(945,8,'testuser2/files/Subnetting Table.xlsx','f5db61c3a9cf2a0f7ec53f85739cf149',938,'Subnetting Table.xlsx',21,8,11500,1681983708,1681983708,0,0,'15d5d87b21c5cf6c34d2de780335b11a',27,''),(946,8,'testuser2/files/Photos','60a08dae24901f4201e6f2a6f284e1f3',938,'Photos',2,1,2360011,1681985165,1681983708,0,0,'64410e8da0250',31,''),(947,8,'testuser2/files/05_subnetting.pdf','30ae8620f9fce8cf69b2431f7356739b',938,'05_subnetting.pdf',9,8,210523,1681983708,1681983708,0,0,'0e432ababb781df926a369a7e0eac428',27,''),(948,8,'testuser2/files/bbw-logo.png','d85200a5494abdd12e6c732be4c3ff7f',938,'bbw-logo.png',4,3,6878,1681983708,1681983708,0,0,'887520800e0d33cf475fcef4cac90fde',27,''),(949,8,'testuser2/files/Nextcloud.mp4','39551c55592e24f9303e2a1c226229f6',938,'Nextcloud.mp4',7,6,462413,1681983708,1681983708,0,0,'a91c1d2411beeb590f7271fd9d2b102a',27,''),(950,8,'testuser2/files/Documents/About.odt','193acb451e48790773ba6adb9b1a8351',940,'About.odt',10,8,77422,1681983708,1681983708,0,0,'91c8f7854eab0eff59702feb9fdf7ab8',27,''),(951,8,'testuser2/files/Documents/About.txt','6ad7ca0f4c6c2a9c0b3ef4aa8d719366',940,'About.txt',12,11,1074,1681983708,1681983708,0,0,'0c4c2f3f55ff1f1a28e99e8082cd835b',27,''),(952,8,'testuser2/files/Photos/Nut.jpg','a369608d8c01360667988e068a7e3c80',946,'Nut.jpg',5,3,955026,1681983708,1681983708,0,0,'7225fe6dfa1e04aaaf18572df9cb630b',27,''),(953,8,'testuser2/files/Photos/Coast.jpg','0256930786d22986d1b97445ddc69ddb',946,'Coast.jpg',5,3,819766,1681983708,1681983708,0,0,'b2c90d2984135c91198e957118a86096',27,''),(954,8,'testuser2/files/Photos/Hummingbird.jpg','7f9d5e528e174f5d4766263c0c2a8132',946,'Hummingbird.jpg',5,3,585219,1681983708,1681983708,0,0,'f9325078cf912d78e50edc35fe6ccfab',27,''),(955,3,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,0,1681985165,1681985165,0,0,'64410e8dafeb6',23,''),(956,4,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,0,1681985165,1681985165,0,0,'64410e8db994c',23,''),(957,6,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,0,1681985165,1681985165,0,0,'64410e8dc347c',23,''),(958,8,'appdata_oc5ev4nda8xn/js/core/merged-template-prepend.js','9effb1e71fd98e7914c5bed7aa0f8945',564,'merged-template-prepend.js',13,8,11396,1682686187,1682686187,0,0,'12b0cc545d89de7696a832e4852266b8',27,''),(959,8,'appdata_oc5ev4nda8xn/js/core/merged-template-prepend.js.deps','2c54a9552a9ce49b9a7aa581aa12d8df',564,'merged-template-prepend.js.deps',14,8,310,1682686187,1682686187,0,0,'3ece1fc84c067fbe07f92f40f0827c45',27,''),(960,8,'appdata_oc5ev4nda8xn/js/core/merged-template-prepend.js.gzip','f94dd47a20d50d68f7bb727b45b3cc44',564,'merged-template-prepend.js.gzip',15,8,2982,1682686188,1682686188,0,0,'a55a4ccbbb651d41950db21d0e61e573',27,''),(961,8,'appdata_oc5ev4nda8xn/css/theming/39b9-88d1-theming.css','664022fc72135d705d710cb47a352e67',650,'39b9-88d1-theming.css',16,11,1377,1682686188,1682686188,0,0,'3f86b5ffd4780e20a3d55392b2ed8b0b',27,''),(962,8,'appdata_oc5ev4nda8xn/css/theming/39b9-88d1-theming.css.deps','87d97a653aa7a173b80d05a0db5aca02',650,'39b9-88d1-theming.css.deps',14,8,227,1682686188,1682686188,0,0,'ecf439fd756e47f13841fe458f199356',27,''),(963,8,'appdata_oc5ev4nda8xn/css/theming/39b9-88d1-theming.css.gzip','5855071cc55ac4b7f550fa03cd359a74',650,'39b9-88d1-theming.css.gzip',15,8,482,1682686188,1682686188,0,0,'9a012bc2a2a538efdeffe8ef6d722170',27,''),(964,8,'appdata_oc5ev4nda8xn/dashboard','ff1cc210d143beaa3588265e7f4bf5f7',366,'dashboard',2,1,0,1682686195,1682686195,0,0,'644bc0f3b1ecc',31,''),(965,8,'appdata_oc5ev4nda8xn/dashboard/vok','0ac2deac47bccffdce79dcdf944bccda',964,'vok',2,1,0,1682686195,1682686195,0,0,'644bc0f3b9f57',31,''),(966,8,'appdata_oc5ev4nda8xn/js/activity/activity-sidebar.js','22433d952077fa8e358ffeb78507093d',582,'activity-sidebar.js',13,8,39092,1682686195,1682686195,0,0,'b1c1fc3a500e7cd431982c8b380b0558',27,''),(967,8,'appdata_oc5ev4nda8xn/js/activity/activity-sidebar.js.deps','5dc18182ef8c8299eb2a0c0c9ab231b6',582,'activity-sidebar.js.deps',14,8,604,1682686195,1682686195,0,0,'ec1b71b11f4eed582d78daa933d0f0f5',27,''),(968,8,'appdata_oc5ev4nda8xn/js/activity/activity-sidebar.js.gzip','6a391a5c95fffff18ef54de42efe13ed',582,'activity-sidebar.js.gzip',15,8,5776,1682686195,1682686195,0,0,'d95d926f1440336691b1e7e7dd49ba8a',27,''),(969,8,'appdata_oc5ev4nda8xn/css/icons/icons-vars.css','989bafd75d521b9220a6b87405d6c564',660,'icons-vars.css',16,11,158963,1682686235,1682686235,0,0,'e124e22c56e3757d5ce113b70527e957',27,''),(970,8,'appdata_oc5ev4nda8xn/css/icons/icons-list.template','e5ef76db298eefe65c901decf59f2dee',660,'icons-list.template',14,8,18609,1682686235,1682686235,0,0,'a5babde707d828ecb624788614156ebe',27,''),(971,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-server.css','965ebe6de356c8c8d5c1b345a592d842',614,'217b-88d1-server.css',16,11,137463,1682686196,1682686196,0,0,'e6f63972d3445b12d3ba79616b75121c',27,''),(972,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-server.css.deps','61e26a05076775d03a6cf5f977095c1d',614,'217b-88d1-server.css.deps',14,8,983,1682686196,1682686196,0,0,'ff4bbb14e970e2cf0958c21db3f863f4',27,''),(973,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-server.css.gzip','746ad649d0281822bdc15531876b9e32',614,'217b-88d1-server.css.gzip',15,8,19573,1682686196,1682686196,0,0,'9003504690c9d525fb7a80209b06eba1',27,''),(974,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-css-variables.css','9271d4360ace1957f85a378836b93fb6',614,'217b-88d1-css-variables.css',16,11,1380,1682686196,1682686196,0,0,'a27616bd3037ef65973613304effdf16',27,''),(975,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-css-variables.css.deps','da3021690aec93e1d21cd8b307625275',614,'217b-88d1-css-variables.css.deps',14,8,224,1682686196,1682686196,0,0,'90ba0c0a08f4b88c8c0829c46bfd810f',27,''),(976,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-css-variables.css.gzip','5aa1e7186069968547dec5ca84b51390',614,'217b-88d1-css-variables.css.gzip',15,8,574,1682686196,1682686196,0,0,'488cc5d64f8d21d4a31e349cb30d63a2',27,''),(977,8,'appdata_oc5ev4nda8xn/css/notifications','059def1fbf0452351aed448d79661e45',613,'notifications',2,1,0,1682686196,1682686196,0,0,'644bc0f47e546',31,''),(978,8,'appdata_oc5ev4nda8xn/css/notifications/0577-88d1-styles.css','1e43e19d0704bcee09dcb4d1e51c044b',977,'0577-88d1-styles.css',16,11,3455,1682686196,1682686196,0,0,'573ec55da8a986e0afa83cd38baa271b',27,''),(979,8,'appdata_oc5ev4nda8xn/css/notifications/0577-88d1-styles.css.deps','6dccb405ed2b68845f596aa29be2ad12',977,'0577-88d1-styles.css.deps',14,8,232,1682686196,1682686196,0,0,'8d0b1e3077da9fcd381a1fdafc72cfc2',27,''),(980,8,'appdata_oc5ev4nda8xn/css/notifications/0577-88d1-styles.css.gzip','a5ecc103c662c156f9943f1ac5a2b515',977,'0577-88d1-styles.css.gzip',15,8,846,1682686196,1682686196,0,0,'66a6b8e9ee9ab937d744f5be359c8a01',27,''),(981,8,'appdata_oc5ev4nda8xn/css/dashboard','1b447bae63904c3af7c0d78bf0ae2390',613,'dashboard',2,1,0,1682686196,1682686196,0,0,'644bc0f4afdb4',31,''),(982,8,'appdata_oc5ev4nda8xn/css/dashboard/d149-88d1-dashboard.css','a96f8aff0770bc164f617a76e56dc32b',981,'d149-88d1-dashboard.css',16,11,2062,1682686196,1682686196,0,0,'028d8fcc3427bb7bc8c5b0546a1d7b95',27,''),(983,8,'appdata_oc5ev4nda8xn/css/dashboard/d149-88d1-dashboard.css.deps','6537d52f026aacfdfb7c001ed3202466',981,'d149-88d1-dashboard.css.deps',14,8,231,1682686196,1682686196,0,0,'57330a7f8336cb64ac3522d670258b53',27,''),(984,8,'appdata_oc5ev4nda8xn/css/dashboard/d149-88d1-dashboard.css.gzip','bf492df6b07c39f7d2fba731775a7931',981,'d149-88d1-dashboard.css.gzip',15,8,602,1682686196,1682686196,0,0,'3f184120b8a011c20d29dd8cb218ce08',27,''),(985,8,'appdata_oc5ev4nda8xn/css/activity','1d0abf98834a8af3938dc23f4339c632',613,'activity',2,1,0,1682686197,1682686197,0,0,'644bc0f4d7de7',31,''),(986,8,'appdata_oc5ev4nda8xn/css/activity/a3ad-88d1-style.css','b9824e19d42cf38503a35075a33d4972',985,'a3ad-88d1-style.css',16,11,3621,1682686196,1682686196,0,0,'8546f23620d96099fcfca44f6ccf0497',27,''),(987,8,'appdata_oc5ev4nda8xn/css/activity/a3ad-88d1-style.css.deps','f9077f3059d43d1efc3a1b2bea3e7b4a',985,'a3ad-88d1-style.css.deps',14,8,226,1682686197,1682686197,0,0,'e8772f28498d74af9314dace6aa53d45',27,''),(988,8,'appdata_oc5ev4nda8xn/css/activity/a3ad-88d1-style.css.gzip','c8a1c14375e22f135e3bb7da70a6edd2',985,'a3ad-88d1-style.css.gzip',15,8,1168,1682686197,1682686197,0,0,'e0364be9ff9c760dad4ee3dcb3bac59f',27,''),(989,8,'appdata_oc5ev4nda8xn/css/text','ad5860464f12d88b8796f54dddf26ada',613,'text',2,1,0,1682686197,1682686197,0,0,'644bc0f512ec2',31,''),(990,8,'appdata_oc5ev4nda8xn/css/text/232d-88d1-icons.css','79a5afa261566637b111c48ac60cfe77',989,'232d-88d1-icons.css',16,11,2710,1682686197,1682686197,0,0,'da6ec7c77e9759dfd9db89e17dd5e045',27,''),(991,8,'appdata_oc5ev4nda8xn/css/text/232d-88d1-icons.css.deps','fd9bcdf70c3ca6a63cad1ddae67e9ff4',989,'232d-88d1-icons.css.deps',14,8,222,1682686197,1682686197,0,0,'0fdd766c6b94832c907dddaf5a6ef64c',27,''),(992,8,'appdata_oc5ev4nda8xn/css/text/232d-88d1-icons.css.gzip','a76d7be208e87ac4c949172b51ba6c08',989,'232d-88d1-icons.css.gzip',15,8,417,1682686197,1682686197,0,0,'b090996b098ccfaecbfa93360aa938d9',27,''),(993,8,'appdata_oc5ev4nda8xn/css/user_status','79a00b3c0d70694bb5be0112a80cd5d1',613,'user_status',2,1,0,1682686197,1682686197,0,0,'644bc0f546376',31,''),(994,8,'appdata_oc5ev4nda8xn/css/user_status/3acc-88d1-user-status-menu.css','214d9ace11c7df9aa909bdeadc123e8a',993,'3acc-88d1-user-status-menu.css',16,11,1039,1682686197,1682686197,0,0,'9e5f1f556c2d7222d603d1d9e4ec04a9',27,''),(995,8,'appdata_oc5ev4nda8xn/css/user_status/3acc-88d1-user-status-menu.css.deps','e03173147d1d3f64d9acf285968bc048',993,'3acc-88d1-user-status-menu.css.deps',14,8,240,1682686197,1682686197,0,0,'4d31fb5de85888e1583ea74e0bdb4516',27,''),(996,8,'appdata_oc5ev4nda8xn/css/user_status/3acc-88d1-user-status-menu.css.gzip','30d477996737d4b94c1719ff6d553ce9',993,'3acc-88d1-user-status-menu.css.gzip',15,8,240,1682686197,1682686197,0,0,'0cda1fab041277051d6d55ecfac7f829',27,''),(997,8,'appdata_oc5ev4nda8xn/avatar/vok','7cdd10e1808b3f6421ba4cb3c308f090',569,'vok',2,1,0,1682686211,1682686211,0,0,'644bc0f69dc69',31,''),(998,8,'appdata_oc5ev4nda8xn/avatar/vok/avatar.png','6bfd0f546724f25d23d4e8871d155168',997,'avatar.png',4,3,15778,1682686198,1682686198,0,0,'2cf884b0e777392f2efcf178447da8f6',27,''),(999,8,'appdata_oc5ev4nda8xn/avatar/vok/generated','b86eafef5d4392d314d4a7c092daa1ef',997,'generated',14,8,0,1682686198,1682686198,0,0,'f90915ce29332aa2dd56d187863a38ab',27,''),(1000,8,'appdata_oc5ev4nda8xn/avatar/vok/avatar.32.png','0d04c58ed1376281155df00925126bc8',997,'avatar.32.png',4,3,428,1682686198,1682686198,0,0,'2072e13e1a4bd88d9a3d940a6983a91f',27,''),(1001,8,'appdata_oc5ev4nda8xn/preview/c','73cffc8efc8f1cf742bd1c5628be88f6',566,'c',2,1,0,1682686203,1682686203,0,0,'644bc0fbab142',31,''),(1002,8,'appdata_oc5ev4nda8xn/preview/c/2','136c82a02693c4013bf4c84d677b5c6a',1001,'2',2,1,0,1682686203,1682686203,0,0,'644bc0fbaaff8',31,''),(1003,8,'appdata_oc5ev4nda8xn/preview/c/2/4','1b5271adabaf767218762334b7d9e550',1002,'4',2,1,0,1682686203,1682686203,0,0,'644bc0fbaaeac',31,''),(1004,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c','163f948c9f503413c3887427703aaf5e',1003,'c',2,1,0,1682686203,1682686203,0,0,'644bc0fbaad5f',31,''),(1005,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d','da00993a7f5b609e75e18df64c617a77',1004,'d',2,1,0,1682686203,1682686203,0,0,'644bc0fbaac10',31,''),(1006,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d/7','2a6d02df27b4830db3d098a10d13169d',1005,'7',2,1,0,1682686203,1682686203,0,0,'644bc0fbaaabd',31,''),(1007,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d/7/6','fa8799daa01be66d7539467865834b2a',1006,'6',2,1,0,1682686203,1682686203,0,0,'644bc0fbaa966',31,''),(1008,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d/7/6/253','dde0c2bbb8e14bc6cf3cf2b67956b79b',1007,'253',2,1,0,1682686203,1682686203,0,0,'644bc0fbaa7b0',31,''),(1009,8,'appdata_oc5ev4nda8xn/theming','778764207f0011a0b5fd4e58ffa1fb88',366,'theming',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3c3c',31,''),(1010,8,'appdata_oc5ev4nda8xn/preview/1','8b1f797f2e2b6f05a82a1f03e57f7d2e',566,'1',2,1,0,1682686203,1682686203,0,0,'644bc0fbc42c2',31,''),(1011,8,'appdata_oc5ev4nda8xn/preview/1/9','6a4c81f7cd41d924b0c209de3c9bebb6',1010,'9',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3e35',31,''),(1012,8,'appdata_oc5ev4nda8xn/preview/1/9/f','3537dc16c8d505d307dcf126b4b33e41',1011,'f',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3b49',31,''),(1013,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3','0c18c73260a76b0859ee8cbfbeb6c9a8',1012,'3',2,1,0,1682686203,1682686203,0,0,'644bc0fbc39a3',31,''),(1014,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c','9c51e145c85c00b6fd22c8c3f521c952',1013,'c',2,1,0,1682686203,1682686203,0,0,'644bc0fbc37e2',31,''),(1015,8,'appdata_oc5ev4nda8xn/theming/1','377c5c0291c13e8b40c14a7ed7823d48',1009,'1',2,1,0,1682686210,1682686210,0,0,'644bc0fbcccf4',31,''),(1016,8,'appdata_oc5ev4nda8xn/preview/c/0','9748c43066027a0d72f8fccf6473c7fd',1001,'0',2,1,0,1682686203,1682686203,0,0,'644bc0fbcca02',31,''),(1017,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c/d','dbb7540087cc83f9dd8e2b10762741ee',1014,'d',2,1,0,1682686203,1682686203,0,0,'644bc0fbc35f0',31,''),(1018,8,'appdata_oc5ev4nda8xn/preview/c/0/4','e08ef4fca4e7a8258b2651ae3304c0d5',1016,'4',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc89a',31,''),(1019,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c/d/3','61d49f1ade091eb39a7d1ec373a71358',1017,'3',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3489',31,''),(1020,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2','d02473f62e64a6756ad67f563d1229c1',1018,'2',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc6e8',31,''),(1021,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c/d/3/251','764ab1d2f2e032f325f4cff05363d476',1019,'251',2,1,0,1682686203,1682686203,0,0,'644bc0fbc32a8',31,''),(1022,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f','cd2a5fe1690db3016da052d9e2666c40',1020,'f',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc536',31,''),(1023,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4','b5bbb80ee92205c8dfa6fadec5057b9c',1022,'4',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc3b2',31,''),(1024,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_video.svg','2daa5b0a1f9df0bc7ad8c22b9f20057e',1015,'icon-core-filetypes_video.svg',17,3,277,1682686203,1682686203,0,0,'ff8ea34247bbb5197313516cabf01f6c',27,''),(1025,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d','f05f598cbfa8d987e5dd65dc36ce2018',1023,'d',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc227',31,''),(1026,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332','538cb3e814e253859767c750c376ee4d',1025,'332',2,1,0,1682686237,1682686237,0,0,'644bc0fbcc003',31,''),(1027,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_text.svg','b316b012b2eb01b8a4a73719faa8300d',1015,'icon-core-filetypes_text.svg',17,3,295,1682686203,1682686203,0,0,'c893451b70b16f5522888169fb0958f8',27,''),(1028,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_image.svg','479173dfdcd2d84d914a18ec50ac56a2',1015,'icon-core-filetypes_image.svg',17,3,352,1682686203,1682686203,0,0,'e0f72f2e9bd9d57aa415b81ee1751848',27,''),(1029,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_application-pdf.svg','2e8157c21ec007ba5b8c9797c971cb45',1015,'icon-core-filetypes_application-pdf.svg',17,3,1054,1682686204,1682686204,0,0,'899133eeb8588c69cd47bb5e1c7fe720',27,''),(1030,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332/2560-1440-max.jpg','4bf91c82cc8cc0c098f472dfea5d6fbe',1026,'2560-1440-max.jpg',5,3,469369,1682686204,1682686204,0,0,'cee7b16a20e501b55e5b5f7c9ce726d5',27,''),(1031,8,'appdata_oc5ev4nda8xn/preview/3/8','298ff0cfef3ca7b55a3d27cec6b9f985',654,'8',2,1,0,1682686204,1682686204,0,0,'644bc0fc38038',31,''),(1032,8,'appdata_oc5ev4nda8xn/preview/3/8/d','58d891abfea6c7ef1ab1f0b1e5a47228',1031,'d',2,1,0,1682686204,1682686204,0,0,'644bc0fc37eca',31,''),(1033,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b','878d9bb9dc455941d188613a79f953bc',1032,'b',2,1,0,1682686204,1682686204,0,0,'644bc0fc37d60',31,''),(1034,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3','ffb726b41f127967109daf9e87fe7a28',1033,'3',2,1,0,1682686204,1682686204,0,0,'644bc0fc37bc2',31,''),(1035,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a','975dde1e7c6307f6ee9692adcb165ed8',1034,'a',2,1,0,1682686204,1682686204,0,0,'644bc0fc3791c',31,''),(1036,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e','b49e0e68ec2550937c27875b25259dc7',1035,'e',2,1,0,1682686204,1682686204,0,0,'644bc0fc377be',31,''),(1037,8,'appdata_oc5ev4nda8xn/preview/5','9c24dd2a1975c400e3cf4b5ce76aff7a',566,'5',2,1,0,1682686237,1682686237,0,0,'644bc29540ef1',31,''),(1038,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e/246','db025b6f148fab7715ff73c256943951',1036,'246',2,1,0,1682686204,1682686204,0,0,'644bc0fc374fd',31,''),(1039,8,'appdata_oc5ev4nda8xn/preview/5/3','da1ac45568fd34887978a6fde6c40bad',1037,'3',2,1,0,1682686204,1682686204,0,0,'644bc0fc3f853',31,''),(1040,8,'appdata_oc5ev4nda8xn/preview/5/3/9','2a44886a5515d03c08674308cbac152b',1039,'9',2,1,0,1682686204,1682686204,0,0,'644bc0fc3f259',31,''),(1041,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f','3dc023dbaa644c544e0c4358046cf4de',1040,'f',2,1,0,1682686204,1682686204,0,0,'644bc0fc3f0e7',31,''),(1042,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d','06f889511128b46560688c568fbb6352',1041,'d',2,1,0,1682686204,1682686204,0,0,'644bc0fc3ef81',31,''),(1043,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5','e1f445aab9b54d4ecebf21a38b5e8fb9',1042,'5',2,1,0,1682686204,1682686204,0,0,'644bc0fc3ede0',31,''),(1044,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3','014ed8092a168e0d34f2c0da3f64fe7c',1043,'3',2,1,0,1682686204,1682686204,0,0,'644bc0fc3ec05',31,''),(1045,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237','d8a5781c42643770d220b4c8f826b941',1044,'237',2,1,0,1682686231,1682686231,0,0,'644bc0fc3e9c4',31,''),(1046,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237/193-96-max.png','592cd9cc2749e3f278d95df277548cae',1045,'193-96-max.png',4,3,6148,1682686204,1682686204,0,0,'2625f5535b4e9106560d75bc026d9569',27,''),(1047,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237/64-64-crop.png','c60a27a1ad5b94346e2e0ee88e6f9625',1045,'64-64-crop.png',4,3,2669,1682686204,1682686204,0,0,'1598bd5ff26e7388706236596e2b4199',27,''),(1048,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332/64-64-crop.jpg','59a4cdbc73320dddf83d08eb68070fec',1026,'64-64-crop.jpg',5,3,2096,1682686204,1682686204,0,0,'4988e5c80afdfceba7f5014cf7daf19a',27,''),(1049,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e/246/4096-4096-max.png','f69cf7158bc1574382040ddacf2ef5f8',1038,'4096-4096-max.png',4,3,115061,1682686204,1682686204,0,0,'4bc4d9cfa21078ad2ab06e3d55cd7856',27,''),(1050,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e/246/64-64-crop.png','44736c42c274117930196dab411f9642',1038,'64-64-crop.png',4,3,2669,1682686204,1682686204,0,0,'610e97335ce270861fe7cc9df935bc36',27,''),(1051,8,'appdata_oc5ev4nda8xn/dav-photocache/7f3e57a2397831bc58cb185e0becdc72','fc237aafd67c81663ad6b981d6eff899',663,'7f3e57a2397831bc58cb185e0becdc72',2,1,0,1682686209,1682686209,0,0,'644bc10198927',31,''),(1052,8,'appdata_oc5ev4nda8xn/dav-photocache/7641eb079d4bafa5e2024031c65e28de','ec74647052fc5f5e004864f7362e46f7',663,'7641eb079d4bafa5e2024031c65e28de',2,1,0,1682686209,1682686209,0,0,'644bc1019bab1',31,''),(1053,8,'appdata_oc5ev4nda8xn/dav-photocache/502776188a7bf675bc6b6065c729f8dd','129aea9fe6f1bbdc51d07358eb50564b',663,'502776188a7bf675bc6b6065c729f8dd',2,1,0,1682686209,1682686209,0,0,'644bc1019e09f',31,''),(1054,8,'appdata_oc5ev4nda8xn/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f','2db23e2341726b0382775803be1a4305',663,'7a6e71f6fabbda1da4a96ba84be4329f',2,1,0,1682686209,1682686209,0,0,'644bc1019f709',31,''),(1055,8,'appdata_oc5ev4nda8xn/dav-photocache/274857af206af2cd22deae7c56f80766','e4b47b95d2e7b6acdfaff0f40daae7a0',663,'274857af206af2cd22deae7c56f80766',2,1,0,1682686209,1682686209,0,0,'644bc101a1631',31,''),(1056,8,'appdata_oc5ev4nda8xn/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.png','d965e898cac10d6d8810594bdd427372',1051,'photo.png',4,3,14490,1682686209,1682686209,0,0,'0d0c820e1f223bc7aa88ea4b181b6bba',27,''),(1057,8,'appdata_oc5ev4nda8xn/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.png','11c58085e0a16b59c801bc6923b3dbbf',1052,'photo.png',4,3,6662,1682686209,1682686209,0,0,'96e8650b1197301bfa2609703b21ccce',27,''),(1058,8,'appdata_oc5ev4nda8xn/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.png','72c054cf1419df9d11e2c142a6208977',1054,'photo.png',4,3,6661,1682686209,1682686209,0,0,'9d782c2ba2842e0b3b3201c2aac02479',27,''),(1059,8,'appdata_oc5ev4nda8xn/dav-photocache/502776188a7bf675bc6b6065c729f8dd/photo.png','b4e73f3a5aebd7b9148ac32e98464833',1053,'photo.png',4,3,3958,1682686209,1682686209,0,0,'52510e88d02c9452dd34575040846efb',27,''),(1060,8,'appdata_oc5ev4nda8xn/dav-photocache/274857af206af2cd22deae7c56f80766/photo.png','0d3a83c8d2e7c4f2bdbc33b095594d40',1055,'photo.png',4,3,6658,1682686209,1682686209,0,0,'83035e8b68dad588fac7a65a597f9fd0',27,''),(1061,8,'appdata_oc5ev4nda8xn/dav-photocache/502776188a7bf675bc6b6065c729f8dd/photo.32.png','1e35a9baed5ec643bc56362710459d24',1053,'photo.32.png',4,3,1095,1682686209,1682686209,0,0,'301a1c871de6a99caf192a00934ff2bb',27,''),(1062,8,'appdata_oc5ev4nda8xn/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.32.png','2c0c12f3d079b5666a95800395b15e24',1051,'photo.32.png',4,3,426,1682686209,1682686209,0,0,'430e342cf1948a449e9c00163342da24',27,''),(1063,8,'appdata_oc5ev4nda8xn/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.32.png','9ad2bad26b1da2a7e469a3858f98ab20',1052,'photo.32.png',4,3,268,1682686209,1682686209,0,0,'77f08d50a0028ba97fca8a05aab1ab35',27,''),(1064,8,'appdata_oc5ev4nda8xn/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.32.png','c6c986582dea25f932244eb51535a317',1054,'photo.32.png',4,3,266,1682686209,1682686209,0,0,'5d9d3c6ecb1b5d283177d392d34746b7',27,''),(1065,8,'appdata_oc5ev4nda8xn/dav-photocache/274857af206af2cd22deae7c56f80766/photo.32.png','157aa2d746bf32141b456cd0bccc741f',1055,'photo.32.png',4,3,264,1682686209,1682686209,0,0,'1a99c13d8b274b1b4ce890e557d8ce00',27,''),(1066,8,'appdata_oc5ev4nda8xn/css/settings','0d634086313f09160cd61697d7a97093',613,'settings',2,1,0,1682686210,1682686210,0,0,'644bc10232b3c',31,''),(1067,8,'appdata_oc5ev4nda8xn/css/settings/62ab-88d1-settings.css','2da95c2bac9fa977a1cda68000a980db',1066,'62ab-88d1-settings.css',16,11,33068,1682686210,1682686210,0,0,'1e53c4543768c9c2b837d252fde00108',27,''),(1068,8,'appdata_oc5ev4nda8xn/css/settings/62ab-88d1-settings.css.deps','5cd2b3110390fd74dc12dfdca9ccb918',1066,'62ab-88d1-settings.css.deps',14,8,229,1682686210,1682686210,0,0,'00c3ff33f5318eb0ffd75deda14c9146',27,''),(1069,8,'appdata_oc5ev4nda8xn/css/settings/62ab-88d1-settings.css.gzip','b877eb60896e0c1ca5d01d9ce19c8e72',1066,'62ab-88d1-settings.css.gzip',15,8,6051,1682686210,1682686210,0,0,'7865cd8b21322d1f73df34c59a07f453',27,''),(1070,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_folder.svg','a51135983e90dd53f6c50e43436efb8e',1015,'icon-core-filetypes_folder.svg',17,3,255,1682686210,1682686210,0,0,'9d485030eaad135fcfe04596f4772cab',27,''),(1071,8,'appdata_oc5ev4nda8xn/avatar/vok/avatar.145.png','73a48939a70e3186ada0d922c9eefca0',997,'avatar.145.png',4,3,1593,1682686211,1682686211,0,0,'ef27fac5245808c80fea1be7ebbe26c8',27,''),(1072,8,'appdata_oc5ev4nda8xn/js/files/merged-index.js','8d016b31518709a093420277a31f1c1b',578,'merged-index.js',13,8,410613,1682686230,1682686230,0,0,'e7c661510790e6a528f2ea5887725cec',27,''),(1073,8,'appdata_oc5ev4nda8xn/js/files/merged-index.js.deps','6dcded04e2c2f2e488c24d3ce04e9c4a',578,'merged-index.js.deps',14,8,2520,1682686230,1682686230,0,0,'aed573c0a6a124612827fdc87bf32a0d',27,''),(1074,8,'appdata_oc5ev4nda8xn/js/files/merged-index.js.gzip','47d73058475b371232f077bdbf420e3e',578,'merged-index.js.gzip',15,8,92688,1682686230,1682686230,0,0,'0b62558d1e1ae88a7f04ad3c22efc7c9',27,''),(1075,8,'appdata_oc5ev4nda8xn/css/files/d71e-88d1-merged.css','fc809cc86dd5d628730f7d1452a0b7d9',627,'d71e-88d1-merged.css',16,11,29015,1682686230,1682686230,0,0,'f80a0932039cad3b6ae8172e23ca2614',27,''),(1076,8,'appdata_oc5ev4nda8xn/css/files/d71e-88d1-merged.css.deps','6f9f10278d8c447ec3674b385c6d84e0',627,'d71e-88d1-merged.css.deps',14,8,608,1682686230,1682686230,0,0,'c038360631b146c04dd2082588b8859c',27,''),(1077,8,'appdata_oc5ev4nda8xn/css/files/d71e-88d1-merged.css.gzip','e70964f3634422a1e5ff98c80bb751eb',627,'d71e-88d1-merged.css.gzip',15,8,5676,1682686230,1682686230,0,0,'d6b77032b095259c2f08f2fdc913014a',27,''),(1078,8,'appdata_oc5ev4nda8xn/css/files_sharing/4472-88d1-icons.css','60605a4078fb40f85b4acce437349332',639,'4472-88d1-icons.css',16,11,174,1682686230,1682686230,0,0,'4cda81f42c874e49507df4a97c32bc4e',27,''),(1079,8,'appdata_oc5ev4nda8xn/css/files_sharing/4472-88d1-icons.css.deps','3da0212633e69bb74411632f3e60b5e5',639,'4472-88d1-icons.css.deps',14,8,231,1682686230,1682686230,0,0,'e5cc9e38038d06f92abfd79f54e9b943',27,''),(1080,8,'appdata_oc5ev4nda8xn/css/files_sharing/4472-88d1-icons.css.gzip','19c07666b4707784a6481cc794adfa01',639,'4472-88d1-icons.css.gzip',15,8,102,1682686230,1682686230,0,0,'61dfc992be0f732656f4494d32c36e4b',27,''),(1081,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237/96-96-crop.png','39e2051293f791055b590db88ce1ef6f',1045,'96-96-crop.png',4,3,3679,1682686231,1682686231,0,0,'a974063662b72d7d41b1f6b75fb27549',27,''),(1082,8,'appdata_oc5ev4nda8xn/css/photos','cb477a0f514211f49499a8a8053fafe4',613,'photos',2,1,0,1682686235,1682686235,0,0,'644bc11ae9d2f',31,''),(1083,8,'appdata_oc5ev4nda8xn/css/photos/b0e8-88d1-icons.css','ee81fde7b112637d5a5fe9337050ee6c',1082,'b0e8-88d1-icons.css',16,11,383,1682686235,1682686235,0,0,'4d63f40df1e061edb286e54e06f03509',27,''),(1084,8,'appdata_oc5ev4nda8xn/css/photos/b0e8-88d1-icons.css.deps','242f760c53f8489edf0423b25450bffe',1082,'b0e8-88d1-icons.css.deps',14,8,224,1682686235,1682686235,0,0,'b7d67b5ab86ad78c952518c48d7fee73',27,''),(1085,8,'appdata_oc5ev4nda8xn/css/photos/b0e8-88d1-icons.css.gzip','555dd648e7284fd19eddb32e27263dca',1082,'b0e8-88d1-icons.css.gzip',15,8,130,1682686235,1682686235,0,0,'905917da08e4e60c8957c15fdc330d54',27,''),(1086,8,'appdata_oc5ev4nda8xn/preview/e','05bac600b86ca7cd4c478e7ce45d9e33',566,'e',2,1,0,1682686237,1682686237,0,0,'644bc11d3f127',31,''),(1087,8,'appdata_oc5ev4nda8xn/preview/e/4','7314d9bff20ccd03c045054841fbf0de',1086,'4',2,1,0,1682686237,1682686237,0,0,'644bc11d3efc7',31,''),(1088,8,'appdata_oc5ev4nda8xn/preview/e/4/a','619bddfeda8304f44c050cb91c3ecdcc',1087,'a',2,1,0,1682686237,1682686237,0,0,'644bc11d3ee68',31,''),(1089,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6','4f0a2264af2e04dde3bcf079ade7ee42',1088,'6',2,1,0,1682686237,1682686237,0,0,'644bc11d3ec3e',31,''),(1090,8,'appdata_oc5ev4nda8xn/preview/3/3','c137c93e40ac52264d3886199d504a9f',654,'3',2,1,0,1682686237,1682686237,0,0,'644bc11d453aa',31,''),(1091,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2','5fc916fc3840729706bb7f2bf2d420b6',1089,'2',2,1,0,1682686237,1682686237,0,0,'644bc11d3ea1a',31,''),(1092,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2/2','ada38adc00c417eda15dad2d95f26cd6',1091,'2',2,1,0,1682686237,1682686237,0,0,'644bc11d3e8c3',31,''),(1093,8,'appdata_oc5ev4nda8xn/preview/5/5','906f5d5fc6e3ec85fef21bf67768b840',1037,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d47916',31,''),(1094,8,'appdata_oc5ev4nda8xn/preview/f','2a24336e7910030a4aa20a735af123cd',566,'f',2,1,0,1682686237,1682686237,0,0,'644bc11d46acb',31,''),(1095,8,'appdata_oc5ev4nda8xn/preview/3/3/5','c11292a509166e36d8a6644074335b4b',1090,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d451d0',31,''),(1096,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2/2/2','e39be89b81c0a9fbfa2d1d7c8b4636a2',1092,'2',2,1,0,1682686237,1682686237,0,0,'644bc11d3e766',31,''),(1097,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f','c8ff62d488e0d9e45a4fdc631cb755ca',1095,'f',2,1,0,1682686237,1682686237,0,0,'644bc11d44841',31,''),(1098,8,'appdata_oc5ev4nda8xn/preview/5/5/5','93ed0a17a835769a163a45266f8bc9b8',1093,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d477b3',31,''),(1099,8,'appdata_oc5ev4nda8xn/preview/f/3','b36cd38e2d7a3bda4b13100cf9c54f89',1094,'3',2,1,0,1682686237,1682686237,0,0,'644bc11d4637a',31,''),(1100,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2/2/2/242','a8aa5fb579b46e32794ca1bae7403b74',1096,'242',2,1,0,1682686237,1682686237,0,0,'644bc11d3e58e',31,''),(1101,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d','d709785b9a83d28884868649846439e9',1098,'d',2,1,0,1682686237,1682686237,0,0,'644bc11d4764e',31,''),(1102,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5','3e80e5896bf2e9f52a651939e4713837',1097,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d446d5',31,''),(1103,8,'appdata_oc5ev4nda8xn/preview/f/3/4','0a3f4616c0bf712a7a72eae91fa8c42c',1099,'4',2,1,0,1682686237,1682686237,0,0,'644bc11d46119',31,''),(1104,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6','a3605101d55776df21a0c0d8414fd6a8',1101,'6',2,1,0,1682686237,1682686237,0,0,'644bc11d474c5',31,''),(1105,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3','93ac55058f67cfd58ef84f593a100470',1102,'3',2,1,0,1682686237,1682686237,0,0,'644bc11d44561',31,''),(1106,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0','3e6c7ecfb42490efe28240f8b83c4d8e',1103,'0',2,1,0,1682686237,1682686237,0,0,'644bc11d45f8c',31,''),(1107,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7','6568d2500636056bfde908006448d7c2',1104,'7',2,1,0,1682686237,1682686237,0,0,'644bc11d4735a',31,''),(1108,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5','6615529d6e1a5fd5100d633491e581e8',1105,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d443dd',31,''),(1109,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0','122c6131b915bd9c104fa60b2e086213',1107,'0',2,1,0,1682686237,1682686237,0,0,'644bc11d471d6',31,''),(1110,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f','13eb43950e987c8783f908745c7c5836',1106,'f',2,1,0,1682686237,1682686237,0,0,'644bc11d45e21',31,''),(1111,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5/240','1ecbd12aa3477318f398b3a71baaf74d',1108,'240',2,1,0,1682686237,1682686237,0,0,'644bc11d4417d',31,''),(1112,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0/239','3ba598cccb300add52f7fb49f4f4b6a4',1109,'239',2,1,0,1682686237,1682686237,0,0,'644bc11d46fab',31,''),(1113,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1','59bd20912676287131598bb0ef9d68c3',1110,'1',2,1,0,1682686237,1682686237,0,0,'644bc11d45be8',31,''),(1114,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b','5ed9eeb876065da45d70d3eb9f36ff40',1113,'b',2,1,0,1682686237,1682686237,0,0,'644bc11d45a5d',31,''),(1115,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b/241','33abe9706ae05f3fd5f5ad9d3858306f',1114,'241',2,1,0,1682686237,1682686237,0,0,'644bc11d4588a',31,''),(1116,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332/256-144.jpg','502848f994e015d78197eefae79af3cb',1026,'256-144.jpg',5,3,9045,1682686237,1682686237,0,0,'9b7656961a6ac7416cc22857802f6d49',27,''),(1117,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0/239/1100-734-max.jpg','2e2b69be3a01817e8edded2733e7660c',1112,'1100-734-max.jpg',5,3,272443,1682686237,1682686237,0,0,'95cc361e535f8c5c754fe262ac915fab',27,''),(1118,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5/240/2000-1333-max.jpg','8a95e2f606c99d2f17198072eaa32b99',1111,'2000-1333-max.jpg',5,3,461107,1682686237,1682686237,0,0,'6629266739a223a7ff34ebd6741a9dce',27,''),(1119,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0/239/256-171.jpg','178f586c937c5de85d67867ae30ebad5',1112,'256-171.jpg',5,3,16279,1682686237,1682686237,0,0,'5b2ef2363fd60abd7ccef82f76179cef',27,''),(1120,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b/241/2000-1333-max.jpg','5d117a2ed3eb4a92b84ae7e429bf6d3b',1115,'2000-1333-max.jpg',5,3,665863,1682686237,1682686237,0,0,'a99b651e5c919465efb83e52efa82427',27,''),(1121,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5/240/256-171.jpg','367988441c72d5c7dcac61ed104aa309',1111,'256-171.jpg',5,3,12711,1682686237,1682686237,0,0,'c1a31bcfb93d41d5baa464b944edf8d6',27,''),(1122,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b/241/256-171.jpg','fc746e7a26c6acd044b5e9ab3c4c4ae2',1115,'256-171.jpg',5,3,13404,1682686237,1682686237,0,0,'92d5b204608d27aa2f777a7435d6c1c8',27,'');
/*!40000 ALTER TABLE `oc_filecache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_filecache_extended`
--

DROP TABLE IF EXISTS `oc_filecache_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_filecache_extended` (
  `fileid` bigint unsigned NOT NULL,
  `metadata_etag` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `creation_time` bigint NOT NULL DEFAULT '0',
  `upload_time` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileid`),
  KEY `fce_ctime_idx` (`creation_time`),
  KEY `fce_utime_idx` (`upload_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_filecache_extended`
--

LOCK TABLES `oc_filecache_extended` WRITE;
/*!40000 ALTER TABLE `oc_filecache_extended` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_filecache_extended` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_files_trash`
--

DROP TABLE IF EXISTS `oc_files_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_files_trash` (
  `auto_id` bigint NOT NULL AUTO_INCREMENT,
  `id` varchar(250) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `user` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `timestamp` varchar(12) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `location` varchar(512) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` varchar(4) COLLATE utf8mb4_bin DEFAULT NULL,
  `mime` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`),
  KEY `id_index` (`id`),
  KEY `timestamp_index` (`timestamp`),
  KEY `user_index` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_files_trash`
--

LOCK TABLES `oc_files_trash` WRITE;
/*!40000 ALTER TABLE `oc_files_trash` DISABLE KEYS */;
INSERT INTO `oc_files_trash` VALUES (1,'deletedfile.txt','admin','1678356431','.',NULL,NULL);
/*!40000 ALTER TABLE `oc_files_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_flow_checks`
--

DROP TABLE IF EXISTS `oc_flow_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_flow_checks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `operator` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `value` longtext COLLATE utf8mb4_bin,
  `hash` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flow_unique_hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_flow_checks`
--

LOCK TABLES `oc_flow_checks` WRITE;
/*!40000 ALTER TABLE `oc_flow_checks` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_flow_checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_flow_operations`
--

DROP TABLE IF EXISTS `oc_flow_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_flow_operations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `checks` longtext COLLATE utf8mb4_bin,
  `operation` longtext COLLATE utf8mb4_bin,
  `entity` varchar(256) COLLATE utf8mb4_bin NOT NULL DEFAULT 'OCAWorkflowEngineEntityFile',
  `events` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_flow_operations`
--

LOCK TABLES `oc_flow_operations` WRITE;
/*!40000 ALTER TABLE `oc_flow_operations` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_flow_operations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_flow_operations_scope`
--

DROP TABLE IF EXISTS `oc_flow_operations_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_flow_operations_scope` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `operation_id` int NOT NULL DEFAULT '0',
  `type` int NOT NULL DEFAULT '0',
  `value` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `flow_unique_scope` (`operation_id`,`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_flow_operations_scope`
--

LOCK TABLES `oc_flow_operations_scope` WRITE;
/*!40000 ALTER TABLE `oc_flow_operations_scope` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_flow_operations_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_group_admin`
--

DROP TABLE IF EXISTS `oc_group_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_group_admin` (
  `gid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`gid`,`uid`),
  KEY `group_admin_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_group_admin`
--

LOCK TABLES `oc_group_admin` WRITE;
/*!40000 ALTER TABLE `oc_group_admin` DISABLE KEYS */;
INSERT INTO `oc_group_admin` VALUES ('testgroup','testuser');
/*!40000 ALTER TABLE `oc_group_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_group_user`
--

DROP TABLE IF EXISTS `oc_group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_group_user` (
  `gid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`gid`,`uid`),
  KEY `gu_uid_index` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_group_user`
--

LOCK TABLES `oc_group_user` WRITE;
/*!40000 ALTER TABLE `oc_group_user` DISABLE KEYS */;
INSERT INTO `oc_group_user` VALUES ('admin','admin'),('testgroup','disableduser'),('testgroup','testuser'),('testgroup2','testuser2'),('testgroup','testuser3'),('testgroup2','testuser3'),('admin','vok'),('testgroup','vok');
/*!40000 ALTER TABLE `oc_group_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_groups`
--

DROP TABLE IF EXISTS `oc_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_groups` (
  `gid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `displayname` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT 'name',
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_groups`
--

LOCK TABLES `oc_groups` WRITE;
/*!40000 ALTER TABLE `oc_groups` DISABLE KEYS */;
INSERT INTO `oc_groups` VALUES ('admin','admin'),('emptygroup','emptygroup'),('testgroup','testgroup'),('testgroup2','testgroup2');
/*!40000 ALTER TABLE `oc_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_jobs`
--

DROP TABLE IF EXISTS `oc_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `argument` varchar(4000) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `last_run` int DEFAULT '0',
  `last_checked` int DEFAULT '0',
  `reserved_at` int DEFAULT '0',
  `execution_duration` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_class_index` (`class`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_jobs`
--

LOCK TABLES `oc_jobs` WRITE;
/*!40000 ALTER TABLE `oc_jobs` DISABLE KEYS */;
INSERT INTO `oc_jobs` VALUES (2,'OCA\\Files_Sharing\\DeleteOrphanedSharesJob','null',1682685732,1682686613,0,0),(3,'OCA\\Files_Sharing\\ExpireSharesJob','null',1682677618,1682686614,0,0),(4,'OCA\\Files_Versions\\BackgroundJob\\ExpireVersions','null',1682685732,1682686612,0,0),(5,'OCA\\Activity\\BackgroundJob\\EmailNotification','null',1682686612,1682686612,0,0),(6,'OCA\\Activity\\BackgroundJob\\ExpireActivities','null',1682676716,1682686612,0,0),(7,'OCA\\Files_Trashbin\\BackgroundJob\\ExpireTrash','null',1682684831,1682686612,0,0),(8,'OCA\\NextcloudAnnouncements\\Cron\\Crawler','null',1682603703,1682686612,0,0),(9,'OCA\\Federation\\SyncJob','null',1682677618,1682686612,0,0),(10,'OCA\\UpdateNotification\\Notification\\BackgroundJob','null',1678457922,1682720819,0,1),(11,'OCA\\Files\\BackgroundJob\\ScanFiles','null',1682686612,1682686612,0,1),(12,'OCA\\Files\\BackgroundJob\\DeleteOrphanedItems','null',1682684831,1682686613,0,0),(13,'OCA\\Files\\BackgroundJob\\CleanupFileLocks','null',1682686613,1682686613,0,0),(14,'OC\\Authentication\\Token\\DefaultTokenCleanupJob','null',1682686613,1682686613,0,0),(15,'OC\\Log\\Rotate','null',1682686613,1682686613,0,0),(36,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.ne\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956844}',0,1682720819,0,0),(37,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.net\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956849}',0,1682720819,0,0),(39,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.ne\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956869}',0,1682720819,0,0),(41,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.net\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956888}',0,1682720819,0,0),(48,'OC\\Settings\\RemoveOrphaned','null',0,1682720819,0,0),(49,'OC\\Repair\\NC11\\MoveAvatarsBackgroundJob','null',0,1682720819,0,0),(50,'OC\\Repair\\NC11\\CleanPreviewsBackgroundJob','{\"uid\":\"admin\"}',0,1682720819,0,0),(51,'OC\\Repair\\NC11\\CleanPreviewsBackgroundJob','{\"uid\":\"testuser2\"}',0,1682720819,0,0),(52,'OC\\Repair\\NC11\\CleanPreviewsBackgroundJob','{\"uid\":\"vok\"}',0,1682720819,0,0),(53,'OC\\Preview\\BackgroundCleanupJob','null',1682684831,1682686613,0,0),(55,'OCA\\DAV\\BackgroundJob\\CleanupDirectLinksJob','null',1682677618,1682686613,0,0),(56,'OCA\\DAV\\BackgroundJob\\UpdateCalendarResourcesRoomsBackgroundJob','null',1682684831,1682686613,0,0),(57,'OCA\\DAV\\BackgroundJob\\CleanupInvitationTokenJob','null',1682677618,1682686613,0,0),(58,'OCA\\Files_Sharing\\BackgroundJob\\FederatedSharesDiscoverJob','null',1682677618,1682686613,0,0),(59,'OCA\\Support\\BackgroundJobs\\CheckSubscription','null',1682686613,1682686613,0,0),(64,'OC\\Core\\BackgroundJobs\\CleanupLoginFlowV2','null',1682684831,1682686613,0,0),(66,'OCA\\DAV\\BackgroundJob\\EventReminderJob','null',1682686614,1682686613,0,0),(67,'OCA\\Text\\Cron\\Cleanup','null',1682686614,1682686614,0,0),(68,'OCA\\Files\\BackgroundJob\\CleanupDirectEditingTokens','null',1682685733,1682686614,0,0),(69,'OCA\\WorkflowEngine\\BackgroundJobs\\Rotate','null',1682677618,1682686614,0,0),(70,'OCA\\ContactsInteraction\\BackgroundJob\\CleanupJob','null',1682677618,1682686614,0,1),(78,'OCA\\Activity\\BackgroundJob\\DigestMail','null',1682684831,1682686614,0,0),(80,'OCA\\ServerInfo\\Jobs\\UpdateStorageStats','null',1682677619,1682686614,0,0),(82,'OCA\\UserStatus\\BackgroundJob\\ClearOldStatusesBackgroundJob','null',1682686614,1682686614,0,0);
/*!40000 ALTER TABLE `oc_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_login_flow_v2`
--

DROP TABLE IF EXISTS `oc_login_flow_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_login_flow_v2` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint unsigned NOT NULL,
  `started` smallint unsigned NOT NULL DEFAULT '0',
  `poll_token` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `login_token` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `public_key` text COLLATE utf8mb4_bin NOT NULL,
  `private_key` text COLLATE utf8mb4_bin NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `login_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `server` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `app_password` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `poll_token` (`poll_token`),
  UNIQUE KEY `login_token` (`login_token`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_login_flow_v2`
--

LOCK TABLES `oc_login_flow_v2` WRITE;
/*!40000 ALTER TABLE `oc_login_flow_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_login_flow_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_migrations`
--

DROP TABLE IF EXISTS `oc_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_migrations` (
  `app` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `version` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`app`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_migrations`
--

LOCK TABLES `oc_migrations` WRITE;
/*!40000 ALTER TABLE `oc_migrations` DISABLE KEYS */;
INSERT INTO `oc_migrations` VALUES ('activity','2006Date20170808154933'),('activity','2006Date20170808155040'),('activity','2006Date20170919095939'),('activity','2007Date20181107114613'),('activity','2008Date20181011095117'),('activity','2010Date20190416112817'),('activity','2011Date20201006132544'),('activity','2011Date20201006132545'),('activity','2011Date20201006132546'),('activity','2011Date20201006132547'),('activity','2011Date20201207091915'),('contactsinteraction','010000Date20200304152605'),('core','13000Date20170705121758'),('core','13000Date20170718121200'),('core','13000Date20170814074715'),('core','13000Date20170919121250'),('core','13000Date20170926101637'),('core','13000Date20180516101403'),('core','14000Date20180129121024'),('core','14000Date20180404140050'),('core','14000Date20180516101403'),('core','14000Date20180518120534'),('core','14000Date20180522074438'),('core','14000Date20180626223656'),('core','14000Date20180710092004'),('core','14000Date20180712153140'),('core','15000Date20180926101451'),('core','15000Date20181015062942'),('core','15000Date20181029084625'),('core','16000Date20190207141427'),('core','16000Date20190212081545'),('core','16000Date20190427105638'),('core','16000Date20190428150708'),('core','17000Date20190514105811'),('core','18000Date20190920085628'),('core','18000Date20191014105105'),('core','18000Date20191204114856'),('core','19000Date20200211083441'),('core','20000Date20201109081915'),('core','20000Date20201109081918'),('core','20000Date20201109081919'),('core','20000Date20201111081915'),('core','21000Date20201120141228'),('core','23000Date20210906132259'),('dav','1004Date20170825134824'),('dav','1004Date20170919104507'),('dav','1004Date20170924124212'),('dav','1004Date20170926103422'),('dav','1005Date20180413093149'),('dav','1005Date20180530124431'),('dav','1006Date20180619154313'),('dav','1006Date20180628111625'),('dav','1008Date20181030113700'),('dav','1008Date20181105104826'),('dav','1008Date20181105104833'),('dav','1008Date20181105110300'),('dav','1008Date20181105112049'),('dav','1008Date20181114084440'),('dav','1011Date20190725113607'),('dav','1011Date20190806104428'),('dav','1012Date20190808122342'),('dav','1016Date20201109085907'),('federatedfilesharing','1010Date20200630191755'),('federatedfilesharing','1011Date20201120125158'),('federation','1010Date20200630191302'),('files','11301Date20191205150729'),('files_sharing','11300Date20201120141438'),('files_sharing','21000Date20201223143245'),('files_trashbin','1010Date20200630192639'),('notifications','2004Date20190107135757'),('oauth2','010401Date20181207190718'),('oauth2','010402Date20190107124745'),('privacy','100Date20190217131943'),('text','010000Date20190617184535'),('text','030001Date20200402075029'),('text','030201Date20201116110353'),('text','030201Date20201116123153'),('twofactor_backupcodes','1002Date20170607104347'),('twofactor_backupcodes','1002Date20170607113030'),('twofactor_backupcodes','1002Date20170919123342'),('twofactor_backupcodes','1002Date20170926101419'),('twofactor_backupcodes','1002Date20180821043638'),('user_status','0001Date20200602134824'),('user_status','0002Date20200902144824'),('user_status','1000Date20201111130204'),('workflowengine','2000Date20190808074233'),('workflowengine','2200Date20210805101925');
/*!40000 ALTER TABLE `oc_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_mimetypes`
--

DROP TABLE IF EXISTS `oc_mimetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_mimetypes` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `mimetype` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mimetype_id_index` (`mimetype`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_mimetypes`
--

LOCK TABLES `oc_mimetypes` WRITE;
/*!40000 ALTER TABLE `oc_mimetypes` DISABLE KEYS */;
INSERT INTO `oc_mimetypes` VALUES (8,'application'),(23,'application/comicbook+7z'),(24,'application/comicbook+ace'),(25,'application/comicbook+rar'),(26,'application/comicbook+tar'),(27,'application/comicbook+truecrypt'),(28,'application/comicbook+zip'),(13,'application/javascript'),(20,'application/json'),(14,'application/octet-stream'),(9,'application/pdf'),(33,'application/vnd.oasis.opendocument.graphics-template'),(32,'application/vnd.oasis.opendocument.presentation-template'),(31,'application/vnd.oasis.opendocument.spreadsheet-template'),(10,'application/vnd.oasis.opendocument.text'),(30,'application/vnd.oasis.opendocument.text-template'),(21,'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),(18,'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),(22,'application/vnd.visio'),(15,'application/x-gzip'),(19,'application/yaml'),(1,'httpd'),(2,'httpd/unix-directory'),(3,'image'),(34,'image/heic'),(5,'image/jpeg'),(4,'image/png'),(17,'image/svg+xml'),(11,'text'),(16,'text/css'),(29,'text/html'),(12,'text/plain'),(6,'video'),(7,'video/mp4'),(35,'video/quicktime');
/*!40000 ALTER TABLE `oc_mimetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_mounts`
--

DROP TABLE IF EXISTS `oc_mounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_mounts` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `storage_id` bigint NOT NULL,
  `root_id` bigint NOT NULL,
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `mount_point` varchar(4000) COLLATE utf8mb4_bin NOT NULL,
  `mount_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mounts_user_root_index` (`user_id`,`root_id`),
  KEY `mounts_user_index` (`user_id`),
  KEY `mounts_storage_index` (`storage_id`),
  KEY `mounts_root_index` (`root_id`),
  KEY `mounts_mount_id_index` (`mount_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_mounts`
--

LOCK TABLES `oc_mounts` WRITE;
/*!40000 ALTER TABLE `oc_mounts` DISABLE KEYS */;
INSERT INTO `oc_mounts` VALUES (1,1,1,'admin','/admin/',NULL),(2,7,234,'vok','/vok/',NULL),(3,5,340,'testuser2','/testuser2/',NULL),(4,3,955,'disableduser','/disableduser/',NULL),(5,4,956,'testuser','/testuser/',NULL),(6,6,957,'testuser3','/testuser3/',NULL);
/*!40000 ALTER TABLE `oc_mounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_notifications`
--

DROP TABLE IF EXISTS `oc_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_notifications` (
  `notification_id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `user` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `timestamp` int NOT NULL DEFAULT '0',
  `object_type` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `object_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `subject` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `subject_parameters` longtext COLLATE utf8mb4_bin,
  `message` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `message_parameters` longtext COLLATE utf8mb4_bin,
  `link` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `icon` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `actions` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`notification_id`),
  KEY `oc_notifications_app` (`app`),
  KEY `oc_notifications_user` (`user`),
  KEY `oc_notifications_timestamp` (`timestamp`),
  KEY `oc_notifications_object` (`object_type`,`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_notifications`
--

LOCK TABLES `oc_notifications` WRITE;
/*!40000 ALTER TABLE `oc_notifications` DISABLE KEYS */;
INSERT INTO `oc_notifications` VALUES (1,'firstrunwizard','admin',1678356375,'user','admin','profile','[]','','[]','','','[]'),(2,'firstrunwizard','vok',1678358784,'user','vok','profile','[]','','[]','','','[]'),(3,'admin_notifications','admin',1681984784,'admin_notifications','64410d10','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 16.0.11snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(4,'admin_notifications','vok',1681984785,'admin_notifications','64410d11','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 16.0.11snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(5,'admin_notifications','admin',1681984939,'admin_notifications','64410dab','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 17.0.10snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(6,'admin_notifications','vok',1681984941,'admin_notifications','64410dad','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 17.0.10snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(7,'admin_notifications','admin',1681985075,'admin_notifications','64410e33','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 18.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(8,'admin_notifications','vok',1681985077,'admin_notifications','64410e35','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 18.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(9,'firstrunwizard','testuser2',1681985164,'user','testuser2','profile','[]','','[]','','','[]'),(10,'survey_client','admin',1681985166,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(11,'survey_client','vok',1681985166,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(12,'admin_notifications','admin',1681985170,'admin_notifications','64410e92','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 19.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(13,'admin_notifications','vok',1681985173,'admin_notifications','64410e95','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 19.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(14,'survey_client','admin',1681985341,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(15,'survey_client','vok',1681985341,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(16,'admin_notifications','admin',1681985349,'admin_notifications','64410f45','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 20.0.14snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(17,'admin_notifications','vok',1681985351,'admin_notifications','64410f47','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 20.0.14snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(18,'survey_client','admin',1681986242,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(19,'survey_client','vok',1681986242,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(20,'firstrunwizard','vok',1682686195,'app','groupfolders','apphint-groupfolders','[]','','[]','','','[]'),(21,'firstrunwizard','vok',1682686195,'app','social','apphint-social','[]','','[]','','','[]'),(22,'firstrunwizard','vok',1682686195,'app','notes','apphint-notes','[]','','[]','','','[]'),(23,'firstrunwizard','vok',1682686195,'app','deck','apphint-deck','[]','','[]','','','[]'),(24,'firstrunwizard','vok',1682686195,'app','tasks','apphint-tasks','[]','','[]','','','[]');
/*!40000 ALTER TABLE `oc_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_notifications_pushtokens`
--

DROP TABLE IF EXISTS `oc_notifications_pushtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_notifications_pushtokens` (
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `token` int NOT NULL DEFAULT '0',
  `deviceidentifier` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `devicepublickey` varchar(512) COLLATE utf8mb4_bin NOT NULL,
  `devicepublickeyhash` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `pushtokenhash` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `proxyserver` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `apptype` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'unknown',
  UNIQUE KEY `oc_notifpushtoken` (`uid`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_notifications_pushtokens`
--

LOCK TABLES `oc_notifications_pushtokens` WRITE;
/*!40000 ALTER TABLE `oc_notifications_pushtokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_notifications_pushtokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_oauth2_access_tokens`
--

DROP TABLE IF EXISTS `oc_oauth2_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_oauth2_access_tokens` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `token_id` int NOT NULL,
  `client_id` int NOT NULL,
  `hashed_code` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `encrypted_token` varchar(786) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_access_hash_idx` (`hashed_code`),
  KEY `oauth2_access_client_id_idx` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_oauth2_access_tokens`
--

LOCK TABLES `oc_oauth2_access_tokens` WRITE;
/*!40000 ALTER TABLE `oc_oauth2_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_oauth2_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_oauth2_clients`
--

DROP TABLE IF EXISTS `oc_oauth2_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_oauth2_clients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8mb4_bin NOT NULL,
  `client_identifier` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `secret` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_client_id_idx` (`client_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_oauth2_clients`
--

LOCK TABLES `oc_oauth2_clients` WRITE;
/*!40000 ALTER TABLE `oc_oauth2_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_oauth2_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_preferences`
--

DROP TABLE IF EXISTS `oc_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_preferences` (
  `userid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `appid` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configkey` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configvalue` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`userid`,`appid`,`configkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_preferences`
--

LOCK TABLES `oc_preferences` WRITE;
/*!40000 ALTER TABLE `oc_preferences` DISABLE KEYS */;
INSERT INTO `oc_preferences` VALUES ('admin','avatar','generated','false'),('admin','avatar','version','2'),('admin','core','lang','en'),('admin','core','timezone','Europe/Berlin'),('admin','files','file_sorting','name'),('admin','files','file_sorting_direction','asc'),('admin','firstrunwizard','show','0'),('admin','login','lastLogin','1681984278'),('admin','lookup_server_connector','dataSend','1'),('disableduser','avatar','generated','false'),('disableduser','core','enabled','false'),('testuser','avatar','generated','false'),('testuser','files','quota','1 GB'),('testuser2','avatar','generated','false'),('testuser2','core','lang','en'),('testuser2','core','timezone','Europe/Berlin'),('testuser2','files','quota','5 GB'),('testuser2','firstrunwizard','show','0'),('testuser2','login','lastLogin','1678956925'),('testuser2','lookup_server_connector','dataSend','1'),('testuser3','avatar','generated','false'),('testuser3','files','quota','395 MB'),('vok','activity','configured','yes'),('vok','avatar','generated','true'),('vok','core','lang','en'),('vok','core','timezone','Europe/Zurich'),('vok','dashboard','firstRun','0'),('vok','firstrunwizard','apphint','18'),('vok','firstrunwizard','show','0'),('vok','login','lastLogin','1682686195'),('vok','login_token','y2IX25Np5GfRhc+Ag1sBtGxZWATKRcWq','1682686195'),('vok','lookup_server_connector','dataSend','1'),('vok','password_policy','failedLoginAttempts','0'),('vok','settings','email','kevin.vo@sunrise.net');
/*!40000 ALTER TABLE `oc_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_privacy_admins`
--

DROP TABLE IF EXISTS `oc_privacy_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_privacy_admins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `displayname` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_privacy_admins`
--

LOCK TABLES `oc_privacy_admins` WRITE;
/*!40000 ALTER TABLE `oc_privacy_admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_privacy_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_properties`
--

DROP TABLE IF EXISTS `oc_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_properties` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `userid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `propertypath` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `propertyname` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `propertyvalue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_index` (`userid`),
  KEY `properties_path_index` (`userid`,`propertypath`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_properties`
--

LOCK TABLES `oc_properties` WRITE;
/*!40000 ALTER TABLE `oc_properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_ratelimit_entries`
--

DROP TABLE IF EXISTS `oc_ratelimit_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_ratelimit_entries` (
  `hash` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `delete_after` datetime NOT NULL,
  KEY `ratelimit_hash` (`hash`),
  KEY `ratelimit_delete_after` (`delete_after`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_ratelimit_entries`
--

LOCK TABLES `oc_ratelimit_entries` WRITE;
/*!40000 ALTER TABLE `oc_ratelimit_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_ratelimit_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_recent_contact`
--

DROP TABLE IF EXISTS `oc_recent_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_recent_contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `actor_uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `uid` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `federated_cloud_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `card` longblob NOT NULL,
  `last_contact` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recent_contact_actor_uid` (`actor_uid`),
  KEY `recent_contact_id_uid` (`id`,`actor_uid`),
  KEY `recent_contact_uid` (`uid`),
  KEY `recent_contact_email` (`email`),
  KEY `recent_contact_fed_id` (`federated_cloud_id`),
  KEY `recent_contact_last_contact` (`last_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_recent_contact`
--

LOCK TABLES `oc_recent_contact` WRITE;
/*!40000 ALTER TABLE `oc_recent_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_recent_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_schedulingobjects`
--

DROP TABLE IF EXISTS `oc_schedulingobjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_schedulingobjects` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendardata` longblob,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastmodified` int unsigned DEFAULT NULL,
  `etag` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `schedulobj_principuri_index` (`principaluri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_schedulingobjects`
--

LOCK TABLES `oc_schedulingobjects` WRITE;
/*!40000 ALTER TABLE `oc_schedulingobjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_schedulingobjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_share`
--

DROP TABLE IF EXISTS `oc_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_share` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `share_type` smallint NOT NULL DEFAULT '0',
  `share_with` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uid_owner` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid_initiator` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `parent` bigint DEFAULT NULL,
  `item_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `item_source` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `item_target` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `file_source` bigint DEFAULT NULL,
  `file_target` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `permissions` smallint NOT NULL DEFAULT '0',
  `stime` bigint NOT NULL DEFAULT '0',
  `accepted` smallint NOT NULL DEFAULT '0',
  `expiration` datetime DEFAULT NULL,
  `token` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `mail_send` smallint NOT NULL DEFAULT '0',
  `share_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `password_by_talk` tinyint(1) DEFAULT '0',
  `note` longtext COLLATE utf8mb4_bin,
  `hide_download` smallint DEFAULT '0',
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_share_type_index` (`item_type`,`share_type`),
  KEY `file_source_index` (`file_source`),
  KEY `token_index` (`token`),
  KEY `share_with_index` (`share_with`),
  KEY `parent_index` (`parent`),
  KEY `owner_index` (`uid_owner`),
  KEY `initiator_index` (`uid_initiator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_share`
--

LOCK TABLES `oc_share` WRITE;
/*!40000 ALTER TABLE `oc_share` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_share_external`
--

DROP TABLE IF EXISTS `oc_share_external`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_share_external` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `remote` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT 'Url of the remove owncloud instance',
  `remote_id` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `share_token` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'Public share token',
  `password` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Optional password for the public share',
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'Original name on the remote server',
  `owner` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'User that owns the public share on the remote server',
  `user` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'Local user which added the external share',
  `mountpoint` varchar(4000) COLLATE utf8mb4_bin NOT NULL COMMENT 'Full path where the share is mounted',
  `mountpoint_hash` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'md5 hash of the mountpoint',
  `accepted` int NOT NULL DEFAULT '0',
  `parent` bigint DEFAULT '-1',
  `share_type` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sh_external_mp` (`user`,`mountpoint_hash`),
  KEY `sh_external_user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_share_external`
--

LOCK TABLES `oc_share_external` WRITE;
/*!40000 ALTER TABLE `oc_share_external` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_share_external` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_storages`
--

DROP TABLE IF EXISTS `oc_storages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_storages` (
  `numeric_id` bigint NOT NULL AUTO_INCREMENT,
  `id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `available` int NOT NULL DEFAULT '1',
  `last_checked` int DEFAULT NULL,
  PRIMARY KEY (`numeric_id`),
  UNIQUE KEY `storages_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_storages`
--

LOCK TABLES `oc_storages` WRITE;
/*!40000 ALTER TABLE `oc_storages` DISABLE KEYS */;
INSERT INTO `oc_storages` VALUES (1,'home::admin',1,NULL),(2,'local::/var/www/nextcloud/data/',1,NULL),(3,'home::disableduser',1,NULL),(4,'home::testuser',1,NULL),(5,'home::testuser2',1,NULL),(6,'home::testuser3',1,NULL),(7,'home::vok',1,NULL),(8,'local::/var/snap/nextcloud/common/nextcloud/data/',1,NULL);
/*!40000 ALTER TABLE `oc_storages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_storages_credentials`
--

DROP TABLE IF EXISTS `oc_storages_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_storages_credentials` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `identifier` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `credentials` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stocred_ui` (`user`,`identifier`),
  KEY `stocred_user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_storages_credentials`
--

LOCK TABLES `oc_storages_credentials` WRITE;
/*!40000 ALTER TABLE `oc_storages_credentials` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_storages_credentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_systemtag`
--

DROP TABLE IF EXISTS `oc_systemtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_systemtag` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `visibility` smallint NOT NULL DEFAULT '1',
  `editable` smallint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_ident` (`name`,`visibility`,`editable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_systemtag`
--

LOCK TABLES `oc_systemtag` WRITE;
/*!40000 ALTER TABLE `oc_systemtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_systemtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_systemtag_group`
--

DROP TABLE IF EXISTS `oc_systemtag_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_systemtag_group` (
  `gid` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `systemtagid` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`,`systemtagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_systemtag_group`
--

LOCK TABLES `oc_systemtag_group` WRITE;
/*!40000 ALTER TABLE `oc_systemtag_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_systemtag_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_systemtag_object_mapping`
--

DROP TABLE IF EXISTS `oc_systemtag_object_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_systemtag_object_mapping` (
  `objectid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `objecttype` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `systemtagid` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`objecttype`,`objectid`,`systemtagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_systemtag_object_mapping`
--

LOCK TABLES `oc_systemtag_object_mapping` WRITE;
/*!40000 ALTER TABLE `oc_systemtag_object_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_systemtag_object_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_text_documents`
--

DROP TABLE IF EXISTS `oc_text_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_text_documents` (
  `id` bigint unsigned NOT NULL,
  `current_version` bigint unsigned DEFAULT '0',
  `last_saved_version` bigint unsigned DEFAULT '0',
  `last_saved_version_time` bigint unsigned NOT NULL,
  `last_saved_version_etag` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  `base_version_etag` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_text_documents`
--

LOCK TABLES `oc_text_documents` WRITE;
/*!40000 ALTER TABLE `oc_text_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_text_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_text_sessions`
--

DROP TABLE IF EXISTS `oc_text_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_text_sessions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `guest_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `color` varchar(7) COLLATE utf8mb4_bin DEFAULT NULL,
  `token` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `document_id` bigint NOT NULL,
  `last_contact` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rd_session_token_idx` (`token`),
  KEY `ts_docid_lastcontact` (`document_id`,`last_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_text_sessions`
--

LOCK TABLES `oc_text_sessions` WRITE;
/*!40000 ALTER TABLE `oc_text_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_text_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_text_steps`
--

DROP TABLE IF EXISTS `oc_text_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_text_steps` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `document_id` bigint unsigned NOT NULL,
  `session_id` bigint unsigned NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  `version` bigint unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rd_steps_did_idx` (`document_id`),
  KEY `rd_steps_version_idx` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_text_steps`
--

LOCK TABLES `oc_text_steps` WRITE;
/*!40000 ALTER TABLE `oc_text_steps` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_text_steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_trusted_servers`
--

DROP TABLE IF EXISTS `oc_trusted_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_trusted_servers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT 'Url of trusted server',
  `url_hash` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'sha1 hash of the url without the protocol',
  `token` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'token used to exchange the shared secret',
  `shared_secret` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'shared secret used to authenticate',
  `status` int NOT NULL DEFAULT '2' COMMENT 'current status of the connection',
  `sync_token` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'cardDav sync token',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_hash` (`url_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_trusted_servers`
--

LOCK TABLES `oc_trusted_servers` WRITE;
/*!40000 ALTER TABLE `oc_trusted_servers` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_trusted_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_twofactor_backupcodes`
--

DROP TABLE IF EXISTS `oc_twofactor_backupcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_twofactor_backupcodes` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `code` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `used` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `twofactor_backupcodes_uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_twofactor_backupcodes`
--

LOCK TABLES `oc_twofactor_backupcodes` WRITE;
/*!40000 ALTER TABLE `oc_twofactor_backupcodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_twofactor_backupcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_twofactor_providers`
--

DROP TABLE IF EXISTS `oc_twofactor_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_twofactor_providers` (
  `provider_id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `enabled` smallint NOT NULL,
  PRIMARY KEY (`provider_id`,`uid`),
  KEY `twofactor_providers_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_twofactor_providers`
--

LOCK TABLES `oc_twofactor_providers` WRITE;
/*!40000 ALTER TABLE `oc_twofactor_providers` DISABLE KEYS */;
INSERT INTO `oc_twofactor_providers` VALUES ('backup_codes','admin',0),('backup_codes','testuser2',0),('backup_codes','vok',0);
/*!40000 ALTER TABLE `oc_twofactor_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_user_status`
--

DROP TABLE IF EXISTS `oc_user_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_user_status` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `status_timestamp` int unsigned NOT NULL,
  `is_user_defined` tinyint(1) DEFAULT NULL,
  `message_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `custom_icon` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `custom_message` longtext COLLATE utf8mb4_bin,
  `clear_at` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_status_uid_ix` (`user_id`),
  KEY `user_status_clr_ix` (`clear_at`),
  KEY `user_status_tstmp_ix` (`status_timestamp`),
  KEY `user_status_iud_ix` (`is_user_defined`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_user_status`
--

LOCK TABLES `oc_user_status` WRITE;
/*!40000 ALTER TABLE `oc_user_status` DISABLE KEYS */;
INSERT INTO `oc_user_status` VALUES (1,'vok','online',1682686203,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `oc_user_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_user_transfer_owner`
--

DROP TABLE IF EXISTS `oc_user_transfer_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_user_transfer_owner` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `source_user` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `target_user` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `file_id` bigint NOT NULL,
  `node_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_user_transfer_owner`
--

LOCK TABLES `oc_user_transfer_owner` WRITE;
/*!40000 ALTER TABLE `oc_user_transfer_owner` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_user_transfer_owner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_users`
--

DROP TABLE IF EXISTS `oc_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_users` (
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `displayname` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid_lower` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `user_uid_lower` (`uid_lower`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_users`
--

LOCK TABLES `oc_users` WRITE;
/*!40000 ALTER TABLE `oc_users` DISABLE KEYS */;
INSERT INTO `oc_users` VALUES ('admin',NULL,'1|$2y$10$pwuAfa9lvVEXM6c.3XvodetHQI5yb7crJsuiaXwpis05lPvP0Frnm','admin'),('disableduser',NULL,'1|$2y$10$HIAL/GP0YIT7bvVC/EbcNuqntJ8dIyvVGJJgVVQL6sDLUgrCyZkDO','disableduser'),('testuser',NULL,'1|$2y$10$94cGGZtHKjsL5w9zkOrjl.61/2r81sX5fb4XJqrBrxC/XMvTLXt3a','testuser'),('testuser2',NULL,'1|$2y$10$tZz1S34E5EqSMJ95bLuoheqe7PJ/vo1DDipJPnoGedm2ywP4Ag1je','testuser2'),('testuser3',NULL,'1|$2y$10$WmvFmouMzydpIzM/lpzqzexRzg92PRyklEtlYevF1Jk.AerHZvdVa','testuser3'),('vok',NULL,'3|$argon2id$v=19$m=65536,t=4,p=1$di9GbHc5cm0yVXdBaDNqQg$BS3ngT8dGdrraMVSZNMCRV3mwUAfEHc+cUi1cYpBWTw','vok');
/*!40000 ALTER TABLE `oc_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_vcategory`
--

DROP TABLE IF EXISTS `oc_vcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_vcategory` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `category` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `uid_index` (`uid`),
  KEY `type_index` (`type`),
  KEY `category_index` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_vcategory`
--

LOCK TABLES `oc_vcategory` WRITE;
/*!40000 ALTER TABLE `oc_vcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_vcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_vcategory_to_object`
--

DROP TABLE IF EXISTS `oc_vcategory_to_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_vcategory_to_object` (
  `categoryid` bigint unsigned NOT NULL DEFAULT '0',
  `objid` bigint unsigned NOT NULL DEFAULT '0',
  `type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`categoryid`,`objid`,`type`),
  KEY `vcategory_objectd_index` (`objid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_vcategory_to_object`
--

LOCK TABLES `oc_vcategory_to_object` WRITE;
/*!40000 ALTER TABLE `oc_vcategory_to_object` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_vcategory_to_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_webauthn`
--

DROP TABLE IF EXISTS `oc_webauthn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_webauthn` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `public_key_credential_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `webauthn_uid` (`uid`),
  KEY `webauthn_publicKeyCredentialId` (`public_key_credential_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_webauthn`
--

LOCK TABLES `oc_webauthn` WRITE;
/*!40000 ALTER TABLE `oc_webauthn` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_webauthn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_whats_new`
--

DROP TABLE IF EXISTS `oc_whats_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_whats_new` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '11',
  `etag` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `last_check` int unsigned NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `version` (`version`),
  KEY `version_etag_idx` (`version`,`etag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_whats_new`
--

LOCK TABLES `oc_whats_new` WRITE;
/*!40000 ALTER TABLE `oc_whats_new` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_whats_new` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `nextcloud`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `nextcloud` /*!40100 DEFAULT CHARACTER SET latin1 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `nextcloud`;

--
-- Table structure for table `oc_accounts`
--

DROP TABLE IF EXISTS `oc_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_accounts` (
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_accounts`
--

LOCK TABLES `oc_accounts` WRITE;
/*!40000 ALTER TABLE `oc_accounts` DISABLE KEYS */;
INSERT INTO `oc_accounts` VALUES ('admin','{\"displayname\":{\"value\":\"admin\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('disableduser','{\"displayname\":{\"value\":\"disableduser\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('testuser','{\"displayname\":{\"value\":\"testuser\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('testuser2','{\"displayname\":{\"value\":\"testuser2\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('testuser3','{\"displayname\":{\"value\":\"testuser3\",\"scope\":\"contacts\",\"verified\":\"0\"},\"address\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":null,\"scope\":\"contacts\",\"verified\":\"0\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}'),('vok','{\"displayname\":{\"value\":\"vok\",\"scope\":\"contacts\"},\"address\":{\"value\":\"\",\"scope\":\"private\"},\"website\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"},\"email\":{\"value\":\"kevin.vo@sunrise.net\",\"scope\":\"private\",\"verified\":\"1\"},\"avatar\":{\"scope\":\"contacts\"},\"phone\":{\"value\":\"\",\"scope\":\"private\"},\"twitter\":{\"value\":\"\",\"scope\":\"private\",\"verified\":\"0\"}}');
/*!40000 ALTER TABLE `oc_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_activity`
--

DROP TABLE IF EXISTS `oc_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_activity` (
  `activity_id` bigint NOT NULL AUTO_INCREMENT,
  `timestamp` int NOT NULL DEFAULT '0',
  `priority` int NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `affecteduser` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `app` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `subjectparams` longtext COLLATE utf8mb4_bin NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `messageparams` longtext COLLATE utf8mb4_bin,
  `file` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `link` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `object_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `object_id` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`),
  KEY `activity_user_time` (`affecteduser`,`timestamp`),
  KEY `activity_filter_by` (`affecteduser`,`user`,`timestamp`),
  KEY `activity_filter` (`affecteduser`,`type`,`app`,`timestamp`),
  KEY `activity_object` (`object_type`,`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_activity`
--

LOCK TABLES `oc_activity` WRITE;
/*!40000 ALTER TABLE `oc_activity` DISABLE KEYS */;
INSERT INTO `oc_activity` VALUES (1,1678194775,30,'file_created','admin','admin','files','created_self','[{\"3\":\"\\/bbw-logo.png\"}]','','[]','/bbw-logo.png','http://localhost/index.php/apps/files/?dir=/','files',3),(2,1678194775,30,'file_created','admin','admin','files','created_self','[{\"4\":\"\\/Photos\"}]','','[]','/Photos','http://localhost/index.php/apps/files/?dir=/','files',4),(3,1678194775,30,'file_created','admin','admin','files','created_self','[{\"5\":\"\\/Photos\\/Coast.jpg\"}]','','[]','/Photos/Coast.jpg','http://localhost/index.php/apps/files/?dir=/Photos','files',5),(4,1678194775,30,'file_created','admin','admin','files','created_self','[{\"6\":\"\\/Photos\\/Hummingbird.jpg\"}]','','[]','/Photos/Hummingbird.jpg','http://localhost/index.php/apps/files/?dir=/Photos','files',6),(5,1678194775,30,'file_created','admin','admin','files','created_self','[{\"7\":\"\\/Photos\\/Nut.jpg\"}]','','[]','/Photos/Nut.jpg','http://localhost/index.php/apps/files/?dir=/Photos','files',7),(6,1678194775,30,'file_created','admin','admin','files','created_self','[{\"8\":\"\\/Nextcloud.mp4\"}]','','[]','/Nextcloud.mp4','http://localhost/index.php/apps/files/?dir=/','files',8),(7,1678194775,30,'file_created','admin','admin','files','created_self','[{\"9\":\"\\/Nextcloud Manual.pdf\"}]','','[]','/Nextcloud Manual.pdf','http://localhost/index.php/apps/files/?dir=/','files',9),(8,1678194775,30,'file_created','admin','admin','files','created_self','[{\"10\":\"\\/Documents\"}]','','[]','/Documents','http://localhost/index.php/apps/files/?dir=/','files',10),(9,1678194775,30,'file_created','admin','admin','files','created_self','[{\"11\":\"\\/Documents\\/About.odt\"}]','','[]','/Documents/About.odt','http://localhost/index.php/apps/files/?dir=/Documents','files',11),(10,1678194775,30,'file_created','admin','admin','files','created_self','[{\"12\":\"\\/Documents\\/About.txt\"}]','','[]','/Documents/About.txt','http://localhost/index.php/apps/files/?dir=/Documents','files',12),(11,1678194775,30,'calendar','admin','admin','dav','calendar_add_self','{\"actor\":\"admin\",\"calendar\":{\"id\":1,\"uri\":\"personal\",\"name\":\"Personal\"}}','','[]','','','calendar',1),(12,1678194776,30,'calendar','admin','admin','dav','calendar_add_self','{\"actor\":\"admin\",\"calendar\":{\"id\":2,\"uri\":\"admincal\",\"name\":\"admincal\"}}','','[]','','','calendar',2),(13,1678351486,30,'file_created','admin','admin','files','created_self','[{\"144\":\"\\/Test\"}]','','[]','/Test','http://fair-coral.maas/index.php/apps/files/?dir=/','files',144),(14,1678351646,30,'file_created','admin','admin','files','created_self','[{\"145\":\"\\/Test\\/10.1_ContainerVirtualisierung.pdf\"}]','','[]','/Test/10.1_ContainerVirtualisierung.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',145),(15,1678351646,30,'file_created','admin','admin','files','created_self','[{\"146\":\"\\/Test\\/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf\"}]','','[]','/Test/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',146),(16,1678351647,30,'file_created','admin','admin','files','created_self','[{\"147\":\"\\/Test\\/10.3_Container_oder_VM_Pros_und_Kontras.pdf\"}]','','[]','/Test/10.3_Container_oder_VM_Pros_und_Kontras.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',147),(17,1678351647,30,'file_created','admin','admin','files','created_self','[{\"148\":\"\\/Test\\/10.4_CV_Versus_VM.docx\"}]','','[]','/Test/10.4_CV_Versus_VM.docx','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',148),(18,1678351647,30,'file_created','admin','admin','files','created_self','[{\"149\":\"\\/Test\\/10.5_Docker Einstieg - CLI-Befehle.pdf\"}]','','[]','/Test/10.5_Docker Einstieg - CLI-Befehle.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',149),(19,1678351647,30,'file_created','admin','admin','files','created_self','[{\"150\":\"\\/Test\\/docker-compose.yml\"}]','','[]','/Test/docker-compose.yml','http://fair-coral.maas/index.php/apps/files/?dir=/Test','files',150),(20,1678352135,30,'file_created','admin','admin','files','created_self','[{\"152\":\"\\/Testfile.txt\"}]','','[]','/Testfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',152),(21,1678352141,30,'file_changed','admin','admin','files','changed_self','[{\"152\":\"\\/Testfile.txt\"}]','','[]','/Testfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',152),(22,1678352217,30,'file_created','admin','admin','files','created_self','[{\"159\":\"\\/m129_bandbreitenberechnung.pdf\"}]','','[]','/m129_bandbreitenberechnung.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',159),(23,1678352218,30,'file_created','admin','admin','files','created_self','[{\"160\":\"\\/m129_bandbreitenberechnung-lsg.pdf\"}]','','[]','/m129_bandbreitenberechnung-lsg.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',160),(24,1678352368,30,'calendar','admin','system','dav','calendar_add','{\"actor\":\"admin\",\"calendar\":{\"id\":3,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',3),(25,1678356423,30,'file_created','admin','admin','files','created_self','[{\"193\":\"\\/deletedfile.txt\"}]','','[]','/deletedfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',193),(26,1678356431,30,'file_deleted','admin','admin','files','deleted_self','[{\"193\":\"\\/deletedfile.txt\"}]','','[]','/deletedfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',193),(27,1678358159,30,'file_created','vok','vok','files','created_self','[{\"237\":\"\\/bbw-logo.png\"}]','','[]','/bbw-logo.png','http://fair-coral.maas/index.php/apps/files/?dir=/','files',237),(28,1678358159,30,'file_created','vok','vok','files','created_self','[{\"238\":\"\\/Photos\"}]','','[]','/Photos','http://fair-coral.maas/index.php/apps/files/?dir=/','files',238),(29,1678358159,30,'file_created','vok','vok','files','created_self','[{\"239\":\"\\/Photos\\/Coast.jpg\"}]','','[]','/Photos/Coast.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',239),(30,1678358159,30,'file_created','vok','vok','files','created_self','[{\"240\":\"\\/Photos\\/Hummingbird.jpg\"}]','','[]','/Photos/Hummingbird.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',240),(31,1678358159,30,'file_created','vok','vok','files','created_self','[{\"241\":\"\\/Photos\\/Nut.jpg\"}]','','[]','/Photos/Nut.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',241),(32,1678358159,30,'file_created','vok','vok','files','created_self','[{\"242\":\"\\/Nextcloud.mp4\"}]','','[]','/Nextcloud.mp4','http://fair-coral.maas/index.php/apps/files/?dir=/','files',242),(33,1678358159,30,'file_created','vok','vok','files','created_self','[{\"243\":\"\\/Nextcloud Manual.pdf\"}]','','[]','/Nextcloud Manual.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',243),(34,1678358159,30,'file_created','vok','vok','files','created_self','[{\"244\":\"\\/Documents\"}]','','[]','/Documents','http://fair-coral.maas/index.php/apps/files/?dir=/','files',244),(35,1678358159,30,'file_created','vok','vok','files','created_self','[{\"245\":\"\\/Documents\\/About.odt\"}]','','[]','/Documents/About.odt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',245),(36,1678358160,30,'file_created','vok','vok','files','created_self','[{\"246\":\"\\/Documents\\/About.txt\"}]','','[]','/Documents/About.txt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',246),(37,1678358160,30,'calendar','vok','vok','dav','calendar_add_self','{\"actor\":\"vok\",\"calendar\":{\"id\":4,\"uri\":\"personal\",\"name\":\"Personal\"}}','','[]','','','calendar',4),(38,1678358180,30,'file_created','vok','vok','files','created_self','[{\"251\":\"\\/testfile.txt\"}]','','[]','/testfile.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',251),(39,1678358187,30,'file_created','vok','vok','files','created_self','[{\"253\":\"\\/testfile2.txt\"}]','','[]','/testfile2.txt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',253),(40,1678358226,30,'personal_settings','vok','vok','settings','email_changed_self','[]','','[]','','','',0),(41,1678358227,30,'personal_settings','vok','vok','settings','email_changed_self','[]','','[]','','','',0),(42,1678359111,30,'file_created','vok','vok','files','created_self','[{\"331\":\"\\/!!!IMPORTANT!!!\"}]','','[]','/!!!IMPORTANT!!!','http://fair-coral.maas/index.php/apps/files/?dir=/','files',331),(43,1678359119,30,'file_created','vok','vok','files','created_self','[{\"332\":\"\\/!!!IMPORTANT!!!\\/WIN_20211028_15_17_04_Pro.jpg\"}]','','[]','/!!!IMPORTANT!!!/WIN_20211028_15_17_04_Pro.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/%21%21%21IMPORTANT%21%21%21','files',332),(44,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"343\":\"\\/bbw-logo.png\"}]','','[]','/bbw-logo.png','http://fair-coral.maas/index.php/apps/files/?dir=/','files',343),(45,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"344\":\"\\/Photos\"}]','','[]','/Photos','http://fair-coral.maas/index.php/apps/files/?dir=/','files',344),(46,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"345\":\"\\/Photos\\/Coast.jpg\"}]','','[]','/Photos/Coast.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',345),(47,1678956925,30,'file_created','testuser2','testuser2','files','created_self','[{\"346\":\"\\/Photos\\/Hummingbird.jpg\"}]','','[]','/Photos/Hummingbird.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',346),(48,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"347\":\"\\/Photos\\/Nut.jpg\"}]','','[]','/Photos/Nut.jpg','http://fair-coral.maas/index.php/apps/files/?dir=/Photos','files',347),(49,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"348\":\"\\/Nextcloud.mp4\"}]','','[]','/Nextcloud.mp4','http://fair-coral.maas/index.php/apps/files/?dir=/','files',348),(50,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"349\":\"\\/Nextcloud Manual.pdf\"}]','','[]','/Nextcloud Manual.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',349),(51,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"350\":\"\\/Documents\"}]','','[]','/Documents','http://fair-coral.maas/index.php/apps/files/?dir=/','files',350),(52,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"351\":\"\\/Documents\\/About.odt\"}]','','[]','/Documents/About.odt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',351),(53,1678956926,30,'file_created','testuser2','testuser2','files','created_self','[{\"352\":\"\\/Documents\\/About.txt\"}]','','[]','/Documents/About.txt','http://fair-coral.maas/index.php/apps/files/?dir=/Documents','files',352),(54,1678956926,30,'calendar','testuser2','testuser2','dav','calendar_add_self','{\"actor\":\"testuser2\",\"calendar\":{\"id\":5,\"uri\":\"personal\",\"name\":\"Personal\"}}','','[]','','','calendar',5),(55,1678956975,30,'file_created','testuser2','testuser2','files','created_self','[{\"357\":\"\\/Subnetting Table.xlsx\"}]','','[]','/Subnetting Table.xlsx','http://fair-coral.maas/index.php/apps/files/?dir=/','files',357),(56,1678956996,30,'file_created','testuser2','testuser2','files','created_self','[{\"359\":\"\\/05_subnetting.pdf\"}]','','[]','/05_subnetting.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',359),(57,1678956996,30,'file_created','testuser2','testuser2','files','created_self','[{\"360\":\"\\/Subnetting (Post-Routing).pkt\"}]','','[]','/Subnetting (Post-Routing).pkt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',360),(58,1678956997,30,'file_created','testuser2','testuser2','files','created_self','[{\"361\":\"\\/Subnetting.docx\"}]','','[]','/Subnetting.docx','http://fair-coral.maas/index.php/apps/files/?dir=/','files',361),(59,1678956997,30,'file_created','testuser2','testuser2','files','created_self','[{\"363\":\"\\/Subnetting.pdf\"}]','','[]','/Subnetting.pdf','http://fair-coral.maas/index.php/apps/files/?dir=/','files',363),(60,1678956998,30,'file_created','testuser2','testuser2','files','created_self','[{\"364\":\"\\/Subnetting.pkt\"}]','','[]','/Subnetting.pkt','http://fair-coral.maas/index.php/apps/files/?dir=/','files',364),(61,1681985340,30,'calendar','admin','admin','dav','calendar_add_self','{\"actor\":\"admin\",\"calendar\":{\"id\":6,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',6),(62,1681985340,30,'calendar','testuser2','testuser2','dav','calendar_add_self','{\"actor\":\"testuser2\",\"calendar\":{\"id\":7,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',7),(63,1681985340,30,'calendar','vok','vok','dav','calendar_add_self','{\"actor\":\"vok\",\"calendar\":{\"id\":8,\"uri\":\"contact_birthdays\",\"name\":\"Contact birthdays\"}}','','[]','','','calendar',8);
/*!40000 ALTER TABLE `oc_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_activity_mq`
--

DROP TABLE IF EXISTS `oc_activity_mq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_activity_mq` (
  `mail_id` bigint NOT NULL AUTO_INCREMENT,
  `amq_timestamp` int NOT NULL DEFAULT '0',
  `amq_latest_send` int NOT NULL DEFAULT '0',
  `amq_type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amq_affecteduser` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `amq_appid` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `amq_subject` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amq_subjectparams` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `object_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `object_id` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`mail_id`),
  KEY `amp_user` (`amq_affecteduser`),
  KEY `amp_latest_send_time` (`amq_latest_send`),
  KEY `amp_timestamp_time` (`amq_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_activity_mq`
--

LOCK TABLES `oc_activity_mq` WRITE;
/*!40000 ALTER TABLE `oc_activity_mq` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_activity_mq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_addressbookchanges`
--

DROP TABLE IF EXISTS `oc_addressbookchanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_addressbookchanges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `addressbookid` bigint NOT NULL,
  `operation` smallint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `addressbookid_synctoken` (`addressbookid`,`synctoken`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_addressbookchanges`
--

LOCK TABLES `oc_addressbookchanges` WRITE;
/*!40000 ALTER TABLE `oc_addressbookchanges` DISABLE KEYS */;
INSERT INTO `oc_addressbookchanges` VALUES (1,'Database:vok.vcf',1,3,1),(2,'Database:testuser.vcf',2,3,1),(3,'Database:testuser2.vcf',3,3,1),(4,'Database:testuser2.vcf',4,3,2),(5,'Database:testuser.vcf',5,3,2),(6,'Database:testuser3.vcf',6,3,1),(7,'Database:testuser3.vcf',7,3,2),(8,'Database:disableduser.vcf',8,3,1),(9,'Database:disableduser.vcf',9,3,2),(10,'Database:admin.vcf',10,3,1),(11,'Database:admin.vcf',11,3,2),(12,'Database:admin.vcf',12,3,2),(13,'Database:admin.vcf',13,3,2),(14,'Database:vok.vcf',14,3,2),(15,'Database:vok.vcf',15,3,2),(16,'Database:vok.vcf',16,3,2),(17,'Database:vok.vcf',17,3,2),(18,'Database:vok.vcf',18,3,2),(19,'Database:vok.vcf',19,3,2),(20,'Database:vok.vcf',20,3,2),(21,'Database:vok.vcf',21,3,2),(22,'Database:vok.vcf',22,3,2);
/*!40000 ALTER TABLE `oc_addressbookchanges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_addressbooks`
--

DROP TABLE IF EXISTS `oc_addressbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_addressbooks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `addressbook_index` (`principaluri`,`uri`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_addressbooks`
--

LOCK TABLES `oc_addressbooks` WRITE;
/*!40000 ALTER TABLE `oc_addressbooks` DISABLE KEYS */;
INSERT INTO `oc_addressbooks` VALUES (1,'principals/users/admin','Contacts','contacts',NULL,1),(2,'principals/users/admin','admincard','admincard',NULL,1),(3,'principals/system/system','system','system','System addressbook which holds all users of this instance',23),(4,'principals/users/vok','Contacts','contacts',NULL,1),(5,'principals/users/testuser2','Contacts','contacts',NULL,1);
/*!40000 ALTER TABLE `oc_addressbooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_appconfig`
--

DROP TABLE IF EXISTS `oc_appconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_appconfig` (
  `appid` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configkey` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configvalue` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`appid`,`configkey`),
  KEY `appconfig_config_key_index` (`configkey`),
  KEY `appconfig_appid_key` (`appid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_appconfig`
--

LOCK TABLES `oc_appconfig` WRITE;
/*!40000 ALTER TABLE `oc_appconfig` DISABLE KEYS */;
INSERT INTO `oc_appconfig` VALUES ('accessibility','enabled','yes'),('accessibility','installed_version','1.6.0'),('accessibility','types',''),('activity','enabled','yes'),('activity','installed_version','2.13.4'),('activity','types','filesystem'),('backgroundjob','lastjob','82'),('cloud_federation_api','enabled','yes'),('cloud_federation_api','installed_version','1.3.0'),('cloud_federation_api','types','filesystem'),('comments','enabled','yes'),('comments','installed_version','1.10.0'),('comments','types','logging'),('contactsinteraction','enabled','yes'),('contactsinteraction','installed_version','1.1.0'),('contactsinteraction','types','dav'),('core','backgroundjobs_mode','cron'),('core','enterpriseLogoChecked','yes'),('core','installedat','1678194770.1388'),('core','lastcron','1682686614'),('core','lastupdateResult','{\"version\":\"13.0.12.1\",\"versionstring\":\"Nextcloud 13.0.12\",\"url\":\"https:\\/\\/download.nextcloud.com\\/server\\/releases\\/nextcloud-13.0.12.zip\",\"web\":\"https:\\/\\/docs.nextcloud.com\\/server\\/13\\/admin_manual\\/maintenance\\/upgrade.html\",\"autoupdater\":\"1\",\"eol\":\"1\"}'),('core','lastupdatedat','0'),('core','moveavatarsdone','yes'),('core','oc.integritycheck.checker','[]'),('core','previewsCleanedUp','1'),('core','public_files','files_sharing/public.php'),('core','public_webdav','dav/appinfo/v1/publicwebdav.php'),('core','scss.variables','81ed5474f262cf7e7b7290f9a8cb0f1a'),('core','theming.variables','e06163e2b33a3c3c3e83e3a3e06955fa'),('core','vendor','nextcloud'),('dashboard','enabled','yes'),('dashboard','installed_version','7.0.0'),('dashboard','types',''),('dav','buildCalendarReminderIndex','yes'),('dav','buildCalendarSearchIndex','yes'),('dav','chunks_migrated','1'),('dav','enabled','yes'),('dav','installed_version','1.16.2'),('dav','regeneratedBirthdayCalendarsForYearFix','yes'),('dav','types','filesystem'),('federatedfilesharing','enabled','yes'),('federatedfilesharing','installed_version','1.10.2'),('federatedfilesharing','types',''),('federation','enabled','yes'),('federation','installed_version','1.10.1'),('federation','types','authentication'),('files','cronjob_scan_files','500'),('files','enabled','yes'),('files','installed_version','1.15.0'),('files','types','filesystem'),('files_pdfviewer','enabled','yes'),('files_pdfviewer','installed_version','2.0.2'),('files_pdfviewer','types',''),('files_rightclick','enabled','yes'),('files_rightclick','installed_version','0.17.0'),('files_rightclick','types',''),('files_sharing','enabled','yes'),('files_sharing','installed_version','1.12.2'),('files_sharing','types','filesystem'),('files_texteditor','enabled','no'),('files_texteditor','installed_version','2.8.0'),('files_texteditor','types',''),('files_trashbin','enabled','yes'),('files_trashbin','installed_version','1.10.1'),('files_trashbin','types','filesystem,dav'),('files_versions','enabled','yes'),('files_versions','installed_version','1.13.0'),('files_versions','types','filesystem,dav'),('files_videoplayer','enabled','yes'),('files_videoplayer','installed_version','1.9.0'),('files_videoplayer','types',''),('firstrunwizard','enabled','yes'),('firstrunwizard','installed_version','2.9.0'),('firstrunwizard','types','logging'),('gallery','enabled','no'),('gallery','installed_version','18.4.0'),('gallery','types',''),('logreader','enabled','yes'),('logreader','installed_version','2.5.0'),('logreader','types',''),('lookup_server_connector','enabled','yes'),('lookup_server_connector','installed_version','1.8.0'),('lookup_server_connector','types','authentication'),('nextcloud_announcements','enabled','yes'),('nextcloud_announcements','installed_version','1.9.0'),('nextcloud_announcements','pub_date','Thu, 24 Oct 2019 00:00:00 +0200'),('nextcloud_announcements','types','logging'),('notifications','enabled','yes'),('notifications','installed_version','2.8.0'),('notifications','types','logging'),('oauth2','enabled','yes'),('oauth2','installed_version','1.8.0'),('oauth2','types','authentication'),('password_policy','enabled','yes'),('password_policy','installed_version','1.10.1'),('password_policy','types','authentication'),('photos','enabled','yes'),('photos','installed_version','1.2.3'),('photos','types',''),('privacy','enabled','yes'),('privacy','installed_version','1.4.0'),('privacy','types',''),('provisioning_api','enabled','yes'),('provisioning_api','installed_version','1.10.0'),('provisioning_api','types','prevent_group_restriction'),('recommendations','enabled','yes'),('recommendations','installed_version','0.8.0'),('recommendations','types',''),('serverinfo','cached_count_filecache','616'),('serverinfo','cached_count_storages','8'),('serverinfo','enabled','yes'),('serverinfo','installed_version','1.10.0'),('serverinfo','types',''),('settings','enabled','yes'),('settings','installed_version','1.2.0'),('settings','types',''),('sharebymail','enabled','yes'),('sharebymail','installed_version','1.10.0'),('sharebymail','types','filesystem'),('support','SwitchUpdaterServerHasRun','yes'),('support','enabled','yes'),('support','installed_version','1.3.0'),('support','types','session'),('survey_client','enabled','yes'),('survey_client','installed_version','1.8.0'),('survey_client','types',''),('systemtags','enabled','yes'),('systemtags','installed_version','1.10.0'),('systemtags','types','logging'),('text','enabled','yes'),('text','installed_version','3.1.0'),('text','types','dav'),('theming','cachebuster','1'),('theming','color','#000000'),('theming','enabled','yes'),('theming','installed_version','1.11.0'),('theming','types','logging'),('twofactor_backupcodes','enabled','yes'),('twofactor_backupcodes','installed_version','1.9.0'),('twofactor_backupcodes','types',''),('updatenotification','core','13.0.12.1'),('updatenotification','enabled','no'),('updatenotification','installed_version','1.3.0'),('updatenotification','types',''),('updatenotification','update_check_errors','0'),('user_status','enabled','yes'),('user_status','installed_version','1.0.1'),('user_status','types',''),('viewer','enabled','yes'),('viewer','installed_version','1.4.0'),('viewer','types',''),('weather_status','enabled','yes'),('weather_status','installed_version','1.0.0'),('weather_status','types',''),('workflowengine','enabled','yes'),('workflowengine','installed_version','2.2.1'),('workflowengine','types','filesystem');
/*!40000 ALTER TABLE `oc_appconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_authtoken`
--

DROP TABLE IF EXISTS `oc_authtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_authtoken` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `login_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `password` longtext COLLATE utf8mb4_bin,
  `name` longtext COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` smallint unsigned DEFAULT '0',
  `remember` smallint unsigned DEFAULT '0',
  `last_activity` int unsigned DEFAULT '0',
  `last_check` int unsigned DEFAULT '0',
  `scope` longtext COLLATE utf8mb4_bin,
  `expires` int unsigned DEFAULT NULL,
  `private_key` longtext COLLATE utf8mb4_bin,
  `public_key` longtext COLLATE utf8mb4_bin,
  `version` smallint unsigned NOT NULL DEFAULT '1',
  `password_invalid` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `authtoken_token_index` (`token`),
  KEY `authtoken_last_activity_index` (`last_activity`),
  KEY `authtoken_uid_index` (`uid`),
  KEY `authtoken_version_index` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_authtoken`
--

LOCK TABLES `oc_authtoken` WRITE;
/*!40000 ALTER TABLE `oc_authtoken` DISABLE KEYS */;
INSERT INTO `oc_authtoken` VALUES (8,'vok','vok','PO4XURwIHNrBJonyzSrIn+CGKqO9ZDWjCTxHQPVPq4xmh5uzenzgJcypvR6xi8N4HQZZvfIEH3OlUeAxoSveir7uTrMq3SOztTj9vIeqcrNUYyWT8x+QJzWEPSVGX0qwkCEbHgpQR92vn3ZJ7WePFIByfT3awgfUWYBlj1sxcEf0Z/VHLlcAO4jGUgmihjTEKDXNoyGk7OyJtgeM/r1z9ZvrJOo4WuCnAlS6XPAPMNgzAEKDHiBRtwmh6KTSFUB59VFR7LbBdnpyTKbtUgnRz8Eq8x1BIFlx5+9QOVNq/uES60ErNgcfjTX6GR5GonTg+g88bUNgMXAxhPhv6M0peg==','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 Edg/112.0.1722.58','ffc586bc024535783081a534670880f2392489af4e788a003130e719b0e0408077d00782c32e82201797e7afd1d20c9c2dd1b19effa9a40eead7b2be1fb5fe4f',0,1,1682686782,1682686512,NULL,NULL,'c3a2957b22318c4116aebc9f693ed05f4f6e0790f44de2453a2a7ce45852a087392ded68719b326a3c84c5d837f42dc4e7200b6cf0b2ee941ffa160fbb4be8960a6ef720df770c2b5d28e527f8475596752d4ab029041b3c97bf665add517a720fa35b26f001da688236163eb4f7815f4a13300da1de26ba2642d3c5248ddc9f1bae3563fb77879340c8615b807d3b53c51c669fb81389969afd0121748457cc94332855439dc01de916c2c0231f962284bf11d246ac74c99909825d3f4ac46a82a5539a460d3d3b5fbd2e21e80188d4636f91324d3957e60f4f8acfe0f45b78dfd7705c3209a91df255c90700e2f641ab6948289fe93daa330660ceb4465de48294693290adc3c1f52de6712ed2893563d4bfb869b98559fdd7542707177b4389ca62d591066a84eed91ba07c1980ee67a55699aa0f29d6d423b324be44c475ec27aa7d30f8671670903e46db15909c2346584ad93c01aa2787eda35ebef00356e3cca22b94133a4b2a2a9f9cad3d50334c6f2b2e9624eee155320f9956889f0049be03e66c606212294093f6a97eb8f8259952be4a1463b27df7f0b2b362146759eef5eaa6fd4c35b15cc86b5c6de6e2beabbb547fb5daa5b01a1c42e462c4ed5ff6e5bc6d5184879e55ef201c8020ba1a496fb9f676f5e85139cfae353d4a4013ce7ccc56d2dee0e9cdbfea0922bdf1e5bce9ac3920128ce84e4beffd776a0989047499d601d4e7433a8936fc60c6f8287c54c874a2614f988e5b23c5872e95faa045ca1b5bd488183ecef135745d2ef7153d7da4c765a21b44c2182afac374bfbcb234c93e2b3f37fba836d888de9090dce12f63b9838c2a126ebd97df16ecf20b6c256b2da10da969098e14e1b89739dda84dcba0c5a8d67501dcdf41f0eb25cbbd81069a90a32064c8f8f97e6d7a4a501430678a3e258e119acd8f7816953062fd097fea882009b503932803612af1c9b04b31e9de44a02f967381598db58cbe68f571ba742d1dee25a6554e34dc9bb8a0924d8dd934cd211a3352bc979d720e5fa47bdcded6b69ba4a1546d509d55d19d7ac85e5e162a3e38225914af1ab3043c31f6902493f739da926edc2a8cf69878818ec0584f85a47d0937beb312ce326b41aa7b3ecd2eb3cfd753cb76837177fde980e5f1cee56c5899a20aa140004d41e95b8413f4a6470e183be02578c204ed16d80999f28bb10ddd381bdbd5daf29c429e769dff592996679fae63c65f5443cb94b2a847e4d8ed216092e4da946ce7acdc7717d5a6847b88ac2906cbd27f9eb4ff6f9c95e2367adcff06b144dd61a089a92ee13dafcd754638a9191a96550b4754e550ce920c700d7d826dff2ff16255897acaf26ce46affd5d6a93ee9ed19d9c06034621b64451be7cb65c3db039cee519b6000aae4c05d9021690a89065ba120a1eaf1b9b83103b2297f05200500b0af03d78619aceacaec038dc2f1bafefa93383c31b33e25616b8938adc271d0ad3f22fa4df7b98c95fd26070c55603ff5015f0738fa8201b9183b2f498a8a35f5fc9f1e0c1ef8436668dbc52222a3f6591400166d6c8f92c9013977900d80106e4a18888ec76d04465cb1d803d1e15b2e9684b1552a93ee57a78d2396ce7bccf8300998b26278f9b39af3fd2500e46b642d8024a09b5304856191849479ae306bde15dafa8d235c88968ad0942d1153fc89db9be139f872a45bf19479e52057002f772030e7ea54b693279729e723f7505d096e11150c7275a34280947af54f0d753139fc04698fd3b78f49b9d19ff84026fb03cec1e52e90590f1c0ab2b71ea00f6edb98f4659b62118f549c236c1afe75bafbed31c389bf8caf7964ff6d5ffda9ebd724c96a3780b4f06753991bd00c5486f2408ced29fe44fb2f0902298cbd599dd24a6d731d68b30528a0dc6b933a2ba33464c4468158b46d77ff7e447826d93cb41dcd94c4291dcb70b0d9df6e3508d68e23aeae1d026af395f0064db3b7a662db663c9c88b85105c4a9476db2c3a01d2922c8899c745aabe801d9328e0b5fb5d81a2dcfe1fc0e20873c0ba441a5c61d41ddc2ce2e2122c698281bc0eca6591a997491c4a5b033f92fcd7520aa639a23be26ef759469ebce37c6c20e8fbe2c1db278f332e34936f622947f1bc37e8849ade71667bd6ed5bfd0151d0b39d57bb48fde1e5c506cb813d76f2cd35ff8d3fbc725594c97fca7e71f1087d24b39f18e63fece60e9f9b4937922df64aaf54b9c254f28199d69b1c7b44beff880e14322a940ad4720fa57f53100c89d073ff80f18104c57230144606626a7f4c3d06523cc27d80260836509c3c353d7e1126d7efabd29b84068e9ed31f4b9c5b9b6a9f4248e9984488b012d89379139a6983a6b397586ec3ef4313c236b6a7628ab15c8f4ca96b8cec71ef67c|5001d51b550d2209862a8dfeef7c355b|0603843efc4a4e25fe93d724d048c724e891cf2f7c75d0c707074dbf8592fe6e971273d941a97433072bda3ec5c6cab4216ecf6937363a7276294f043503c880|2','-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA12enRrFVZTLqrR9x0gDx\nZaLMzPfF4NyJxnICd6lEcv5DWZFTtC0dPVygk80bFA4VAi6IKB5k0wy/qGcRiFXX\n0wrr+wa7O1Y+M7YOPjXlnvQCGYjmsBKHd6/mLXusPTzFQKAgS8vM+wa/ZCwJS9M8\nyR1CcT3N6oATqDP0AnyzG0yBCvTEmYaYH3Q+HxydALve66ZaRNeMVWrrrluiydS4\nKYITtFCqkAHqjstfhfD5GM1gNDeSwMAXvNaw1jkGbRO3PHaaV6V0QxAvOD3wlQlC\nw2+t1+aMruHcDBjmFGWic9CWGDODZ4g4o1UQ+MI7AdilCzXwWH0RD7b/4tru2glC\n5QIDAQAB\n-----END PUBLIC KEY-----\n',2,0);
/*!40000 ALTER TABLE `oc_authtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_bruteforce_attempts`
--

DROP TABLE IF EXISTS `oc_bruteforce_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_bruteforce_attempts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `occurred` int unsigned NOT NULL DEFAULT '0',
  `ip` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `subnet` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `metadata` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `bruteforce_attempts_ip` (`ip`),
  KEY `bruteforce_attempts_subnet` (`subnet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_bruteforce_attempts`
--

LOCK TABLES `oc_bruteforce_attempts` WRITE;
/*!40000 ALTER TABLE `oc_bruteforce_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_bruteforce_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_invitations`
--

DROP TABLE IF EXISTS `oc_calendar_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_invitations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `recurrenceid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `attendee` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `organizer` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sequence` bigint unsigned DEFAULT NULL,
  `token` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `expiration` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_invitation_tokens` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_invitations`
--

LOCK TABLES `oc_calendar_invitations` WRITE;
/*!40000 ALTER TABLE `oc_calendar_invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_reminders`
--

DROP TABLE IF EXISTS `oc_calendar_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_reminders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `calendar_id` bigint NOT NULL,
  `object_id` bigint NOT NULL,
  `is_recurring` smallint DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `recurrence_id` bigint unsigned DEFAULT NULL,
  `is_recurrence_exception` smallint NOT NULL,
  `event_hash` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `alarm_hash` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `is_relative` smallint NOT NULL,
  `notification_date` bigint unsigned NOT NULL,
  `is_repeat_based` smallint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_reminder_objid` (`object_id`),
  KEY `calendar_reminder_uidrec` (`uid`,`recurrence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_reminders`
--

LOCK TABLES `oc_calendar_reminders` WRITE;
/*!40000 ALTER TABLE `oc_calendar_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_resources`
--

DROP TABLE IF EXISTS `oc_calendar_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_resources` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `backend_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `resource_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `group_restrictions` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_resources_bkdrsc` (`backend_id`,`resource_id`),
  KEY `calendar_resources_email` (`email`),
  KEY `calendar_resources_name` (`displayname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_resources`
--

LOCK TABLES `oc_calendar_resources` WRITE;
/*!40000 ALTER TABLE `oc_calendar_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_resources_md`
--

DROP TABLE IF EXISTS `oc_calendar_resources_md`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_resources_md` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` bigint unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `value` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_resources_md_idk` (`resource_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_resources_md`
--

LOCK TABLES `oc_calendar_resources_md` WRITE;
/*!40000 ALTER TABLE `oc_calendar_resources_md` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_resources_md` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_rooms`
--

DROP TABLE IF EXISTS `oc_calendar_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_rooms` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `backend_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `resource_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `group_restrictions` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_rooms_bkdrsc` (`backend_id`,`resource_id`),
  KEY `calendar_rooms_email` (`email`),
  KEY `calendar_rooms_name` (`displayname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_rooms`
--

LOCK TABLES `oc_calendar_rooms` WRITE;
/*!40000 ALTER TABLE `oc_calendar_rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendar_rooms_md`
--

DROP TABLE IF EXISTS `oc_calendar_rooms_md`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendar_rooms_md` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `room_id` bigint unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `value` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `calendar_rooms_md_idk` (`room_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendar_rooms_md`
--

LOCK TABLES `oc_calendar_rooms_md` WRITE;
/*!40000 ALTER TABLE `oc_calendar_rooms_md` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendar_rooms_md` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarchanges`
--

DROP TABLE IF EXISTS `oc_calendarchanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarchanges` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `calendarid` bigint NOT NULL,
  `operation` smallint NOT NULL,
  `calendartype` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `calid_type_synctoken` (`calendarid`,`calendartype`,`synctoken`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarchanges`
--

LOCK TABLES `oc_calendarchanges` WRITE;
/*!40000 ALTER TABLE `oc_calendarchanges` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarchanges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarobjects`
--

DROP TABLE IF EXISTS `oc_calendarobjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarobjects` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `calendardata` longblob,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendarid` bigint unsigned NOT NULL,
  `lastmodified` int unsigned DEFAULT NULL,
  `etag` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  `componenttype` varchar(8) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstoccurence` bigint unsigned DEFAULT NULL,
  `lastoccurence` bigint unsigned DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `classification` int DEFAULT '0',
  `calendartype` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `calobjects_index` (`calendarid`,`calendartype`,`uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarobjects`
--

LOCK TABLES `oc_calendarobjects` WRITE;
/*!40000 ALTER TABLE `oc_calendarobjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarobjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarobjects_props`
--

DROP TABLE IF EXISTS `oc_calendarobjects_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarobjects_props` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `calendarid` bigint NOT NULL DEFAULT '0',
  `objectid` bigint unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `parameter` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendartype` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `calendarobject_index` (`objectid`,`calendartype`),
  KEY `calendarobject_name_index` (`name`,`calendartype`),
  KEY `calendarobject_value_index` (`value`,`calendartype`),
  KEY `calendarobject_calid_index` (`calendarid`,`calendartype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarobjects_props`
--

LOCK TABLES `oc_calendarobjects_props` WRITE;
/*!40000 ALTER TABLE `oc_calendarobjects_props` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarobjects_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendars`
--

DROP TABLE IF EXISTS `oc_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendars` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendarorder` int unsigned NOT NULL DEFAULT '0',
  `calendarcolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `timezone` longtext COLLATE utf8mb4_bin,
  `components` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `transparent` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `calendars_index` (`principaluri`,`uri`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendars`
--

LOCK TABLES `oc_calendars` WRITE;
/*!40000 ALTER TABLE `oc_calendars` DISABLE KEYS */;
INSERT INTO `oc_calendars` VALUES (1,'principals/users/admin','Personal','personal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(2,'principals/users/admin','admincal','admincal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(3,'principals/system/system','Contact birthdays','contact_birthdays',1,NULL,0,'#FFFFCA',NULL,'VEVENT',0),(4,'principals/users/vok','Personal','personal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(5,'principals/users/testuser2','Personal','personal',1,NULL,0,NULL,NULL,'VEVENT,VTODO',0),(6,'principals/users/admin','Contact birthdays','contact_birthdays',1,NULL,0,'#E9D859',NULL,'VEVENT',0),(7,'principals/users/testuser2','Contact birthdays','contact_birthdays',1,NULL,0,'#E9D859',NULL,'VEVENT',0),(8,'principals/users/vok','Contact birthdays','contact_birthdays',1,NULL,0,'#E9D859',NULL,'VEVENT',0);
/*!40000 ALTER TABLE `oc_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_calendarsubscriptions`
--

DROP TABLE IF EXISTS `oc_calendarsubscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_calendarsubscriptions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `displayname` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `refreshrate` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendarorder` int unsigned NOT NULL DEFAULT '0',
  `calendarcolor` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `striptodos` smallint DEFAULT NULL,
  `stripalarms` smallint DEFAULT NULL,
  `stripattachments` smallint DEFAULT NULL,
  `lastmodified` int unsigned DEFAULT NULL,
  `synctoken` int unsigned NOT NULL DEFAULT '1',
  `source` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `calsub_index` (`principaluri`,`uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_calendarsubscriptions`
--

LOCK TABLES `oc_calendarsubscriptions` WRITE;
/*!40000 ALTER TABLE `oc_calendarsubscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_calendarsubscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_cards`
--

DROP TABLE IF EXISTS `oc_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_cards` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `addressbookid` bigint NOT NULL DEFAULT '0',
  `carddata` longblob,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastmodified` bigint unsigned DEFAULT NULL,
  `etag` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  `uid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_abid` (`addressbookid`),
  KEY `cards_abiduri` (`addressbookid`,`uri`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_cards`
--

LOCK TABLES `oc_cards` WRITE;
/*!40000 ALTER TABLE `oc_cards` DISABLE KEYS */;
INSERT INTO `oc_cards` VALUES (1,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:vok\r\nFN:vok\r\nN:vok;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOzd2XJb17Y/5sGeki3baiyrl6zWVn/+e\r\n ZdUHiOVSlXeJZXnSOUueQIDoEiLtChvi7Ityg3pbVEmGgLIhfY5Z7uRLIoAJrDG913uU8ceJhe\r\n BNX9zjDmn/o///Vk/AAAAgEqbLl0AAAAAMHwCAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAA\r\n AAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAAC\r\n ABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEB\r\n AAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAA\r\n AAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAk\r\n IAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAA\r\n AAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAA\r\n AASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABI\r\n QAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAA\r\n AAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAAB\r\n AAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACA\r\n gAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAA\r\n AAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAAS\r\n EAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAA\r\n AAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAA\r\n AAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAk\r\n IAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAA\r\n AAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAA\r\n gAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABA\r\n QAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAA\r\n AACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJ\r\n CAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAA\r\n AAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAA\r\n IAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQ\r\n EAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAA\r\n AAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAAC\r\n QgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAA\r\n AAAAAASEAAAAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAA\r\n AABIQAAAAAEACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAE\r\n hAAAAAAQAICAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAA\r\n AAABAAgIAAAAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAA\r\n EACAgAAAABIQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAI\r\n CAAAAAEhAAAAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAA\r\n AAASEAAAAAAAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAAB\r\n IQAAAAAAACQgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAA\r\n AAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAgAQEAAAAAJCAAAAAAAASEAAAAABAAgIAAAAASEAAAAA\r\n AAAkIAAAAACABAQAAAAAkIAAAAACABAQAAAAAkIAAAAAAABIQAAAAAEACAgAAAABIQAAAAAAAC\r\n QgAAAAAIAEBAAAAACQgAAAAAIAEBAAAAACQgAAAAAAAEhAAAAAAQAICAAAAAEhAAAAAAAAJCAA\r\n AAAAggdnSBQBU0f/6vx2PU6fnSpdR1PqjVvxf/+d26TJgIv3P/8uH8T/+p0Olyyju//m/X8T/9\r\n /++LF0GQGXoAAAYgnqtWbqE4j69PB/vve9rBvZrZibi5q2F0mWMhUbdZynAIHkzAxgCL60RMzN\r\n Tcfu2RQzs1/UbC7G46BVt40k7tre7pcsAqBTfLgBDsL3djY0n7dJlFHf33mLpEmDi+Lt5paaTC\r\n mDgBAAAQ+Ll1RgA7NfsrPb/iIhutx9LDZ+hAIPmrQxgSJYazeh2+6XLKGp6eiru3LWbCW/rxmc\r\n LsbDg9ezxejte7vRKlwFQOb5hAIbk5U4vvn5sDEA7M7w9fy+vOEcFYDgEAABD5CU24tKluXj/i\r\n K8b+DuzsxGffa79v9Ppx/IDn50Aw+CNDGCIHiw1Y2/PGIAxAPh7n32u/T8iYm21Fa1W7s9NgGH\r\n xLQMwRK1WP9ZWW6XLKE5bM/y9e/cPlS5hLNS+2C1dAkBlCQAAhswYQMTFi8YA4E3m5l4dAJhds\r\n 9mL1YdCU4Bh8TYGMGRfrjSj1cp9mvX09JQuAHiDz28uxvz8VOkyiltZbkW3W7oKgOoSAAAM2d7\r\n eq5fa7JwDAK8nIHulXtP+DzBMAgCAEWjUvdRevDgXRz7wtQN/ND8/pf0/InZedGP9katTAYbJm\r\n xjACDz6qh0vXxoD0AUAf3bz1kLMzWn/X2o0o+/wf4ChEgAAjECvF+61Dm3O8Ff8XbxSq/mMBBg\r\n 2AQDAiNRdbRUXLszFBx/66oH/ND8/Fdeua//f2tqLpxud0mUAVJ63MIAR+cc/OvHPX3Ifb20MA\r\n H7v1m3t/xGuSwUYFQEAwAg1Gl5y72l3hv+i/f+VuvZ/gJEQAACMkCuuIi5cnDcGABGxsKD9PyJ\r\n i81knnm/ulS4DIAVvYAAj9P13e/Hjj150dQHAq/b/2Vnt/3b/AUZHAAAwYmZdtT1DRMTde4dKl\r\n zAWfCYCjI4AAGDE3AYQcf7CfHz0ka8g8lpcnIqr1+ZLl1HcxpN2bG/nPhwVYJS8fQGM2E8/deP\r\n 771x3pQuAzG7fWdT+HxE17f8AIyUAACjAYYDan8lNABbR7fZjyc0oACMlAAAooF5vRq/XL11GU\r\n efOz8XRozOly4CRO3RoKq5c1f7/eL0dL3d6pcsASEUAAFDAr//sxZMnxgDu3HUFGvncvrMYMzP\r\n a/x3+BzB6AgCAQhwGaAyAnLT/R3Q6/Vh+IAAAGDUBAEAhS41mdLvGAIwBkMnhw1Nx+Yr2/7XVV\r\n rRauT//AEoQAAAUsrvbj/VH7dJlFGc3lEy0/79S0wEFUIQAAKAgtwEIAMjF8x7RbPZi9WGrdBk\r\n AKQkAAApaWW5Fp5O7Dfbsubk4dswYANX33vvT8ell7f8ry63odktXAZCTAACgoHa7bycs7IqSw\r\n 5272v8jdD4BlCQAACjMLKwAgBw85xE7L7rOPgEoSAAAUNjaaiuazV7pMoo6c3Yujh83BkB1vX9\r\n kOi5dmitdRnFLjWb0c089ARQlAAAorNsN92GH3VGq7c7dxZie1v5fq/msAyhJAAAwBupeigUAV\r\n JrnO2Jray+ebnRKlwGQmgAAYAw8Xm/Hzovcx2KfPjMXJ04YA6B6jnwwHRcvav9v1AWdAKUJAAD\r\n GQL//ajY2uzt37ZJSPdr/X9HpBFCeAABgTNgd0yZNNXmuIzafdeL55l7pMgDSEwAAjIknTzqxt\r\n ZX7BdkYAFXzwYfTceGC9n+7/wDjQQAAMEZ0AdgtpVru3dP+H+GzDWBcCAAAxoiXZAEA1eJ5jth\r\n 40o7t7dyHnAKMCwEAwBjZfLYXz5/nHgM4dXouPv7YGACT76OPpuP8hfnSZRRX0/4PMDYEAABjp\r\n v7FbukSirNrShV4jiO63b4bTgDGiAAAYMwYA4i4e/9Q6RLgwO7e8xw/Xm/Hy51e6TIA+BcBAMC\r\n Y2drqxtONdukyivrkk9k4edIYAJPr6NGZOHfe6f8CTYDxIgAAGEN1L826AJho2v8jOp1+LD/wW\r\n QYwTgQAAGOoUW9Gr9cvXUZRFlBMMs9vxNpqK1qt3J9jAONGAAAwhnZe9OLrx7nHAE6enI1PTs2\r\n WLgP27dixmTh7Tvt/zYGmAGNHAAAwpszO2kVlMt2777ltNnux+rBVugwA/kAAADCmHiw1Y28vd\r\n /vsnbsWUkwewVXEynIrut3SVQDwRwIAgDHVbPbjq7XcO2jGAJg0x4/PxOkz2v/rNe3/AONIAAA\r\n wxowB2E1lsmj/j9h50Y31R7nPMAEYVwIAgDH25Uoz2u3cYwACACaJ5zViqdGMfu6PLYCxJQAAG\r\n GOdzqsQILOPP56NU6eNATD+Pv54Jk6d1v5fq+X+zAIYZwIAgDFX9zJtV5WJ4DmN2Nrai6cbndJ\r\n lAPAaAgCAMffVWit++61XuoyiLKyYBHfvHypdQnHOLQEYbwIAgDHX60UsP8j9Un3ixGycPmMMg\r\n PF18uRMfPKJZ1THEsB4EwAATAAv1boAGG/3/sPu/+azTjzf3CtdBgBvIAAAmABfP27Hr//sli6\r\n jqDt3BQCML8+noBJgEggAACbEUiP3y/WJE7Nx5qwWa8bPqdOzcfKkZ9P8P8D4EwAATIhabbd0C\r\n cUZA2AceS4jNp60Y3s7d5cSwCQQAABMiO++3Yuffso9X6vNmnEkAIioaf8HmAgCAIAJkr3F9vh\r\n xYwCMl9NnZuPEidzPZLfbTz+iBDApBAAAE6T+hTEAu62ME89jxOP1drzc6ZUuA4C3IAAAmCA//\r\n tiNZ993SpdRlAUX48TzqDMJYJIIAAAmTParto4dm42z53K3XDMezpydjePHcz+LnU4/lh/k/kw\r\n CmCQCAIAJ06jvRq/XL11GUXfvHSpdAsS9+57DtdVWtFq5P48AJokAAGDC/PJLLzY2jAFAaXfuL\r\n pQuobiac0kAJooAAGACZR8DOHp0Js6dnytdBomdOz8Xx47lbv9vNnux+rBVugwA9kEAADCBluq\r\n 70e3mbrvVBUBJnr+IleVWdLulqwBgPwQAABPot9/68Xi9XbqMou7ctQCjHM9fRL2m/R9g0ggAA\r\n CZU9pfvo0dn4vwFYwCM3vkLc3H06EzpMoraedGN9Ue5Q0iASSQAAJhQK8ut6HRyjwHc04ZNAZ6\r\n 7iKVGM/q5P34AJpIAAGBCtVr9WFvNfQCXNmxK8NxF1JIfRAowqQQAABMs+xVcH340ExcvGgNgd\r\n C5enIsPP8rd/r+1tRdPk19FCjCpBAAAE2z1YSuazV7pMoqyG8so3bvveWvU7f4DTCoBAMAE63Z\r\n fnQWQmQCAUbp9x/NW1/4PMLEEAAATrlE3BmAMgFH49NO5+ODD3O3/m8868Xxzr3QZALwjAQDAh\r\n Hv0VTtevsw9BnDXqeyMgOfM7j/ApBMAAEy4fj9iKXkXgDEARkH7v/l/gEknAACogOwv5R98OBO\r\n XLhkDYHguX5mPIx/kbv/feNKO7e1u6TIAOAABAEAFfPNNJ/2LufZshsnzFVHT/g8w8QQAABWR/\r\n TBA7dkMy9RUxO3bC6XLKKrb7cdSQwAAMOkEAAAVYQxgJj791BgAg3f5yny8fyR3+//j9Xa83Ml\r\n 92ChAFQgAACri2fd78cMPua/n0qbNMNy777nKHjACVIUAAKBCGjVjADBIU1MRt27nfq46nX4sP\r\n xAAAFSBAACgQrLf0X3kg5m4fGW+dBlUyNVr8/Hee7lfl9ZWW9Fq9UuXAcAA5P5GA6iYn3/uxrd\r\n PO6XLKMoYAIPkeYqofZG7swigSgQAABWT/jaA2wsxNVW6Cqpgelr7f7PZi9WHrdJlADAgAgCAi\r\n qnXm9Hr5W3Xff/ITHx62RgAB3f12nwcPpz7VWlluRXdbukqABiU3N9qABX04tde/OPrdukyitK\r\n 2zSB4jiLqyQ8WBagaAQBABWW/sssYAAel/T9i50U31h/lDhMBqkYAAFBBD5aasbeXewzAbQAcx\r\n LXr83HoUO7XpKVGM/p5P0YAKin3NxtARe3u9tPv3Gnf5iDu3T9UuoTiasmvFQWoIgEAQEVln92\r\n 9fWfRGADvZGYm4uathdJlFLW1tRdPN3JfKQpQRQIAgIpaWW5Gu523f/e996bjylVjAOzf9RsLs\r\n biY+xUp+zkiAFWV+9sNoMI6nYiHX+Z+iTcGwLvw3ETUtf8DVJIAAKDCsr/E37ptDID9mZmJ+Px\r\n m7vb/zWedeL65V7oMAIZAAABQYWurrdjd7ZUuo5j33puOq9eMAfD2bnym/T97cAhQZbm/4QAqr\r\n teLWH6Q+2VeOzf74Xkx/w9QZQIAgIrLvpt36/ZiTPu24y3Mzmr/33jSju3tbukyABgSr0QAFff\r\n 143a8+DXvC/3hw24D4O3c+GwhFhZyvxrVkgeGAFWX+1sOIIF+P2KpkfulXls3b+P+fxwqXUJR3\r\n W4//WcFQNUJAAASyL6rZwyAvzM396oDILPH6+14uZP30FCADLwOASTw7dNO/Pxz3mu9Dh92GwB\r\n v9vnNxZifz31npMP/AKpPAACQRPaXe2MAvEn256PT6ae/MQQgAwEAQBL1L3ZLl1CUMQBeZ24u4\r\n vqN3O3/a6utaLX6pcsAYMi8CgEk8cMP3dh81ildRjGHDk3HtevGAPizm7e0/9eSB4QAWQgAABK\r\n pJz8M8O693Ke889eyt/83m71YfdgqXQYAIyAAAEgk+zkAN28tGAPgd+bnp9K3/68st6LbLV0FA\r\n KPgNQggke3tbmw8aZcuo5hDh6bTL/b4vZu3FmJuLnf7f72m/R8gCwEAQDK19GMAudu9+b1793M\r\n /DzsvurH+KG8oCJCNAAAgmaVGM7rdvKd937y1EDMzpatgHCwsTMW167k7QpYazejn/TgASEcAA\r\n JDMy51efP04747f4uJ0+kUfr9y6vRCzs7nb/7N3BAFkIwAASMhtALnbvnkl+60QW1t78XQj79W\r\n gABkJAAASWn7QjL29vH2/xgBYXJyKq9fmS5dRVPZbQQAyEgAAJNRq9WNtNe+934uLbgPI7tbtx\r\n fTt/9k7gQAyEgAAJJV9988YQG7Zf/+bzzrxfHOvdBkAjJgAACCpL1ea0Wr1SpdRjDGAvLT/2/0\r\n HyEoAAJDU3l7EynLeMYCFhem48ZkxgIxu31mMmZnc7f/ZO4AAshIAACTWqO+WLqGo7G3gWd27n\r\n /v3vvGkHdvb3dJlAFCAAAAgsUdftePly7xjAJ/fXIjZ2dJVMEqHDk3F5Su52/9r2v8B0hIAACT\r\n W60U8WMq7GDAGkM+du7nb/7vdfiw18v7NA2QnAABIrlEzBkAe2X/fj9fb8XInb9cPQHYCAIDk/\r\n vGPTvzzl7zzwJ99bgwgi8OHp+LTy7nb/x3+B5CbAACAaCRuCV5YmI7PPjcGkEH29v9Opx/LD/L\r\n +rQMgAAAgIurJxwDu3M3dFp5F9vb/tdVWtFr90mUAUJAAAID4/ru9+PHHvdJlFPP5zUVjABX33\r\n vvT6dv/a1/kDvoAEAAA8C+ZZ4Pn56eMAVTcnbuLMT2dt/2/2ezF6sNW6TIAKEwAAEBERNST7w7\r\n eu3+odAkM0b37udv/V5Zb0c171icA/yIAACAiIn76qRvffdspXUYxNz5biLm50lUwDO8fmY6LF\r\n 3P/crOf8wHAKwIAAP5Lo553kTA/PxWf38y9S1xVd+/lbv/fedGN9Uft0mUAMAYEAAD8l3q9Gb1\r\n e3lPCs58SX1XZf69LjWb08/5ZA/BvBAAA/Jdf/9mLJ0+MAVAdRz6YjgsXcv9Sa7W8B3wC8HsCA\r\n AB+J/NhgHNzU3HzVu7d4qrJ3v6/tbUXTzfyhnoA/J4AAIDfWWo0o9vN2y+cvV28au4l/31mvt4\r\n TgD8TAADwO7u7/dQHhl2/sRDz83l3jKvkgw+n49z53O3/de3/APwbAQAAf5L5yrBXYwALpctgA\r\n O4lb//ffNaJ55t7pcsAYIwIAAD4k5XlVnQ6xgCYbPfuHypdQlF2/wH4IwEAAH/Sbvdj9WGrdBn\r\n FXLtuDGDSHT06k7793/w/AH8kAADgL9XS3wZgDGCSZe/i2HjSju3tbukyABgzAgAA/tLaait2d\r\n 3ulyygm+wJy0mX//dW0/wPwFwQAAPylbjdiZTnvIuLa9YVYWDAGMImOHZuJs+fytv93u/1YauT\r\n 92wXg9QQAALxW5kPEjAFMruy7/4/X2/FyJ2/3DgCvJwAA4LUer7dj50XeOeLsC8lJlf335vA/A\r\n F5HAADAa/X7kbqV2BjA5Dl+fCbOnM3b/t/p9GP5Qd6/WQDeTAAAwBtlPkxsdnYqbt02BjBJsu/\r\n +r622otXqly4DgDElAADgjZ5udGJra690GcXcvXeodAnsw737uQOAzNd3AvD3BAAA/K3MM8VXr\r\n 83H4qIxgElw4sRMnDqdt/2/2ezF6sNW6TIAGGMCAAD+VuYA4NUYQO5d5UmRffd/ZbkV3bxndgL\r\n wFgQAAPytzWd78fx55jGA3AvLSXH3fu5xjXpN+z8AbyYAAOCt1BPPFl+5agxg3J08OROffDJbu\r\n oxidl50Y/1Ru3QZAIw5AQAAb8UYgC6AcZZ993+p0Yy+w/8B+BsCAADeytZWN55u5N1hNAYw3rL\r\n /fjJf1wnA2xMAAPDWMi8yrl6bj0OHjAGMo09OzcbJk3nb/7e29uLpRqd0GQBMAAEAAG9tqdGMX\r\n i9nn/HMjDGAcZV99z/zeA4A+yMAAOCt7bzoxdePjQEwXrL/XuqJO3MA2B8BAAD7knm38cpVYwD\r\n j5vSZ2fj447zt/5vPOvF8M+8VnQDsjwAAgH15sNSMvb28YwC37+TebR43dv/zBnIA7J8AAIB9a\r\n Tb78dVaq3QZxWRfcI6bO3dz/z4yd+QAsH8CAAD2LfOi4/KV+Th82BjAODhzdjZOnMjb/r/xpB3\r\n b293SZQAwQQQAAOzblyvNaLV6pcsowhjA+MjejZH5Wk4A3o0AAIB963QivlwxBkBZmX8P3W4/l\r\n hoCAAD2RwAAwDvJPAbw6WVjAKWdPTcbx47lbf9/vN6Olzs5u3AAeHcCAADeyVdrrfjtt5wLkJm\r\n ZqfSHz5V2996h0iUUlTmAA+DdCQAAeCe9XsTyg7yLEAFAWZnb/zudfuq/PQDenQAAgHeW+Q7yy\r\n 1fm4733fY2WcP7CXBw9OlO6jGLWVlvRavVLlwHABPLmAsA7+/pxO379Z85ryKanp+L27YXSZaR\r\n 0L/Huf0RE7Yvd0iUAMKEEAAAcSOaTyDO3oZeUefyi2ezF6sO8N3AAcDACAAAOpFbLuxv56WVjA\r\n KN28eJcfPhR3vb/leVWdHM23QAwAN5aADiQ777di59+2itdRhHT024DGLXsXRf1xIEbAAcnAAD\r\n gwDJfSZZ9QTpqmQOXnRfdWH/ULl0GABNMAADAgdUTH0p26dJcvH/E1+koXLo0Fx98mLf9f6nRj\r\n L7D/wE4AG8sABzYjz9249n3ndJlFGEMYHSyd1vUEl+7CcBgCAAAGIh64sVJ9oXpqGQOWra29uL\r\n pRs6QDYDBEQAAMBCN+m70ejn7ky9eNAYwbJevzMeRD/K2/2c+ZwOAwfG2AsBA/PJLLzaS7lBOT\r\n 0/pAhiy7D/fzB02AAyOAACAgcm8SMm+QB2mqamI27cXSpdRzOazTjzfzHnVJgCDJQAAYGCW6rv\r\n R7eYcA7hwYS6OfOBrdRguX5mP94/kbf/PHKwBMFjeVAAYmN9+68fj9Zz3lLsNYHiyd1eY/wdgU\r\n AQAAAxUvbZbuoRisi9Uh2FqKuL2nbw/140n7dje7pYuA4CKEAAAMFDLD1rR6eQdA/jgQ1+tg3T\r\n l6ny8917en2lN+z8AA5T3GxWAoWi3+7G22ipdRhHGAAYvc1dFt9uPpYYAAIDBEQAAMHC1L/KOA\r\n dxLvGAdtOnp3O3/j9fb8XKnV7oMACpEAADAwK0+bEWzmXPhcuHivDGAAbl6bT4OH877s3T4HwC\r\n DlvdbFYCh6XYjVpZzjgFE6AIYlMzt/51OP5YfCAAAGCwBAABD4TYADmJ6OuLW7bw/x7XVVrRaO\r\n Q/TBGB4BAAADMX6o3bsvMh5fdn5C/Px0Ue+Yg/i2vX5OHQo788w8zkaAAxP3m9WAIaq3494sJS\r\n 3hVkXwMHcvSpFjwEAAB5wSURBVHeodAnFNJu9WH2Yd4QGgOERAAAwNJkPMcu8gD2omZmIm7cWS\r\n pdRzMpyK7o5m2cAGDIBAABD8803ndjezrmSOXd+Lo4enSldxkS6dn0hdft/5vMzABiuvN+uAIx\r\n Eo553MXPnbt5d7IPIPD6x86Ib64/apcsAoKIEAAAMlTEA9iN7+/9Soxl9h/8DMCQCAACG6tn3e\r\n /HDD3ulyyjCGMD+3fhsIRYX876e1Gp5AzMAhi/vNywAI1NPfKVZ5nb2d5H557W1tRdPNzqlywC\r\n gwgQAAAxd7jGAvAva/Zqdjfj8Zt72/8x/JwCMhgAAgKH7+edufPs0587m2XNzceyYMYC3ceOzh\r\n VhYyPtqUtf+D8CQ5f2WBWCkaomvNtMF8HYy/5w2n3Xi+WbOszIAGB0BAAAjsdRoRq+X83jzzAv\r\n bt/Wq/T/vz8nuPwCjIAAAYCRe/NqLf3yd837zM2fn4vhxYwBv8tnnCzE/P1W6jGLM/wMwCgIAA\r\n EYm8yJHF8Cb3bt/qHQJxWw8acf2drd0GQAkIAAAYGSWGs3Y2zMGwO/Nzb06ADCrmvZ/AEZEAAD\r\n AyDSb/Xj0Vat0GUWcPjMXJ04YA/grN28tpm3/73b7sdQQAAAwGgIAAEbKGAB/lPnn8ni9HS93e\r\n qXLACAJAQAAI7Wy3Ix2O+cYwJ27eRe6rzM/PxXXb+Rt/88ciAEwegIAAEaq04n4ciXnoscYwJ/\r\n dvLUQc3M52/87nX4sP8j5twBAGQIAAEYu865n5nb3v5L557G22opWK2c3DABlCAAAGLm11Vbs7\r\n uace8684P2j+fmpuHY9b/t/7Yvd0iUAkIwAAICR6/UibevzqdNz8fHHxgAiIm7dztv+32z2YvV\r\n hzhsxAChHAABAEfXEd5/rAngl889hZbkV3W7pKgDIRgAAQBGP19vx4tecK6C79w+VLqG4hYXc7\r\n f/1mvZ/AEZPAABAMUuNnF0An3wyGydP5h4DuH1nMWZnc7b/77zoxvqjdukyAEhIAABAMbXMYwD\r\n JuwAyt/8vNZrRd/g/AAUIAAAo5tunnfj5573SZRSReQG8uDgVV67Oly6jmMzBFwBlCQAAKKpRz\r\n 7kYOnlyNj45NVu6jCIyt/9vbe3F041O6TIASEoAAEBR9cR3oWftAsj63x2RN/ACYDwIAAAo6oc\r\n furH5LOeO6J27+RbChw7lbv/PfP0lAOUJAAAoLuuiKOMYwO07izEzk7P9f/NZJ55v5jzzAoDxI\r\n AAAoLjMbdHZ2uGz/ff+u6xBFwDjQwAAQHHb293YeJLzXvRMC+LDh6fi8pW87f+Zgy4AxoMAAIC\r\n xkPVqtI8/no1Tp3OMAdy5m7f9f+NJO7a3u6XLACA5AQAAY2Gp0Yxut1+6jCKydAFk+e/8K1kDL\r\n gDGiwAAgLHwcqcXXz82BlBV770/HZ9eztn+3+32Y6khAACgPAEAAGMj6yFpJ07Mxukz1R4DuHN\r\n 3Maanc7b/P15vx8udXukyAEAAAMD4WH7QjE7HGEAVVf2/700c/gfAuBAAADA2Wq1+fLXWKl1GE\r\n XfuVneB/P6R6bh0aa50GUV0Ov1YfiAAAGA8CAAAGCu1L3ZLl1DEiROzceZsNccAMrf/r622otX\r\n K2dUCwPgRAAAwVlYftqLVyjkvXdU2+ar+d72NrIEWAONJAADAWNnbi1hZNgZQFUc+mI6LF3O2/\r\n zebvVh9mPNZBmA8CQAAGDv1Ws5d0+PHqzcGcPde3vb/leVWdLulqwCA/yYAAGDsrD9qx8uXxgC\r\n q4F7F/nv2I2uQBcD4EgAAMHZ6vYgHSzlPTq9SAPDBh9Nx7nzO9v+dF91Yf9QuXQYA/I4AAICx1\r\n Ei6e3rs2GycPVeNMYB7idv/lxrN6Dv8H4AxIwAAYCz94x+d+OcvOQeo7947VLqEgahSN8N+1Wo\r\n 5O1gAGG8CAADGVr2ecxFVhYXzRx9Nx/kL86XLKGJray+ebnRKlwEAfyIAAGBsNeo5xwCOHp2Z+\r\n Nn5KoQY76qRNLgCYPwJAAAYW99/txc//rhXuowiJn0BXZUxhndR1/4PwJgSAAAw1upf5OwCuHN\r\n 3cgOAKnQwvKvNZ514vpkztAJg/AkAABhrWdupjx6difMXJnMRfe/+5IYXB2X3H4BxJgAAYKz99\r\n FM3vvs254Fq9yZ0DGDSxxcOImtgBcBkEAAAMPayHgY4iWMAJ07MxJmzk9m5cFAbT9qxvZ3z6ko\r\n AJoMAAICxV683o9frly5j5D78aCYuX5msq/Qyt//XtP8DMOYEAACMvV//2Ytvvsk5BvAf/2OyF\r\n tT3/iPn6f/dbj+WGgIAAMabAACAidCo5RwDuH1nMWZmSlfxdj45NRsnT86WLqOIx+vteLnTK10\r\n GALyRAACAibDUaEa3m28M4NCh6fj85kLpMt7K7duTUecwOPwPgEkgAABgIuzu9uPRV+3SZRRxf\r\n 0La6ifx0MJB6HT6sfxAAADA+BMAADAxst4GcOOzhZifnypdxhudODETp07nPP1/bbUVrVa+7hQ\r\n AJo8AAICJsbLcinY730Jrbm4qbt4a7/b623dy7v5HRNS+yBlMATB5BAAATIx2ux9rq63SZRQx7\r\n tfr3Uo6/99s9mL1Yc5nEoDJIwAAYKJk3W29dn0hFhfHcwzg/SPTcfZczvb/leVWdLulqwCAtyM\r\n AAGCirK22Ync333Vrs7PjOwZw69ZCTE+PZzgxbPWk11MCMJkEAABMlG43YmU554nr4zpnf/PWe\r\n NY1bDsvurH+KOfNFABMJgEAABOnXssZAFy7Pn63AczNRVy+Ml+6jCKWGs3o5zuTEoAJJgAAYOI\r\n 8Xm/Hzot8g9dzc1Px+c3xGgO4dn0h5ubGK5QYlVrSIAqAySUAAGDi9Puvdl8zGrcxgHE9l2DYt\r\n rb24ulGp3QZALAvAgAAJlLW3dfrN+ZjZqZ0Ff/ts89yBgCNes7nD4DJJgAAYCI93ejE1tZe6TJ\r\n GbmFhOq5dH49F97nzc/H+kTFKI0Yo6zkUAEw2AQAAEyvrLuy4tN2P23kEo7L5rBPPN/OFTwBMP\r\n gEAABMr6y7szTFZeGdt/8/63AEw+QQAAEys55t78fx5vp3Y94/MxMWLc4VrmI6z58rWUErWzhM\r\n AJp8AAICJVv9it3QJRXxeeAwg6+7/xpN2bG/nu4ISgGoQAAAw0bLuxt68VfY6wM8+zxkAZL19A\r\n oBqEAAAMNG2trrxdKNduoyRO3lyNo4dK3MC//R0xNVr80X+3SV1u/1YaggAAJhcAgAAJl7WXdn\r\n PCh0GePHiXCwu5nuFeLzejpc7vdJlAMA7y/ftDUDlLDWa0ev1S5cxcp8XasO/kbT9P+u4CQDVI\r\n QAAYOLtvOjF14/zjQF8enk+5uenRv7vvXEjXwDQ6fRj+YEAAIDJJgAAoBIy3s0+OzsV166Pdhb\r\n /yAfTcfpMvuv/1lZb0Wrl6zIBoFoEAABUwvKDZuzt5Vug3RjxdXzXr+fb/Y+IqCW9bhKAahEAA\r\n FAJzWY/vlprlS5j5D4bdQBwI9/p/81mL1Yf5nu2AKgeAQAAlZFxDOCDD2fi1OnZkf37riXsAFh\r\n ZbkW3W7oKADg4AQAAlfHlSjNarXzXtI3qUL5z5+fi8OF8rw71mvZ/AKoh37c4AJW1txfx5Uq+V\r\n u1RXct3I2H7/86Lbqw/ynfDBADVJAAAoFIy3tV+8eLcSK4DvJbw+r+lRjP6+c6WBKCiBAAAVMp\r\n Xa6347bdcYwAzM1Nx5epwd+cXFqbi/Pl81//VEp4rAUB1CQAAqJReL+LBUr5F27CvA7xydT5mZ\r\n obfZTBOtrb24ulGp3QZADAwAgAAKifjGMC168PtALiesP0/43MEQLUJAAConK8ft+PXf+a6t+3\r\n 48dk4fnxmaP/8YQcM4yjjtZIAVJsAAIBKqifcvR3WIX3Hjs3E8eOzQ/lnj6vNZ514vrlXugwAG\r\n CgBAACV1Kjnu7t9WNf0Xb1m9x8AqkAAAEAlffftXvz0U64d3E8vz8fUEM7pM/8PANUgAACgsrI\r\n t4hYXp+PChcFf1Xf5Sq4OgI0n7djeznWGBAA5CAAAqKz6F/nGAAbdrn/23GwcPpzrdaGm/R+Ai\r\n sr1jQ5AKj/+2I3vv8t1j/vV64Nt1792LVf7f7fbj6WGAACAahIAAFBp2cYALlyYi/n5wR0EkO0\r\n AwMfr7Xi50ytdBgAMhQAAgEpr1Hej1+uXLmNkZmamBjazPzMTcfFSrgAgW2AEQC4CAAAq7Zdfe\r\n vHkSa4xgGsD2rW/9Ol8zM0N4VqBMdXp9GP5gQAAgOoSAABQedl2da9cHUwAMKh/zqRYW21Fq5W\r\n nWwSAfAQAAFTeUn03ut08C7tTp+fivfcP/hV/NVkAUEt4awQAuQgAAKi8337rx+P1dukyRuqgi\r\n /e5uYiz5+YGVM34azZ7sfqwVboMABgqAQAAKdRruXZ3D9q+/+nl+ZiZyTP/v7Lcim63dBUAMFw\r\n CAABSWH7Qik4nzxjAQQOAK1cXBlTJZMgWEAGQkwAAgBTa7X6sreZp8T5+fDY++ujdv+avDOgqw\r\n Umw86Ib649yjYgAkJMAAIA0sh3y9q67+PPzU3H6zOyAqxlfS41m9PM0hwCQmAAAgDRWH7ai2ey\r\n VLmNkLr/jLv6lT+dSzf/XarmuiQQgLwEAAGl0u68Oe8viXW8C+PRynvb/ra29eLrRKV0GAIyEA\r\n ACAVDId9vbhRzNx7NjMvv//Ms3/N+p2/wHIQwAAQCrrj9qx8yLPfW/7vQ1gdjbizNm5IVUzfur\r\n a/wFIRAAAQCr9fsSDpTyLvv2eA3Dx0nzMzuaY/9981onnm3ulywCAkREAAJBOpl3f/bbzZ5r/z\r\n /QcAECEAACAhJ486cT2do4xgA8+3N85AJcu5Wn/N/8PQDYCAABSatTzHAb4tmMAU1MR5y/kCAA\r\n 2nrTThEAA8J8EAACklKn9+23b+k+fmY2FhRyvBrVEv38A+E85vuUB4A82n+3FDz/kOADu08tvt\r\n 6t/6dMc8//dbj+WGgIAAPIRAACQVv2LHGMAx47Nxkcf/f1X/sWLOdr/H6+34+VOr3QZADByAgA\r\n A0sp0CNzb7O5fupSjAyDT7x0A/p0AAIC0fv65G98+7ZQuYyT+LgD44MPp+PCjt78tYFJ1Ov1Yf\r\n iAAACAnAQAAqdVqOcYA/u56vwtJTv9fW21Fq9UvXQYAFCEAACC1pUYzer3qLwhPfjIbi4tTr/2\r\n /Z2n/ryU59wEA/ooAAIDUXvzai3983S5dxtBNT0+9cQzgQoIDAJvNXqw+bJUuAwCKEQAAkF49y\r\n Z3wF18zBjA9HXH6TPUDgJXlVnS7pasAgHIEAACk92CpGXt71R8DuPiaNv/TZ2Zjbu714wFVUU9\r\n y3gMAvI4AAID0ms1+PPqq+q3h587NxdRfrPPPX6j+/P/Oi26sP6r+qAcAvIkAAAAix93w8/NTc\r\n frM7J/+93Pn/vy/Vc1Soxn96jd5AMAbCQAAICJWlpvRbld/hXjh4p93+zN0ANSSnPMAAG8iAAC\r\n AiOh0Ir5cqf4i8cKF3x/2NzcX8fHHM4WqGY2trb14utEpXQYAFCcAAIB/yTAGcP4PAcDZs3MxP\r\n V3tAwAz/F4B4G0IAADgX9ZWW7G72ytdxlAdPz4TCwv/veA/e7761/9lueYRAP6OAAAA/qXXe3U\r\n lYJVNT0/9rgvgzJlqBwCbzzrxfHOvdBkAMBYEAADwbzK0i58799+L/rNnq30DgN1/APhvAgAA+\r\n DeP19vx4tdu6TKG6uy/AoDZ2YiPT1Y7AMgQ6ADA2xIAAMAfLDWqvWg8869d/1On52JmproHAG4\r\n 8acf2drXDHADYDwEAAPxB1e+MP358NhYWpuL06Wrv/lf99wgA+yUAAIA/+PZpJ37+udoHx50+P\r\n VvpAKDb7Ve+kwMA9ksAAAB/oeqz46fOzMWpCt8A8Hi9HS93qn2lIwDslwAAAP5C/Yvd0iUM1al\r\n Ts3HqVHU7AKoe4ADAuxAAAMBf+OGHbjz7vlO6jKG5fmM+Dh+u5mtAp9OP5QcCAAD4o2p+8wPAA\r\n FR5F/nYseru/q+ttqLV6pcuAwDGjgAAAF6jXqv2GEBV1So+vgEA70oAAACv8csvvdh40i5dBvv\r\n QbPZi9WGrdBkAMJYEAADwBu6Snywry63odktXAQDjSQAAAG+w1GhGt2uefFIY2wCA1xMAAMAbv\r\n NzpxdePjQFMgp0X3Vh/5HcFAK8jAACAv1E3BjARlhrN6GvWAIDXEgAAwN9YftCMTsfKctw5rwE\r\n A3kwAAAB/o9Xqx1drTpYfZ1tbe/F0o1O6DAAYawIAAHgL7pYfb4263X8A+DsCAAB4C6sPW9Fq9\r\n UqXwWs4pwEA/p4AAADewt7eqzvmGT+bzzrxfHOvdBkAMPYEAADwltwxP57s/gPA2xEAAMBbevR\r\n VO16+NAYwbsz/A8DbEQAAwFvq9yMeLFlsjpONJ+3Y3u6WLgMAJoIAAAD2oe42gLFS0/4PAG9NA\r\n AAA+/DNN5345y92nMdBt9uPpYYAAADelgAAAPapbuZ8LDxeb8fLHWcyAMDbEgAAwD416sYAxoH\r\n D/wBgfwQAALBP33+3Fz/+6N75kjqdfiw/EAAAwH4IAADgHTgMsKy11Va0Wv3SZQDARBEAAMA70\r\n H5eVk0AAwD7JgAAgHfw00/d+O7bTukyUmo2e7H6sFW6DACYOAIAAHhH9Zpd6BJWllvRdRMjAOy\r\n bAAAA3lG93oxezxz6qAleAODdCAAA4B29+LUX33xjDGCUdl50Y/1Ru3QZADCRBAAAcAANu9Ejt\r\n dRoRl/TBQC8EwEAABzAUqMZ3a4V6ajUam5fAIB3JQAAgAPY3e3Ho6+0pI/C1tZePN0wcgEA70o\r\n AAAAH1KgbAxiFRt3uPwAchAAAAA5oZbkV7bYxgGGra/8HgAMRAADAAbXb/Xj4pcXpMG0+68Tzz\r\n b3SZQDARBMAAMAAaE8fLrv/AHBwAgAAGIC11Vbs7vZKl1FZAhYAODgBAAAMQLcbsbJskToMG0/\r\n asb3dLV0GAEw8AQAADIg29eGo+bkCwEAIAABgQB6vt+PFr3aqB6nb7cdSQwAAAIMgAACAAen3I\r\n x4sWawO0uP1drzccbYCAAyCAAAABki7+mA5/A8ABkcAAAAD9HSjE1tb7qsfhE6nH8sPBAAAMCg\r\n CAAAYMLvWg7G22opWq1+6DACoDAEAAAyY2wAGo/bFbukSAKBSBAAAMGDPN/di81mndBkTrdnsx\r\n erDVukyAKBSBAAAMATGAA5mZbkVXTcqAsBACQAAYAiMARxMvab9HwAGTQAAAEOwvd2Npxvt0mV\r\n MpJ0X3Vh/5GcHAIMmAAD4/9u7t96YojAAw8t/lviXeiROleACNwTFTDudcVtJIyrWHvo+zy/4s\r\n u72u9cBJjmwC+CPHB+txs7l/wDw1wkAADDJ8dFqbLe+ZG9LOAGAOQQAAJjk/Mt2vDyzlf02Pnz\r\n YjDevvaAAADMIAAAwkcsAb8frCQAwjwAAABOdnqzGZuMYwO8STABgHgEAACZarXbj2dP1vsf4L\r\n 7x7eznev9vsewwAuLMEAACYzF/t32OdAGAuAQAAJnv8aDXW6+2+x/jnOf8PAHMJAAAw2WYzxuN\r\n HjgH8yutXF+Pjx6t9jwEAd5oAAAALODz4vu8R/mkHtv8DwHQCAAAs4Pmzi/Htm2MAN7m62o3jI\r\n wEAAGYTAABgAdvtGCfHPnJvcvbiYnw9F0cAYDYBAAAW4pK7m1kXAFiGAAAAC3l5djE+f3LR3XW\r\n Xl7txeiIAAMASBAAAWNChv90/efpkPdbr3b7HAIAEAQAAFnR06DWA6w4eWg8AWMq9B/ffyu4AA\r\n ABwx9kBAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIA\r\n AAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECA\r\n AAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAg\r\n AAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAA\r\n BAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAA\r\n AQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAA\r\n AECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAA\r\n ABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAA\r\n AAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEDAD9vZqXwrTO92AAAAAElFTkSuQ\r\n mCC\r\nCLOUD:vok@fair-coral.maas\r\nEND:VCARD\r\n','Database:vok.vcf',1678358259,'748bcd238e92b751faebc36c2b1815b0',22470,'vok'),(2,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:testuser\r\nFN:testuser\r\nN:testuser;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAZtElEQVR4nOzbPUocUBhG4WsYhRBIYWljiqwpW3AD7\r\n sbeLmuxmcpOkBjSBAfBGeJksgIt/OESz/Os4K0P37e3vDndDQAAAOBd+zB7AAAAAPD2BAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAA\r\n AAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAA\r\n AACBAAAAAAIEAAAAAAgQAAAAACAgMXsAQA836eDr+PL4cnsGQBjjDHuNpfj6vfZ7BkAPMIFAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAELGYPAOD\r\n 5HrarcXt/MXsGr+Dj/vE4WBzOnjHNar0cu92f2TN4ofXDz9kTAHiCAADwH9tsf43r2/PZM3gFR\r\n 5+/pQPAj9X3sf17N3sGALxrXgAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAA\r\n BAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAA\r\n gQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAA\r\n AAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAICAveXN6\r\n W72CAAAAOBtuQAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAA\r\n AACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAA\r\n AAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAA\r\n AAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAA\r\n AAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAA\r\n QAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAI\r\n EAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAAC\r\n BAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAA\r\n gQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAA\r\n IAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAA\r\n AAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAA\r\n AAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAA\r\n AAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAA\r\n AAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAA\r\n BAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAA\r\n gQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAA4F87diAAAAAAIMjfepALIwBgQAAAAADAgAAAAACAAQEAAAAAAwIAAAAABgQAAAAAD\r\n ARyyy5ISToc2wAAAABJRU5ErkJggg==\r\nCLOUD:testuser@fair-coral.maas\r\nEND:VCARD\r\n','Database:testuser.vcf',1678353689,'fa794aa5f15801c74f72ef860fca262d',9428,'testuser'),(3,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:testuser2\r\nFN:testuser2\r\nN:testuser2;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAZt0lEQVR4nOzbv0keQACH4VM0YCPoAjY2KbOMK7iA2\r\n 6S3cwRniIUByQImCoaIRMU/XybQwigH3/s8E/y6O17uVo4PzhcDAAAAWGqrswcAAAAAH08AAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAA\r\n AAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAA\r\n AACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAA\r\n AAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAE\r\n AAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACB\r\n AAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAg\r\n QAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAI\r\n EAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAAC\r\n AAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAA\r\n AACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAA\r\n AAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAA\r\n AAAIEAAAAAAgAABAAAAAAIEAAAAAAhYmz0AgLfb2v00vuxvz54BMMYY4+rH/Tj5+nv2DABe4AU\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAASszR4\r\n AwNvdXz+NX99uZ8/gHWzurI+N7e6xfHl6N54fFrNn8J9ufj7OngDAK7o3DYAl8PfiaXw//DN7B\r\n u/g895mOgCcHV2Ph5vn2TMAYKn5AgAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAASsH\r\n B+cL2aPAAAAAD6WFwAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIA\r\n AAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECA\r\n AAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAg\r\n AAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAA\r\n BAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAA\r\n AQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAA\r\n AECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAA\r\n ABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAA\r\n AAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQI\r\n AAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEC\r\n AAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAA\r\n AECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAwL927EAAAAAAQJC/9SAXRgAMCAAAAAAYEAAAAAAwIAAAAABgQAAAAADAgAAAA\r\n ACAgQBSHi37UFl+ZQAAAABJRU5ErkJggg==\r\nCLOUD:testuser2@fair-coral.maas\r\nEND:VCARD\r\n','Database:testuser2.vcf',1678353673,'3ab9c2e4f4315785a044f3ad3f23e772',9436,'testuser2'),(4,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:testuser3\r\nFN:testuser3\r\nN:testuser3;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAZuElEQVR4nOzbPU4VUBRG0Qt52pCQQOxtnAA942AKT\r\n oDZ0NMZZmFFooWVNQFCQ3yYyI/wHAEWCLmRvdYIvnrnnLX9s6PVAAAAAF619dkDAAAAgJcnAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgA\r\n AAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIA\r\n AAAABAgAAAAAECAAAAAAAABAgAAAAAELGYPAODpPrx9Nz5u786eATDGGOP7zcU4uPw8ewYAj3A\r\n BAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n QIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAA\r\n ECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAA\r\n BAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAA\r\n AQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAA\r\n AAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABi9k\r\n DAHi65f3N+PLrZPYMnsH7N1tje7Exe8Y0365Px93qYfYM/tH57+XsCQD8hQAA8B+7uL8ahz+OZ\r\n 8/gGext7qQDwKfl1/Hz4Xb2DAB41bwAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAA\r\n AAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQI\r\n AAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEC\r\n AAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAA\r\n AECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAA\r\n Wv7Z0er2SMAAACAl+UCAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAA\r\n gQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAA\r\n IAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAA\r\n AAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAA\r\n AAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAA\r\n AAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAA\r\n AAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAA\r\n BAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAA\r\n gQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAA\r\n IEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAA\r\n CBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAA\r\n AgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAA\r\n AAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAA\r\n AAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQA\r\n AAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEA\r\n AAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAA\r\n AEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAA\r\n CBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAA\r\n AgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAA\r\n AIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAA\r\n ACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQA\r\n AAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEA\r\n AAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBA\r\n AAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQ\r\n AAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIA\r\n AAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAA\r\n AIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAA\r\n ACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAA\r\n AAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAA\r\n AAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABA\r\n AAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQ\r\n AAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIE\r\n AAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAgAABAAAAAAIEAAAAAAgQAAAAACB\r\n AAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAAAAgQAAAAACBAAAAAAIEAAAAAAg\r\n AABAAAAAAIEAAAAAAgQAAAAACBAAAAAAIAAAQAAAAACBAAAAAAIEAAAAAAgQAAAAACAAAEAAAA\r\n AAgQAAAAACBAAAIA/7diBAAAAAIAgf+tBLowAgAEBAAAAAAMCAAAAAAYEAAAAAAwIAAAAABgQA\r\n AAAADAQ2KMtzY+ltekAAAAASUVORK5CYII=\r\nCLOUD:testuser3@fair-coral.maas\r\nEND:VCARD\r\n','Database:testuser3.vcf',1678353743,'1668c9ab3128a660dae9ccdc26469848',9436,'testuser3'),(5,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:disableduser\r\nFN:disableduser\r\nN:disableduser;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSu\r\n DAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nOzdabDd9Z3f+e//nHPPuat0dSW0IBBCb\r\n GLxwo6NV7BDu+3udtuxk3S7nO7KpNvJJJWqzEzN43k0VamkJlMz1Q+mk47jsbudjjPtdtsG08Q\r\n N2GYxmzGbQWAQiwRC0pV097POA0BCZsfS/f3P//d6qaiLBVX+2A90rt76/f7/4v/c+z8PAgAAA\r\n Ki0WuoBAAAAwMknAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAAC\r\n QAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABA\r\n QAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAA\r\n AADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAM\r\n iAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAA\r\n AAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAA\r\n EAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAY\r\n EAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAA\r\n AAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAAD\r\n IgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAA\r\n AAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAA\r\n AABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAG\r\n RAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAA\r\n AAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAA\r\n CADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAM\r\n CAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAA\r\n AAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAAB\r\n kQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAA\r\n AAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAA\r\n AgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACAD\r\n AgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAA\r\n AAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAA\r\n JABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAE\r\n BAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAA\r\n AAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAA\r\n yIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgA\r\n AAAAEAGBAAAAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAA\r\n AQAYEAAAAAMiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABAB\r\n gQAAAAAyIAAAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAA\r\n AAADIgAAAAAAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAA\r\n MiAAAAAAAAZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIA\r\n AAAAAABkQAAAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAA\r\n AAAGRAAAAAAIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAA\r\n ZEAAAAAAgAwIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQA\r\n AAAACADAgAAAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGRAAAAA\r\n AIAMCAAAAAGRAAAAAAIAMCAAAAACQAQEAAAAAMiAAAAAAQAYEAAAAAMiAAAAAAAAZEAAAAAAgA\r\n wIAAAAAZEAAAAAAgAwIAAAAAJABAQAAAAAyIAAAAABABgQAAAAAyIAAAAAAABkQAAAAACADAgA\r\n AAABkQAAAAACADAgAAAAAkAEBAAAAADIgAAAAAEAGBAAAAADIgAAAAAAAGWikHgBAeVw99em4d\r\n OJjqWdwEg0G/ehFL/qDfgyi/9KPQS/6L//c0R+DbvQGvegMVqI9WHnpa3852oOVWBksR2fQfvk\r\n /v/T3K/2lWOzPx1J/PnrRTf0/EwB4HQIAAGSkKGrRiFpEcfL+O1b6y7HUn4/F/lws9udjsXckF\r\n vpzsdSfj4X+XCz25+Jw90AsDxZP3ggA4DUEAADghGrVRqNVG43p2PCm/167vxJHegfjSO9gHO4\r\n diCO92Ze+dl/6+250VmkxAORBAAAAkmjWWrGhtiU2jGx53X++2JuL2d6LcaCzNw50n4/93Ze+t\r\n gcrq7wUAKpBAAAASmm8PhXj9anY2txx3M/P9Q7Fge7zL/3Vef7o3/ejl2gpAAwHAQAAGCpT9em\r\n Yqk/H9tbOoz/XG3Tjxc6eeL6zO57vPB17O7tjrjebcCUAlI8AAAAMvXrRiM3NbbG5ue3ozy325\r\n o7GgOc7T8cL7ac9VwCArAkAAEAljdenYkf9wtgxemFERPQHvdjXeS6eae+Kp9u7Ym/7KdcGAMi\r\n KAAAAZKFW1I+eErg8ro3OoB3PtX8Zz6y8FAQOdPemnggAJ5UAAABkaaRoxvbWzqPPEljszcUz7\r\n cdj98qj8eTKw7EyWEq8EABOLAEAACBeujJw3tjFcd7YxdEf9GJP+8l4fOXBeGL5gVjoH0k9DwB\r\n +bQIAAMCvqBX1OK11dpzWOjs+OvXb8ULn2Xhi5YF4fPmBONw7kHoeALwrAgAAwJsoitrRZwdcP\r\n fXpONB9IR5f/nn8YukeMQCAoSIAAAC8A+sbm2L95CfjyslPxp72U/HI0l2xa/n+aA9WUk8DgDc\r\n lAAAAvEunNrfHqc3t8ZE1vxO/XH4wHl66O55p70o9CwBelwAAAPBrGimacd7YJXHe2CUx15uNX\r\n yzdGw8v3eWKAAClIgAAAJxAU/V1cfnktXHZxMdjd/ux+NnCj+Lp9mOpZwGAAAAAcDIURS22t3b\r\n G9tbOONjdF/cv/CgeWbonutFJPQ2ATAkAAAAn2UxjY3x87efjA1OfigcX74yfL94W8/1DqWcBk\r\n BkBAABglYzWxuOyyY/HJRMfiSdWHop7F26OFzrPpJ4FQCYEAACAVVYr6nHO6HvjnNH3xlMrv4g\r\n 7528UAgA46QQAAICEXnlOgBAAwMkmAAAAlIAQAMDJJgAAAJTIq0PAHXM/iH3dZ1NPAqAiBAAAg\r\n BLa3toZZzTPjUeX74ufzH0/FvpHUk8CYMgJAAAAJVUUtdg5dmmcNfqeuGf+7+KehZujF93UswA\r\n YUrXUAwAAeHMjRTOumrouvnzK/xrnjV6Seg4AQ0oAAAAYElP16bhu+h/FF2f+ZWwe2ZZ6DgBDR\r\n gAAABgym5vb4ovr/2V8cu0/iFYxlnoOAEPCMwAAAIbU+WOXxRnN8+LWue/EY8s/Sz0HgJJzAgA\r\n AYIiN16fiN6Z/P35r+g9jorYm9RwASkwAAACogDNHL4gvbfhf4j1jV6WeAkBJCQAAABXRqo3Gx\r\n 9d+Pj4/889iur4h9RwASkYAAAComK3NHfF7G/51vHf86tRTACgRAQAAoIIaxUh8bM1n4zPTf+B\r\n NAQBEhAAAAFBpO0YvjN/f8D/F1uZZqacAkJgAAABQcZP1tfG5dX8UV01eFxFF6jkAJCIAAABko\r\n ChqccXkJ+Lvz/zzmKxNp54DQAICAABARk5tbo/f3/Cv48zWBamnALDKBAAAgMy0amPxmel/HJd\r\n NXJN6CgCrSAAAAMhQUdTig1Ofit9Y+/tRj0bqOQCsAgEAACBj5469P76w/n+Mydra1FMAOMkEA\r\n ACAzG0cOS3+4fp/FVtGzkg9BYCTSAAAACDG61PxuZmvxIVjV6SeAsBJIgAAABAREfWiEdeu/UJ\r\n 8cPI3U08B4CQQAAAAOM5lkx+Pa9d8ISKK1FMAOIEEAAAAXuPC8Svi09NfjlrUU08B4AQRAAAAe\r\n F1njV4UvzvzR9EsWqmnAHACCAAAALyhrc0d8fmZfxZjtcnUUwD4NQkAAAC8qVNGtsYXZ/5FrKn\r\n PpJ4CwK9BAAAA4C2tbayPvz/zz2NtfX3qKQC8SwIAAABvy2R9bXxu5isxVV+XegoA74IAAADA2\r\n zZVn47Pr/tKTNamU08B4B1qpB4AAGVx65HvxFxvNvWMk6ZeNKKIWtSKWtSjHkVRj9orP4p6tIr\r\n RGCma0ayNRrMYjWbRipGiFc3aaLSK0RirTUSt8Eo4ItY0ZuLzM1+Jbx38k1joH0k9B4C3SQAAg\r\n Jc9vfJoHOztSz2j1EaL8Zior4nx2lSM1SZj4uWva+rrYm1jfaytr4/R2njqmayCtY318bmXI8B\r\n Sfz71HADeBgEAAHjblgeLsdxdjAPx/Bv+OyNFM9bU18fa+kysra+P6cYpsaGxJWYam6JVG13Ft\r\n Zxs6xqnxOdmvhL/7cCfxPJgMfUcAN6CAAAAnFCdQTsOdPfGge7e1/yzydp0bBjZEusbm2NDY0u\r\n cMrI11tU3RFF4LNGwWt/YFJ+b+eP41sE/ifZgJfUcAN6EAAAArJr5/qGYXzkUT608cvTnmsVob\r\n Bk5I7Y0t8fmkW2xaWSbkwJDZsPIqfGZ6T+Iv5r90xhEP/UcAN6AAAAAJNUeLMfu9qOxu/3o0Z9\r\n b39gcpzXPjm2tc2PryI5o1loJF/J2nNY6Oz659otx4+Fvpp4CwBsQAACA0jnQfT4OdJ+P+xd/H\r\n EXU4tTmmbGteU5sa50bGxtbXRkoqZ1jl8bh3sG4c/7G1FMAeB0CAABQaoPox3PtJ+K59hNx+/w\r\n NMVqMx9mj74mzR98bpzXP8mrCkrly8pNxpHcwHlm6O/UUAH6FAAAADJXlwWI8uHRnPLh0Z7SKs\r\n Thr9KI4e/S9cXrz7KgXvrUpg2vWfD7meofi2fbjqacA8Co+JQGAobUyWIqHl+6Kh5fuimYxGue\r\n NXRIXjV0Rp4xsTT0ta/WiEZ+e/nL81wP/dxzs7Us9B4CXuUAHAFRCe7AcDyzeFn9x4N/Hn+//P\r\n +LnCz+Jlf5S6lnZatXG4ndm/odoFWOppwDwMgEAAKic/d09cfPct+NP9/1vccOhP48XOs+knpS\r\n lqfq6+NT0l1LPAOBlrgAAAJXVj148tnxfPLZ8X2xtnhWXjH8ktrd2eovAKtrWOjeumrwu7pj/Q\r\n eopANkTAACALLzyJoF19VPi4omPxs6xS6JRjKSelYXLJ66Jve2nYnf70dRTALImfwMAWZntvRg\r\n /PPKt+OqL/3v8fPH26A26qSdVXlHU4rrp34vJ2nTqKQBZEwAAgCwt9ufi5iP/X3xt/7+JXyzdE\r\n 4NBP/WkShutjcen1305alFPPQUgWwIAAJC1ud5s3Hj4m/GN/f8uHl9+IPWcSts0cnp8dM1nU88\r\n AyJYAAAAQEQd7++L7h74W3zrwJ3Gg+0LqOZX1nvGr4tzRi1PPAMiSAAAA8Cp7Ok/GN/b/u/jRk\r\n b+Jlf5y6jmV9PE1vxsTtTWpZwBkRwAAAHiNQdy3eGv8v/v/TTy6dF/qMZXTqo3FJ9Z+MfUMgOw\r\n IAAAAb2CxPxc/OPzn8dcH/0PM9w6nnlMpZ7TOi/eMXZV6BkBWBAAAgLewu/1ofH3/v3Ua4AS7e\r\n uozsaY+k3oGQDYEAACAt6E9WI4fHP7zuP7Q12O5v5h6TiU0a634e2v/UeoZANkQAAAA3oFdy/f\r\n H1/f/23hmZVfqKZVwanN7XDrxsdQzALIgAAAAvEOL/bn4q9k/jbvnf5h6SiVcNXldzNQ3pp4BU\r\n HkCAADAuzKI2+avj+/OftXrAn9N9aIR16z9QuoZAJUnAAAA/Bp+ufJQfPPAv4/9nb2ppwy1U5v\r\n b4/yxy1LPAKg0AQAA4Nd0uHcg/vLA/xVPrjySespQu3ryN6NZjKaeAVBZAgAAwAnQjU78zex/i\r\n gcX70w9ZWiN16fig5O/kXoGQGUJAAAAJ8wgfnjkW3HH3A9SDxla7xn/QJzS2Jp6BkAlCQAAACf\r\n YTxduir89/F+iP+ilnjJ0iqIWH1/zudQzACpJAAAAOAkeWbo7vnfoa9EbdFNPGTqbm9viwrErU\r\n s8AqBwBAADgJHly5eG4/tDXRYB34YNTvxnNopV6BkClCAAAACfRL1ceiusPfd11gHdorDYRl01\r\n ck3oGQKUIAAAAJ5kI8O68b+JDMVabTD0DoDIEAACAVfDEyoPxg8N/EYNBP/WUoTFSNOPKiU+kn\r\n gFQGQIAAMAq2bV8f/x47nupZwyVC8evjKn6utQzACpBAAAAWEX3Ld4a9y/8OPWMoVEvGvGByet\r\n SzwCoBAEAAGCV3TL31/HE8oOpZwyN80YvjvWNzalnAAw9AQAAIIEbDn0j9rZ3p54xFIqiFh+c/\r\n FTqGQBDTwAAAEigF9343qH/HAu9I6mnDIUzRy+IzSPbUs8AGGoCAABAIov9Oa8HfAcum7gm9QS\r\n AoSYAAAAktKfzZNw2d33qGUPhzNb5MV3fkHoGwNASAAAAErt38ZZ4fPmB1DNKryhqccnEx1LPA\r\n BhaAgAAQAn87eFvxqHu/tQzSm/n2CUxXptKPQNgKAkAAAAl0Bm048bDfxGDQT/1lFJrFCPx/vE\r\n PpZ4BMJQEAACAkni+83Tcs3BL6hml957xD8RI0Uw9A2DoCAAAACVyx/wPYn9nb+oZpdaqjcVFY\r\n 1elngEwdAQAAIAS6Ucvbjz8zegNuqmnlNr7Jz4chW9lAd4Rv2oCAJTM/u6e+On8TalnlNpUfTp\r\n 2tC5IPQNgqAgAAAAldPfC38XB7r7UM0rtonHXAADeCQEAAKCEBtGPW458O/WMUtvWPCem6utSz\r\n wAYGgIAAEBJPdPeFbuWf556RmkVRS0uGrsy9QyAoSEAAACU2I+OfCc6g3bqGaV1/thlEVGkngE\r\n wFAQAAIASm+8f9kDANzFZXxtnts5PPQNgKAgAAAAld9/CrTHXm009o7Q8DBDg7REAAABKrh+9u\r\n GP+xtQzSmt787yYrK1NPQOg9AQAAIAh8MjSPV4L+AaKohYXjF2eegZA6QkAAABDYRB3zN2QekR\r\n pnTt2ceoJAKUnAAAADInHVx6IfZ1nU88opZnGxljf2Jx6BkCpCQAAAEPkdqcA3tA5o+9LPQGg1\r\n AQAAIAhsrv9aOzv7Ek9o5QEAIA3JwAAAAyZexZuST2hlNY1TokNjVNTzwAoLQEAAGDIPLb8s5j\r\n rHUo9o5TOdQoA4A0JAAAAQ2YQ/fjZwo9Szyils0ffm3oCQGkJAAAAQ+jBpTtipb+UekbpTDc2x\r\n CmNralnAJSSAAAAMIQ6g3Y8uHRn6hmldI5TAACvSwAAABhSDy7eEYNBP/WM0tne2pl6AkApCQA\r\n AAEPqcO9APNf+ZeoZpbNh5NSYqK1JPQOgdAQAAIAh5hrA6zujdV7qCQClIwAAAAyxx5cfiOX+Y\r\n uoZpXOGawAAryEAAAAMsX704pGlu1PPKJ1tzXMiokg9A6BUBAAAgCH30KJrAL+qVRuLLSPbUs8\r\n AKBUBAABgyB3s7Yv9nT2pZ5SOawAAxxMAAAAq4LHl+1NPKB2vAwQ4ngAAAFABjy3/LPWE0jmlc\r\n WqMFuOpZwCUhgAAAFABR3oHY1/n2dQzSqUoarG1uSP1DIDSEAAAACpil2sAr3Fq88zUEwBKQwA\r\n AAKgIzwF4rS0j21NPACgNAQAAoCLmerPxYue51DNKZePI1qhHI/UMgFIQAAAAKuSplV+knlAqt\r\n aIem0ZOTz0DoBQEAACAChEAXstzAABeIgAAAFTI3s7uWOkvpZ5RKgIAwEsEAACAShnE0+1dqUe\r\n UypaRM1JPACgFAQAAoGKeWnkk9YRSadXGYn1jc+oZAMkJAAAAFbN75dHUE0pn88i21BMAkhMAA\r\n AAqZrE/Fwe7+1LPKJVTGqemngCQnAAAAFBBz7V/mXpCqWwYEQAABAAAgAra034y9YRSWd/Ykno\r\n CQHICAABABTkBcLxWbTSm6utSzwBISgAAAKig+f6hmOsdSj2jVDwHAMidAAAAUFGuARxvg2sAQ\r\n OYEAACAitrjGsBxPAgQyJ0AAABQUS90nk09oVRcAQByJwAAAFTU/u7e6A96qWeUxpr6uhgpmql\r\n nACQjAAAAVFQ/enGw+0LqGaVRFLWYaWxKPQMgGQEAAKDC9nWeSz2hVNbWN6SeAJCMAAAAUGH7u\r\n gLAq03X16eeAJCMAAAAUGEvOgFwnOmGEwBAvgQAAIAKe7HzXAwG/dQzSmONEwBAxgQAAIAK60Y\r\n njvRmU88oDVcAgJwJAAAAFXewty/1hNIYr095FSCQLQEAAKDiDnVfTD2hVFwDAHIlAAAAVNxs1\r\n wmAV3MNAMiVAAAAUHGzvf2pJ5SKNwEAuRIAAAAqzgmA47kCAORKAAAAqLjF/lys9JdTzyiNydq\r\n a1BMAkhAAAAAycKjnQYCvGK9PpZ4AkIQAAACQgbneodQTSmPCCQAgUwIAAEAG5nqzqSeUxnhtM\r\n vUEgCQEAACADDgBcEytqMdoMZ56BsCqEwAAADIwLwAcZ6LuGgCQHwEAACADc30B4NU8BwDIkQA\r\n AAJABVwCON17zJgAgPwIAAEAGFvtz0Rt0U88ojQmvAgQyJAAAAGRisT+XekJpOAEA5EgAAADIx\r\n GJ/PvWE0vAqQCBHAgAAQCaW+gupJ5RGsxhNPQFg1QkAAACZWBYAjmrWBAAgPwIAAEAmnAA4plm\r\n 0Uk8AWHUCAABAJgSAY1wBAHIkAAAAZMIVgGNcAQByJAAAAGTCCYBjXAEAciQAAABkQgA4pl40o\r\n h6N1DMAVpUAAACQic6gnXpCqbgGAORGAAAAyERnsJJ6Qqm4BgDkRgAAAMiEEwDH8yYAIDcCAAB\r\n AJpwAOF6jGEk9AWBVCQAAAJloOwFwnFpRTz0BYFUJAAAA2RhEd9BJPaI06iEAAHkRAAAAMiIAH\r\n FPzrTCQGb/qAQBkpN1fTj2hNIrCt8JAXvyqBwCQkX70U08oDScAgNz4VQ8AICMCwDH1opF6AsC\r\n qEgAAADLSH/RSTyiNwrfCQGb8qgcAkJGBEwBH1TwDAMiMX/UAADLiCsAxXgMI5EYAAADISH8gA\r\n LyiKAQAIC8CAABARlwBOKaIIvUEgFUlAAAAZMRDAI8Z+P8CyIwAAACQkUEMUk8oDc9DAHIjAAA\r\n AZKTm3vtRAgCQGwEAACAjhW//juq5AgBkxicAAEBGar79O8oDEYHc+AQAAMhIrfDt3ys8EBHIj\r\n U8AAICMOAFwTC8EACAvPgEAADJSeAjgUYOBKwBAXgQAAICMOAFwjLcAALnxCQAAkBEB4BgBAMi\r\n NTwAAgIzUXAE4qj/opp4AsKoEAACAjDSLVuoJpdEVAIDMCAAAABkZKZqpJ5RGZ7CSegLAqhIAA\r\n AAyUY+GKwCv0hYAgMwIAAAAmfCn/8dr95dTTwBYVQIAAEAmRmru/7+iP+hFNzqpZwCsKgEAACA\r\n THgB4TGfQTj0BYNUJAAAAmXAF4Jj2wPF/ID8CAABAJkacADhqxf1/IEMCAABAJsZqE6knlIY3A\r\n AA5EgAAADIxVptMPaE0XAEAciQAAABkwgmAY7wCEMiRAAAAkAknAI5ZcQIAyJAAAACQiXEB4Kj\r\n l/kLqCQCrTgAAAMiEEwDHLPTnUk8AWHUCAABAJjwD4JjF3pHUEwBWnQAAAJAJVwCOcQIAyJEAA\r\n ACQgXo0olUbSz2jNBYFACBDAgAAQAbW1NelnlAqAgCQIwEAACADUwLAUZ1BOzqDduoZAKtOAAA\r\n AyIATAMcs9vzpP5AnAQAAIANOABzjAYBArgQAAIAMOAFwjPv/QK4EAACADDgBcMxi70jqCQBJC\r\n AAAABkQAI6Z6x9KPQEgCQEAAKDi6tGIydqa1DNK41B3f+oJAEkIAAAAFbeusTGKwrd9rzjcO5h\r\n 6AkASPgkAACpuprEx9YRSOdI7kHoCQBICAABAxc00NqWeUBpL/YVoD1ZSzwBIQgAAAKi4dU4AH\r\n HW460//gXwJAAAAFecEwDGHHf8HMiYAAABUWhHT9fWpR5SGAADkTAAAAKiw6fr6qBeN1DNKQwA\r\n AciYAAABU2DQ+BZAAABhASURBVMaR01NPKBXPAAByJgAAAFTYppHTUk8oFScAgJwJAAAAFbbJC\r\n YCjVvpLsdA/knoGQDICAABAZRWxoXFq6hGlcaD7fOoJAEkJAAAAFTVTPyWatVbqGaWxv7Mn9QS\r\n ApAQAAICK2tTclnpCqezv7k09ASApAQAAoKI2NTwA8NUEACB3AgAAQEVtaW5PPaE0BoN+7O8IA\r\n EDeBAAAgApqFqOxobEl9YzSONKbjW50Us8ASEoAAACooK3NHVEUvtV7heP/AAIAAEAlndY8K/W\r\n EUhEAAAQAAIBKEgCO5xWAAAIAAEDluP//Wvu6z6WeAJCcAAAAUDGnNc9y//9V5nuHY643m3oGQ\r\n HI+GQAAKmZb85zUE0plb2d36gkApSAAAABUzJmjF6SeUCp720+lngBQCgIAAECFrG9sian6utQ\r\n zSmVv56nUEwBKQQAAAKiQHS1/+v9q3UEn9nU8ABAgQgAAAKiUHaMXpp5QKvs6z8Yg+qlnAJSCA\r\n AAAUBHjtanY2Niaekap7Gk/mXoCQGkIAAAAFXFm63yv//sVe9z/BzjKJwQAQEWcPfre1BNKxxs\r\n AAI4RAAAAKmCsNhmnN89OPaNUDnRfiJXBUuoZAKUhAAAAVMC5o++PWlFPPaNUnl55NPUEgFIRA\r\n AAAKmDn2CWpJ5TObgEA4DgCAADAkFtbXx+bRk5PPaNUuoNOPNf+ZeoZAKUiAAAADLmdY5emnlA\r\n 6z7V/Gb3opp4BUCoCAADAkDtv9OLUE0rH8X+A1xIAAACG2OnNc2K6sSH1jNLZvfKL1BMASkcAA\r\n AAYYu8bvzr1hNKZ683GbO/F1DMASkcAAAAYUpO1tbG9tTP1jNLZvfJY6gkApSQAAAAMqfeMfyB\r\n qRT31jNJx/B/g9QkAAABDqIhaXDh2ReoZpdMddOLpthMAAK9HAAAAGELnjL4vxutTqWeUztMrj\r\n 0Vn0E49A6CUBAAAgCF06cRHU08opceW7089AaC0BAAAgCGzvbkzThnZmnpG6XQHnXhy5aHUMwB\r\n KSwAAABgyl01em3pCKTn+D/DmBAAAgCGytXlWnNrcnnpGKTn+D/DmBAAAgCFy+cQ1qSeUkuP/A\r\n G9NAAAAGBKbRk6Pba1zU88oJcf/Ad6aAAAAMCQ+OPmp1BNKy/F/gLcmAAAADIHtzZ1xeuuc1DN\r\n KqTNoO/4P8DYIAAAApVfE1Ws+k3pEae1aut/xf4C3QQAAACi5i8auiPWNTalnlNZDS3emngAwF\r\n AQAAIASa8RIXDn591LPKK2D3X2xt7M79QyAoSAAAACU2GWT18REfU3qGaX14OIdqScADA0BAAC\r\n gpKbrG+LSiY+lnlFavUE3Hlm6O/UMgKEhAAAAlNS1a78Q9aKRekZpPbH8YKwMllLPABgaAgAAQ\r\n AldOHZFbG3uSD2j1B708D+Ad0QAAAAombHaZFw99enUM0rtcPdAPNt+PPUMgKEiAAAAlMxHpn4\r\n 7RmvjqWeUmj/9B3jnBAAAgBI5q3VRnDd2ceoZpdbur8QDi7enngEwdAQAAICSmKytjWvXfiH1j\r\n NJ7eOmn0R4sp54BMHQEAACAkrhu+vcc/X8L/UEv7l24NfUMgKEkAAAAlMDlE9d66v/bsGv55zH\r\n fP5R6BsBQEgAAABLbNHJ6XDn5ydQzhsI9CzenngAwtAQAAICEWsVYfGrtl6JW1FNPKb2nVx6L/\r\n d09qWcADC0BAAAgkSJq8enpL8eaxkzqKUPh3oVbUk8AGGoCAABAIh9d89k4rXV26hlDYX9nTzz\r\n dfiz1DIChJgAAACRw0diV8d7xD6SeMTTuWvhh6gkAQ08AAABYZVtGtsdH13w29Yyh8WLnudi1f\r\n H/qGQBDTwAAAFhF6+qnxGfW/UHUi0bqKUPjtrnrU08AqAQBAABglUzW1sZnZ/4oxmoTqacMjT3\r\n tJ2N3+9HUMwAqQQAAAFgFo8V4/O7MH8dUfTr1lKHyk7nvp54AUBkCAADASTZSNOOzM/801jVOS\r\n T1lqDy58kjs7TyVegZAZQgAAAAnUT0a8VvTfxgbR05LPWWoDAb9uH3uhtQzACrF02cAAE6SkaI\r\n Zv73un8TW5o7UU4bOY8v3x/7untQzACpFAAAAOAmaxWh8dt0/jc3NbamnDJ3eoBt3zP8g9QyAy\r\n hEAAABOsNFiPD4388exYeTU1FOG0n0Lt8bh3oHUMwAqRwAAADiBJmpr4ndn/jhmGhtTTxlKc73\r\n Z+On8TalnAFSSAAAAcIKsb2yJ3173hzFVX5d6ytC69ch3ohud1DMAKkkAAAA4Ac5sXRDXrf29a\r\n NZaqacMradWfhFPrDyYegZAZQkAAAC/pkvGPxpXT/1mFIU3LL9b3UEnbjny7dQzACpNAAAAeJe\r\n KqMU1az4fF45fkXrK0Lt34RYP/gM4yQQAAIB3YbI2HZ+a/lJsaZ6ResrQO9I9GHfN//fUMwAqT\r\n wAAAHiHdrQujE+s/WKM1sZTT6mEm+e+Hb3opp4BUHkCAADA21SLenxo6tPx/okPp55SGQ8v3hV\r\n PrTySegZAFgQAAIC3YW19fXxq+kuxceS01FMqY643G7fMefAfwGoRAAAA3lQRF49/OK6aui5Gi\r\n mbqMZUxGPTjxsP/JTqDduopANkQAAAA3sBMfWN8Yu0/iM3NbamnVM79iz+J59pPpJ4BkBUBAAD\r\n gVxRRi8snronLJ6+NeuHbpRPtYHdf/GTu+6lnAGTHJxoAwKtsbZ4VH536ndgwsiX1lErqD3px4\r\n 6G/8NR/gAQEAACAiFhTn4kPT/1WnDV6UeoplXbXwg9jX/fZ1DMAsiQAAABZaxatuHzyE/H+8Q8\r\n 57n+S7Wk/FT+dvyn1DIBs+ZQDALJUi3pcOH5lXDnxiRivT6WeU3mLvbn4/qGvxSD6qacAZEsAA\r\n ACy8spv/C+b+HhM1adTz8lCf9CL6w9/Ixb7c6mnAGRNAAAAslCPRlw0flVcOvGxmKyvTT0nK7f\r\n NXe+VfwAlIAAAAJXWLEbjorEr4+KJj8REfU3qOdl5fPmBuHfxltQzAAgBAACoqJn6xnjfxIdj5\r\n 9glMVI0U8/J0mz3xfjbw99MPQOAlwkAAEClnNm6IN4//qE4vXVO6ilZ6wza8b3Zr0Zn0E49BYC\r\n XCQAAwNCbrm+InWOXxvljl8ZUfV3qOdkbDPpx0+G/jIO9famnAPAqAgAAMJRaxVicO3ZxnD96a\r\n Wxubks9h1e5bf762LV8f+oZAPwKAQAAGBrNohXbW+fHOaPvi+2tnVEvfCtTNj9fvD3uWbg59Qw\r\n AXodPTQCg1CZqa2JH64LYMXpRnNY8y2/6S+zJ5Yfj5iN/lXoGAG/AJygAUDJFbGxsjW2tc2NH6\r\n 8LYNHJaFEUt9SjewgudZ+L6Q1+PiEHqKQC8AQEAAEhufWNznNY8O05vnh1bmzuiVRtLPYl34Ej\r\n 3YHxn9s+iG53UUwB4EwIAALCq6tGIjSNbY9PIttg8si1Oa54V4/Wp1LN4l5b7i/Ht2T+Npf586\r\n ikAvAUBAAA4iYpY39gUm0ZOf/k3/KfH+sbmqBX11MM4Adr9lfib2T+LQ739qacA8DYIAADACVD\r\n EuvqGmGlsOvrX+samWNfY6KF9FdUZtOOvZ/9D7O3sTj0FgLfJJzIA8La0irFYU5+JNfV1x39tr\r\n I/p+nq/0c9IZ9CO78z+WeztPJV6CgDvgE9qAMhUI0aiWRuNZtGKsdpEjNUmY7w2+dqv9amYqk1\r\n Hs9ZKPZkS6Aza8Tez/ymeaz+RegoA75AAAAAvu2Lyk7E8WEo94x2rvfKjqEURtagV9aM/VxS1a\r\n EQjRmqtGCla0Sxe+dr0aj3ese6gE9+d/Wo823489RQA3gUBAABedu7Y+1NPgNJ65Tf/z7R3pZ4\r\n CwLsk/QMA8Ka6g058b/Y/x9Ptx1JPAeDX4AQAAABvaKW/HN899FV3/gEqQAAAAOB1zfcOx1/P/\r\n sc40N2begoAJ4AAAADAa8x2X4y/Ovj/xHz/UOopAJwgAgAAAMfZ294d35n9j7EyhG/FAOCNCQA\r\n AABz15Moj8f3Zr0UvuqmnAHCCCQAAAERExMOLd8VNR/5rRAxSTwHgJBAAAAAyNxj047b5G+Keh\r\n b9LPQWAk0gAAADI2HJ/MW449I14uv1Y6ikAnGQCAABApvZ39sZ3D301jvQOpp4CwCoQAAAAMvT\r\n Y0s/ipsN/Gd3opJ4CwCoRAAAAMvLSff/r456Fm1NPAWCVCQAAAJlY7i/G9Ye+Hs+0d6WeAkACA\r\n gAAQAaeWdkVNx7+Ziz0j6SeAkAiAgAAQIX1Bt24fe6GuHfxltRTAEhMAAAAqKgD3RfihkPfiAP\r\n dvamnAFACAgAAQAXdv/Dj+PHc96IX3dRTACgJAQAAoEIWekfipsN/Gbvbj6aeAkDJCAAAABXx6\r\n NJ9ccuRb8fyYDH1FABKSAAAABhyh7sH4odH/pvX+wHwpgQAAIAh1Rt0496FW+Kn8ze56w/AWxI\r\n AAACG0J72k/HDw9+Kg719qacAMCQEAACAIbLcX4yfzH0vHlr6aeopAAwZAQAAYAj0B714aOmuu\r\n H3ueg/5A+BdEQAAAEruyeWH48dz343Z3ouppwAwxAQAAICSer79dPx47ruxp/Nk6ikAVIAAAAB\r\n QMoe7B+L2+RviseWfpZ4CQIUIAAAAJbHcX4yfzt8U9y/+JAbRTz0HgIoRAAAAElvoHYn7Fm6Nn\r\n y/eFt3opJ4DQEUJAAAAicz1ZuPuhZvjocU7ox+91HMAqDgBAABglc12X4y7F34Yv1i611F/AFa\r\n NAAAAsEr2d/bEXQv/f/t2syPFeYBh9EuuOTeSG8ouziKKUbCDwfzZZgAP4n+Gme7qrqosbE3iy\r\n EqIDS7gOUdqfaXqzVvLflr1p3F79/etpwAQJAAAALxHyzqP+/uvxvWLz8aD6fbWcwAIEwAAAN6\r\n DN/PrcePyr+OLi7+M8+XV1nMAQAAAAHiXHk3fjOsXfx63d9e93w/AB0UAAAD4laZlP27tro3rF\r\n 5+NZ8fHW88BgJ8lAAAA/ALLOo9vp6/HrcvPx93dl2Mex60nAcB/JQAAAPwfvp++Gzd3n4+vL6+\r\n N3Xqx9RwAeGsCAADA//Dy+HTc2l0bNy//Nl7Nz7aeAwC/iAAAAPAznh1Px/3dP8ad3RfjyfFk6\r\n zkA8KsJAAAA44d3+h9N98e9/Y1xd//lOJtfbD0JAN4pAQAAyNovu/Ht/ua4t78xvtl/NaZ1t/U\r\n kAHhvBAAAIGNej+P0cDJOpjvjwXRnPJruj3UsW88CgN+EAAAAfLKWdR5PDg/HyXRnnEx3x6Pp/\r\n jiOw9azAGATAgAA8MmY1+N4enw8Tqa74+H+7nh4uDcO67T1LAD4IAgAAMBHaV2X8WJ+Op4cHoz\r\n vDw/Gk8OD8eTwcCxj3noaAHyQBAAA4KNwNr8Yp4eTcXr47ur07z4AvD0BAAD4oJzPr8bz4+nV5\r\n 9mP53693HoaAHzUBAAA4Dc3r8dxNr8cL+enP/mx//x4OqZ1v/U8APgkCQAAwDt3XA/jbH45Xs/\r\n Pr87X84tx9uP5Znm99UQAyBEAAIC3sqzzuFzejMvlfFws51fnv19fLufjbH45LpazrecCAP9BA\r\n ACAj9yyzmMZy1jWZaw/XF3dW9dlLD+5N4/jehyHdT+mdT8Oy/7qelr347BOY1p2//p+3Y/dcjE\r\n ulvMxrbutHxUA+BV+98fHf1i3HgEAAAC8X7/fegAAAADw/gkAAAAAECAAAAAAQIAAAAAAAAECA\r\n AAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAg\r\n AAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQI\r\n AAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAEC\r\n AAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAA\r\n AECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAA\r\n ABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAA\r\n AAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAA\r\n AAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAA\r\n AAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQI\r\n AAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAEC\r\n AAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABA\r\n gAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQ\r\n IAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAA\r\n AAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAA\r\n AAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAA\r\n AABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAA\r\n AAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAA\r\n AAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABA\r\n gAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQ\r\n IAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAE\r\n CAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAAB\r\n AgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAA\r\n AABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAA\r\n AAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAA\r\n AAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAA\r\n AAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAA\r\n AAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAE\r\n CAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAAB\r\n AgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAA\r\n QIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAA\r\n ECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAA\r\n AAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAA\r\n AAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgAAAAAECAAAAAAAABAgAAAAAECAA\r\n AAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIAAAAAAAAECAAAAAAQIAAAAABAgA\r\n AAAAECAAAAAAAABAgAAAAAECAAAAAAQIAAAAABAgAAAAAAAAQIAAAAABAgAAAAAECAAAAAAQIA\r\n AAAAAAAH/BM5DXVlq2TI+AAAAAElFTkSuQmCC\r\nCLOUD:disableduser@fair-coral.maas\r\nEND:VCARD\r\n','Database:disableduser.vcf',1678353836,'e854666b7b50503a88b923483f330504',20307,'disableduser'),(6,3,_binary 'BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Sabre//Sabre VObject 4.1.2//EN\r\nUID:admin\r\nFN:admin\r\nN:admin;;;;\r\nPHOTO;ENCODING=b;TYPE=image/png:iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc\r\n 4AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAPKElEQVR4nO2de7BV1X3HP9w5wzAMw9yxxmEMUWqId\r\n WgSFa2PHXQbqA8kbQyPELUGY2KTmlhC2DRj12S73GTVIWxib1TSFoyPBBNMajRRQQ3iTnBrCKE\r\n EE0sNUqSUUHrL3DLMnTvMnZv+8VuHezj3nHv2Xud1k57vzJkLa+/fWr+1f3u9fq89Lk7935Idf\r\n cB+YDewFXgu8JIjOehHIE79e4AwJ9kQ0Au8CewEtgAvBF4yUA8v7cC4nAIoxyCwGVgdeMmPXSp\r\n wFEAlHAO+ZXnZ34D6WoKuOukLwIeAJE795+PUn94AnlwxGbgDeCtO/fvi1J/cRl4yo14BlOIa4\r\n PU49T/VwDpd0AV8HvhFnPqXtJmXmmikAAAmAOvi1O9pcL0umAa8Eqf+wnYzMhoaLYAi/jpO/XV\r\n NqjsPCsDGOPU/2m5GqqFZAgD4VJz6f9vE+rOiC9gQp/6sdjNSCc0UAMDKOPWvbHIbWVAcCae1m\r\n 5FyNFsAXcBDceqPb3I7WXAmsLrdTJSj2QIAmA58rgXtZMFtcepf0G4mStEKAQCsGCOjAOCudjN\r\n QilYJYArw5y1qqxbmx6l/eruZKKIAfC3DfV3IQ7wSOMOxrRuB71UofzUjD0WMR/b4VyHnjrwoA\r\n POBf3KgbTjG5bk5Tv0CMp+vRjqSB33AHwReMpSTrhovpwFrgFsdyJ8MvGRBI/ioF7kEUESc+vO\r\n Bf3Yg/aPAS950aXMUXr4OfCYn2YHAS85uJB+ucFoDAi95EviBA+l5Lu3VwAogr0r8rDj1Xaavh\r\n qOeRfibDjQNPwgFXnIct5fBdS1rKOoRwD4Hmu462hsNv3agmdRwLhxQjwBc3ub+OtobDe9woGk\r\n WL7lQjwDmOdD01dHeaLjOgeZYw7lwgJMArOXrLx1IXaatWrx8HHhvTrK+wEuONpoXF+QWQJz65\r\n wHPAxNzkg4Cv8zbXg1e5gP/6EC6q5F81INCnPpXZbkP0SZeDXyM/IcwgNcqeS3EqT8NOdlmxQR\r\n EwbcAOQ27YKsjXcNRoHXMfL9K+SdojFdEHjzZ4vaqolXKuBOIy8hYwPbASxo6FdaDVgngkXodu\r\n BqINe1moBStEEA/sLIF7WTBjsBLnmg3E6VohQBU4CUHW9BOLQwCt7ebiXK47Gby4LnAS/6+yW1\r\n kxYrAS0bdflqnsivyVBp4yZIs98WpvwYYYQhqtgBONLn+PMjCyxXAx3PWm0kAwELgrPLCZk9BN\r\n 8Spn1dX3yzcF6d+3hNz09GKNWB1nPpTWtBOLYzH7dTcVLRCAJOAL7WgnSzw4tS/od1MlKJV54D\r\n bxpBX2op2M1CKVglgAnBTi9qqBc8qFMcECsBHMt57JjAHuAE3wS0CHqhQ/m3gX3LUUwDOQZRxr\r\n v7/C4EvO9I2FIXAS57Kcf9a69r3NBW2VDVwWZz64wMvOWU7GHjJHmBPzroAvmJtAQ+RfzvtM0Y\r\n EkPtNtoeZa4G8AXHjyW84qcXLY8BfOZBe3Eg+6oGrW8oeYL0D6Tku7dXgZT35R1B3nPrNchDIh\r\n XoW4WcdaJq1E3rOgeZ3XgAu6uW8Zsys+C8HmkpuKblVJzm8viuuU/UIYJoDTbM8Ed7tQHM8Y1k\r\n t1AzNjVN/EuLcPAL1COBmB5reOtqrCOti6HK6PVyh7DcO9dyY4Z75VHnWrm4p19tK88Jlu1kL9\r\n 5LfzfBglbQGLo7DX4hTf0a1i1YPdm+167n2z3HqdwG3AffnobM4Bux1oKvGy2SkY3c4kO/IWT4\r\n aJiKZApYEXnLKZsAGim9ADrEVMc7maqiFAjAVmG3/uuCpwEtGnLqtW8wHc9QzEZl3ZyPpCVzw2\r\n cBL1la6EKf+r8kwr1fBG0CKWN9mkuGkXqB1LiEbq5R/sIU8gGRaqRSpU8TjuPMzw/4yo1XKuF4\r\n gj8qjmfhBDQ+NRxEhtQStEsCaMZTLx4x2MfCSfbTQh6kVAtgPjBXD/OOBl2RZaBUtcl9vhQBuH\r\n yNv/xFgWZYbrRtNS+KJmy2ALwde8qMmt5EFQ8CNebzzAi/5Gi3wIW2mAB4LvGSs2II/GXjJSw5\r\n 0tyDbyqahWQJYj3g9txuDwJLASx5xIQ68pB+xfTRtFDdaACeApYGX3N6ogOw6cBiYY402zrBRm\r\n HOBrzaEq1PxRCMFsA24yM6d7cZ64I9dMzmWI/CSwcBLliM28Ubosw4BiwMvWdwI18SXgVWBl2x\r\n uQF31YAB4wvLyRjMaCLzkpTj134dECS0H8qa+2Qv0AOuLO8O8AhhEAu12AwlyqjyQs45GoZ/hx\r\n K0vAs/Y6aKpCLxkEDmofctqQa9H1CkzGGkjOYSMmG3As4GXbC+vb1wO2+gJuyg1FFafnzdtQH+\r\n 5d8VYgTW+FAIvaVZIbgcddNBBBx100EEHHXTQQQf1YJyOzC/sv48jR/seHaqmpHPRkZmP6FCe1\r\n aH6u1Hu+4kOVdV4XR2ZTUjAx2zgXB2quMp9FwM361BlsoS1A11IsIKPGB9+BiQ6MlUdiepED/B\r\n Jaqt231/j+kHEynUa8K5R7ptEE1ziG4mCDlVRZ9EHrNWRWQzMQjSLAOjInAugQ1XRdU9HZgIwq\r\n EM1WKO9qTpUJ9W5lu4c4LgO1Qilno7MZETBtV+H6qRjrw7V7fZ6JV6mIc5bTvlJdWSmIoJ9U4d\r\n qoOzaFCTa/U0dqhNl16r2RUdmku3HER2qU8yiIxK32inpEzpUO3VkZgDfZTjV2FnAR3So9unI9\r\n CCxXYuQiJM5yNRydkld3cDb9tpDyJu9G1iFvMF3I9rM6Yh2c54OVb+OzP8inglLEG3iJcAGHar\r\n I1vs2cD7ilHuhDtVSHZnTkJxEQ4gx5lzEx2eODtWHdWQSYGnp9Koj8zRwH/Ly3Y94cHQjhqXLb\r\n F+368iMBx5G8p7usX+fAabpUC3RkVmIJCQp9mXA9uW4jszfII7MuxH19W4dqptt+1cVdGRutfx\r\n MQiw/T+lQ7bRlGwGlQ/WUJfgQki/0A/a6ApbrUM2zD7sidKh2AOfryPxWh+p8W9d/AJfrUB20/\r\n 9+ApCFei7zBFwGX6lAN2bcr0ZHZpUNVLUdoD7ClREhnICrzrCPhMmB1sX4dmTuALyLBgAoY0KG\r\n 6yF47Hfg5YgsBSYHj61Dtt9cfBW7VkXkECYt9lw7VgI5MF/CqjsyfImnTvt0F/KH9vQcRwsU6M\r\n t06Mhcg08pJjzYdqmeA0+0wBdg8ygOphaPAQvt2ASxl2AuhAKzUoRqy7Q7YTt5SqSIdmQKSnf0\r\n rJbweQd7arDhc1peXGNbv/wUlDl06VL3IiC6it6wvy21fBmxfrteR6bL9WYCMlIXAMwUdqrvLO\r\n nM/IvFXgGklu6QiuhmOkMwTXlqORbZTX9SR2YNMH/9grw3pUJVnWNxL9aCQM4He8jnb0nygwv2\r\n VUN5e8eEV699f4f5iYMiNyBT0tu3L08BaHapBHZl5yFR7v47MLmCjDtVjOjLvAX5VySa8BZmrj\r\n wOv6VCdX/pD5t7iFFXL8F7Re9kOxUM6VIuAdyJvzDyGPzHSZRfgUnQjo6YSjlE55qu8rLy/WXd\r\n 7vRXunQqn9GUxw32ZC6yxI2KXDtW1wNlI/5bqyHwO+B/gHZUEcDUyb24HZtr5DtvYZOStrxQXd\r\n RyZnkqtW9dU6dBU4N+Kw9KuOfcgLt1FlH//awEyp4+A3ckd0JEp/2BQaYr6I5RsSe0aUWu7W8R\r\n zlHjV2V1N0e1mCvBWWV/uRjYms7DBjDpUgzpULyOL/YWIq8vCgo5MMT5gIrLb6Aau1aE6piOzC\r\n njR3jOEuOutt9fKH8KgjsxLwMM6Mg8j01TF7Lo6VAd0ZHYAG3VkHrJt38XwvHoIuMUu7DuRl+I\r\n q4PJRHtIy4FEdmbuRXdDNyO6mGIz3fcDoyAwhu5w7yR6QcRewyR4A9yAPVtbDUB2y/f6ujsw62\r\n 5cVSGDGj4FuHZmvI+n+pyDT+2ft7mpzF/Dv9vdzS3ihDtVh+6C+ajt2OXJYW6lDVfR224AYm0u\r\n xGPgp8uC77P9LA6lLnbUWIHPlHGTHc6cOVXENWGrr6EdSKfwG2REVzwLL7bVtlg/s2zUX2Uxcj\r\n SzAdyK7I3SoHrd9uQIZaZ9Gdjl7gAOMDCPqxWZ50aE6qkN1qRXEDy3PbzEcdHgLIuBiX5bpUD1\r\n gz0VXAK9b3qYBf6ZD9YKtd6nTBxz+v8GO0n8tqjzsvP8isE6H6jv11N3slGW/L1gJ/FBH5lJkt\r\n MxC1CF1Z2DsjIAc0JGZiagi9ldTy3TQQQcd/A7h5BpgVa3XAU/oUPWXlJ+BnA92A9N1qFwCHZx\r\n htZyX6FC12/m3KSg9Cd+E7J1nl92zENmzz0T25zWhI9OjI+P8fXkdGWOVgSAHujH1/cdGolQAc\r\n 4FHkIiQUswBNgEvkD338gXU95Wi9zJG8vk0G+MAdGQmIie7S4GtOlTvtuVdwH8jp8szkangG1a\r\n Z9CPEvDgT0bOsQk6Vq5BR8zKin/+Gjsx5yOl1OqJnWmWNOhOQTyOut38PIxrGjyL68q3IabcHO\r\n ZUuQxR82wCjQ3VCR+Y2YLsO1clvAujI3IRMmXtL6r8DsUi5ZPpqGooj4EpgmzWl9evIFJVWFwD\r\n 7dKiOIg/vw7Z8EWKY2YpMT5uQk2EBOZL32mvbrYlwC/LdmQW2fItV8k1A1BObLC87EeXVYeAnD\r\n Ks6piMqjWX2fg/RqWB5Kp/urrVlpfUXGNbijhkUT8JzESZBpprrEMvUbKDa4veoDlUxO8hjOjL\r\n LEBPdyzoyfcAOHapf6sg8iHhaFPMzfMfamD+DpLGcAcwtXWR1ZHoRNe4uuxZMQsykA/b6vWRfF\r\n 0bUP5ZQFMA1yNQB8gYuQwRwNdU/vlCuSTxIhfTsyA5qQEfm8yVl70SmtAeAoxkezhtlxpajZM8\r\n R1DdWHz5AQUfmLMT0eMiWbQM2WJ33+4HXqtCWW5+qGWfGI8aH0oiRVxnOWJUlrChvNEypvWJMf\r\n LCtGgrIQnoy0ZBd2HYAX0AsYrVcTWphL+LGcTJFjI7MLNxz8pTjGCMNRLlSxrQTXciDfr6s/Fn\r\n ENlBenhUnGE5RuQ64xx7oih4FD1I5Z1sRA1RJclcBv6LE8qUj8zHcEgq2BV0MR/GVYjNidC6dO\r\n /sYNlzvZWQ2kX0MTycPAw/qyNxn598e4BUdmdcRg806Wz6IZJkqxzeBL1k9fD8jU531Mxyv+wA\r\n wUUfmP3Vkfgb8CWJc6Rul/jGD/wPvjOyyoyJ2YQAAAABJRU5ErkJggg==\r\nCLOUD:admin@fair-coral.maas\r\nEND:VCARD\r\n','Database:admin.vcf',1678356476,'9d26a70cf7994d9788508ecc4607cf21',5669,'admin');
/*!40000 ALTER TABLE `oc_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_cards_properties`
--

DROP TABLE IF EXISTS `oc_cards_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_cards_properties` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `addressbookid` bigint NOT NULL DEFAULT '0',
  `cardid` bigint unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `preferred` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `card_contactid_index` (`cardid`),
  KEY `card_name_index` (`name`),
  KEY `card_value_index` (`value`),
  KEY `cards_prop_abid` (`addressbookid`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_cards_properties`
--

LOCK TABLES `oc_cards_properties` WRITE;
/*!40000 ALTER TABLE `oc_cards_properties` DISABLE KEYS */;
INSERT INTO `oc_cards_properties` VALUES (13,3,3,'UID','testuser2',0),(14,3,3,'FN','testuser2',0),(15,3,3,'N','testuser2;;;;',0),(16,3,3,'CLOUD','testuser2@fair-coral.maas',0),(17,3,2,'UID','testuser',0),(18,3,2,'FN','testuser',0),(19,3,2,'N','testuser;;;;',0),(20,3,2,'CLOUD','testuser@fair-coral.maas',0),(25,3,4,'UID','testuser3',0),(26,3,4,'FN','testuser3',0),(27,3,4,'N','testuser3;;;;',0),(28,3,4,'CLOUD','testuser3@fair-coral.maas',0),(33,3,5,'UID','disableduser',0),(34,3,5,'FN','disableduser',0),(35,3,5,'N','disableduser;;;;',0),(36,3,5,'CLOUD','disableduser@fair-coral.maas',0),(49,3,6,'UID','admin',0),(50,3,6,'FN','admin',0),(51,3,6,'N','admin;;;;',0),(52,3,6,'CLOUD','admin@fair-coral.maas',0),(93,3,1,'UID','vok',0),(94,3,1,'FN','vok',0),(95,3,1,'N','vok;;;;',0),(96,3,1,'CLOUD','vok@fair-coral.maas',0);
/*!40000 ALTER TABLE `oc_cards_properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_collres_accesscache`
--

DROP TABLE IF EXISTS `oc_collres_accesscache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_collres_accesscache` (
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `collection_id` bigint NOT NULL DEFAULT '0',
  `resource_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `resource_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `access` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`user_id`,`collection_id`,`resource_type`,`resource_id`),
  KEY `collres_user_res` (`user_id`,`resource_type`,`resource_id`),
  KEY `collres_user_coll` (`user_id`,`collection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_collres_accesscache`
--

LOCK TABLES `oc_collres_accesscache` WRITE;
/*!40000 ALTER TABLE `oc_collres_accesscache` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_collres_accesscache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_collres_collections`
--

DROP TABLE IF EXISTS `oc_collres_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_collres_collections` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_collres_collections`
--

LOCK TABLES `oc_collres_collections` WRITE;
/*!40000 ALTER TABLE `oc_collres_collections` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_collres_collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_collres_resources`
--

DROP TABLE IF EXISTS `oc_collres_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_collres_resources` (
  `collection_id` bigint NOT NULL,
  `resource_type` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `resource_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`collection_id`,`resource_type`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_collres_resources`
--

LOCK TABLES `oc_collres_resources` WRITE;
/*!40000 ALTER TABLE `oc_collres_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_collres_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_comments`
--

DROP TABLE IF EXISTS `oc_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_comments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint unsigned NOT NULL DEFAULT '0',
  `topmost_parent_id` bigint unsigned NOT NULL DEFAULT '0',
  `children_count` int unsigned NOT NULL DEFAULT '0',
  `actor_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `actor_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `message` longtext COLLATE utf8mb4_bin,
  `verb` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `creation_timestamp` datetime DEFAULT NULL,
  `latest_child_timestamp` datetime DEFAULT NULL,
  `object_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `object_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `reference_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_parent_id_index` (`parent_id`),
  KEY `comments_topmost_parent_id_idx` (`topmost_parent_id`),
  KEY `comments_object_index` (`object_type`,`object_id`,`creation_timestamp`),
  KEY `comments_actor_index` (`actor_type`,`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_comments`
--

LOCK TABLES `oc_comments` WRITE;
/*!40000 ALTER TABLE `oc_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_comments_read_markers`
--

DROP TABLE IF EXISTS `oc_comments_read_markers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_comments_read_markers` (
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `marker_datetime` datetime DEFAULT NULL,
  `object_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `object_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`,`object_type`,`object_id`),
  KEY `comments_marker_object_index` (`object_type`,`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_comments_read_markers`
--

LOCK TABLES `oc_comments_read_markers` WRITE;
/*!40000 ALTER TABLE `oc_comments_read_markers` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_comments_read_markers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_dav_cal_proxy`
--

DROP TABLE IF EXISTS `oc_dav_cal_proxy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_dav_cal_proxy` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `proxy_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `permissions` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dav_cal_proxy_uidx` (`owner_id`,`proxy_id`,`permissions`),
  KEY `dav_cal_proxy_ioid` (`owner_id`),
  KEY `dav_cal_proxy_ipid` (`proxy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_dav_cal_proxy`
--

LOCK TABLES `oc_dav_cal_proxy` WRITE;
/*!40000 ALTER TABLE `oc_dav_cal_proxy` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_dav_cal_proxy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_dav_shares`
--

DROP TABLE IF EXISTS `oc_dav_shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_dav_shares` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `access` smallint DEFAULT NULL,
  `resourceid` bigint unsigned NOT NULL,
  `publicuri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dav_shares_index` (`principaluri`,`resourceid`,`type`,`publicuri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_dav_shares`
--

LOCK TABLES `oc_dav_shares` WRITE;
/*!40000 ALTER TABLE `oc_dav_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_dav_shares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_direct_edit`
--

DROP TABLE IF EXISTS `oc_direct_edit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_direct_edit` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `editor_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `file_id` bigint NOT NULL,
  `user_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `share_id` bigint DEFAULT NULL,
  `timestamp` bigint unsigned NOT NULL,
  `accessed` tinyint(1) DEFAULT '0',
  `file_path` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4D5AFECA5F37A13B` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_direct_edit`
--

LOCK TABLES `oc_direct_edit` WRITE;
/*!40000 ALTER TABLE `oc_direct_edit` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_direct_edit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_directlink`
--

DROP TABLE IF EXISTS `oc_directlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_directlink` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `file_id` bigint unsigned NOT NULL,
  `token` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `expiration` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `directlink_token_idx` (`token`),
  KEY `directlink_expiration_idx` (`expiration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_directlink`
--

LOCK TABLES `oc_directlink` WRITE;
/*!40000 ALTER TABLE `oc_directlink` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_directlink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_federated_reshares`
--

DROP TABLE IF EXISTS `oc_federated_reshares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_federated_reshares` (
  `share_id` bigint NOT NULL,
  `remote_id` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT 'share ID at the remote server',
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_federated_reshares`
--

LOCK TABLES `oc_federated_reshares` WRITE;
/*!40000 ALTER TABLE `oc_federated_reshares` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_federated_reshares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_file_locks`
--

DROP TABLE IF EXISTS `oc_file_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_file_locks` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `lock` int NOT NULL DEFAULT '0',
  `key` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `ttl` int NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lock_key_index` (`key`),
  KEY `lock_ttl_index` (`ttl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_file_locks`
--

LOCK TABLES `oc_file_locks` WRITE;
/*!40000 ALTER TABLE `oc_file_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_file_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_filecache`
--

DROP TABLE IF EXISTS `oc_filecache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_filecache` (
  `fileid` bigint NOT NULL AUTO_INCREMENT,
  `storage` bigint NOT NULL DEFAULT '0',
  `path` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `path_hash` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `parent` bigint NOT NULL DEFAULT '0',
  `name` varchar(250) COLLATE utf8mb4_bin DEFAULT NULL,
  `mimetype` bigint NOT NULL DEFAULT '0',
  `mimepart` bigint NOT NULL DEFAULT '0',
  `size` bigint NOT NULL DEFAULT '0',
  `mtime` bigint NOT NULL DEFAULT '0',
  `storage_mtime` bigint NOT NULL DEFAULT '0',
  `encrypted` int NOT NULL DEFAULT '0',
  `unencrypted_size` bigint NOT NULL DEFAULT '0',
  `etag` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `permissions` int DEFAULT '0',
  `checksum` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`fileid`),
  UNIQUE KEY `fs_storage_path_hash` (`storage`,`path_hash`),
  KEY `fs_parent_name_hash` (`parent`,`name`),
  KEY `fs_storage_mimetype` (`storage`,`mimetype`),
  KEY `fs_storage_mimepart` (`storage`,`mimepart`),
  KEY `fs_storage_size` (`storage`,`size`,`fileid`),
  KEY `fs_mtime` (`mtime`)
) ENGINE=InnoDB AUTO_INCREMENT=1123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_filecache`
--

LOCK TABLES `oc_filecache` WRITE;
/*!40000 ALTER TABLE `oc_filecache` DISABLE KEYS */;
INSERT INTO `oc_filecache` VALUES (1,1,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,9905229,1681984278,1681984278,0,0,'64410b1667762',23,''),(2,1,'files','45b963397aa40d4a0063e0d85e4fe7a1',1,'files',2,1,9905227,1678356431,1678356431,0,0,'6409afcfb0665',31,''),(3,1,'files/bbw-logo.png','7fb0f51046121e4c805f9aeaa14b3093',2,'bbw-logo.png',4,3,6878,1678194775,1678194775,0,0,'972426fd6bed6b70f53d0d27754adfe9',27,''),(4,1,'files/Photos','d01bb67e7b71dd49fd06bad922f521c9',2,'Photos',2,1,2360011,1678194775,1678194775,0,0,'6407385779d97',31,''),(5,1,'files/Photos/Coast.jpg','a6fe87299d78b207e9b7aba0f0cb8a0a',4,'Coast.jpg',5,3,819766,1678194775,1678194775,0,0,'1596c03ef5ec36933baae96da91cb612',27,''),(6,1,'files/Photos/Hummingbird.jpg','e077463269c404ae0f6f8ea7f2d7a326',4,'Hummingbird.jpg',5,3,585219,1678194775,1678194775,0,0,'d32a8581eec3b7d5b5731d974865aa98',27,''),(7,1,'files/Photos/Nut.jpg','aa8daeb975e1d39412954fd5cd41adb4',4,'Nut.jpg',5,3,955026,1678194775,1678194775,0,0,'256e6896a2f199bbb350840437da5569',27,''),(8,1,'files/Nextcloud.mp4','77a79c09b93e57cba23c11eb0e6048a6',2,'Nextcloud.mp4',7,6,462413,1678194775,1678194775,0,0,'9063e610529ab31e9336f34f622a1517',27,''),(9,1,'files/Nextcloud Manual.pdf','2bc58a43566a8edde804a4a97a9c7469',2,'Nextcloud Manual.pdf',9,8,4540242,1678194775,1678194775,0,0,'109de5b17a51edf1af106c125dfaa018',27,''),(10,1,'files/Documents','0ad78ba05b6961d92f7970b2b3922eca',2,'Documents',2,1,78496,1678194775,1678194775,0,0,'64073857cdc2c',31,''),(11,1,'files/Documents/About.odt','b2ee7d56df9f34a0195d4b611376e885',10,'About.odt',10,8,77422,1678194775,1678194775,0,0,'e912fc845a9fc17c736c911a57984880',27,''),(12,1,'files/Documents/About.txt','9da7b739e7a65d74793b2881da521169',10,'About.txt',12,11,1074,1678194775,1678194775,0,0,'eab457e5fd52af56158222024013d85c',27,''),(13,2,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,8556983,1678956997,1678352236,0,0,'6412d9c5736b6',23,''),(14,2,'appdata_ocp2ck7yo4uj','511e1603a617f66d86c36ae02930e2d1',13,'appdata_ocp2ck7yo4uj',2,1,8556983,1678956997,1678358224,0,0,'6412d9c5736b6',31,''),(15,2,'appdata_ocp2ck7yo4uj/js','493274afae6aa7897fd145345985624b',14,'js',2,1,2662994,1678351427,1678351366,0,0,'64099c4369691',31,''),(16,2,'appdata_ocp2ck7yo4uj/js/core','f14d31b7798843b58fe4e295bde30d57',15,'core',2,1,355676,1678351366,1678351366,0,0,'64099c06dba3f',31,''),(17,2,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js','579d4b039f255abe4dbed89444d56310',16,'merged-template-prepend.js',13,8,148486,1678351188,1678351188,0,0,'e20b2006015acddb2a586fc6dde240ad',27,''),(18,2,'appdata_ocp2ck7yo4uj/preview','b9b868100017c68bd74bd35b4838053c',14,'preview',2,1,4492361,1678956928,1678956927,0,0,'6412d9802992e',31,''),(19,2,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.deps','49447c4a958dfb13087fc02658d2161d',16,'merged-template-prepend.js.deps',14,8,1180,1678351188,1678351188,0,0,'9a7cb82c69ffe85bfeeb7957f9660027',27,''),(20,2,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.gzip','996d183e420339a81267d7d48a42842b',16,'merged-template-prepend.js.gzip',15,8,40626,1678351188,1678351188,0,0,'8316dc498fcf2c472ca491b867a4788a',27,''),(21,2,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js','2d6f839fff5c210df1345235e03f12b7',16,'merged-share-backend.js',13,8,105817,1678351356,1678351356,0,0,'9a1c152a7627532ec07e6796780263bd',27,''),(22,2,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.deps','dced322d42d4338d1a49cb39c9f9e53a',16,'merged-share-backend.js.deps',14,8,692,1678351356,1678351356,0,0,'ac79ea18a2974d2a70f1070f558df70f',27,''),(23,2,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.gzip','64df495640c2992fa06f955533ba6a75',16,'merged-share-backend.js.gzip',15,8,22728,1678351356,1678351356,0,0,'49e8e81b93fd7d61f9247f74b1ee4058',27,''),(24,2,'appdata_ocp2ck7yo4uj/js/core/merged-login.js','1286373dbb5f6e81fb13b4f5c0843496',16,'merged-login.js',13,8,7563,1678351356,1678351356,0,0,'9d10b1932b8ae4f3bec30e57a07cbd7d',27,''),(25,2,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.deps','3b52fc8139d5575cb46dffc48d35120b',16,'merged-login.js.deps',14,8,247,1678351356,1678351356,0,0,'5be01bc7f4dd83f5602cf809aa5b40c0',27,''),(26,2,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.gzip','b654b22875e4d115d122da548cb98865',16,'merged-login.js.gzip',15,8,2276,1678351356,1678351356,0,0,'62121c928d6be1ebfbfd523778ee536d',27,''),(27,2,'appdata_ocp2ck7yo4uj/css','c7ebe2c99c887f1959d7328bdce2dcb8',14,'css',2,1,190031,1678358962,1678352252,0,0,'6409b9b291a9f',31,''),(28,2,'appdata_ocp2ck7yo4uj/css/theming','b7c0b45ef82251f3f095726b639e914b',27,'theming',2,1,1163,1678358268,1678358268,0,0,'6409b6fc9ce89',31,''),(29,2,'appdata_ocp2ck7yo4uj/theming','1a917cff90046d01d35343c92b270d5b',14,'theming',2,1,2705,1678956997,1678358756,0,0,'6412d9c5736b6',31,''),(33,1,'cache','0fea6a13c52b4d4725368f24b045ca84',1,'cache',2,1,0,1681984278,1681984278,0,0,'64410b1662a05',31,''),(34,2,'appdata_ocp2ck7yo4uj/avatar','91d888474bf0e119311f6ea5fc91c88b',14,'avatar',2,1,65286,1678956927,1678353832,0,0,'6412d97f862ff',31,''),(35,2,'appdata_ocp2ck7yo4uj/avatar/admin','1fb69ff79d77a83aba1b7ef55d030cbb',34,'admin',2,1,17066,1678356482,1678356482,0,0,'6409b002e75a2',31,''),(36,2,'appdata_ocp2ck7yo4uj/js/notifications','b1b946b5fe6500cf3bfc4d9ba1503c65',15,'notifications',2,1,26448,1678351364,1678351364,0,0,'64099c04a4d22',31,''),(37,2,'appdata_ocp2ck7yo4uj/js/notifications/merged.js','7f97a755d5335843011a8c28e0dc4578',36,'merged.js',13,8,20888,1678351364,1678351364,0,0,'8cf96d09e60eb13b892ca6ed2e00a8ec',27,''),(38,2,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.deps','0d71ebc93bb58de39f0db5e2fba9c879',36,'merged.js.deps',14,8,306,1678351364,1678351364,0,0,'1c8fb6519db06cd44b07e1174d699d4a',27,''),(39,2,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.gzip','3e6adf3e96590e9411ded0cf69a9cbeb',36,'merged.js.gzip',15,8,5254,1678351364,1678351364,0,0,'ee1349753da533d50688648d51ab0338',27,''),(40,2,'appdata_ocp2ck7yo4uj/js/files','3eb7143fc18697e4e2511eb3adbc241c',15,'files',2,1,420072,1678351364,1678351364,0,0,'64099c04f15e8',31,''),(41,2,'appdata_ocp2ck7yo4uj/js/files/merged-index.js','ea21876dfbd6b920e81f7bc213706376',40,'merged-index.js',13,8,337930,1678351364,1678351364,0,0,'22f43eac4706f53793b6108578982653',27,''),(42,2,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.deps','adfa388cbf45f6b392d7960797e5880e',40,'merged-index.js.deps',14,8,1957,1678351364,1678351364,0,0,'7bdd0aec4eb1ebfc227b7445c0b61f7f',27,''),(43,2,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.gzip','b5b4e4222a3423e2acb28c7699cd86b6',40,'merged-index.js.gzip',15,8,80185,1678351364,1678351364,0,0,'c69e42953b2fef9b2b2481feefd7532f',27,''),(44,2,'appdata_ocp2ck7yo4uj/js/activity','a9f39bf5207544df2d5b895a84618a1e',15,'activity',2,1,21189,1678351365,1678351365,0,0,'64099c053c4df',31,''),(45,2,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js','5b53ceef539881b19ca36f7752013cfb',44,'activity-sidebar.js',13,8,16380,1678351365,1678351365,0,0,'4718ff67f87346fdd6447587ff498ea3',27,''),(46,2,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.deps','fb3cfe9931d0a1786203ab7ccddec138',44,'activity-sidebar.js.deps',14,8,458,1678351365,1678351365,0,0,'e83fd081d410c9077b2f1502f36e8dd6',27,''),(47,2,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.gzip','ab64a435f0684089954a1503a4915c2f',44,'activity-sidebar.js.gzip',15,8,4351,1678351365,1678351365,0,0,'13539329f95ddac9d0edb616efd9f4b5',27,''),(48,2,'appdata_ocp2ck7yo4uj/js/comments','e4648049e6995efacee6f80096521231',15,'comments',2,1,80480,1678351365,1678351365,0,0,'64099c057ed18',31,''),(49,2,'appdata_ocp2ck7yo4uj/js/comments/merged.js','406aebeee235fd67c35f4ebeb199dfe9',48,'merged.js',13,8,62511,1678351365,1678351365,0,0,'122e12d85b1fbf617030560bc635b8e4',27,''),(50,2,'appdata_ocp2ck7yo4uj/js/comments/merged.js.deps','18af4dd92bc976d39f038edb58b42c61',48,'merged.js.deps',14,8,788,1678351365,1678351365,0,0,'3d760584163ba5053579614f47c76a83',27,''),(51,2,'appdata_ocp2ck7yo4uj/js/comments/merged.js.gzip','0647ad3df0fb06da8883d8d7cb01b546',48,'merged.js.gzip',15,8,17181,1678351365,1678351365,0,0,'fd8eb6aef0d23660b35cdcb35fba4328',27,''),(52,2,'appdata_ocp2ck7yo4uj/js/files_sharing','7676e75f6ea45d19eb1827a880982349',15,'files_sharing',2,1,19382,1678351365,1678351365,0,0,'64099c05ba5f2',31,''),(53,2,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js','253b8ab876fa052002ac07f2240a1656',52,'additionalScripts.js',13,8,14539,1678351365,1678351365,0,0,'bd836a3b3a58ca8d781327c3b7f0f66d',27,''),(54,2,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.deps','342d284054df0c2482eac265157e4431',52,'additionalScripts.js.deps',14,8,316,1678351365,1678351365,0,0,'0678d24e0e8237781227d07aed31daea',27,''),(55,2,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.gzip','51d06c1265f8fff99a85a72ac1cf63d4',52,'additionalScripts.js.gzip',15,8,4527,1678351365,1678351365,0,0,'141f6968a945046b63653e36a05fb3e1',27,''),(56,2,'appdata_ocp2ck7yo4uj/js/files_texteditor','8e34386727f9050bc69e0087ca33fd6a',15,'files_texteditor',2,1,839303,1678351366,1678351365,0,0,'64099c061f9ab',31,''),(57,2,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js','ee0652870e1e2acecbadd6d595ab596e',56,'merged.js',13,8,699810,1678351365,1678351365,0,0,'369a0fb2e75e06a8a21c0914db0dff31',27,''),(58,2,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.deps','000a73daa8b6451f2642c89504a07157',56,'merged.js.deps',14,8,346,1678351365,1678351365,0,0,'56323a79016a7f0f216f3baaf970c033',27,''),(59,2,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.gzip','f23ada379b79c7c7cab74dbb34067beb',56,'merged.js.gzip',15,8,139147,1678351366,1678351366,0,0,'de8a5530286366639ae84f30adfff4af',27,''),(60,2,'appdata_ocp2ck7yo4uj/js/files_versions','80e7253edd9ea7831366f098c11d692a',15,'files_versions',2,1,16695,1678351366,1678351366,0,0,'64099c065e126',31,''),(61,2,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js','9f3977c48c9a7a5bee51eb406a30ab06',60,'merged.js',13,8,12719,1678351366,1678351366,0,0,'ee1cdc4736f00a0cf5e8e4f0d8450b6d',27,''),(62,2,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.deps','dde041654f1d7b4eebc3a5c5e5c1d384',60,'merged.js.deps',14,8,394,1678351366,1678351366,0,0,'17a5e750f416f979e1d92c79158e0127',27,''),(63,2,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.gzip','ceff3f6abe8b8b5f8268c4188be76a80',60,'merged.js.gzip',15,8,3582,1678351366,1678351366,0,0,'698c0dc68028b1310a5358647dc0d734',27,''),(64,2,'appdata_ocp2ck7yo4uj/js/gallery','0868b0a242e983dabdfec4727a60e7b9',15,'gallery',2,1,859739,1678351427,1678351427,0,0,'64099c4369691',31,''),(65,2,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js','f82849093986c955349f665498cef5fa',64,'scripts-for-file-app.js',13,8,227059,1678351366,1678351366,0,0,'76f83fd272da623f4ac3c36bca2e71fc',27,''),(66,2,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.deps','8cee64de4107b037e19a6e0001274792',64,'scripts-for-file-app.js.deps',14,8,796,1678351366,1678351366,0,0,'46be51c28a2e30d78bbd262e605b4bd1',27,''),(67,2,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.gzip','4aa0f263d2fc5e9829ce1349ab3e2fbd',64,'scripts-for-file-app.js.gzip',15,8,54659,1678351366,1678351366,0,0,'0bc9932bfcabf2786a450fe4bf675522',27,''),(68,2,'appdata_ocp2ck7yo4uj/js/core/merged.js','bf362a7fdbb6f0c38deadb41e68acde7',16,'merged.js',13,8,20224,1678351366,1678351366,0,0,'8f708cd26297318c05f38ee1432c098f',27,''),(69,2,'appdata_ocp2ck7yo4uj/js/core/merged.js.deps','0672ccdef2d9384a411dd0895964d57b',16,'merged.js.deps',14,8,472,1678351366,1678351366,0,0,'87afe98da0cc24906db3f0f304831173',27,''),(70,2,'appdata_ocp2ck7yo4uj/js/core/merged.js.gzip','3e19032edbb0a5d54a4d25606c53c3c8',16,'merged.js.gzip',15,8,5365,1678351366,1678351366,0,0,'31ef8556e4269056493caad90755405d',27,''),(71,2,'appdata_ocp2ck7yo4uj/js/systemtags','ee888bbc00ce11acc6ae76273ea221c3',15,'systemtags',2,1,24010,1678351367,1678351367,0,0,'64099c0723690',31,''),(72,2,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js','01377a4b3140da5070416af63e7d8c4c',71,'merged.js',13,8,18223,1678351367,1678351367,0,0,'17d1a37a2da2319ff75fabcb9db66266',27,''),(73,2,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.deps','b0e572499441370a48b9f9765d801f8e',71,'merged.js.deps',14,8,459,1678351367,1678351367,0,0,'6834d82ce7962b4188232fdf05b02946',27,''),(74,2,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.gzip','d8d4c0de28d2f5c53f1f945631fa5152',71,'merged.js.gzip',15,8,5328,1678351367,1678351367,0,0,'45b38bf400228f2a878747907268a181',27,''),(75,2,'appdata_ocp2ck7yo4uj/css/core','e5da66b8fc55196b7f2a80eba5b06ea6',27,'core',2,1,129494,1678358701,1678358701,0,0,'6409b8ad65b48',31,''),(88,2,'appdata_ocp2ck7yo4uj/css/files','c22100d5f13d55aac69ac2c832874652',27,'files',2,1,27898,1678358962,1678358962,0,0,'6409b9b291a9f',31,''),(92,2,'appdata_ocp2ck7yo4uj/css/files_trashbin','5f32bdfb01ceaf60105bd7ab5ef25a2d',27,'files_trashbin',2,1,642,1678358700,1678358700,0,0,'6409b8ac59fd9',31,''),(96,2,'appdata_ocp2ck7yo4uj/css/comments','19602c40a64f0290173a3ebf1dfa08f6',27,'comments',2,1,1523,1678358700,1678358700,0,0,'6409b8ac95c26',31,''),(100,2,'appdata_ocp2ck7yo4uj/css/files_sharing','80ca69a7ea85a44940833be82f45fe5e',27,'files_sharing',2,1,3412,1678358700,1678358700,0,0,'6409b8acd76fc',31,''),(104,2,'appdata_ocp2ck7yo4uj/css/files_texteditor','4ebecdcef2c2d8caa84532be9b7f3701',27,'files_texteditor',2,1,5799,1678358701,1678358701,0,0,'6409b8ad2a82e',31,''),(114,2,'appdata_ocp2ck7yo4uj/preview/3','cc5565c4ba6fd56871cbbe502dfe977c',18,'3',2,1,13632,1678356467,1678356467,0,0,'6409aff3ad578',31,''),(116,2,'appdata_ocp2ck7yo4uj/preview/3/193-96-max.png','56da6505a652ce6ec813851f00831d3a',114,'193-96-max.png',4,3,6148,1678351374,1678351374,0,0,'85be394f5f032347d6e8027ac9701705',27,''),(118,2,'appdata_ocp2ck7yo4uj/preview/3/64-64-crop.png','2beb2205135ee5e848582beea9a83e2a',114,'64-64-crop.png',4,3,2669,1678351374,1678351374,0,0,'4f8f1a105e6a031415e761f3db696025',27,''),(119,2,'appdata_ocp2ck7yo4uj/preview/5','eaca83211066f046a8313d0843f8f854',18,'5',2,1,335671,1678351426,1678351426,0,0,'64099c42513af',31,''),(120,2,'appdata_ocp2ck7yo4uj/preview/6','825339ba134f3c814c79a812ec5d826a',18,'6',2,1,595528,1678351426,1678351426,0,0,'64099c42646ed',31,''),(121,2,'appdata_ocp2ck7yo4uj/preview/7','803e2a3dffd0cdf960192b097016e362',18,'7',2,1,815617,1678351426,1678351426,0,0,'64099c424b3f3',31,''),(122,2,'appdata_ocp2ck7yo4uj/preview/5/1100-734-max.jpg','9263bb56a9b788110fa9893a71d48566',119,'1100-734-max.jpg',5,3,308351,1678351398,1678351398,0,0,'c606799c80a202f0f774971650051873',27,''),(123,2,'appdata_ocp2ck7yo4uj/preview/6/2000-1333-max.jpg','f934b86c92bf5aa5b42031f2f87f5ad4',120,'2000-1333-max.jpg',5,3,570843,1678351398,1678351398,0,0,'1c50de7acc3f8a946f9ec940cf6dd9cc',27,''),(124,2,'appdata_ocp2ck7yo4uj/preview/7/2000-1333-max.jpg','0be546cd23b5453d595aa086facd9409',121,'2000-1333-max.jpg',5,3,789147,1678351398,1678351398,0,0,'31df4942ffe9736d2ddaa36eac790133',27,''),(125,2,'appdata_ocp2ck7yo4uj/preview/5/64-64-crop.jpg','0540a52b35796b32c2efc19f39d3dbb0',119,'64-64-crop.jpg',5,3,2543,1678351398,1678351398,0,0,'97e4dc1325e79f8852a7f70e308315cf',27,''),(126,2,'appdata_ocp2ck7yo4uj/preview/6/64-64-crop.jpg','ae9b59d54212644239c4b8823bee2d1d',120,'64-64-crop.jpg',5,3,3420,1678351398,1678351398,0,0,'c2f920c346bccaea075030e977ba833b',27,''),(127,2,'appdata_ocp2ck7yo4uj/preview/7/64-64-crop.jpg','7a7f631972b2911536a530b3786f2f39',121,'64-64-crop.jpg',5,3,2929,1678351398,1678351398,0,0,'8510b854f355bf2be16e4ce4b0e087ca',27,''),(130,2,'appdata_ocp2ck7yo4uj/preview/12','a2d7ac5cb617daa0d638f3fa78cbbd51',18,'12',2,1,180145,1678351426,1678351426,0,0,'64099c42a29ef',31,''),(131,2,'appdata_ocp2ck7yo4uj/preview/12/4096-4096-max.png','96a2825d9dd0f53d9edd6e78d4b13f59',130,'4096-4096-max.png',4,3,153221,1678351406,1678351406,0,0,'d772fd1734b86350af642f60a785d9d3',27,''),(132,2,'appdata_ocp2ck7yo4uj/preview/12/64-64-crop.png','ad96ec8ee7f3c392ed81f564e110fd6f',130,'64-64-crop.png',4,3,4748,1678351406,1678351406,0,0,'1865a84c6e496606731b41df3ef8798a',27,''),(133,2,'appdata_ocp2ck7yo4uj/preview/7/256-256-crop.jpg','21fa823be8e84c0851bf52f9808645eb',121,'256-256-crop.jpg',5,3,23541,1678351426,1678351426,0,0,'eea70dab3e79892caed35a841c71912e',27,''),(134,2,'appdata_ocp2ck7yo4uj/preview/5/256-256-crop.jpg','856c67cb75dbd07fb50abeace7c52b97',119,'256-256-crop.jpg',5,3,24777,1678351426,1678351426,0,0,'b87caf0df726e15d976653b703e04a16',27,''),(135,2,'appdata_ocp2ck7yo4uj/preview/6/256-256-crop.jpg','3d70421864eda45bdf9ca5029d09ff9c',120,'256-256-crop.jpg',5,3,21265,1678351426,1678351426,0,0,'ff1a9a8327d68494833cd665492cfeda',27,''),(136,2,'appdata_ocp2ck7yo4uj/preview/3/96-96-crop.png','bbd949a06059ebc98eab286a0a5e0f3d',114,'96-96-crop.png',4,3,3679,1678351426,1678351426,0,0,'e8608f721186d203a97d3f61b967a7de',27,''),(137,2,'appdata_ocp2ck7yo4uj/preview/12/256-256-crop.png','80986e4e6dbfe180dd06dfd6de914e1e',130,'256-256-crop.png',4,3,22176,1678351426,1678351426,0,0,'cd89db7343678c936555931f449c8362',27,''),(138,2,'appdata_ocp2ck7yo4uj/js/gallery/merged.js','fdc2b62e3b85db6ebc8c9fd2be985c0e',64,'merged.js',13,8,451474,1678351427,1678351427,0,0,'de319d4155b06f5e478f7f3904d0e320',27,''),(139,2,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.deps','195df1a1ff873a95bbbe8f3accf90d45',64,'merged.js.deps',14,8,2069,1678351427,1678351427,0,0,'f9c929e4e1e16278d0e3f7075470a4a6',27,''),(140,2,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.gzip','86129e9991d4a6f79317be5e30ec6f3b',64,'merged.js.gzip',15,8,123682,1678351427,1678351427,0,0,'db78c615decb6a6b11c1fc481314cb05',27,''),(144,1,'files/Test','56c303da11d07c61688947bb3296276a',2,'Test',2,1,2190811,1678351647,1678351647,0,0,'64099d1feb055',31,''),(145,1,'files/Test/10.1_ContainerVirtualisierung.pdf','d865080338acdc012d25bb242b7b25ea',144,'10.1_ContainerVirtualisierung.pdf',9,8,110769,1678351646,1678351646,0,0,'ef0d11062b0789404dafe0bb6aa6bbb6',27,''),(146,1,'files/Test/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf','81af18266b5aec175e8251f990177f32',144,'10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf',9,8,307530,1678351646,1678351646,0,0,'9cce7ce6a8e45186fd51be7ce262fc88',27,''),(147,1,'files/Test/10.3_Container_oder_VM_Pros_und_Kontras.pdf','77f9d70222dbb272c1062128f5a7ef58',144,'10.3_Container_oder_VM_Pros_und_Kontras.pdf',9,8,1266125,1678351647,1678351647,0,0,'c7ee593e13da6558f8b0ad2d7b3cd818',27,''),(148,1,'files/Test/10.4_CV_Versus_VM.docx','66fe42e6e6d9b94bd7393f4d15c691df',144,'10.4_CV_Versus_VM.docx',18,8,47076,1678351647,1678351647,0,0,'6630f112a5192a3cd1ac9b78cc6ac33c',27,''),(149,1,'files/Test/10.5_Docker Einstieg - CLI-Befehle.pdf','f8d47cef2d136cd900b087ed86528ff6',144,'10.5_Docker Einstieg - CLI-Befehle.pdf',9,8,458836,1678351647,1678351647,0,0,'8b5a443fd1b61f6b17b6ee4e0ea8df36',27,''),(150,1,'files/Test/docker-compose.yml','8592a5cf0fcd129b3e6b5dad6b57afbc',144,'docker-compose.yml',19,8,475,1678351647,1678351647,0,0,'c66811eb4d0f2288d2987a6d8256c19e',27,''),(152,1,'files/Testfile.txt','1724763b6e378096e6bc77a84f84ba06',2,'Testfile.txt',12,11,9,1678352141,1678352141,0,0,'09a1c99817b4fe994cb78e50d1e69b79',27,''),(154,1,'files_versions','9692aae50022f45f1098646939b287b1',1,'files_versions',2,1,1,1678352141,1678352141,0,0,'64099f0d12f5c',31,''),(155,1,'files_versions/Testfile.txt.v1678352135','66b38ad6af1c8f42b26dd08dbf78ab16',154,'Testfile.txt.v1678352135',12,11,1,1678352141,1678352141,0,0,'af59dd808d2376ea3f1183e9ee07e5c6',27,''),(156,2,'appdata_ocp2ck7yo4uj/preview/152','d3564f308ae3aa82990f73f8a55fd41e',18,'152',2,1,30985,1678457879,1678457879,0,0,'640b3c1782aa1',31,''),(157,2,'appdata_ocp2ck7yo4uj/preview/152/4096-4096-max.png','e993f925a7782bf3037be3a606399ccd',156,'4096-4096-max.png',4,3,26048,1678352141,1678352141,0,0,'146fbdac137757811dc6e041952930e2',27,''),(158,2,'appdata_ocp2ck7yo4uj/preview/152/64-64-crop.png','082f6736b1f106ad596c51a2489663b6',156,'64-64-crop.png',4,3,644,1678352141,1678352141,0,0,'313910f831e5d23d495ae3e97f2bd82a',27,''),(159,1,'files/m129_bandbreitenberechnung.pdf','9bb0b1c54fe00704b9d0ac508976895e',2,'m129_bandbreitenberechnung.pdf',9,8,161663,1678352217,1678352217,0,0,'cb458eae1a9bc6e28e99197151978050',27,''),(160,1,'files/m129_bandbreitenberechnung-lsg.pdf','df131c44fd2d73cd29505fda7a3524d1',2,'m129_bandbreitenberechnung-lsg.pdf',9,8,104704,1678352218,1678352218,0,0,'f6650da8a37a9e13191aa18163bcab58',27,''),(161,2,'files_external','c270928b685e7946199afdfb898d27ea',13,'files_external',2,1,0,1678352236,1678352236,0,0,'64099f6cef560',31,''),(162,2,'appdata_ocp2ck7yo4uj/preview/152/256-256-crop.png','e43ed5fc69ef670c38697cdb7d776f79',156,'256-256-crop.png',4,3,3896,1678352237,1678352237,0,0,'4fcdfecf7258cfc92990beecde404732',27,''),(163,2,'appdata_ocp2ck7yo4uj/appstore','2345d9a5592e7a2e8d79bf2223c8e348',14,'appstore',2,1,1087363,1678457923,1678352241,0,0,'640b3c4398298',31,''),(164,2,'appdata_ocp2ck7yo4uj/appstore/apps.json','aea0a87bc9c12b12737ee801593ccba8',163,'apps.json',20,8,1087363,1678457923,1678457923,0,0,'32572096144682a63fba401729a04653',27,''),(165,2,'appdata_ocp2ck7yo4uj/css/settings','2fd09612cf55a6d85d4729e9fa0f7a9e',27,'settings',2,1,20100,1678358698,1678358698,0,0,'6409b8aa3a871',31,''),(169,2,'appdata_ocp2ck7yo4uj/avatar/vok','0a0486437d7af5fe0661d5870080b6fe',34,'vok',2,1,17536,1678358217,1678358217,0,0,'6409b6c958fcc',31,''),(170,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.png','1c5e17f3562bf0026ff2433bc8359af1',169,'avatar.png',4,3,14672,1678352368,1678352368,0,0,'5e7317446f7ca30a43bf906dc382830a',27,''),(171,2,'appdata_ocp2ck7yo4uj/avatar/vok/generated','92d0578812637bcd106a74c51bd2c0cd',169,'generated',14,8,0,1678352368,1678352368,0,0,'c81b737665ebd870f489378c561e3907',27,''),(172,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.40.png','d709c254c65347b092c445a6e5dbaaa3',169,'avatar.40.png',4,3,458,1678352369,1678352369,0,0,'8e687f814173036489e31beeb8b9cfbf',27,''),(173,2,'appdata_ocp2ck7yo4uj/avatar/testuser','35512c74e396de5a72f211830497ab48',34,'testuser',2,1,5695,1678352389,1678352389,0,0,'6409a00556fe5',31,''),(174,2,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.png','344c673f2655b9feace05731093b597a',173,'avatar.png',4,3,5477,1678352388,1678352388,0,0,'9cc3a2c1253c5445587071e8ef3c7b04',27,''),(175,2,'appdata_ocp2ck7yo4uj/avatar/testuser/generated','62fd23db4be5617c3426ec24aa711f91',173,'generated',14,8,0,1678352388,1678352388,0,0,'3f5bed1bb547512efbab8bd8df4700f5',27,''),(176,2,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.40.png','1165c2f5e3965d783a95654d53e4147c',173,'avatar.40.png',4,3,218,1678352389,1678352389,0,0,'8b000c6e9818e4478cd5a842b0da47f3',27,''),(177,2,'appdata_ocp2ck7yo4uj/avatar/testuser2','638d7e7ca3dce9c416dada796c49c1ed',34,'testuser2',2,1,5910,1678956927,1678956927,0,0,'6412d97f862ff',31,''),(178,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.png','a3783e8054fcad0ccf5744275f000992',177,'avatar.png',4,3,5487,1678352409,1678352409,0,0,'e0a7231e74cd76ebaee9b1a9ad8080fe',27,''),(179,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/generated','27d667766eef561885e247be37aeaa76',177,'generated',14,8,0,1678352409,1678352409,0,0,'6eb1a2c062b1ee736c393328bd7e2652',27,''),(180,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.40.png','8a34e8e5a8ce95f5ead5eddf1e5abfe2',177,'avatar.40.png',4,3,218,1678352410,1678352410,0,0,'0dbfe30f10ecba1c7b3fd7280cd689db',27,''),(181,2,'appdata_ocp2ck7yo4uj/dav-photocache','d30c65c6c0c584cdd932ec953747fc71',14,'dav-photocache',2,1,52222,1678457885,1678457885,0,0,'640b3c1d3642f',31,''),(184,2,'appdata_ocp2ck7yo4uj/avatar/testuser3','d086ba37569c9161e11461068373ee41',34,'testuser3',2,1,5706,1678353724,1678353724,0,0,'6409a53c42b97',31,''),(185,2,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.png','05d1d04fde756a5ccfe6c591210c7cf1',184,'avatar.png',4,3,5487,1678353723,1678353723,0,0,'7fdc7822a2babcb7b5c1e072931a12bd',27,''),(186,2,'appdata_ocp2ck7yo4uj/avatar/testuser3/generated','c68c06a199b0826020e178163f42cf75',184,'generated',14,8,0,1678353723,1678353723,0,0,'7bc0c312f74c568932c106817e7ff121',27,''),(187,2,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.40.png','ccce2d61522438868b4e0d107682f425',184,'avatar.40.png',4,3,219,1678353724,1678353724,0,0,'1fef0d8a25d0319b97e43a0744bd465e',27,''),(189,2,'appdata_ocp2ck7yo4uj/avatar/disableduser','6830f17f340ba5039fbbae652584970f',34,'disableduser',2,1,13373,1678353833,1678353833,0,0,'6409a5a947d3d',31,''),(190,2,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.png','ecb540d8c6611cb24a2fe7a69867fb31',189,'avatar.png',4,3,12924,1678353832,1678353832,0,0,'95abc1852407b4700b14e866e530d950',27,''),(191,2,'appdata_ocp2ck7yo4uj/avatar/disableduser/generated','0b4666d8647679ba143b11759a36c07f',189,'generated',14,8,0,1678353832,1678353832,0,0,'7f35922cb7556e35a6c50b33a8f1d1dc',27,''),(192,2,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.40.png','d6e11c84318f70f5e74c5d1ec67c1a61',189,'avatar.40.png',4,3,449,1678353833,1678353833,0,0,'b3f2e2f5f3350e540db4e25f37430a76',27,''),(193,1,'files_trashbin/files/deletedfile.txt.d1678356431','98c2b27aefac656d26d415cde28742e0',196,'deletedfile.txt.d1678356431',14,8,1,1678356423,1678356423,0,0,'d80f2479b1f5e9995f04a51b09a72b83',27,''),(195,1,'files_trashbin','fb66dca5f27af6f15c1d1d81e6f8d28b',1,'files_trashbin',2,1,1,1678356431,1678356431,0,0,'6409afcfa757f',31,''),(196,1,'files_trashbin/files','3014a771cbe30761f2e9ff3272110dbf',195,'files',2,1,1,1678356431,1678356431,0,0,'6409afcfa757f',31,''),(197,1,'files_trashbin/versions','c639d144d3f1014051e14a98beac5705',195,'versions',2,1,0,1678356431,1678356431,0,0,'6409afcf89a4d',31,''),(198,1,'files_trashbin/keys','9195c7cfc1b867f229ac78cc1b6a0be3',195,'keys',2,1,0,1678356431,1678356431,0,0,'6409afcf914de',31,''),(199,2,'appdata_ocp2ck7yo4uj/preview/193','461e154eff9f19485709a164ac7546df',18,'193',2,1,0,1678356445,1678356445,0,0,'6409afdda426f',31,''),(200,2,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de','bbbf0016034ecc5a6096c8b52da6b8b7',181,'7641eb079d4bafa5e2024031c65e28de',2,1,6925,1678356448,1678356448,0,0,'6409afe0530f7',31,''),(201,2,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.png','75e1798c3f63cfc444cd65c58a550396',200,'photo.png',4,3,6662,1678356448,1678356448,0,0,'20c33d2234ea37d72ca38bf3111dfe76',27,''),(204,2,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f','23789bb835b044e302311e35484613a6',181,'7a6e71f6fabbda1da4a96ba84be4329f',2,1,6927,1678356448,1678356448,0,0,'6409afe059ab6',31,''),(205,2,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.png','695384511c2fa6076a55b047624a7fd9',204,'photo.png',4,3,6661,1678356448,1678356448,0,0,'64e9a293d835c49190e4719a0f89ecdc',27,''),(206,2,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766','1619c8ef2c9355e72c8b5bf03aeaeecc',181,'274857af206af2cd22deae7c56f80766',2,1,6922,1678356448,1678356448,0,0,'6409afe0650a2',31,''),(207,2,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.png','c22bc875ab14a2f3b737b1c87bec25df',206,'photo.png',4,3,6658,1678356448,1678356448,0,0,'2eed36fe45b22d01eada2811cd0b3633',27,''),(208,2,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.32.png','6141d37c71dd6fb7888d80b871c78c13',200,'photo.32.png',4,3,263,1678356448,1678356448,0,0,'412f1b12bd00165d56a0166429749d8b',27,''),(210,2,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.32.png','c5c4f3df014addc24a28b254941e8b46',204,'photo.32.png',4,3,266,1678356448,1678356448,0,0,'59b2f19cea4d45c9fae129bffaf31e7c',27,''),(211,2,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72','141f595693cdf6f12110583a7998ba56',181,'7f3e57a2397831bc58cb185e0becdc72',2,1,14912,1678356448,1678356448,0,0,'6409afe07f443',31,''),(212,2,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.32.png','cc6bc3653657a63f28ac9ad9968055e1',206,'photo.32.png',4,3,264,1678356448,1678356448,0,0,'972a572b0a5c72e1b32560c6a283d9d5',27,''),(213,2,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.png','6a4bff6c7c59d57e1e88f93db2059b40',211,'photo.png',4,3,14490,1678356448,1678356448,0,0,'3943cdc30ab2f320c4ebd9b4ea9f5da4',27,''),(214,2,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.32.png','a21e27f03088f448ee3fcd45b61158d5',211,'photo.32.png',4,3,422,1678356448,1678356448,0,0,'adfa6ccef4fa3c0ff9d67fe43ee378e4',27,''),(225,2,'appdata_ocp2ck7yo4uj/preview/3/32-32-crop.png','089ced233440fe37d36fb62dac5497c0',114,'32-32-crop.png',4,3,1136,1678356467,1678356467,0,0,'0d3d6dd76c6506db30276aae0b8e615a',27,''),(227,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.png','d8dd45159872dc1cb96d8f1d5e3764d6',35,'avatar.png',4,3,3958,1678356476,1678356476,0,0,'18be4d8cd34807df71d8440b263d09ca',27,''),(231,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.40.png','4519fa97dd131e66e97ae8eb7202e4d6',35,'avatar.40.png',4,3,1464,1678356477,1678356477,0,0,'1931144bcedc5df2a494283f1886b4a9',27,''),(232,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.182.png','392718da6f4eea3f15b499074c74f5ea',35,'avatar.182.png',4,3,10549,1678356477,1678356477,0,0,'ea78b9fd42a93d007809e5e04622e552',27,''),(233,2,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.32.png','f091d9d8c6eb1c6f6f27a4b1b4a29b70',35,'avatar.32.png',4,3,1095,1678356482,1678356482,0,0,'2f4ac614d8ca044c8bf1f3ff25167c82',27,''),(234,7,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,8717771,1678359119,1678358159,0,0,'6409ba4f48a7c',23,''),(235,7,'cache','0fea6a13c52b4d4725368f24b045ca84',234,'cache',2,1,0,1678358158,1678358158,0,0,'6409b68ee3f7d',31,''),(236,7,'files','45b963397aa40d4a0063e0d85e4fe7a1',234,'files',2,1,8717771,1678359119,1678359111,0,0,'6409ba4f48a7c',31,''),(237,7,'files/bbw-logo.png','7fb0f51046121e4c805f9aeaa14b3093',236,'bbw-logo.png',4,3,6878,1678358159,1678358159,0,0,'484ee96f0c045bb6f0941c216056ec31',27,''),(238,7,'files/Photos','d01bb67e7b71dd49fd06bad922f521c9',236,'Photos',2,1,2360011,1678358159,1678358159,0,0,'6409b68f7a659',31,''),(239,7,'files/Photos/Coast.jpg','a6fe87299d78b207e9b7aba0f0cb8a0a',238,'Coast.jpg',5,3,819766,1678358159,1678358159,0,0,'7f07cfa245d7a2c4a09896aaa4a78c9f',27,''),(240,7,'files/Photos/Hummingbird.jpg','e077463269c404ae0f6f8ea7f2d7a326',238,'Hummingbird.jpg',5,3,585219,1678358159,1678358159,0,0,'8ef6f24b2e602c7a4555de6f20dfa173',27,''),(241,7,'files/Photos/Nut.jpg','aa8daeb975e1d39412954fd5cd41adb4',238,'Nut.jpg',5,3,955026,1678358159,1678358159,0,0,'631741074b2a805c25deeb198b968d6e',27,''),(242,7,'files/Nextcloud.mp4','77a79c09b93e57cba23c11eb0e6048a6',236,'Nextcloud.mp4',7,6,462413,1678358159,1678358159,0,0,'300c4d282e4b4eb0a8922a1907c7f715',27,''),(243,7,'files/Nextcloud Manual.pdf','2bc58a43566a8edde804a4a97a9c7469',236,'Nextcloud Manual.pdf',9,8,4540242,1678358159,1678358159,0,0,'b29a2e654ce8ea8ecd32fd60efe5c589',27,''),(244,7,'files/Documents','0ad78ba05b6961d92f7970b2b3922eca',236,'Documents',2,1,78496,1678358160,1678358160,0,0,'6409b6900d783',31,''),(245,7,'files/Documents/About.odt','b2ee7d56df9f34a0195d4b611376e885',244,'About.odt',10,8,77422,1678358159,1678358159,0,0,'3d88bfa35567e4ce4dfd645816333ef9',27,''),(246,7,'files/Documents/About.txt','9da7b739e7a65d74793b2881da521169',244,'About.txt',12,11,1074,1678358160,1678358160,0,0,'04562c6ec48108348c279641cab91d36',27,''),(247,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.32.png','19097930e38bfc9c4f8bd42e72127c39',169,'avatar.32.png',4,3,391,1678358160,1678358160,0,0,'e71517f26ce97e1fe56fce127697f219',27,''),(248,2,'appdata_ocp2ck7yo4uj/preview/237','2659a3cdbcb2a96b3b3cdbf0a5df6f7f',18,'237',2,1,12496,1678358965,1678358965,0,0,'6409b9b5d5cf5',31,''),(249,2,'appdata_ocp2ck7yo4uj/preview/237/193-96-max.png','50d08586a69e0184430ad8cc773a0c95',248,'193-96-max.png',4,3,6148,1678358161,1678358161,0,0,'51266f6d1ca3c428dde8312651da4bfa',27,''),(250,2,'appdata_ocp2ck7yo4uj/preview/237/64-64-crop.png','9368924db841411cae5010b37e353748',248,'64-64-crop.png',4,3,2669,1678358161,1678358161,0,0,'69da871175186bac898e8407633148ed',27,''),(251,7,'files/testfile.txt','cd075662fbab9b754c348f29c20c8571',236,'testfile.txt',12,11,1,1678358180,1678358180,0,0,'09fa80b27a41f88d6b8bb46916c0867d',27,''),(252,2,'appdata_ocp2ck7yo4uj/preview/251','decab0cb33d2a303801bc42cf78f89cb',18,'251',2,1,0,1678358181,1678358181,0,0,'6409b6a553a6b',31,''),(253,7,'files/testfile2.txt','c5f6c5296e18f9ced7fb289925ff0752',236,'testfile2.txt',12,11,1,1678358187,1678358187,0,0,'bf8c980b853092c2fdeadce01522cb85',27,''),(254,2,'appdata_ocp2ck7yo4uj/preview/253','5c622a885af00ec11d18377abac472d4',18,'253',2,1,0,1678358188,1678358188,0,0,'6409b6ac11cf6',31,''),(255,2,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.182.png','9bee4d8d9dc2e2f87b9f92344499726a',169,'avatar.182.png',4,3,2015,1678358217,1678358217,0,0,'518d9b3db9955bbe3eb6aa72ea8cefb8',27,''),(256,2,'appdata_ocp2ck7yo4uj/identityproof','2d4281eebef45968390785218dc56c63',14,'identityproof',2,1,4021,1678358224,1678358224,0,0,'6409b6d0bccff',31,''),(257,2,'appdata_ocp2ck7yo4uj/identityproof/user-vok','645643f49f95bb6a22d54366a668d7a2',256,'user-vok',2,1,4021,1678358224,1678358224,0,0,'6409b6d0bccff',31,''),(258,2,'appdata_ocp2ck7yo4uj/identityproof/user-vok/private','0eb8ad1208a413e19deb14433329e20e',257,'private',14,8,3570,1678358224,1678358224,0,0,'9502d6278776d5ced454aeedaaaaf01c',27,''),(259,2,'appdata_ocp2ck7yo4uj/identityproof/user-vok/public','a004b7c46da2c2254f89de1e50172352',257,'public',14,8,451,1678358224,1678358224,0,0,'02fbce812942da9a85f811c95e10b243',27,''),(268,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css','78bff418cb5b8be898905e26105f439d',75,'89a5-dd39-server.css',16,11,100134,1678358268,1678358268,0,0,'586be42b71e881e76f165e342ed84c27',27,''),(269,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.deps','baa462ce2528e7565d9e8e94861d584e',75,'89a5-dd39-server.css.deps',14,8,772,1678358268,1678358268,0,0,'f2578b7376527e11ff6decae1be137c9',27,''),(270,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.gzip','88dd674bc368509aa3ea198c2f4626f9',75,'89a5-dd39-server.css.gzip',15,8,15888,1678358268,1678358268,0,0,'3971f0dedcc495100e354ac8e77d7a24',27,''),(271,2,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css','684553ec9bd093db235f659c57b15af8',28,'2bf5-dd39-theming.css',16,11,724,1678358268,1678358268,0,0,'d768235dab8080bda78988c9a95f3c2d',27,''),(272,2,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.deps','666a810bd01910cf9caa1734d6bbca52',28,'2bf5-dd39-theming.css.deps',14,8,132,1678358268,1678358268,0,0,'5fe767ab48e48f2d1060ac3e703b785d',27,''),(273,2,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.gzip','b61b61c25ad60bb4846e5b819a0e4baa',28,'2bf5-dd39-theming.css.gzip',15,8,307,1678358268,1678358268,0,0,'075a03ee07ce7f78602c1934e12fa816',27,''),(274,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css','1dbfa0f3f5c9cfd5492550c33ffa95ff',75,'89a5-dd39-jquery-ui-fixes.css',16,11,4102,1678358697,1678358697,0,0,'43597846dcf2b569df15d68b01defc49',27,''),(275,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.deps','635838ae2dacee8a4eaacc0163b9ae97',75,'89a5-dd39-jquery-ui-fixes.css.deps',14,8,131,1678358697,1678358697,0,0,'e853db3d6a11ffcc31670ae108a36276',27,''),(276,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.gzip','31f45e6cbf6c6109a5c543594268b0f7',75,'89a5-dd39-jquery-ui-fixes.css.gzip',15,8,846,1678358697,1678358697,0,0,'492275de2e3a7b38160fe217d460de05',27,''),(277,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css','72958c1e1ceabf47c101dc39f746f375',75,'89a5-dd39-share.css',16,11,2685,1678358697,1678358697,0,0,'bb8092c7df6aac42684436d0d2fc48fb',27,''),(278,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.deps','5e1996bb860233f579b5cc0685c018c8',75,'89a5-dd39-share.css.deps',14,8,121,1678358697,1678358697,0,0,'27959d71aa020b7f07aa4b19ce69e565',27,''),(279,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.gzip','05f6e2f8484349624c1cbb5c85ae1de5',75,'89a5-dd39-share.css.gzip',15,8,957,1678358697,1678358697,0,0,'335bfa703f8d2a4ac435309a6c6ac4a2',27,''),(280,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css','86681b8bc226a647a798fea77f821f8b',75,'89a5-dd39-jquery.ocdialog.css',16,11,1253,1678358697,1678358697,0,0,'1fca2a34000b177db54cb6e25b64c2ca',27,''),(281,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.deps','f5564a3bc71307439ccf0e75917842ec',75,'89a5-dd39-jquery.ocdialog.css.deps',14,8,131,1678358697,1678358697,0,0,'4214ce481031d57878145b447dc6f4f5',27,''),(282,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.gzip','3d9bffd046bcb0bd6f922550fd1728e7',75,'89a5-dd39-jquery.ocdialog.css.gzip',15,8,560,1678358697,1678358697,0,0,'f426e96192eb0ab7a329118cdcf388c9',27,''),(283,2,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css','c32a464f00dea1e419cb2153dea8d226',165,'89a5-dd39-settings.css',16,11,15629,1678358698,1678358698,0,0,'416da9a531bb1347c9937099cb6a74a1',27,''),(284,2,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.deps','70ca02cf7791d1bf0ab2ee6e6190a08d',165,'89a5-dd39-settings.css.deps',14,8,128,1678358698,1678358698,0,0,'4bdb0e0af5ff0a06628e61b30b7536dc',27,''),(285,2,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.gzip','7509ce07ce4d63163a95c2d37570e5db',165,'89a5-dd39-settings.css.gzip',15,8,4343,1678358698,1678358698,0,0,'670c7bdf2e161c9007ad6fa151fd14ee',27,''),(286,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css','a7455aea2749eb5babf9882432e43052',88,'4fda-dd39-merged.css',16,11,18493,1678358700,1678358700,0,0,'ec6b5e2bc38d56188e0e3d97a5fb7264',27,''),(287,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.deps','014a08cbc68d833047d7ed9ef00582d5',88,'4fda-dd39-merged.css.deps',14,8,397,1678358700,1678358700,0,0,'558889ca166a01d91b9220882e386872',27,''),(288,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.gzip','7417c522a6b92d18106617a0fe0cd93c',88,'4fda-dd39-merged.css.gzip',15,8,4220,1678358700,1678358700,0,0,'75b7b86bdfd89f247a1b1757d171037b',27,''),(289,2,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css','cead350d6af9fb0d4b0acf5c57433d0a',92,'1980-dd39-trash.css',16,11,344,1678358700,1678358700,0,0,'5c79c16cb5211c5fe2d623ba0a8c7c7e',27,''),(290,2,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.deps','ddc57b10ad9af48a3ed703e6db4d325d',92,'1980-dd39-trash.css.deps',14,8,137,1678358700,1678358700,0,0,'469659a800067a06cd226ef84357096e',27,''),(291,2,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.gzip','cd58401fa37b93e6d8ece58f6793ad07',92,'1980-dd39-trash.css.gzip',15,8,161,1678358700,1678358700,0,0,'e1f4d1cb3fb71611590d8f0a61ff39d2',27,''),(292,2,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css','315c9a71ed6d9bfc55b67efa3639823c',96,'1980-dd39-autocomplete.css',16,11,964,1678358700,1678358700,0,0,'8a4a5386a4a2db011e136787350e0c3b',27,''),(293,2,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.deps','25caf287d9b6f3709e7fb0f6909daef3',96,'1980-dd39-autocomplete.css.deps',14,8,138,1678358700,1678358700,0,0,'5a45300ef8a72b3ed8041a3f052aff61',27,''),(294,2,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.gzip','4bcfbe4d23bea15b19cc1be6de2ab207',96,'1980-dd39-autocomplete.css.gzip',15,8,421,1678358700,1678358700,0,0,'725bb4db97aff789dbbbee99829da203',27,''),(295,2,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css','bb2bcb3b5216a5bf08870b98ab8108a8',100,'35c3-dd39-mergedAdditionalStyles.css',16,11,2303,1678358700,1678358700,0,0,'9bf47207e8cef093612daf37cde95f26',27,''),(296,2,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.deps','3d2dd721c9177b66fcb27c9ce23724b5',100,'35c3-dd39-mergedAdditionalStyles.css.deps',14,8,316,1678358700,1678358700,0,0,'a063f04225dde33fbd003b11208af35e',27,''),(297,2,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.gzip','d27a4c0564ba11157d932a143b719725',100,'35c3-dd39-mergedAdditionalStyles.css.gzip',15,8,793,1678358700,1678358700,0,0,'f4a78b449c9e5f71c3702a74ee1adc34',27,''),(298,2,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css','9e3066d1300a64bbe4c1bb0a1152a98e',104,'21f4-dd39-merged.css',16,11,4148,1678358701,1678358701,0,0,'61ad3fca826f0199be37f420628b18cc',27,''),(299,2,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.deps','bc43f6c94909d91c8c7b59e21fa67f61',104,'21f4-dd39-merged.css.deps',14,8,389,1678358701,1678358701,0,0,'77ff19a618e3af717c431a9ee36365dd',27,''),(300,2,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.gzip','ccb5610cf876f5e2261d71e2f0e41b28',104,'21f4-dd39-merged.css.gzip',15,8,1262,1678358701,1678358701,0,0,'e7dca8f19b2de327482ac7b24f7024bd',27,''),(301,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css','3dd40571f6f29d0a0c946dce61469748',75,'89a5-dd39-systemtags.css',16,11,1403,1678358701,1678358701,0,0,'93b3e68f642c4d98af181634281cd40f',27,''),(302,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.deps','54f3b5f5c223467ca699ffe87c8678d1',75,'89a5-dd39-systemtags.css.deps',14,8,126,1678358701,1678358701,0,0,'57e594124e7bce772a4a6683e25521d4',27,''),(303,2,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.gzip','ca3d590ed9b309e4099c628c9ba37bfc',75,'89a5-dd39-systemtags.css.gzip',15,8,385,1678358701,1678358701,0,0,'f48e1a27f56a9891c0b1f7fe546bf2e7',27,''),(304,2,'appdata_ocp2ck7yo4uj/theming/1','d20433a493075cc0414e07c5c31de2bf',29,'1',2,1,2705,1678956997,1678956997,0,0,'6412d9c5736b6',31,''),(305,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_video.svg','b2db7a7e6ab13f9ea09a02bd98fa0039',304,'icon-core-filetypes_video.svg',17,3,277,1678358756,1678358756,0,0,'62f2213a9716b94c50f6b459ffc907bf',27,''),(306,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_folder.svg','9c31a0be115de8ea2d86812cb7491591',304,'icon-core-filetypes_folder.svg',17,3,255,1678358756,1678358756,0,0,'5183852be10933eccfc2b38e046a7555',27,''),(307,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_image.svg','ab48ac8e035ddb84f1d34cb43e486f7f',304,'icon-core-filetypes_image.svg',17,3,352,1678358756,1678358756,0,0,'47d86fb3b31ce1db2cf6186b25b92bd2',27,''),(308,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_text.svg','2aaf405bd7368b937bb2172e801b9be5',304,'icon-core-filetypes_text.svg',17,3,295,1678358756,1678358756,0,0,'ef538bc49774f7aeb3a5818337dc732d',27,''),(309,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_application-pdf.svg','1c363d4f97b09c9974eabdf1b1e79195',304,'icon-core-filetypes_application-pdf.svg',17,3,676,1678358756,1678358756,0,0,'ddde1587ba031ca724d51e53f89be219',27,''),(310,2,'appdata_ocp2ck7yo4uj/preview/240','66538cbe3c339e6ff7cd619d5e10bb67',18,'240',2,1,595528,1678358963,1678358963,0,0,'6409b9b3e4e39',31,''),(311,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-document.svg','d5896aaefcf6842432895d5fa19c41b1',304,'icon-core-filetypes_x-office-document.svg',17,3,295,1678358960,1678358960,0,0,'91a8d7bbba00fb12fee92618de11e5f3',27,''),(312,2,'appdata_ocp2ck7yo4uj/preview/240/2000-1333-max.jpg','109640fa4bd202334f7dbbde430c14c2',310,'2000-1333-max.jpg',5,3,570843,1678358961,1678358961,0,0,'2f55898222d8046ea5115cd835059c53',27,''),(313,2,'appdata_ocp2ck7yo4uj/preview/239','6ea1f2e4fabd223bb8b6ad53d41a7fb1',18,'239',2,1,335671,1678358963,1678358963,0,0,'6409b9b3bf6df',31,''),(314,2,'appdata_ocp2ck7yo4uj/preview/240/64-64-crop.jpg','0c9a17ebcebc388d5f438daa79585b05',310,'64-64-crop.jpg',5,3,3420,1678358961,1678358961,0,0,'b5c2c4486dc43637d47dcb8bf33e23e6',27,''),(315,2,'appdata_ocp2ck7yo4uj/preview/239/1100-734-max.jpg','34f53b48ebf376f90df62b71d4ddae26',313,'1100-734-max.jpg',5,3,308351,1678358961,1678358961,0,0,'8c2a04a6bbb92cf0a48d8c53f2ea70c0',27,''),(316,2,'appdata_ocp2ck7yo4uj/preview/246','8734b661d9ef2e41975596cbb7aea946',18,'246',2,1,180145,1678358966,1678358966,0,0,'6409b9b6bb240',31,''),(317,2,'appdata_ocp2ck7yo4uj/preview/241','23fce7333eee9a2ebfdabc3ce0221a0f',18,'241',2,1,815617,1678358964,1678358964,0,0,'6409b9b419874',31,''),(318,2,'appdata_ocp2ck7yo4uj/preview/239/64-64-crop.jpg','65a3deb5ecae8673330bf52779ca6387',313,'64-64-crop.jpg',5,3,2543,1678358961,1678358961,0,0,'bc1cee47be4da62affd00daed325bb84',27,''),(319,2,'appdata_ocp2ck7yo4uj/preview/241/2000-1333-max.jpg','377a6f6a22649ef4fa2e7802496a4aaa',317,'2000-1333-max.jpg',5,3,789147,1678358961,1678358961,0,0,'eb37655d36b1cc1b9186761be9188481',27,''),(320,2,'appdata_ocp2ck7yo4uj/preview/241/64-64-crop.jpg','38014ff76499410a401d139c0a37b5f2',317,'64-64-crop.jpg',5,3,2929,1678358962,1678358962,0,0,'a252554979d0b2c8371f0f06a5f76df8',27,''),(321,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css','8d9996c9ec1c3019ac4ab8ba503e4f1d',88,'4fda-dd39-upload.css',16,11,3643,1678358962,1678358962,0,0,'60a82c308a11dc13af8b3ce096808b11',27,''),(322,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.deps','4dc0211ca2cfd0dc15fa3b1c519d8443',88,'4fda-dd39-upload.css.deps',14,8,129,1678358962,1678358962,0,0,'61340eb196e87520e9b2cc842baf015a',27,''),(323,2,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.gzip','8f74774bd8232922b5c77877921908e9',88,'4fda-dd39-upload.css.gzip',15,8,1016,1678358962,1678358962,0,0,'7c0ed9653d6725f8b0f88b8384ad0606',27,''),(324,2,'appdata_ocp2ck7yo4uj/preview/246/4096-4096-max.png','f01f0d365843fde49e45c08290be1516',316,'4096-4096-max.png',4,3,153221,1678358962,1678358962,0,0,'d97b8651465a5f7eabf90746827165de',27,''),(325,2,'appdata_ocp2ck7yo4uj/preview/246/64-64-crop.png','7fe853813378f83155fba05db04dbaf3',316,'64-64-crop.png',4,3,4748,1678358963,1678358963,0,0,'2a79285654c19434ec6635efe6ab6b07',27,''),(326,2,'appdata_ocp2ck7yo4uj/preview/239/256-256-crop.jpg','886334752446814dcc2b231022778ee9',313,'256-256-crop.jpg',5,3,24777,1678358963,1678358963,0,0,'36b46ef9c63da4bfc0ecef0ab4dec834',27,''),(327,2,'appdata_ocp2ck7yo4uj/preview/240/256-256-crop.jpg','bbd758a5e04a2ed32cec068ffe7a9cbc',310,'256-256-crop.jpg',5,3,21265,1678358963,1678358963,0,0,'716d8e297c3d90d1af7acb41d2d027f4',27,''),(328,2,'appdata_ocp2ck7yo4uj/preview/241/256-256-crop.jpg','98d8c52e04c5bbe4db451b129c85eef4',317,'256-256-crop.jpg',5,3,23541,1678358964,1678358964,0,0,'a5477ce2498c2dd829ea50e3672bbb8d',27,''),(329,2,'appdata_ocp2ck7yo4uj/preview/237/96-96-crop.png','234d24f23ec4cb27e343e4657a87b498',248,'96-96-crop.png',4,3,3679,1678358965,1678358965,0,0,'ce078b7590ce9327d9219bc54064a971',27,''),(330,2,'appdata_ocp2ck7yo4uj/preview/246/256-256-crop.png','4871a26802ed1eb1e25d671f5b1718c6',316,'256-256-crop.png',4,3,22176,1678358966,1678358966,0,0,'fa62b69ad1bc393b22c2b6e857a8320c',27,''),(331,7,'files/!!!IMPORTANT!!!','1d2426dce41e886063fa2165e3cecdc2',236,'!!!IMPORTANT!!!',2,1,1269729,1678359119,1678359119,0,0,'6409ba4f48a7c',31,''),(332,7,'files/!!!IMPORTANT!!!/WIN_20211028_15_17_04_Pro.jpg','27803d8e661a2b1a686e6878458c589c',331,'WIN_20211028_15_17_04_Pro.jpg',5,3,1269729,1678359119,1678359119,0,0,'4b2337404aed1f64d0f21a1615bc20c9',27,''),(333,2,'appdata_ocp2ck7yo4uj/preview/332','b7c45cd7a53996abde18890823dc48ad',18,'332',2,1,572509,1678359120,1678359120,0,0,'6409ba501bbac',31,''),(334,2,'appdata_ocp2ck7yo4uj/preview/332/2560-1440-max.jpg','e223ae728a3547ba402cb5e883945301',333,'2560-1440-max.jpg',5,3,570066,1678359119,1678359119,0,0,'2421b841c11d59b605c265f7ceb29076',27,''),(335,2,'appdata_ocp2ck7yo4uj/preview/332/64-64-crop.jpg','f79b7f498e60cee0ce0053afdb7cec0a',333,'64-64-crop.jpg',5,3,2443,1678359120,1678359120,0,0,'e268566f81751459edd72930d0d64b1c',27,''),(336,2,'appdata_ocp2ck7yo4uj/preview/152/32-32-crop.png','7a9f0a0a8785feade277994e00380778',156,'32-32-crop.png',4,3,397,1678457879,1678457879,0,0,'d09932a7c96580b208ad0b4c6217cb8f',27,''),(337,2,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e','3ef75959df1ffce11a451390f7801a6a',181,'43c2025c19f23a73a880300144252f7e',2,1,16536,1678457885,1678457885,0,0,'640b3c1d3642f',31,''),(338,2,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.png','3e07f6fba42704c88db57c8943598eff',337,'photo.png',4,3,16074,1678457885,1678457885,0,0,'f7ce973f35bae41edeccfb3b9e201103',27,''),(339,2,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.32.png','f144ba0fbdec0447f39f03e4a8939d44',337,'photo.32.png',4,3,462,1678457885,1678457885,0,0,'0c99f45b9d8e432f5237ab047803f2fb',27,''),(340,5,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,9760893,1678956998,1678956925,0,0,'6412d9c60e176',23,''),(341,5,'cache','0fea6a13c52b4d4725368f24b045ca84',340,'cache',2,1,0,1678956925,1678956925,0,0,'6412d97d47e70',31,''),(342,5,'files','45b963397aa40d4a0063e0d85e4fe7a1',340,'files',2,1,9760893,1678956998,1678956998,0,0,'6412d9c60e176',31,''),(343,5,'files/bbw-logo.png','7fb0f51046121e4c805f9aeaa14b3093',342,'bbw-logo.png',4,3,6878,1678956925,1678956925,0,0,'d030f135d10da9754adecdd230cd68cb',27,''),(344,5,'files/Photos','d01bb67e7b71dd49fd06bad922f521c9',342,'Photos',2,1,2360011,1678956926,1678956925,0,0,'6412d97e03380',31,''),(345,5,'files/Photos/Coast.jpg','a6fe87299d78b207e9b7aba0f0cb8a0a',344,'Coast.jpg',5,3,819766,1678956925,1678956925,0,0,'9be4aa90b8a3a954bdfef483d1b2efda',27,''),(346,5,'files/Photos/Hummingbird.jpg','e077463269c404ae0f6f8ea7f2d7a326',344,'Hummingbird.jpg',5,3,585219,1678956925,1678956925,0,0,'c62a3d14746c4e33456cb52897b85f1f',27,''),(347,5,'files/Photos/Nut.jpg','aa8daeb975e1d39412954fd5cd41adb4',344,'Nut.jpg',5,3,955026,1678956926,1678956926,0,0,'15754b0cf56425ee523406a9699eda0b',27,''),(348,5,'files/Nextcloud.mp4','77a79c09b93e57cba23c11eb0e6048a6',342,'Nextcloud.mp4',7,6,462413,1678956926,1678956926,0,0,'aa8f6755a83b040e05119156b3a39faa',27,''),(349,5,'files/Nextcloud Manual.pdf','2bc58a43566a8edde804a4a97a9c7469',342,'Nextcloud Manual.pdf',9,8,4540242,1678956926,1678956926,0,0,'7e1bc0ad65ea057cea6cb4f54e9d74a4',27,''),(350,5,'files/Documents','0ad78ba05b6961d92f7970b2b3922eca',342,'Documents',2,1,78496,1678956926,1678956926,0,0,'6412d97ec4b0e',31,''),(351,5,'files/Documents/About.odt','b2ee7d56df9f34a0195d4b611376e885',350,'About.odt',10,8,77422,1678956926,1678956926,0,0,'d3274d1e8b2f2b6ad76b2a7d9376f172',27,''),(352,5,'files/Documents/About.txt','9da7b739e7a65d74793b2881da521169',350,'About.txt',12,11,1074,1678956926,1678956926,0,0,'091156346d6abf56a46da91345a1a334',27,''),(353,2,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.32.png','92509fe245c48ba6da7d02a8d6b041fa',177,'avatar.32.png',4,3,205,1678956927,1678956927,0,0,'9b477c10177e72ef4259f672575bd675',27,''),(354,2,'appdata_ocp2ck7yo4uj/preview/343','c173e600f50af55367d655c4b562ad62',18,'343',2,1,8817,1678956928,1678956928,0,0,'6412d9802992e',31,''),(355,2,'appdata_ocp2ck7yo4uj/preview/343/193-96-max.png','80cbfcbdf7d4e4f6479db76cd03b7f53',354,'193-96-max.png',4,3,6148,1678956928,1678956928,0,0,'3cea4c5b1b3136be0dca594292c6d6d5',27,''),(356,2,'appdata_ocp2ck7yo4uj/preview/343/64-64-crop.png','cf421d8bcfb182c5e22efabc2ca3c89e',354,'64-64-crop.png',4,3,2669,1678956928,1678956928,0,0,'d76c63e49685a85e0da58b033ab6129f',27,''),(357,5,'files/Subnetting Table.xlsx','588e4af1f3c4fbd43f0d53235e1f7159',342,'Subnetting Table.xlsx',21,8,11500,1678956975,1678956975,0,0,'1d9eb30271f8ccce83ed21af5fbecd7e',27,''),(358,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-spreadsheet.svg','254c6deb7b6882a179b786c4aadf5747',304,'icon-core-filetypes_x-office-spreadsheet.svg',17,3,327,1678956975,1678956975,0,0,'894d7b3114004e1a15a6f59e98513cda',27,''),(359,5,'files/05_subnetting.pdf','57747b9630ad46da935fd8dd69cd80cb',342,'05_subnetting.pdf',9,8,210523,1678956996,1678956996,0,0,'a0849c064b57128e9227bed9ab021834',27,''),(360,5,'files/Subnetting (Post-Routing).pkt','c8ebce43c09b83c4bfba3c02d315d540',342,'Subnetting (Post-Routing).pkt',14,8,68687,1678956996,1678956996,0,0,'293da1e9fe3d66a189f7d13399fe4184',27,''),(361,5,'files/Subnetting.docx','d04d3a524299087873a0f390ae4ccccd',342,'Subnetting.docx',18,8,1270172,1678956997,1678956997,0,0,'e76068ffb37ef25aae98adc7f28264b4',27,''),(362,2,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_file.svg','501c0881d7a4d79f1d7f7ed3c42e1b57',304,'icon-core-filetypes_file.svg',17,3,228,1678956997,1678956997,0,0,'6da28f891acb5e8105408c22c193f4b5',27,''),(363,5,'files/Subnetting.pdf','25b7737e814d7e898606c1fb48220cbf',342,'Subnetting.pdf',9,8,683310,1678956997,1678956997,0,0,'c8bb25b9c09127dc8c9d880bbd4db2a1',27,''),(364,5,'files/Subnetting.pkt','9680faa84e0ac8e8ed90d59fc7934060',342,'Subnetting.pkt',14,8,68661,1678956998,1678956998,0,0,'ad1acca5f9740ddc04426eedc011eebb',27,''),(365,8,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,37697316,1681985165,1681984653,0,0,'64410e8da0250',23,''),(366,8,'appdata_oc5ev4nda8xn','aadf862f9d9aa9893360280d6b549eb9',365,'appdata_oc5ev4nda8xn',2,1,36241,1682686203,1682686203,0,0,'64410c96bc171',31,''),(393,8,'files_external','c270928b685e7946199afdfb898d27ea',365,'files_external',2,1,261889,1681985164,1681983708,0,0,'64410e8da0250',31,''),(563,8,'appdata_oc5ev4nda8xn/js','18f5fbb7894cc66f1f93168d89e094de',366,'js',2,1,0,1681984662,1681984536,0,0,'64410c96bc171',31,''),(564,8,'appdata_oc5ev4nda8xn/js/core','3858020e95639eaf58aaeea02b3522f9',563,'core',2,1,0,1682686188,1682686188,0,0,'64410c95e31ec',31,''),(566,8,'appdata_oc5ev4nda8xn/preview','148964b704a264ffd6f55b095ee03766',366,'preview',2,1,36241,1681984546,1681984545,0,0,'64410c2211108',31,''),(569,8,'appdata_oc5ev4nda8xn/avatar','24c2918bc2d8caf49c791dec052908d3',366,'avatar',2,1,0,1682686198,1682686198,0,0,'64410c156ae31',31,''),(570,8,'appdata_oc5ev4nda8xn/avatar/admin','0bf14e48db6f85f60ec1c27a930aa77d',569,'admin',2,1,0,1681984533,1681984533,0,0,'64410c156604d',31,''),(574,8,'appdata_oc5ev4nda8xn/js/notifications','b45656d799e2f661691010e2183ac950',563,'notifications',2,1,0,1681984662,1681984662,0,0,'64410c968ec7f',31,''),(578,8,'appdata_oc5ev4nda8xn/js/files','15379d7a281e64810b318b92ab85412b',563,'files',2,1,0,1682686230,1682686230,0,0,'64410c96060d9',31,''),(582,8,'appdata_oc5ev4nda8xn/js/activity','dd3bdc73fc335ef7833b57742b2d69ab',563,'activity',2,1,0,1682686195,1682686195,0,0,'64410c958af63',31,''),(586,8,'appdata_oc5ev4nda8xn/js/comments','0f1fa1f621f4eca369f6ddf0a4b29344',563,'comments',2,1,0,1681984661,1681984661,0,0,'64410c95a25ad',31,''),(590,8,'appdata_oc5ev4nda8xn/js/files_sharing','c826960e92f79ff8c857f9c21b9e4817',563,'files_sharing',2,1,0,1681984662,1681984662,0,0,'64410c961ca9d',31,''),(594,8,'appdata_oc5ev4nda8xn/js/files_texteditor','560bf8dd6f0f64ee8a0f0f283677f9b5',563,'files_texteditor',2,1,0,1681984662,1681984662,0,0,'64410c96369da',31,''),(598,8,'appdata_oc5ev4nda8xn/js/files_versions','1be92f2992ecd0ec2aed95df6114d5db',563,'files_versions',2,1,0,1681984662,1681984662,0,0,'64410c9652375',31,''),(602,8,'appdata_oc5ev4nda8xn/js/gallery','59662866997a59ea5291db86640e4c34',563,'gallery',2,1,0,1681984662,1681984662,0,0,'64410c966e2fb',31,''),(609,8,'appdata_oc5ev4nda8xn/js/systemtags','59f965b05051e27b09d2cec1601af10d',563,'systemtags',2,1,0,1681984662,1681984662,0,0,'64410c96bc171',31,''),(613,8,'appdata_oc5ev4nda8xn/css','c550d16cdd4a96489355578284c2274b',366,'css',2,1,0,1682686234,1682686234,0,0,'64410c95709f2',31,''),(614,8,'appdata_oc5ev4nda8xn/css/core','df1a093f82a2be03b117935cb372861e',613,'core',2,1,0,1682686196,1682686196,0,0,'64410c94ef1f6',31,''),(627,8,'appdata_oc5ev4nda8xn/css/files','cde22d9c186456e3caa58ce82acd0a4e',613,'files',2,1,0,1682686230,1682686230,0,0,'64410c9513d3d',31,''),(631,8,'appdata_oc5ev4nda8xn/css/files_trashbin','6b23a907a55f3c8db1ab0012fc29c857',613,'files_trashbin',2,1,0,1681984661,1681984661,0,0,'64410c9559bad',31,''),(635,8,'appdata_oc5ev4nda8xn/css/comments','c8ccaac21c271ff5547850578c34d5cc',613,'comments',2,1,0,1681984660,1681984660,0,0,'64410c946d289',31,''),(639,8,'appdata_oc5ev4nda8xn/css/files_sharing','a61ece2cad2b35fc6d7ce6acf3f50966',613,'files_sharing',2,1,0,1682686230,1682686230,0,0,'64410c952b859',31,''),(643,8,'appdata_oc5ev4nda8xn/css/files_texteditor','457a831184b979282e4f1d9ef5dc5ccf',613,'files_texteditor',2,1,0,1681984661,1681984661,0,0,'64410c95420a1',31,''),(650,8,'appdata_oc5ev4nda8xn/css/theming','36b8448a48f37054e0530d24fae7a1bc',613,'theming',2,1,0,1682686188,1682686188,0,0,'64410c95709f2',31,''),(654,8,'appdata_oc5ev4nda8xn/preview/3','22b0c9c784c1a95cf68b7d2037c817a2',566,'3',2,1,9562,1681984545,1681984545,0,0,'64410c2151838',31,''),(655,8,'appdata_oc5ev4nda8xn/preview/152','1f618c7eb33c79de17c0125e17fc74f2',566,'152',2,1,26679,1681984546,1681984545,0,0,'64410c2211108',31,''),(656,8,'appdata_oc5ev4nda8xn/preview/3/193-96-max.png','f97fffa88b6c93ff53b6b6a2e45b84fe',654,'193-96-max.png',4,3,6127,1681984545,1681984545,0,0,'be3e2f4f8a1bf17c485dad6e65f68670',27,''),(657,8,'appdata_oc5ev4nda8xn/preview/152/4096-4096-max.png','2408f5928b06787d97c16918be4b57de',655,'4096-4096-max.png',4,3,26057,1681984545,1681984545,0,0,'72a9ae03fc796b38cf59bc5aa1acb276',27,''),(658,8,'appdata_oc5ev4nda8xn/preview/3/64-64-crop.png','0be2261332bf87e1c0a671ea46df2ab0',654,'64-64-crop.png',4,3,3435,1681984545,1681984545,0,0,'8c0c6fa4f1f1bf83798c21d221144f24',27,''),(659,8,'appdata_oc5ev4nda8xn/preview/152/64-64-crop.png','334d38e558f19f719b5e409d380ecad8',655,'64-64-crop.png',4,3,622,1681984546,1681984546,0,0,'d47c24bd5ce69448d3a88f74e4a1805e',27,''),(660,8,'appdata_oc5ev4nda8xn/css/icons','068260a98eeb3f3f055cd1220707c9b0',613,'icons',2,1,0,1682686196,1682686196,0,0,'64410c8b42c66',31,''),(661,8,'appdata_oc5ev4nda8xn/appstore','798bc0fb714493dc4858ed7f58af4505',366,'appstore',2,1,0,1681984659,1681984659,0,0,'64410c9311097',31,''),(662,8,'appdata_oc5ev4nda8xn/appstore/apps.json','8073e805b6a8e4de305b58a3b9229c96',661,'apps.json',20,8,1935481,1681985299,1681985299,0,0,'7c4b8d5ceee5ed47f25d2eef56d4421d',27,''),(663,8,'appdata_oc5ev4nda8xn/dav-photocache','090cf76cb5e4c0e9bed207e1453ac995',366,'dav-photocache',2,1,0,1682686209,1682686209,0,0,'64410d005a78a',31,''),(664,8,'appdata_oc5ev4nda8xn/text','38928fe0dfdf43b1b68c5f06e82ea65e',366,'text',2,1,0,1681984938,1681984938,0,0,'64410daa3a952',31,''),(665,8,'appdata_oc5ev4nda8xn/text/documents','77de8713833008a9ef8ebe86d80736e1',664,'documents',2,1,0,1681984938,1681984938,0,0,'64410daa4253a',31,''),(666,8,'index.html','eacf331f0ffc35d4b482f1d15a887d3b',365,'index.html',29,11,0,1681983708,1681983708,0,0,'cca66da160d074a50b7d9f1fb19b78af',27,''),(667,8,'admin','21232f297a57a5a743894a0e4a801fc3',365,'admin',2,1,9905229,1681985164,1681983708,0,0,'64410e8da0250',31,''),(668,8,'vok','96a752ab58e6a44badcdfc33ae8acb0a',365,'vok',2,1,8717771,1681985164,1681983708,0,0,'64410e8da0250',31,''),(669,8,'appdata_ocp2ck7yo4uj','511e1603a617f66d86c36ae02930e2d1',365,'appdata_ocp2ck7yo4uj',2,1,8556983,1681985165,1681983708,0,0,'64410e8da0250',31,''),(670,8,'testuser2','58dd024d49e1d1b83a5d307f09f32734',365,'testuser2',2,1,9760893,1681985165,1681983708,0,0,'64410e8da0250',31,''),(671,8,'.ocdata','a840ac417b1f143f29a22c7261756dad',365,'.ocdata',14,8,0,1681984526,1681984526,0,0,'4781b6bebd8270241f6500efd4a6028d',27,''),(672,8,'nextcloud.log','edbc35c4c40bdf29f0494207fbbf5aeb',365,'nextcloud.log',14,8,458310,1681985137,1681985137,0,0,'9ebe1ab583f3ce14095b0ea51797fa00',27,''),(673,8,'admin/files_versions','6450f18c15be102af37ec1718d428e0b',667,'files_versions',2,1,1,1681985164,1681983708,0,0,'64410e8da0250',31,''),(674,8,'admin/files_trashbin','a72459d4cc20d1f997eaa57f8044fc12',667,'files_trashbin',2,1,1,1681985164,1681983708,0,0,'64410e8da0250',31,''),(675,8,'admin/cache','a5a866a7252a49dd4bfbe22664d16245',667,'cache',2,1,0,1681983708,1681983708,0,0,'64410e8c55456',31,''),(676,8,'admin/files','16cdbbebf30085594dd7a9532a5ecc5e',667,'files',2,1,9905227,1681985164,1681983708,0,0,'64410e8da0250',31,''),(677,8,'admin/files_versions/Testfile.txt.v1678352135','a4047ac4bef4485caddb7b4c629f06c0',673,'Testfile.txt.v1678352135',12,11,1,1681983708,1681983708,0,0,'12c241a4c224831c8b16dc5b55705b7e',27,''),(678,8,'admin/files_trashbin/keys','97944b60ec9948f713738985efccd2be',674,'keys',2,1,0,1681983708,1681983708,0,0,'64410e8c5c03e',31,''),(679,8,'admin/files_trashbin/versions','d47d1b0d8f6afecc12dc8e02fc41655a',674,'versions',2,1,0,1681983708,1681983708,0,0,'64410e8c5c083',31,''),(680,8,'admin/files_trashbin/files','5ff61c230393becfa6af205f09cba358',674,'files',2,1,1,1681985164,1681983708,0,0,'64410e8da0250',31,''),(681,8,'admin/files_trashbin/files/deletedfile.txt.d1678356431','193fffb1c3bb7efee66df95f1588b0e3',680,'deletedfile.txt.d1678356431',14,8,1,1681983708,1681983708,0,0,'7792300717cda2683601ac894a8ba3e7',27,''),(682,8,'admin/files/Nextcloud Manual.pdf','4478603ed9f1b460437fca367632a1b1',676,'Nextcloud Manual.pdf',9,8,4540242,1681983708,1681983708,0,0,'60888a3bcfc003d7dd3074eed878a0ed',27,''),(683,8,'admin/files/Documents','f3228bb96d44c0d5c5de7928d373cb0c',676,'Documents',2,1,78496,1681985164,1681983708,0,0,'64410e8da0250',31,''),(684,8,'admin/files/m129_bandbreitenberechnung.pdf','47b03ea181deaa896f36a89a6a8ee230',676,'m129_bandbreitenberechnung.pdf',9,8,161663,1681983708,1681983708,0,0,'dd0dc7c991e3b74b8cfbf7bb1dbbd93f',27,''),(685,8,'admin/files/Test','8fc6b96d5b26788aaa1b0b5fcdfa5536',676,'Test',2,1,2190811,1681985164,1681983708,0,0,'64410e8da0250',31,''),(686,8,'admin/files/Photos','3fe593e31bd535ec607aa5939d770823',676,'Photos',2,1,2360011,1681985164,1681983708,0,0,'64410e8da0250',31,''),(687,8,'admin/files/Testfile.txt','f0ef3770415318c0a33eb2301b55c749',676,'Testfile.txt',12,11,9,1681983708,1681983708,0,0,'31ea3518f3ac30696161621a8126b334',27,''),(688,8,'admin/files/bbw-logo.png','349c3cc41505fb3328abf23605130d3e',676,'bbw-logo.png',4,3,6878,1681983708,1681983708,0,0,'062c198ff07f5d6dc6ec85daf1a8b017',27,''),(689,8,'admin/files/m129_bandbreitenberechnung-lsg.pdf','a16619b4f3724076881bd2f655095ba8',676,'m129_bandbreitenberechnung-lsg.pdf',9,8,104704,1681983708,1681983708,0,0,'8fd92604bd8faea76f85099300b113a6',27,''),(690,8,'admin/files/Nextcloud.mp4','b41d2ad31f10c1f04a8c2126abe8608a',676,'Nextcloud.mp4',7,6,462413,1681983708,1681983708,0,0,'bc666ce1af1827cd8df4d9ff22423dc7',27,''),(691,8,'admin/files/Documents/About.odt','91a5b1e7629b6dfe7dd53750bf64885c',683,'About.odt',10,8,77422,1681983708,1681983708,0,0,'e49ce22f135d99dca6b20c0687ce88df',27,''),(692,8,'admin/files/Documents/About.txt','983746326dcab80a19bf1ab9d2f4d83f',683,'About.txt',12,11,1074,1681983708,1681983708,0,0,'6e6a7740d28bab91848085ea0b8b07f5',27,''),(693,8,'admin/files/Test/docker-compose.yml','57a7170bb5b2c9a05d5cdca7d7e7630d',685,'docker-compose.yml',19,8,475,1681983708,1681983708,0,0,'501fa86d2c543dac4264576bb27968a7',27,''),(694,8,'admin/files/Test/10.1_ContainerVirtualisierung.pdf','dfb08fb68216df6d58764ea033b2878e',685,'10.1_ContainerVirtualisierung.pdf',9,8,110769,1681983708,1681983708,0,0,'d01c2612064118dcc7806c7a1beacc62',27,''),(695,8,'admin/files/Test/10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf','26beedcee2fb3159b9fb26252d7702f1',685,'10.2_Was_ist_Containervirtualisierung_Konzepte_und_Herkunft_erklart.pdf',9,8,307530,1681983708,1681983708,0,0,'faeec77087b4440941074288c1cd7e9a',27,''),(696,8,'admin/files/Test/10.4_CV_Versus_VM.docx','581bffb29cbf2d8ec526a5283d667b08',685,'10.4_CV_Versus_VM.docx',18,8,47076,1681983708,1681983708,0,0,'509ec99172ff446998b8cad41843126c',27,''),(697,8,'admin/files/Test/10.5_Docker Einstieg - CLI-Befehle.pdf','65cafd9f976e38d62f45bc78e98093bd',685,'10.5_Docker Einstieg - CLI-Befehle.pdf',9,8,458836,1681983708,1681983708,0,0,'5a4a8b7abf4a8ab5d9ad6aa0e5ec9bc4',27,''),(698,8,'admin/files/Test/10.3_Container_oder_VM_Pros_und_Kontras.pdf','1226f1e3a9db339871a8590e236a9409',685,'10.3_Container_oder_VM_Pros_und_Kontras.pdf',9,8,1266125,1681983708,1681983708,0,0,'b55f04d269001207b1a84aa490438d5a',27,''),(699,8,'admin/files/Photos/Nut.jpg','4bce7b916362e3d9134f74339bced071',686,'Nut.jpg',5,3,955026,1681983708,1681983708,0,0,'a598c8ce15d0bd383abf169a2e1e37c9',27,''),(700,8,'admin/files/Photos/Coast.jpg','5f502ce9e69ad43ae074def8c74313f9',686,'Coast.jpg',5,3,819766,1681983708,1681983708,0,0,'083873a3f99baebaff1945f87c430f91',27,''),(701,8,'admin/files/Photos/Hummingbird.jpg','1db9c9307506296c2d9f31b8db14ea5f',686,'Hummingbird.jpg',5,3,585219,1681983708,1681983708,0,0,'22aec2fe406842539326ad0ff493806c',27,''),(702,8,'vok/cache','8063c4cb8080d99f737a81bbbdc5fd67',668,'cache',2,1,0,1681983708,1681983708,0,0,'64410e8c7a5dc',31,''),(703,8,'vok/files','f391312ad3c7157e5cbddc9ac7989765',668,'files',2,1,8717771,1681985164,1681983708,0,0,'64410e8da0250',31,''),(704,8,'vok/files/Nextcloud Manual.pdf','7e0d11a6277bc6c736f78ad221902793',703,'Nextcloud Manual.pdf',9,8,4540242,1681983708,1681983708,0,0,'a7cc85fc300caf961dac86b2f9d6aa81',27,''),(705,8,'vok/files/Documents','eb2ffab66210d758ee687ade3c4bb0af',703,'Documents',2,1,78496,1681985164,1681983708,0,0,'64410e8da0250',31,''),(706,8,'vok/files/testfile2.txt','251a6dc70cbb2af8a82aa36a1951212d',703,'testfile2.txt',12,11,1,1681983708,1681983708,0,0,'3d6f326b3837892e6f3dbcf18d85f21a',27,''),(707,8,'vok/files/!!!IMPORTANT!!!','5bc9bd2d0eb2886f2bff6af1914f2c5f',703,'!!!IMPORTANT!!!',2,1,1269729,1681985164,1681983708,0,0,'64410e8da0250',31,''),(708,8,'vok/files/testfile.txt','44ad54883f2cf6d0956f6201cf11bf4c',703,'testfile.txt',12,11,1,1681983708,1681983708,0,0,'e0da4f3a53744a8bbc0d19696fcebc74',27,''),(709,8,'vok/files/Photos','a8534ea801b1a8398de9758c147f7d8f',703,'Photos',2,1,2360011,1681985164,1681983708,0,0,'64410e8da0250',31,''),(710,8,'vok/files/bbw-logo.png','101efd34f7a3d75e41457c52599006ae',703,'bbw-logo.png',4,3,6878,1681983708,1681983708,0,0,'9c60d75d71db724501f4698ed808528e',27,''),(711,8,'vok/files/Nextcloud.mp4','347927c51f31cb1429de7123a3ba3c8f',703,'Nextcloud.mp4',7,6,462413,1681983708,1681983708,0,0,'847320cf3f47a981e4135938a82c4785',27,''),(712,8,'vok/files/Documents/About.odt','55a0d26dd7c949ba80d3cfe5837f5a4c',705,'About.odt',10,8,77422,1681983708,1681983708,0,0,'e031ef879f96736a62711804eaee16d5',27,''),(713,8,'vok/files/Documents/About.txt','07429a32dcd512d72abfd29e2c03cd48',705,'About.txt',12,11,1074,1681983708,1681983708,0,0,'984e0b92fd4bbfece1a0da9f39deb308',27,''),(714,8,'vok/files/!!!IMPORTANT!!!/WIN_20211028_15_17_04_Pro.jpg','dcf2abded1e0192af7d6e89d7819db52',707,'WIN_20211028_15_17_04_Pro.jpg',5,3,1269729,1681983708,1681983708,0,0,'5538c6bac37e0053e00429d4d6df965d',27,''),(715,8,'vok/files/Photos/Nut.jpg','422bd4f2a46ebeb0b74e7136c587e188',709,'Nut.jpg',5,3,955026,1681983708,1681983708,0,0,'ced358ce14f484336b9960e851a21cc1',27,''),(716,8,'vok/files/Photos/Coast.jpg','3c962a5e6e6a560127f7ccbb43caf47f',709,'Coast.jpg',5,3,819766,1681983708,1681983708,0,0,'08971bc8bd8a06e81e3c05d054486cfe',27,''),(717,8,'vok/files/Photos/Hummingbird.jpg','743080a2b293f84a6f9f4b419b5b392e',709,'Hummingbird.jpg',5,3,585219,1681983708,1681983708,0,0,'90018d645a8872e4e98b733458926ed0',27,''),(718,8,'files_external/rootcerts.crt','96f0eb1b0b28e846975c98dbeb5e734d',393,'rootcerts.crt',14,8,261889,1681983708,1681983708,0,0,'d2865039249584a0b440c9f93166f32e',27,''),(719,8,'appdata_ocp2ck7yo4uj/avatar','91d888474bf0e119311f6ea5fc91c88b',669,'avatar',2,1,65286,1681985164,1681983708,0,0,'64410e8da0250',31,''),(720,8,'appdata_ocp2ck7yo4uj/theming','1a917cff90046d01d35343c92b270d5b',669,'theming',2,1,2705,1681985164,1681983708,0,0,'64410e8da0250',31,''),(721,8,'appdata_ocp2ck7yo4uj/css','c7ebe2c99c887f1959d7328bdce2dcb8',669,'css',2,1,190031,1681985164,1681983708,0,0,'64410e8da0250',31,''),(722,8,'appdata_ocp2ck7yo4uj/appstore','2345d9a5592e7a2e8d79bf2223c8e348',669,'appstore',2,1,1087363,1681985164,1681983708,0,0,'64410e8da0250',31,''),(723,8,'appdata_ocp2ck7yo4uj/identityproof','2d4281eebef45968390785218dc56c63',669,'identityproof',2,1,4021,1681985164,1681983708,0,0,'64410e8da0250',31,''),(724,8,'appdata_ocp2ck7yo4uj/js','493274afae6aa7897fd145345985624b',669,'js',2,1,2662994,1681985165,1681983708,0,0,'64410e8da0250',31,''),(725,8,'appdata_ocp2ck7yo4uj/preview','b9b868100017c68bd74bd35b4838053c',669,'preview',2,1,4492361,1681985165,1681983708,0,0,'64410e8da0250',31,''),(726,8,'appdata_ocp2ck7yo4uj/dav-photocache','d30c65c6c0c584cdd932ec953747fc71',669,'dav-photocache',2,1,52222,1681985165,1681983708,0,0,'64410e8da0250',31,''),(727,8,'appdata_ocp2ck7yo4uj/avatar/admin','1fb69ff79d77a83aba1b7ef55d030cbb',719,'admin',2,1,17066,1681985164,1681983708,0,0,'64410e8da0250',31,''),(728,8,'appdata_ocp2ck7yo4uj/avatar/testuser','35512c74e396de5a72f211830497ab48',719,'testuser',2,1,5695,1681985164,1681983708,0,0,'64410e8da0250',31,''),(729,8,'appdata_ocp2ck7yo4uj/avatar/disableduser','6830f17f340ba5039fbbae652584970f',719,'disableduser',2,1,13373,1681985164,1681983708,0,0,'64410e8da0250',31,''),(730,8,'appdata_ocp2ck7yo4uj/avatar/vok','0a0486437d7af5fe0661d5870080b6fe',719,'vok',2,1,17536,1681985164,1681983708,0,0,'64410e8da0250',31,''),(731,8,'appdata_ocp2ck7yo4uj/avatar/testuser3','d086ba37569c9161e11461068373ee41',719,'testuser3',2,1,5706,1681985164,1681983708,0,0,'64410e8da0250',31,''),(732,8,'appdata_ocp2ck7yo4uj/avatar/testuser2','638d7e7ca3dce9c416dada796c49c1ed',719,'testuser2',2,1,5910,1681985164,1681983708,0,0,'64410e8da0250',31,''),(733,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.40.png','4519fa97dd131e66e97ae8eb7202e4d6',727,'avatar.40.png',4,3,1464,1681983708,1681983708,0,0,'968d4b42d15f425c05c4ebfddfbb5074',27,''),(734,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.182.png','392718da6f4eea3f15b499074c74f5ea',727,'avatar.182.png',4,3,10549,1681983708,1681983708,0,0,'db6bcc09b23e48d4355e8294814e95b6',27,''),(735,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.32.png','f091d9d8c6eb1c6f6f27a4b1b4a29b70',727,'avatar.32.png',4,3,1095,1681983708,1681983708,0,0,'ec384187a1a00bf1525aac903d750e85',27,''),(736,8,'appdata_ocp2ck7yo4uj/avatar/admin/avatar.png','d8dd45159872dc1cb96d8f1d5e3764d6',727,'avatar.png',4,3,3958,1681983708,1681983708,0,0,'51120603c7b76879505b6afcc73afa6e',27,''),(737,8,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.40.png','1165c2f5e3965d783a95654d53e4147c',728,'avatar.40.png',4,3,218,1681983708,1681983708,0,0,'b7f5be06f34512f397a878716b6eb790',27,''),(738,8,'appdata_ocp2ck7yo4uj/avatar/testuser/avatar.png','344c673f2655b9feace05731093b597a',728,'avatar.png',4,3,5477,1681983708,1681983708,0,0,'155025b18df391d13e3291749a527ef4',27,''),(739,8,'appdata_ocp2ck7yo4uj/avatar/testuser/generated','62fd23db4be5617c3426ec24aa711f91',728,'generated',14,8,0,1681983708,1681983708,0,0,'5a86f820d357c7e6b938dda9e1a38bf7',27,''),(740,8,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.40.png','d6e11c84318f70f5e74c5d1ec67c1a61',729,'avatar.40.png',4,3,449,1681983708,1681983708,0,0,'16a9c6c3e9605a990b2d9f0a82b7eab8',27,''),(741,8,'appdata_ocp2ck7yo4uj/avatar/disableduser/avatar.png','ecb540d8c6611cb24a2fe7a69867fb31',729,'avatar.png',4,3,12924,1681983708,1681983708,0,0,'fe47ecd9cce1e9395fbe4dacf7876206',27,''),(742,8,'appdata_ocp2ck7yo4uj/avatar/disableduser/generated','0b4666d8647679ba143b11759a36c07f',729,'generated',14,8,0,1681983708,1681983708,0,0,'20dfae903309f107ca93d2a7b0743814',27,''),(743,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.40.png','d709c254c65347b092c445a6e5dbaaa3',730,'avatar.40.png',4,3,458,1681983708,1681983708,0,0,'27a615b50d51b060bb05e8a09590066a',27,''),(744,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.182.png','9bee4d8d9dc2e2f87b9f92344499726a',730,'avatar.182.png',4,3,2015,1681983708,1681983708,0,0,'1d03806154dcbaa8c1e9e4268448ebf2',27,''),(745,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.32.png','19097930e38bfc9c4f8bd42e72127c39',730,'avatar.32.png',4,3,391,1681983708,1681983708,0,0,'a21d90139198057211552e0c2af431d5',27,''),(746,8,'appdata_ocp2ck7yo4uj/avatar/vok/avatar.png','1c5e17f3562bf0026ff2433bc8359af1',730,'avatar.png',4,3,14672,1681983708,1681983708,0,0,'e919f3fdcea1879b62f84f8a5c77cee0',27,''),(747,8,'appdata_ocp2ck7yo4uj/avatar/vok/generated','92d0578812637bcd106a74c51bd2c0cd',730,'generated',14,8,0,1681983708,1681983708,0,0,'1a59819f2e510ad13cc1411b9c774e54',27,''),(748,8,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.40.png','ccce2d61522438868b4e0d107682f425',731,'avatar.40.png',4,3,219,1681983708,1681983708,0,0,'a54df512ae8ebafdebd9380401d7eaf1',27,''),(749,8,'appdata_ocp2ck7yo4uj/avatar/testuser3/avatar.png','05d1d04fde756a5ccfe6c591210c7cf1',731,'avatar.png',4,3,5487,1681983708,1681983708,0,0,'6a5ca215471111aa2fda785d257c62e0',27,''),(750,8,'appdata_ocp2ck7yo4uj/avatar/testuser3/generated','c68c06a199b0826020e178163f42cf75',731,'generated',14,8,0,1681983708,1681983708,0,0,'1e89787f0188098655c04f2218972182',27,''),(751,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.40.png','8a34e8e5a8ce95f5ead5eddf1e5abfe2',732,'avatar.40.png',4,3,218,1681983708,1681983708,0,0,'60b93b1a35c2925ae48daaf87727a156',27,''),(752,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.32.png','92509fe245c48ba6da7d02a8d6b041fa',732,'avatar.32.png',4,3,205,1681983708,1681983708,0,0,'5542dec688b21a0d7665f98687f31009',27,''),(753,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/avatar.png','a3783e8054fcad0ccf5744275f000992',732,'avatar.png',4,3,5487,1681983708,1681983708,0,0,'c95fa0e4104a47ff5bcf6f3d422f08bf',27,''),(754,8,'appdata_ocp2ck7yo4uj/avatar/testuser2/generated','27d667766eef561885e247be37aeaa76',732,'generated',14,8,0,1681983708,1681983708,0,0,'6a679867e67a80709a9de78653b250ac',27,''),(755,8,'appdata_ocp2ck7yo4uj/theming/1','d20433a493075cc0414e07c5c31de2bf',720,'1',2,1,2705,1681985164,1681983708,0,0,'64410e8da0250',31,''),(756,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-spreadsheet.svg','254c6deb7b6882a179b786c4aadf5747',755,'icon-core-filetypes_x-office-spreadsheet.svg',17,3,327,1681983708,1681983708,0,0,'c50f50df610afbd348a8bd937f38f031',27,''),(757,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_folder.svg','9c31a0be115de8ea2d86812cb7491591',755,'icon-core-filetypes_folder.svg',17,3,255,1681983708,1681983708,0,0,'eec4ee5618a6ed7c7a003d9a9f6c5f4c',27,''),(758,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_text.svg','2aaf405bd7368b937bb2172e801b9be5',755,'icon-core-filetypes_text.svg',17,3,295,1681983708,1681983708,0,0,'8fb6bdf9c1f2d757a52007e426fdc028',27,''),(759,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_application-pdf.svg','1c363d4f97b09c9974eabdf1b1e79195',755,'icon-core-filetypes_application-pdf.svg',17,3,676,1681983708,1681983708,0,0,'466511c749b9da0f990c28ffc153c84e',27,''),(760,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_video.svg','b2db7a7e6ab13f9ea09a02bd98fa0039',755,'icon-core-filetypes_video.svg',17,3,277,1681983708,1681983708,0,0,'f97b81b8c55ab977386cbd2ceb08c036',27,''),(761,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_x-office-document.svg','d5896aaefcf6842432895d5fa19c41b1',755,'icon-core-filetypes_x-office-document.svg',17,3,295,1681983708,1681983708,0,0,'ea92ef468ae2caa83bad87704e59d950',27,''),(762,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_image.svg','ab48ac8e035ddb84f1d34cb43e486f7f',755,'icon-core-filetypes_image.svg',17,3,352,1681983708,1681983708,0,0,'0155978b34f0457d3f08b4720f71bb42',27,''),(763,8,'appdata_ocp2ck7yo4uj/theming/1/icon-core-filetypes_file.svg','501c0881d7a4d79f1d7f7ed3c42e1b57',755,'icon-core-filetypes_file.svg',17,3,228,1681983708,1681983708,0,0,'39bb05305cc59dda37bb20d93737aa0e',27,''),(764,8,'appdata_ocp2ck7yo4uj/css/theming','b7c0b45ef82251f3f095726b639e914b',721,'theming',2,1,1163,1681985164,1681983708,0,0,'64410e8da0250',31,''),(765,8,'appdata_ocp2ck7yo4uj/css/settings','2fd09612cf55a6d85d4729e9fa0f7a9e',721,'settings',2,1,20100,1681985164,1681983708,0,0,'64410e8da0250',31,''),(766,8,'appdata_ocp2ck7yo4uj/css/comments','19602c40a64f0290173a3ebf1dfa08f6',721,'comments',2,1,1523,1681985164,1681983708,0,0,'64410e8da0250',31,''),(767,8,'appdata_ocp2ck7yo4uj/css/files_texteditor','4ebecdcef2c2d8caa84532be9b7f3701',721,'files_texteditor',2,1,5799,1681985164,1681983708,0,0,'64410e8da0250',31,''),(768,8,'appdata_ocp2ck7yo4uj/css/files_trashbin','5f32bdfb01ceaf60105bd7ab5ef25a2d',721,'files_trashbin',2,1,642,1681985164,1681983708,0,0,'64410e8da0250',31,''),(769,8,'appdata_ocp2ck7yo4uj/css/files','c22100d5f13d55aac69ac2c832874652',721,'files',2,1,27898,1681985164,1681983708,0,0,'64410e8da0250',31,''),(770,8,'appdata_ocp2ck7yo4uj/css/core','e5da66b8fc55196b7f2a80eba5b06ea6',721,'core',2,1,129494,1681985164,1681983708,0,0,'64410e8da0250',31,''),(771,8,'appdata_ocp2ck7yo4uj/css/files_sharing','80ca69a7ea85a44940833be82f45fe5e',721,'files_sharing',2,1,3412,1681985164,1681983708,0,0,'64410e8da0250',31,''),(772,8,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.gzip','b61b61c25ad60bb4846e5b819a0e4baa',764,'2bf5-dd39-theming.css.gzip',15,8,307,1681983708,1681983708,0,0,'84ac0f53aa788863f46b918f293bf33b',27,''),(773,8,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css','684553ec9bd093db235f659c57b15af8',764,'2bf5-dd39-theming.css',16,11,724,1681983708,1681983708,0,0,'4418a52945046db14d04da977682762f',27,''),(774,8,'appdata_ocp2ck7yo4uj/css/theming/2bf5-dd39-theming.css.deps','666a810bd01910cf9caa1734d6bbca52',764,'2bf5-dd39-theming.css.deps',14,8,132,1681983708,1681983708,0,0,'46a5d3586356df9a71c02aaa6e1c0e28',27,''),(775,8,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css','c32a464f00dea1e419cb2153dea8d226',765,'89a5-dd39-settings.css',16,11,15629,1681983708,1681983708,0,0,'d4d57f7d08a6e13c16c8078c3475fe46',27,''),(776,8,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.gzip','7509ce07ce4d63163a95c2d37570e5db',765,'89a5-dd39-settings.css.gzip',15,8,4343,1681983708,1681983708,0,0,'e035226adae353a53faa323013a80d72',27,''),(777,8,'appdata_ocp2ck7yo4uj/css/settings/89a5-dd39-settings.css.deps','70ca02cf7791d1bf0ab2ee6e6190a08d',765,'89a5-dd39-settings.css.deps',14,8,128,1681983708,1681983708,0,0,'b02f5d15152c347ca770639043d899b8',27,''),(778,8,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.gzip','4bcfbe4d23bea15b19cc1be6de2ab207',766,'1980-dd39-autocomplete.css.gzip',15,8,421,1681983708,1681983708,0,0,'80bd98bd354e06364722a1d374848f51',27,''),(779,8,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css','315c9a71ed6d9bfc55b67efa3639823c',766,'1980-dd39-autocomplete.css',16,11,964,1681983708,1681983708,0,0,'33d63a1e2a284aaee16a1c998eb85386',27,''),(780,8,'appdata_ocp2ck7yo4uj/css/comments/1980-dd39-autocomplete.css.deps','25caf287d9b6f3709e7fb0f6909daef3',766,'1980-dd39-autocomplete.css.deps',14,8,138,1681983708,1681983708,0,0,'ed234b35b6eb463c974073e977f13d79',27,''),(781,8,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.deps','bc43f6c94909d91c8c7b59e21fa67f61',767,'21f4-dd39-merged.css.deps',14,8,389,1681983708,1681983708,0,0,'0560f413137cfa8285eab9a6377467ea',27,''),(782,8,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css.gzip','ccb5610cf876f5e2261d71e2f0e41b28',767,'21f4-dd39-merged.css.gzip',15,8,1262,1681983708,1681983708,0,0,'d3bcf809f4c26238e078fbe72eee0304',27,''),(783,8,'appdata_ocp2ck7yo4uj/css/files_texteditor/21f4-dd39-merged.css','9e3066d1300a64bbe4c1bb0a1152a98e',767,'21f4-dd39-merged.css',16,11,4148,1681983708,1681983708,0,0,'a81e3885db0eb625961a194d1f5df8c5',27,''),(784,8,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.deps','ddc57b10ad9af48a3ed703e6db4d325d',768,'1980-dd39-trash.css.deps',14,8,137,1681983708,1681983708,0,0,'698e934ba7729511359b31041ddd94b2',27,''),(785,8,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css','cead350d6af9fb0d4b0acf5c57433d0a',768,'1980-dd39-trash.css',16,11,344,1681983708,1681983708,0,0,'52a966adec18b6f29454b8bb7d25e02e',27,''),(786,8,'appdata_ocp2ck7yo4uj/css/files_trashbin/1980-dd39-trash.css.gzip','cd58401fa37b93e6d8ece58f6793ad07',768,'1980-dd39-trash.css.gzip',15,8,161,1681983708,1681983708,0,0,'bb8aa99b2edc038451e0532dea21c766',27,''),(787,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css','a7455aea2749eb5babf9882432e43052',769,'4fda-dd39-merged.css',16,11,18493,1681983708,1681983708,0,0,'d6a36ab92195db37dc17821af519e185',27,''),(788,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.gzip','8f74774bd8232922b5c77877921908e9',769,'4fda-dd39-upload.css.gzip',15,8,1016,1681983708,1681983708,0,0,'16b91d542388d318d8b35d016e6c055b',27,''),(789,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.gzip','7417c522a6b92d18106617a0fe0cd93c',769,'4fda-dd39-merged.css.gzip',15,8,4220,1681983708,1681983708,0,0,'952fa27f4a8b68e69a71432355a5484c',27,''),(790,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css','8d9996c9ec1c3019ac4ab8ba503e4f1d',769,'4fda-dd39-upload.css',16,11,3643,1681983708,1681983708,0,0,'7c117134ce7e3ac84dbbe7d023031ecc',27,''),(791,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-upload.css.deps','4dc0211ca2cfd0dc15fa3b1c519d8443',769,'4fda-dd39-upload.css.deps',14,8,129,1681983708,1681983708,0,0,'cdea3a31df655a3116ba70835ae1577e',27,''),(792,8,'appdata_ocp2ck7yo4uj/css/files/4fda-dd39-merged.css.deps','014a08cbc68d833047d7ed9ef00582d5',769,'4fda-dd39-merged.css.deps',14,8,397,1681983708,1681983708,0,0,'a6e92ed35af52bab5edce2d223711820',27,''),(793,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css','72958c1e1ceabf47c101dc39f746f375',770,'89a5-dd39-share.css',16,11,2685,1681983708,1681983708,0,0,'163718f6a7a4665b7ce65c9015ee48b2',27,''),(794,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.gzip','ca3d590ed9b309e4099c628c9ba37bfc',770,'89a5-dd39-systemtags.css.gzip',15,8,385,1681983708,1681983708,0,0,'38bc64ddbe1b5030803d725d945dab9e',27,''),(795,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css.deps','54f3b5f5c223467ca699ffe87c8678d1',770,'89a5-dd39-systemtags.css.deps',14,8,126,1681983708,1681983708,0,0,'dd5eba060c138ac15f9850812c4dd87b',27,''),(796,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.deps','635838ae2dacee8a4eaacc0163b9ae97',770,'89a5-dd39-jquery-ui-fixes.css.deps',14,8,131,1681983708,1681983708,0,0,'66489741de15f934fb92b92fe739793a',27,''),(797,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css.gzip','31f45e6cbf6c6109a5c543594268b0f7',770,'89a5-dd39-jquery-ui-fixes.css.gzip',15,8,846,1681983708,1681983708,0,0,'ef1403bc7d8348ee793833217e5f3e98',27,''),(798,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css','78bff418cb5b8be898905e26105f439d',770,'89a5-dd39-server.css',16,11,100134,1681983708,1681983708,0,0,'2c290a5e421556c10aca5fcae08ece5c',27,''),(799,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.deps','f5564a3bc71307439ccf0e75917842ec',770,'89a5-dd39-jquery.ocdialog.css.deps',14,8,131,1681983708,1681983708,0,0,'ae6914d61fe60f85ce46bc84a1e8558a',27,''),(800,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery-ui-fixes.css','1dbfa0f3f5c9cfd5492550c33ffa95ff',770,'89a5-dd39-jquery-ui-fixes.css',16,11,4102,1681983708,1681983708,0,0,'b7e712873cb3c248883bd5effde0ffeb',27,''),(801,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.deps','baa462ce2528e7565d9e8e94861d584e',770,'89a5-dd39-server.css.deps',14,8,772,1681983708,1681983708,0,0,'4f52467be182db4c877d208f7c52d677',27,''),(802,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-systemtags.css','3dd40571f6f29d0a0c946dce61469748',770,'89a5-dd39-systemtags.css',16,11,1403,1681983708,1681983708,0,0,'f35dc1c385ab2e2ff413a683b2a8d637',27,''),(803,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css','86681b8bc226a647a798fea77f821f8b',770,'89a5-dd39-jquery.ocdialog.css',16,11,1253,1681983708,1681983708,0,0,'06dc477e9334d804a7518924c9cd7504',27,''),(804,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.gzip','05f6e2f8484349624c1cbb5c85ae1de5',770,'89a5-dd39-share.css.gzip',15,8,957,1681983708,1681983708,0,0,'28a0cc367a6940f84354776341957691',27,''),(805,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-server.css.gzip','88dd674bc368509aa3ea198c2f4626f9',770,'89a5-dd39-server.css.gzip',15,8,15888,1681983708,1681983708,0,0,'486be3def804c36e40f1a71432769d20',27,''),(806,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-share.css.deps','5e1996bb860233f579b5cc0685c018c8',770,'89a5-dd39-share.css.deps',14,8,121,1681983708,1681983708,0,0,'452d9f473a847e4cdea95178fe7061c1',27,''),(807,8,'appdata_ocp2ck7yo4uj/css/core/89a5-dd39-jquery.ocdialog.css.gzip','3d9bffd046bcb0bd6f922550fd1728e7',770,'89a5-dd39-jquery.ocdialog.css.gzip',15,8,560,1681983708,1681983708,0,0,'c5362d2a7fd8d31dab9b1f2b38072647',27,''),(808,8,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.deps','3d2dd721c9177b66fcb27c9ce23724b5',771,'35c3-dd39-mergedAdditionalStyles.css.deps',14,8,316,1681983708,1681983708,0,0,'db0ee15658a4938aea029388a30c2be3',27,''),(809,8,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css','bb2bcb3b5216a5bf08870b98ab8108a8',771,'35c3-dd39-mergedAdditionalStyles.css',16,11,2303,1681983708,1681983708,0,0,'30851a75ad9867e8a581bb8c72c6cff7',27,''),(810,8,'appdata_ocp2ck7yo4uj/css/files_sharing/35c3-dd39-mergedAdditionalStyles.css.gzip','d27a4c0564ba11157d932a143b719725',771,'35c3-dd39-mergedAdditionalStyles.css.gzip',15,8,793,1681983708,1681983708,0,0,'e414120fab01e82886e11c5b21370965',27,''),(811,8,'appdata_ocp2ck7yo4uj/appstore/apps.json','aea0a87bc9c12b12737ee801593ccba8',722,'apps.json',20,8,1087363,1681983708,1681983708,0,0,'cb612b341afb9606453a91945e05fdb9',27,''),(812,8,'appdata_ocp2ck7yo4uj/identityproof/user-vok','645643f49f95bb6a22d54366a668d7a2',723,'user-vok',2,1,4021,1681985164,1681983708,0,0,'64410e8da0250',31,''),(813,8,'appdata_ocp2ck7yo4uj/identityproof/user-vok/public','a004b7c46da2c2254f89de1e50172352',812,'public',14,8,451,1681983708,1681983708,0,0,'ce4e5c289871d5034db972bd6577ae78',27,''),(814,8,'appdata_ocp2ck7yo4uj/identityproof/user-vok/private','0eb8ad1208a413e19deb14433329e20e',812,'private',14,8,3570,1681983708,1681983708,0,0,'3a8ee4531d6afde9cd7501fe6af014e3',27,''),(815,8,'appdata_ocp2ck7yo4uj/js/comments','e4648049e6995efacee6f80096521231',724,'comments',2,1,80480,1681985164,1681983708,0,0,'64410e8da0250',31,''),(816,8,'appdata_ocp2ck7yo4uj/js/files_texteditor','8e34386727f9050bc69e0087ca33fd6a',724,'files_texteditor',2,1,839303,1681985165,1681983708,0,0,'64410e8da0250',31,''),(817,8,'appdata_ocp2ck7yo4uj/js/files_versions','80e7253edd9ea7831366f098c11d692a',724,'files_versions',2,1,16695,1681985165,1681983708,0,0,'64410e8da0250',31,''),(818,8,'appdata_ocp2ck7yo4uj/js/systemtags','ee888bbc00ce11acc6ae76273ea221c3',724,'systemtags',2,1,24010,1681985165,1681983708,0,0,'64410e8da0250',31,''),(819,8,'appdata_ocp2ck7yo4uj/js/notifications','b1b946b5fe6500cf3bfc4d9ba1503c65',724,'notifications',2,1,26448,1681985165,1681983708,0,0,'64410e8da0250',31,''),(820,8,'appdata_ocp2ck7yo4uj/js/activity','a9f39bf5207544df2d5b895a84618a1e',724,'activity',2,1,21189,1681985165,1681983708,0,0,'64410e8da0250',31,''),(821,8,'appdata_ocp2ck7yo4uj/js/gallery','0868b0a242e983dabdfec4727a60e7b9',724,'gallery',2,1,859739,1681985165,1681983708,0,0,'64410e8da0250',31,''),(822,8,'appdata_ocp2ck7yo4uj/js/files','3eb7143fc18697e4e2511eb3adbc241c',724,'files',2,1,420072,1681985165,1681983708,0,0,'64410e8da0250',31,''),(823,8,'appdata_ocp2ck7yo4uj/js/core','f14d31b7798843b58fe4e295bde30d57',724,'core',2,1,355676,1681985165,1681983708,0,0,'64410e8da0250',31,''),(824,8,'appdata_ocp2ck7yo4uj/js/files_sharing','7676e75f6ea45d19eb1827a880982349',724,'files_sharing',2,1,19382,1681985165,1681983708,0,0,'64410e8da0250',31,''),(825,8,'appdata_ocp2ck7yo4uj/js/comments/merged.js.deps','18af4dd92bc976d39f038edb58b42c61',815,'merged.js.deps',14,8,788,1681983708,1681983708,0,0,'4860d3566008a788bdc3362ffdc4f3f2',27,''),(826,8,'appdata_ocp2ck7yo4uj/js/comments/merged.js','406aebeee235fd67c35f4ebeb199dfe9',815,'merged.js',13,8,62511,1681983708,1681983708,0,0,'d57b7412e06ceba28371c04504cb2f5d',27,''),(827,8,'appdata_ocp2ck7yo4uj/js/comments/merged.js.gzip','0647ad3df0fb06da8883d8d7cb01b546',815,'merged.js.gzip',15,8,17181,1681983708,1681983708,0,0,'543397379c45056c6bfa3022a51948f8',27,''),(828,8,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.deps','000a73daa8b6451f2642c89504a07157',816,'merged.js.deps',14,8,346,1681983708,1681983708,0,0,'8ae4607e6afa41a201988418e291f506',27,''),(829,8,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js','ee0652870e1e2acecbadd6d595ab596e',816,'merged.js',13,8,699810,1681983708,1681983708,0,0,'4b9dd86383610e3fe6583963ccfa98b2',27,''),(830,8,'appdata_ocp2ck7yo4uj/js/files_texteditor/merged.js.gzip','f23ada379b79c7c7cab74dbb34067beb',816,'merged.js.gzip',15,8,139147,1681983708,1681983708,0,0,'4a1bddaaaa42b1e80e410415ec8cf425',27,''),(831,8,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.deps','dde041654f1d7b4eebc3a5c5e5c1d384',817,'merged.js.deps',14,8,394,1681983708,1681983708,0,0,'348f33812909dc37e4fb529e61130399',27,''),(832,8,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js','9f3977c48c9a7a5bee51eb406a30ab06',817,'merged.js',13,8,12719,1681983708,1681983708,0,0,'f163cef37a40b61ce485f0fd4d0d65ec',27,''),(833,8,'appdata_ocp2ck7yo4uj/js/files_versions/merged.js.gzip','ceff3f6abe8b8b5f8268c4188be76a80',817,'merged.js.gzip',15,8,3582,1681983708,1681983708,0,0,'8db3640a75d4cff46d62bfc980a28373',27,''),(834,8,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.deps','b0e572499441370a48b9f9765d801f8e',818,'merged.js.deps',14,8,459,1681983708,1681983708,0,0,'9b2689ff764b9923363d1571b2a60203',27,''),(835,8,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js','01377a4b3140da5070416af63e7d8c4c',818,'merged.js',13,8,18223,1681983708,1681983708,0,0,'9aac570363557b07c029782ec1baa2db',27,''),(836,8,'appdata_ocp2ck7yo4uj/js/systemtags/merged.js.gzip','d8d4c0de28d2f5c53f1f945631fa5152',818,'merged.js.gzip',15,8,5328,1681983708,1681983708,0,0,'a888400d853ba8ff35d002589cf1beaa',27,''),(837,8,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.deps','0d71ebc93bb58de39f0db5e2fba9c879',819,'merged.js.deps',14,8,306,1681983708,1681983708,0,0,'6d5f91633daca43ff8c70f16c4e3a80f',27,''),(838,8,'appdata_ocp2ck7yo4uj/js/notifications/merged.js','7f97a755d5335843011a8c28e0dc4578',819,'merged.js',13,8,20888,1681983708,1681983708,0,0,'8290013901bd214bca8a5699d009b6d1',27,''),(839,8,'appdata_ocp2ck7yo4uj/js/notifications/merged.js.gzip','3e6adf3e96590e9411ded0cf69a9cbeb',819,'merged.js.gzip',15,8,5254,1681983708,1681983708,0,0,'a6662c6812534b26cee3e5c1e99a8665',27,''),(840,8,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.gzip','ab64a435f0684089954a1503a4915c2f',820,'activity-sidebar.js.gzip',15,8,4351,1681983708,1681983708,0,0,'afe0aaea9b1db65d94c8a5c4146f3d88',27,''),(841,8,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js','5b53ceef539881b19ca36f7752013cfb',820,'activity-sidebar.js',13,8,16380,1681983708,1681983708,0,0,'b0846456484cbfeaf0f8f4c4b68625d3',27,''),(842,8,'appdata_ocp2ck7yo4uj/js/activity/activity-sidebar.js.deps','fb3cfe9931d0a1786203ab7ccddec138',820,'activity-sidebar.js.deps',14,8,458,1681983708,1681983708,0,0,'db4b0cc7ee48c01ae77848071fa45c5d',27,''),(843,8,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.deps','195df1a1ff873a95bbbe8f3accf90d45',821,'merged.js.deps',14,8,2069,1681983708,1681983708,0,0,'4e2fe96c87d69d75aceafbef1c1cdd20',27,''),(844,8,'appdata_ocp2ck7yo4uj/js/gallery/merged.js','fdc2b62e3b85db6ebc8c9fd2be985c0e',821,'merged.js',13,8,451474,1681983708,1681983708,0,0,'9135e01ee25d7c14cddd69d12056f136',27,''),(845,8,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js','f82849093986c955349f665498cef5fa',821,'scripts-for-file-app.js',13,8,227059,1681983708,1681983708,0,0,'2a4356b5152d8d75d502bc7b64f13586',27,''),(846,8,'appdata_ocp2ck7yo4uj/js/gallery/merged.js.gzip','86129e9991d4a6f79317be5e30ec6f3b',821,'merged.js.gzip',15,8,123682,1681983708,1681983708,0,0,'99fe62608099bb9e49e175cb60197088',27,''),(847,8,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.gzip','4aa0f263d2fc5e9829ce1349ab3e2fbd',821,'scripts-for-file-app.js.gzip',15,8,54659,1681983708,1681983708,0,0,'9db09a1540b2d255888006230e5e6fe2',27,''),(848,8,'appdata_ocp2ck7yo4uj/js/gallery/scripts-for-file-app.js.deps','8cee64de4107b037e19a6e0001274792',821,'scripts-for-file-app.js.deps',14,8,796,1681983708,1681983708,0,0,'5247e104d425e3f8616bb4e243909e0a',27,''),(849,8,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.deps','adfa388cbf45f6b392d7960797e5880e',822,'merged-index.js.deps',14,8,1957,1681983708,1681983708,0,0,'54616e5d7fa98db1e0b34f469be5cc53',27,''),(850,8,'appdata_ocp2ck7yo4uj/js/files/merged-index.js.gzip','b5b4e4222a3423e2acb28c7699cd86b6',822,'merged-index.js.gzip',15,8,80185,1681983708,1681983708,0,0,'cf0d426641ccb13cc0ff83941b875766',27,''),(851,8,'appdata_ocp2ck7yo4uj/js/files/merged-index.js','ea21876dfbd6b920e81f7bc213706376',822,'merged-index.js',13,8,337930,1681983708,1681983708,0,0,'43062eb275ccf42d6cb61efd570cdbe4',27,''),(852,8,'appdata_ocp2ck7yo4uj/js/core/merged.js.deps','0672ccdef2d9384a411dd0895964d57b',823,'merged.js.deps',14,8,472,1681983708,1681983708,0,0,'4b0c8f19e2e1e2ebda8ea132f398889c',27,''),(853,8,'appdata_ocp2ck7yo4uj/js/core/merged.js','bf362a7fdbb6f0c38deadb41e68acde7',823,'merged.js',13,8,20224,1681983708,1681983708,0,0,'93072fbf8ef71a5eb6de2cac94ea12b4',27,''),(854,8,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.deps','3b52fc8139d5575cb46dffc48d35120b',823,'merged-login.js.deps',14,8,247,1681983708,1681983708,0,0,'a99ef95a112ce6485d425a177112add7',27,''),(855,8,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.gzip','996d183e420339a81267d7d48a42842b',823,'merged-template-prepend.js.gzip',15,8,40626,1681983708,1681983708,0,0,'57cec5b58911f5cbe11ebef25df1d902',27,''),(856,8,'appdata_ocp2ck7yo4uj/js/core/merged-login.js','1286373dbb5f6e81fb13b4f5c0843496',823,'merged-login.js',13,8,7563,1681983708,1681983708,0,0,'a1e7096f98060cd5b8ba4501cea210d9',27,''),(857,8,'appdata_ocp2ck7yo4uj/js/core/merged-login.js.gzip','b654b22875e4d115d122da548cb98865',823,'merged-login.js.gzip',15,8,2276,1681983708,1681983708,0,0,'c1a2b8ff23a6ddd69958e4340e6c87ec',27,''),(858,8,'appdata_ocp2ck7yo4uj/js/core/merged.js.gzip','3e19032edbb0a5d54a4d25606c53c3c8',823,'merged.js.gzip',15,8,5365,1681983708,1681983708,0,0,'4259bcf0fa5456ba4f2e027ddc51714e',27,''),(859,8,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.gzip','64df495640c2992fa06f955533ba6a75',823,'merged-share-backend.js.gzip',15,8,22728,1681983708,1681983708,0,0,'5b907fe37d7c80ed0a33450d084710c9',27,''),(860,8,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js.deps','49447c4a958dfb13087fc02658d2161d',823,'merged-template-prepend.js.deps',14,8,1180,1681983708,1681983708,0,0,'1f18c84ffc4d26cee0929cda6c31cedd',27,''),(861,8,'appdata_ocp2ck7yo4uj/js/core/merged-template-prepend.js','579d4b039f255abe4dbed89444d56310',823,'merged-template-prepend.js',13,8,148486,1681983708,1681983708,0,0,'5de3cf132d62952dc696a76a1497c248',27,''),(862,8,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js.deps','dced322d42d4338d1a49cb39c9f9e53a',823,'merged-share-backend.js.deps',14,8,692,1681983708,1681983708,0,0,'16e8057c9a56b1153950a6c880c4df8e',27,''),(863,8,'appdata_ocp2ck7yo4uj/js/core/merged-share-backend.js','2d6f839fff5c210df1345235e03f12b7',823,'merged-share-backend.js',13,8,105817,1681983708,1681983708,0,0,'f17ec2761f35652b048a9431fc015f0b',27,''),(864,8,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js','253b8ab876fa052002ac07f2240a1656',824,'additionalScripts.js',13,8,14539,1681983708,1681983708,0,0,'a9dc84676d0c3d809597ced0a657c3b9',27,''),(865,8,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.deps','342d284054df0c2482eac265157e4431',824,'additionalScripts.js.deps',14,8,316,1681983708,1681983708,0,0,'bfe81404cfab6db9e3eb5f7000295d66',27,''),(866,8,'appdata_ocp2ck7yo4uj/js/files_sharing/additionalScripts.js.gzip','51d06c1265f8fff99a85a72ac1cf63d4',824,'additionalScripts.js.gzip',15,8,4527,1681983708,1681983708,0,0,'d3c51e905b1bfac8b5c4b82feda8afff',27,''),(867,8,'appdata_ocp2ck7yo4uj/preview/343','c173e600f50af55367d655c4b562ad62',725,'343',2,1,8817,1681985165,1681983708,0,0,'64410e8da0250',31,''),(868,8,'appdata_ocp2ck7yo4uj/preview/251','decab0cb33d2a303801bc42cf78f89cb',725,'251',2,1,0,1681983708,1681983708,0,0,'64410e8d296b9',31,''),(869,8,'appdata_ocp2ck7yo4uj/preview/246','8734b661d9ef2e41975596cbb7aea946',725,'246',2,1,180145,1681985165,1681983708,0,0,'64410e8da0250',31,''),(870,8,'appdata_ocp2ck7yo4uj/preview/237','2659a3cdbcb2a96b3b3cdbf0a5df6f7f',725,'237',2,1,12496,1681985165,1681983708,0,0,'64410e8da0250',31,''),(871,8,'appdata_ocp2ck7yo4uj/preview/6','825339ba134f3c814c79a812ec5d826a',725,'6',2,1,595528,1681985165,1681983708,0,0,'64410e8da0250',31,''),(872,8,'appdata_ocp2ck7yo4uj/preview/241','23fce7333eee9a2ebfdabc3ce0221a0f',725,'241',2,1,815617,1681985165,1681983708,0,0,'64410e8da0250',31,''),(873,8,'appdata_ocp2ck7yo4uj/preview/12','a2d7ac5cb617daa0d638f3fa78cbbd51',725,'12',2,1,180145,1681985165,1681983708,0,0,'64410e8da0250',31,''),(874,8,'appdata_ocp2ck7yo4uj/preview/239','6ea1f2e4fabd223bb8b6ad53d41a7fb1',725,'239',2,1,335671,1681985165,1681983708,0,0,'64410e8da0250',31,''),(875,8,'appdata_ocp2ck7yo4uj/preview/193','461e154eff9f19485709a164ac7546df',725,'193',2,1,0,1681983708,1681983708,0,0,'64410e8d29867',31,''),(876,8,'appdata_ocp2ck7yo4uj/preview/240','66538cbe3c339e6ff7cd619d5e10bb67',725,'240',2,1,595528,1681985165,1681983708,0,0,'64410e8da0250',31,''),(877,8,'appdata_ocp2ck7yo4uj/preview/253','5c622a885af00ec11d18377abac472d4',725,'253',2,1,0,1681983708,1681983708,0,0,'64410e8d298e0',31,''),(878,8,'appdata_ocp2ck7yo4uj/preview/3','cc5565c4ba6fd56871cbbe502dfe977c',725,'3',2,1,13632,1681985165,1681983708,0,0,'64410e8da0250',31,''),(879,8,'appdata_ocp2ck7yo4uj/preview/5','eaca83211066f046a8313d0843f8f854',725,'5',2,1,335671,1681985165,1681983708,0,0,'64410e8da0250',31,''),(880,8,'appdata_ocp2ck7yo4uj/preview/152','d3564f308ae3aa82990f73f8a55fd41e',725,'152',2,1,30985,1681985165,1681983708,0,0,'64410e8da0250',31,''),(881,8,'appdata_ocp2ck7yo4uj/preview/332','b7c45cd7a53996abde18890823dc48ad',725,'332',2,1,572509,1681985165,1681983708,0,0,'64410e8da0250',31,''),(882,8,'appdata_ocp2ck7yo4uj/preview/7','803e2a3dffd0cdf960192b097016e362',725,'7',2,1,815617,1681985165,1681983708,0,0,'64410e8da0250',31,''),(883,8,'appdata_ocp2ck7yo4uj/preview/343/193-96-max.png','80cbfcbdf7d4e4f6479db76cd03b7f53',867,'193-96-max.png',4,3,6148,1681983708,1681983708,0,0,'1080401b863dbc239cbfdbb244065b00',27,''),(884,8,'appdata_ocp2ck7yo4uj/preview/343/64-64-crop.png','cf421d8bcfb182c5e22efabc2ca3c89e',867,'64-64-crop.png',4,3,2669,1681983708,1681983708,0,0,'01af5f5d9932f4d0f9db974a759a515a',27,''),(885,8,'appdata_ocp2ck7yo4uj/preview/246/4096-4096-max.png','f01f0d365843fde49e45c08290be1516',869,'4096-4096-max.png',4,3,153221,1681983708,1681983708,0,0,'327e66431006a544e86079e30950408d',27,''),(886,8,'appdata_ocp2ck7yo4uj/preview/246/64-64-crop.png','7fe853813378f83155fba05db04dbaf3',869,'64-64-crop.png',4,3,4748,1681983708,1681983708,0,0,'65e4901fcf3086c98d829252ac63f987',27,''),(887,8,'appdata_ocp2ck7yo4uj/preview/246/256-256-crop.png','4871a26802ed1eb1e25d671f5b1718c6',869,'256-256-crop.png',4,3,22176,1681983708,1681983708,0,0,'36d989dbc90cf0bba9e0a45fcce6d7ad',27,''),(888,8,'appdata_ocp2ck7yo4uj/preview/237/193-96-max.png','50d08586a69e0184430ad8cc773a0c95',870,'193-96-max.png',4,3,6148,1681983708,1681983708,0,0,'6f75754116db11d808317d19f2f093ca',27,''),(889,8,'appdata_ocp2ck7yo4uj/preview/237/64-64-crop.png','9368924db841411cae5010b37e353748',870,'64-64-crop.png',4,3,2669,1681983708,1681983708,0,0,'f2b35437b7f301bb23f4c9beeb042600',27,''),(890,8,'appdata_ocp2ck7yo4uj/preview/237/96-96-crop.png','234d24f23ec4cb27e343e4657a87b498',870,'96-96-crop.png',4,3,3679,1681983708,1681983708,0,0,'c716f6f097bc3d8988c5012404cf3af5',27,''),(891,8,'appdata_ocp2ck7yo4uj/preview/6/64-64-crop.jpg','ae9b59d54212644239c4b8823bee2d1d',871,'64-64-crop.jpg',5,3,3420,1681983708,1681983708,0,0,'1d92f358a97f3e2004d95208070e2502',27,''),(892,8,'appdata_ocp2ck7yo4uj/preview/6/256-256-crop.jpg','3d70421864eda45bdf9ca5029d09ff9c',871,'256-256-crop.jpg',5,3,21265,1681983708,1681983708,0,0,'f4f17177c70774cc4d2b65862594fc94',27,''),(893,8,'appdata_ocp2ck7yo4uj/preview/6/2000-1333-max.jpg','f934b86c92bf5aa5b42031f2f87f5ad4',871,'2000-1333-max.jpg',5,3,570843,1681983708,1681983708,0,0,'cee53563abeb7d006f2eff4d5c524728',27,''),(894,8,'appdata_ocp2ck7yo4uj/preview/241/64-64-crop.jpg','38014ff76499410a401d139c0a37b5f2',872,'64-64-crop.jpg',5,3,2929,1681983708,1681983708,0,0,'bce024d1af1d43ba6f727e8884c56102',27,''),(895,8,'appdata_ocp2ck7yo4uj/preview/241/256-256-crop.jpg','98d8c52e04c5bbe4db451b129c85eef4',872,'256-256-crop.jpg',5,3,23541,1681983708,1681983708,0,0,'c18ca791caaedb298a15d401c546b700',27,''),(896,8,'appdata_ocp2ck7yo4uj/preview/241/2000-1333-max.jpg','377a6f6a22649ef4fa2e7802496a4aaa',872,'2000-1333-max.jpg',5,3,789147,1681983708,1681983708,0,0,'a4e129c0edff9ac7e7db18500cadbe5c',27,''),(897,8,'appdata_ocp2ck7yo4uj/preview/12/4096-4096-max.png','96a2825d9dd0f53d9edd6e78d4b13f59',873,'4096-4096-max.png',4,3,153221,1681983708,1681983708,0,0,'e0c79a023867b015fd14b9282828f34f',27,''),(898,8,'appdata_ocp2ck7yo4uj/preview/12/64-64-crop.png','ad96ec8ee7f3c392ed81f564e110fd6f',873,'64-64-crop.png',4,3,4748,1681983708,1681983708,0,0,'df26082603d0cb35c3a6aff7216a45a2',27,''),(899,8,'appdata_ocp2ck7yo4uj/preview/12/256-256-crop.png','80986e4e6dbfe180dd06dfd6de914e1e',873,'256-256-crop.png',4,3,22176,1681983708,1681983708,0,0,'f05650a2a2bc6b9433071347c10aeb20',27,''),(900,8,'appdata_ocp2ck7yo4uj/preview/239/64-64-crop.jpg','65a3deb5ecae8673330bf52779ca6387',874,'64-64-crop.jpg',5,3,2543,1681983708,1681983708,0,0,'fc6b821d428346bac5d11b1c3056f5e4',27,''),(901,8,'appdata_ocp2ck7yo4uj/preview/239/1100-734-max.jpg','34f53b48ebf376f90df62b71d4ddae26',874,'1100-734-max.jpg',5,3,308351,1681983708,1681983708,0,0,'fbc2b216bbaa1dc770d0602116a81956',27,''),(902,8,'appdata_ocp2ck7yo4uj/preview/239/256-256-crop.jpg','886334752446814dcc2b231022778ee9',874,'256-256-crop.jpg',5,3,24777,1681983708,1681983708,0,0,'4f58770a5fb1dc99d3bd0873d9c788b2',27,''),(903,8,'appdata_ocp2ck7yo4uj/preview/240/64-64-crop.jpg','0c9a17ebcebc388d5f438daa79585b05',876,'64-64-crop.jpg',5,3,3420,1681983708,1681983708,0,0,'ad14745e7440370c4509ab69f040028b',27,''),(904,8,'appdata_ocp2ck7yo4uj/preview/240/256-256-crop.jpg','bbd758a5e04a2ed32cec068ffe7a9cbc',876,'256-256-crop.jpg',5,3,21265,1681983708,1681983708,0,0,'9d5c3ae21ca781785533e0043f4a7142',27,''),(905,8,'appdata_ocp2ck7yo4uj/preview/240/2000-1333-max.jpg','109640fa4bd202334f7dbbde430c14c2',876,'2000-1333-max.jpg',5,3,570843,1681983708,1681983708,0,0,'a706d5e40775568289feb3ec85d2c0a0',27,''),(906,8,'appdata_ocp2ck7yo4uj/preview/3/32-32-crop.png','089ced233440fe37d36fb62dac5497c0',878,'32-32-crop.png',4,3,1136,1681983708,1681983708,0,0,'6a7bba77da59de098c0d6c5f3c68a699',27,''),(907,8,'appdata_ocp2ck7yo4uj/preview/3/193-96-max.png','56da6505a652ce6ec813851f00831d3a',878,'193-96-max.png',4,3,6148,1681983708,1681983708,0,0,'32601d57d6faf77a247bc10bd56f1904',27,''),(908,8,'appdata_ocp2ck7yo4uj/preview/3/64-64-crop.png','2beb2205135ee5e848582beea9a83e2a',878,'64-64-crop.png',4,3,2669,1681983708,1681983708,0,0,'4e207381d4ee77353099a4683dc06c12',27,''),(909,8,'appdata_ocp2ck7yo4uj/preview/3/96-96-crop.png','bbd949a06059ebc98eab286a0a5e0f3d',878,'96-96-crop.png',4,3,3679,1681983708,1681983708,0,0,'4e0e2e967e4a0eaef4a5f781cfea1c39',27,''),(910,8,'appdata_ocp2ck7yo4uj/preview/5/64-64-crop.jpg','0540a52b35796b32c2efc19f39d3dbb0',879,'64-64-crop.jpg',5,3,2543,1681983708,1681983708,0,0,'87336cca7b1b27303f17ac4744f8035f',27,''),(911,8,'appdata_ocp2ck7yo4uj/preview/5/1100-734-max.jpg','9263bb56a9b788110fa9893a71d48566',879,'1100-734-max.jpg',5,3,308351,1681983708,1681983708,0,0,'e0c125ecb0ca4d299f1ec3cf67b90166',27,''),(912,8,'appdata_ocp2ck7yo4uj/preview/5/256-256-crop.jpg','856c67cb75dbd07fb50abeace7c52b97',879,'256-256-crop.jpg',5,3,24777,1681983708,1681983708,0,0,'e009dc0667fafa22473d52468da3fdf0',27,''),(913,8,'appdata_ocp2ck7yo4uj/preview/152/4096-4096-max.png','e993f925a7782bf3037be3a606399ccd',880,'4096-4096-max.png',4,3,26048,1681983708,1681983708,0,0,'964936254cb21a9e679af95963acecf5',27,''),(914,8,'appdata_ocp2ck7yo4uj/preview/152/32-32-crop.png','7a9f0a0a8785feade277994e00380778',880,'32-32-crop.png',4,3,397,1681983708,1681983708,0,0,'9038a8dd938ae5e90eb75be325368303',27,''),(915,8,'appdata_ocp2ck7yo4uj/preview/152/64-64-crop.png','082f6736b1f106ad596c51a2489663b6',880,'64-64-crop.png',4,3,644,1681983708,1681983708,0,0,'e0c56277c760d2a15e5088d9f024d44c',27,''),(916,8,'appdata_ocp2ck7yo4uj/preview/152/256-256-crop.png','e43ed5fc69ef670c38697cdb7d776f79',880,'256-256-crop.png',4,3,3896,1681983708,1681983708,0,0,'bb9f50c82e1cebab89be48d7c25cf3d2',27,''),(917,8,'appdata_ocp2ck7yo4uj/preview/332/64-64-crop.jpg','f79b7f498e60cee0ce0053afdb7cec0a',881,'64-64-crop.jpg',5,3,2443,1681983708,1681983708,0,0,'b7ad885e47c91a03b6478f607800fea9',27,''),(918,8,'appdata_ocp2ck7yo4uj/preview/332/2560-1440-max.jpg','e223ae728a3547ba402cb5e883945301',881,'2560-1440-max.jpg',5,3,570066,1681983708,1681983708,0,0,'63aa729f525721e2c2e641b35e660113',27,''),(919,8,'appdata_ocp2ck7yo4uj/preview/7/64-64-crop.jpg','7a7f631972b2911536a530b3786f2f39',882,'64-64-crop.jpg',5,3,2929,1681983708,1681983708,0,0,'eb99107d367a3886c2c883c0996fee09',27,''),(920,8,'appdata_ocp2ck7yo4uj/preview/7/256-256-crop.jpg','21fa823be8e84c0851bf52f9808645eb',882,'256-256-crop.jpg',5,3,23541,1681983708,1681983708,0,0,'3ab40093b0aadfdfe4d88e51926849cf',27,''),(921,8,'appdata_ocp2ck7yo4uj/preview/7/2000-1333-max.jpg','0be546cd23b5453d595aa086facd9409',882,'2000-1333-max.jpg',5,3,789147,1681983708,1681983708,0,0,'05a3a5042de436c516b4696bd1200071',27,''),(922,8,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e','3ef75959df1ffce11a451390f7801a6a',726,'43c2025c19f23a73a880300144252f7e',2,1,16536,1681985165,1681983708,0,0,'64410e8da0250',31,''),(923,8,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72','141f595693cdf6f12110583a7998ba56',726,'7f3e57a2397831bc58cb185e0becdc72',2,1,14912,1681985165,1681983708,0,0,'64410e8da0250',31,''),(924,8,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f','23789bb835b044e302311e35484613a6',726,'7a6e71f6fabbda1da4a96ba84be4329f',2,1,6927,1681985165,1681983708,0,0,'64410e8da0250',31,''),(925,8,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de','bbbf0016034ecc5a6096c8b52da6b8b7',726,'7641eb079d4bafa5e2024031c65e28de',2,1,6925,1681985165,1681983708,0,0,'64410e8da0250',31,''),(926,8,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766','1619c8ef2c9355e72c8b5bf03aeaeecc',726,'274857af206af2cd22deae7c56f80766',2,1,6922,1681985165,1681983708,0,0,'64410e8da0250',31,''),(927,8,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.png','3e07f6fba42704c88db57c8943598eff',922,'photo.png',4,3,16074,1681983708,1681983708,0,0,'099d213b47f65ab3ef4e80f9be325a16',27,''),(928,8,'appdata_ocp2ck7yo4uj/dav-photocache/43c2025c19f23a73a880300144252f7e/photo.32.png','f144ba0fbdec0447f39f03e4a8939d44',922,'photo.32.png',4,3,462,1681983708,1681983708,0,0,'f0dddef36ddbfb69a889c74198f75054',27,''),(929,8,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.png','6a4bff6c7c59d57e1e88f93db2059b40',923,'photo.png',4,3,14490,1681983708,1681983708,0,0,'bb40ce9115363e8cb29daf00b5455584',27,''),(930,8,'appdata_ocp2ck7yo4uj/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.32.png','a21e27f03088f448ee3fcd45b61158d5',923,'photo.32.png',4,3,422,1681983708,1681983708,0,0,'a1ffebbb7d4908f8af8b279a0eb234ef',27,''),(931,8,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.png','695384511c2fa6076a55b047624a7fd9',924,'photo.png',4,3,6661,1681983708,1681983708,0,0,'23dc05f01a1f7a4d80b962e1513ed2ec',27,''),(932,8,'appdata_ocp2ck7yo4uj/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.32.png','c5c4f3df014addc24a28b254941e8b46',924,'photo.32.png',4,3,266,1681983708,1681983708,0,0,'c3acc4c68a7cd3c705c4d6b0d7b04997',27,''),(933,8,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.png','75e1798c3f63cfc444cd65c58a550396',925,'photo.png',4,3,6662,1681983708,1681983708,0,0,'c83d0932bc4d95d15cabff4f3a49771e',27,''),(934,8,'appdata_ocp2ck7yo4uj/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.32.png','6141d37c71dd6fb7888d80b871c78c13',925,'photo.32.png',4,3,263,1681983708,1681983708,0,0,'2b6f9c4feba1753f9b3517b67a1510f8',27,''),(935,8,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.png','c22bc875ab14a2f3b737b1c87bec25df',926,'photo.png',4,3,6658,1681983708,1681983708,0,0,'1e965cadddc9a61337b7ffd227d93ae4',27,''),(936,8,'appdata_ocp2ck7yo4uj/dav-photocache/274857af206af2cd22deae7c56f80766/photo.32.png','cc6bc3653657a63f28ac9ad9968055e1',926,'photo.32.png',4,3,264,1681983708,1681983708,0,0,'6adee0a336152a12b2c80ffda9983aec',27,''),(937,8,'testuser2/cache','25e025547959d130da07135e7b1daea8',670,'cache',2,1,0,1681983708,1681983708,0,0,'64410e8d8b2f2',31,''),(938,8,'testuser2/files','c34aaebb1c5efff95e3799f0a64394e4',670,'files',2,1,9760893,1681985165,1681983708,0,0,'64410e8da0250',31,''),(939,8,'testuser2/files/Nextcloud Manual.pdf','c61e503f3a8ca531ea4e58216715efd7',938,'Nextcloud Manual.pdf',9,8,4540242,1681983708,1681983708,0,0,'7f920cc5649c6747cb4dbf9857baf46e',27,''),(940,8,'testuser2/files/Documents','3922f16317c8b4d2932440f8f523249b',938,'Documents',2,1,78496,1681985165,1681983708,0,0,'64410e8da0250',31,''),(941,8,'testuser2/files/Subnetting (Post-Routing).pkt','534946c7bb9befafe322cc8169db34ba',938,'Subnetting (Post-Routing).pkt',14,8,68687,1681983708,1681983708,0,0,'1025024c0e022a578bea4cdb82b9a8c5',27,''),(942,8,'testuser2/files/Subnetting.pkt','9e2d9cbc356d08ff3c7925b5df8837d6',938,'Subnetting.pkt',14,8,68661,1681983708,1681983708,0,0,'b55b5bf61520579368facad0f0334673',27,''),(943,8,'testuser2/files/Subnetting.pdf','8c2098393b505bd06d69ac5517ed4bd8',938,'Subnetting.pdf',9,8,683310,1681983708,1681983708,0,0,'f15e181a1fcee8fb0f676dbd1a65fd54',27,''),(944,8,'testuser2/files/Subnetting.docx','ea980273753172e2743d5dfceb20418e',938,'Subnetting.docx',18,8,1270172,1681983708,1681983708,0,0,'9223108dae41e3d4284b852da28c9557',27,''),(945,8,'testuser2/files/Subnetting Table.xlsx','f5db61c3a9cf2a0f7ec53f85739cf149',938,'Subnetting Table.xlsx',21,8,11500,1681983708,1681983708,0,0,'15d5d87b21c5cf6c34d2de780335b11a',27,''),(946,8,'testuser2/files/Photos','60a08dae24901f4201e6f2a6f284e1f3',938,'Photos',2,1,2360011,1681985165,1681983708,0,0,'64410e8da0250',31,''),(947,8,'testuser2/files/05_subnetting.pdf','30ae8620f9fce8cf69b2431f7356739b',938,'05_subnetting.pdf',9,8,210523,1681983708,1681983708,0,0,'0e432ababb781df926a369a7e0eac428',27,''),(948,8,'testuser2/files/bbw-logo.png','d85200a5494abdd12e6c732be4c3ff7f',938,'bbw-logo.png',4,3,6878,1681983708,1681983708,0,0,'887520800e0d33cf475fcef4cac90fde',27,''),(949,8,'testuser2/files/Nextcloud.mp4','39551c55592e24f9303e2a1c226229f6',938,'Nextcloud.mp4',7,6,462413,1681983708,1681983708,0,0,'a91c1d2411beeb590f7271fd9d2b102a',27,''),(950,8,'testuser2/files/Documents/About.odt','193acb451e48790773ba6adb9b1a8351',940,'About.odt',10,8,77422,1681983708,1681983708,0,0,'91c8f7854eab0eff59702feb9fdf7ab8',27,''),(951,8,'testuser2/files/Documents/About.txt','6ad7ca0f4c6c2a9c0b3ef4aa8d719366',940,'About.txt',12,11,1074,1681983708,1681983708,0,0,'0c4c2f3f55ff1f1a28e99e8082cd835b',27,''),(952,8,'testuser2/files/Photos/Nut.jpg','a369608d8c01360667988e068a7e3c80',946,'Nut.jpg',5,3,955026,1681983708,1681983708,0,0,'7225fe6dfa1e04aaaf18572df9cb630b',27,''),(953,8,'testuser2/files/Photos/Coast.jpg','0256930786d22986d1b97445ddc69ddb',946,'Coast.jpg',5,3,819766,1681983708,1681983708,0,0,'b2c90d2984135c91198e957118a86096',27,''),(954,8,'testuser2/files/Photos/Hummingbird.jpg','7f9d5e528e174f5d4766263c0c2a8132',946,'Hummingbird.jpg',5,3,585219,1681983708,1681983708,0,0,'f9325078cf912d78e50edc35fe6ccfab',27,''),(955,3,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,0,1681985165,1681985165,0,0,'64410e8dafeb6',23,''),(956,4,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,0,1681985165,1681985165,0,0,'64410e8db994c',23,''),(957,6,'','d41d8cd98f00b204e9800998ecf8427e',-1,'',2,1,0,1681985165,1681985165,0,0,'64410e8dc347c',23,''),(958,8,'appdata_oc5ev4nda8xn/js/core/merged-template-prepend.js','9effb1e71fd98e7914c5bed7aa0f8945',564,'merged-template-prepend.js',13,8,11396,1682686187,1682686187,0,0,'12b0cc545d89de7696a832e4852266b8',27,''),(959,8,'appdata_oc5ev4nda8xn/js/core/merged-template-prepend.js.deps','2c54a9552a9ce49b9a7aa581aa12d8df',564,'merged-template-prepend.js.deps',14,8,310,1682686187,1682686187,0,0,'3ece1fc84c067fbe07f92f40f0827c45',27,''),(960,8,'appdata_oc5ev4nda8xn/js/core/merged-template-prepend.js.gzip','f94dd47a20d50d68f7bb727b45b3cc44',564,'merged-template-prepend.js.gzip',15,8,2982,1682686188,1682686188,0,0,'a55a4ccbbb651d41950db21d0e61e573',27,''),(961,8,'appdata_oc5ev4nda8xn/css/theming/39b9-88d1-theming.css','664022fc72135d705d710cb47a352e67',650,'39b9-88d1-theming.css',16,11,1377,1682686188,1682686188,0,0,'3f86b5ffd4780e20a3d55392b2ed8b0b',27,''),(962,8,'appdata_oc5ev4nda8xn/css/theming/39b9-88d1-theming.css.deps','87d97a653aa7a173b80d05a0db5aca02',650,'39b9-88d1-theming.css.deps',14,8,227,1682686188,1682686188,0,0,'ecf439fd756e47f13841fe458f199356',27,''),(963,8,'appdata_oc5ev4nda8xn/css/theming/39b9-88d1-theming.css.gzip','5855071cc55ac4b7f550fa03cd359a74',650,'39b9-88d1-theming.css.gzip',15,8,482,1682686188,1682686188,0,0,'9a012bc2a2a538efdeffe8ef6d722170',27,''),(964,8,'appdata_oc5ev4nda8xn/dashboard','ff1cc210d143beaa3588265e7f4bf5f7',366,'dashboard',2,1,0,1682686195,1682686195,0,0,'644bc0f3b1ecc',31,''),(965,8,'appdata_oc5ev4nda8xn/dashboard/vok','0ac2deac47bccffdce79dcdf944bccda',964,'vok',2,1,0,1682686195,1682686195,0,0,'644bc0f3b9f57',31,''),(966,8,'appdata_oc5ev4nda8xn/js/activity/activity-sidebar.js','22433d952077fa8e358ffeb78507093d',582,'activity-sidebar.js',13,8,39092,1682686195,1682686195,0,0,'b1c1fc3a500e7cd431982c8b380b0558',27,''),(967,8,'appdata_oc5ev4nda8xn/js/activity/activity-sidebar.js.deps','5dc18182ef8c8299eb2a0c0c9ab231b6',582,'activity-sidebar.js.deps',14,8,604,1682686195,1682686195,0,0,'ec1b71b11f4eed582d78daa933d0f0f5',27,''),(968,8,'appdata_oc5ev4nda8xn/js/activity/activity-sidebar.js.gzip','6a391a5c95fffff18ef54de42efe13ed',582,'activity-sidebar.js.gzip',15,8,5776,1682686195,1682686195,0,0,'d95d926f1440336691b1e7e7dd49ba8a',27,''),(969,8,'appdata_oc5ev4nda8xn/css/icons/icons-vars.css','989bafd75d521b9220a6b87405d6c564',660,'icons-vars.css',16,11,158963,1682686235,1682686235,0,0,'e124e22c56e3757d5ce113b70527e957',27,''),(970,8,'appdata_oc5ev4nda8xn/css/icons/icons-list.template','e5ef76db298eefe65c901decf59f2dee',660,'icons-list.template',14,8,18609,1682686235,1682686235,0,0,'a5babde707d828ecb624788614156ebe',27,''),(971,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-server.css','965ebe6de356c8c8d5c1b345a592d842',614,'217b-88d1-server.css',16,11,137463,1682686196,1682686196,0,0,'e6f63972d3445b12d3ba79616b75121c',27,''),(972,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-server.css.deps','61e26a05076775d03a6cf5f977095c1d',614,'217b-88d1-server.css.deps',14,8,983,1682686196,1682686196,0,0,'ff4bbb14e970e2cf0958c21db3f863f4',27,''),(973,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-server.css.gzip','746ad649d0281822bdc15531876b9e32',614,'217b-88d1-server.css.gzip',15,8,19573,1682686196,1682686196,0,0,'9003504690c9d525fb7a80209b06eba1',27,''),(974,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-css-variables.css','9271d4360ace1957f85a378836b93fb6',614,'217b-88d1-css-variables.css',16,11,1380,1682686196,1682686196,0,0,'a27616bd3037ef65973613304effdf16',27,''),(975,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-css-variables.css.deps','da3021690aec93e1d21cd8b307625275',614,'217b-88d1-css-variables.css.deps',14,8,224,1682686196,1682686196,0,0,'90ba0c0a08f4b88c8c0829c46bfd810f',27,''),(976,8,'appdata_oc5ev4nda8xn/css/core/217b-88d1-css-variables.css.gzip','5aa1e7186069968547dec5ca84b51390',614,'217b-88d1-css-variables.css.gzip',15,8,574,1682686196,1682686196,0,0,'488cc5d64f8d21d4a31e349cb30d63a2',27,''),(977,8,'appdata_oc5ev4nda8xn/css/notifications','059def1fbf0452351aed448d79661e45',613,'notifications',2,1,0,1682686196,1682686196,0,0,'644bc0f47e546',31,''),(978,8,'appdata_oc5ev4nda8xn/css/notifications/0577-88d1-styles.css','1e43e19d0704bcee09dcb4d1e51c044b',977,'0577-88d1-styles.css',16,11,3455,1682686196,1682686196,0,0,'573ec55da8a986e0afa83cd38baa271b',27,''),(979,8,'appdata_oc5ev4nda8xn/css/notifications/0577-88d1-styles.css.deps','6dccb405ed2b68845f596aa29be2ad12',977,'0577-88d1-styles.css.deps',14,8,232,1682686196,1682686196,0,0,'8d0b1e3077da9fcd381a1fdafc72cfc2',27,''),(980,8,'appdata_oc5ev4nda8xn/css/notifications/0577-88d1-styles.css.gzip','a5ecc103c662c156f9943f1ac5a2b515',977,'0577-88d1-styles.css.gzip',15,8,846,1682686196,1682686196,0,0,'66a6b8e9ee9ab937d744f5be359c8a01',27,''),(981,8,'appdata_oc5ev4nda8xn/css/dashboard','1b447bae63904c3af7c0d78bf0ae2390',613,'dashboard',2,1,0,1682686196,1682686196,0,0,'644bc0f4afdb4',31,''),(982,8,'appdata_oc5ev4nda8xn/css/dashboard/d149-88d1-dashboard.css','a96f8aff0770bc164f617a76e56dc32b',981,'d149-88d1-dashboard.css',16,11,2062,1682686196,1682686196,0,0,'028d8fcc3427bb7bc8c5b0546a1d7b95',27,''),(983,8,'appdata_oc5ev4nda8xn/css/dashboard/d149-88d1-dashboard.css.deps','6537d52f026aacfdfb7c001ed3202466',981,'d149-88d1-dashboard.css.deps',14,8,231,1682686196,1682686196,0,0,'57330a7f8336cb64ac3522d670258b53',27,''),(984,8,'appdata_oc5ev4nda8xn/css/dashboard/d149-88d1-dashboard.css.gzip','bf492df6b07c39f7d2fba731775a7931',981,'d149-88d1-dashboard.css.gzip',15,8,602,1682686196,1682686196,0,0,'3f184120b8a011c20d29dd8cb218ce08',27,''),(985,8,'appdata_oc5ev4nda8xn/css/activity','1d0abf98834a8af3938dc23f4339c632',613,'activity',2,1,0,1682686197,1682686197,0,0,'644bc0f4d7de7',31,''),(986,8,'appdata_oc5ev4nda8xn/css/activity/a3ad-88d1-style.css','b9824e19d42cf38503a35075a33d4972',985,'a3ad-88d1-style.css',16,11,3621,1682686196,1682686196,0,0,'8546f23620d96099fcfca44f6ccf0497',27,''),(987,8,'appdata_oc5ev4nda8xn/css/activity/a3ad-88d1-style.css.deps','f9077f3059d43d1efc3a1b2bea3e7b4a',985,'a3ad-88d1-style.css.deps',14,8,226,1682686197,1682686197,0,0,'e8772f28498d74af9314dace6aa53d45',27,''),(988,8,'appdata_oc5ev4nda8xn/css/activity/a3ad-88d1-style.css.gzip','c8a1c14375e22f135e3bb7da70a6edd2',985,'a3ad-88d1-style.css.gzip',15,8,1168,1682686197,1682686197,0,0,'e0364be9ff9c760dad4ee3dcb3bac59f',27,''),(989,8,'appdata_oc5ev4nda8xn/css/text','ad5860464f12d88b8796f54dddf26ada',613,'text',2,1,0,1682686197,1682686197,0,0,'644bc0f512ec2',31,''),(990,8,'appdata_oc5ev4nda8xn/css/text/232d-88d1-icons.css','79a5afa261566637b111c48ac60cfe77',989,'232d-88d1-icons.css',16,11,2710,1682686197,1682686197,0,0,'da6ec7c77e9759dfd9db89e17dd5e045',27,''),(991,8,'appdata_oc5ev4nda8xn/css/text/232d-88d1-icons.css.deps','fd9bcdf70c3ca6a63cad1ddae67e9ff4',989,'232d-88d1-icons.css.deps',14,8,222,1682686197,1682686197,0,0,'0fdd766c6b94832c907dddaf5a6ef64c',27,''),(992,8,'appdata_oc5ev4nda8xn/css/text/232d-88d1-icons.css.gzip','a76d7be208e87ac4c949172b51ba6c08',989,'232d-88d1-icons.css.gzip',15,8,417,1682686197,1682686197,0,0,'b090996b098ccfaecbfa93360aa938d9',27,''),(993,8,'appdata_oc5ev4nda8xn/css/user_status','79a00b3c0d70694bb5be0112a80cd5d1',613,'user_status',2,1,0,1682686197,1682686197,0,0,'644bc0f546376',31,''),(994,8,'appdata_oc5ev4nda8xn/css/user_status/3acc-88d1-user-status-menu.css','214d9ace11c7df9aa909bdeadc123e8a',993,'3acc-88d1-user-status-menu.css',16,11,1039,1682686197,1682686197,0,0,'9e5f1f556c2d7222d603d1d9e4ec04a9',27,''),(995,8,'appdata_oc5ev4nda8xn/css/user_status/3acc-88d1-user-status-menu.css.deps','e03173147d1d3f64d9acf285968bc048',993,'3acc-88d1-user-status-menu.css.deps',14,8,240,1682686197,1682686197,0,0,'4d31fb5de85888e1583ea74e0bdb4516',27,''),(996,8,'appdata_oc5ev4nda8xn/css/user_status/3acc-88d1-user-status-menu.css.gzip','30d477996737d4b94c1719ff6d553ce9',993,'3acc-88d1-user-status-menu.css.gzip',15,8,240,1682686197,1682686197,0,0,'0cda1fab041277051d6d55ecfac7f829',27,''),(997,8,'appdata_oc5ev4nda8xn/avatar/vok','7cdd10e1808b3f6421ba4cb3c308f090',569,'vok',2,1,0,1682686211,1682686211,0,0,'644bc0f69dc69',31,''),(998,8,'appdata_oc5ev4nda8xn/avatar/vok/avatar.png','6bfd0f546724f25d23d4e8871d155168',997,'avatar.png',4,3,15778,1682686198,1682686198,0,0,'2cf884b0e777392f2efcf178447da8f6',27,''),(999,8,'appdata_oc5ev4nda8xn/avatar/vok/generated','b86eafef5d4392d314d4a7c092daa1ef',997,'generated',14,8,0,1682686198,1682686198,0,0,'f90915ce29332aa2dd56d187863a38ab',27,''),(1000,8,'appdata_oc5ev4nda8xn/avatar/vok/avatar.32.png','0d04c58ed1376281155df00925126bc8',997,'avatar.32.png',4,3,428,1682686198,1682686198,0,0,'2072e13e1a4bd88d9a3d940a6983a91f',27,''),(1001,8,'appdata_oc5ev4nda8xn/preview/c','73cffc8efc8f1cf742bd1c5628be88f6',566,'c',2,1,0,1682686203,1682686203,0,0,'644bc0fbab142',31,''),(1002,8,'appdata_oc5ev4nda8xn/preview/c/2','136c82a02693c4013bf4c84d677b5c6a',1001,'2',2,1,0,1682686203,1682686203,0,0,'644bc0fbaaff8',31,''),(1003,8,'appdata_oc5ev4nda8xn/preview/c/2/4','1b5271adabaf767218762334b7d9e550',1002,'4',2,1,0,1682686203,1682686203,0,0,'644bc0fbaaeac',31,''),(1004,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c','163f948c9f503413c3887427703aaf5e',1003,'c',2,1,0,1682686203,1682686203,0,0,'644bc0fbaad5f',31,''),(1005,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d','da00993a7f5b609e75e18df64c617a77',1004,'d',2,1,0,1682686203,1682686203,0,0,'644bc0fbaac10',31,''),(1006,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d/7','2a6d02df27b4830db3d098a10d13169d',1005,'7',2,1,0,1682686203,1682686203,0,0,'644bc0fbaaabd',31,''),(1007,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d/7/6','fa8799daa01be66d7539467865834b2a',1006,'6',2,1,0,1682686203,1682686203,0,0,'644bc0fbaa966',31,''),(1008,8,'appdata_oc5ev4nda8xn/preview/c/2/4/c/d/7/6/253','dde0c2bbb8e14bc6cf3cf2b67956b79b',1007,'253',2,1,0,1682686203,1682686203,0,0,'644bc0fbaa7b0',31,''),(1009,8,'appdata_oc5ev4nda8xn/theming','778764207f0011a0b5fd4e58ffa1fb88',366,'theming',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3c3c',31,''),(1010,8,'appdata_oc5ev4nda8xn/preview/1','8b1f797f2e2b6f05a82a1f03e57f7d2e',566,'1',2,1,0,1682686203,1682686203,0,0,'644bc0fbc42c2',31,''),(1011,8,'appdata_oc5ev4nda8xn/preview/1/9','6a4c81f7cd41d924b0c209de3c9bebb6',1010,'9',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3e35',31,''),(1012,8,'appdata_oc5ev4nda8xn/preview/1/9/f','3537dc16c8d505d307dcf126b4b33e41',1011,'f',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3b49',31,''),(1013,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3','0c18c73260a76b0859ee8cbfbeb6c9a8',1012,'3',2,1,0,1682686203,1682686203,0,0,'644bc0fbc39a3',31,''),(1014,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c','9c51e145c85c00b6fd22c8c3f521c952',1013,'c',2,1,0,1682686203,1682686203,0,0,'644bc0fbc37e2',31,''),(1015,8,'appdata_oc5ev4nda8xn/theming/1','377c5c0291c13e8b40c14a7ed7823d48',1009,'1',2,1,0,1682686210,1682686210,0,0,'644bc0fbcccf4',31,''),(1016,8,'appdata_oc5ev4nda8xn/preview/c/0','9748c43066027a0d72f8fccf6473c7fd',1001,'0',2,1,0,1682686203,1682686203,0,0,'644bc0fbcca02',31,''),(1017,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c/d','dbb7540087cc83f9dd8e2b10762741ee',1014,'d',2,1,0,1682686203,1682686203,0,0,'644bc0fbc35f0',31,''),(1018,8,'appdata_oc5ev4nda8xn/preview/c/0/4','e08ef4fca4e7a8258b2651ae3304c0d5',1016,'4',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc89a',31,''),(1019,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c/d/3','61d49f1ade091eb39a7d1ec373a71358',1017,'3',2,1,0,1682686203,1682686203,0,0,'644bc0fbc3489',31,''),(1020,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2','d02473f62e64a6756ad67f563d1229c1',1018,'2',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc6e8',31,''),(1021,8,'appdata_oc5ev4nda8xn/preview/1/9/f/3/c/d/3/251','764ab1d2f2e032f325f4cff05363d476',1019,'251',2,1,0,1682686203,1682686203,0,0,'644bc0fbc32a8',31,''),(1022,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f','cd2a5fe1690db3016da052d9e2666c40',1020,'f',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc536',31,''),(1023,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4','b5bbb80ee92205c8dfa6fadec5057b9c',1022,'4',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc3b2',31,''),(1024,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_video.svg','2daa5b0a1f9df0bc7ad8c22b9f20057e',1015,'icon-core-filetypes_video.svg',17,3,277,1682686203,1682686203,0,0,'ff8ea34247bbb5197313516cabf01f6c',27,''),(1025,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d','f05f598cbfa8d987e5dd65dc36ce2018',1023,'d',2,1,0,1682686203,1682686203,0,0,'644bc0fbcc227',31,''),(1026,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332','538cb3e814e253859767c750c376ee4d',1025,'332',2,1,0,1682686237,1682686237,0,0,'644bc0fbcc003',31,''),(1027,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_text.svg','b316b012b2eb01b8a4a73719faa8300d',1015,'icon-core-filetypes_text.svg',17,3,295,1682686203,1682686203,0,0,'c893451b70b16f5522888169fb0958f8',27,''),(1028,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_image.svg','479173dfdcd2d84d914a18ec50ac56a2',1015,'icon-core-filetypes_image.svg',17,3,352,1682686203,1682686203,0,0,'e0f72f2e9bd9d57aa415b81ee1751848',27,''),(1029,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_application-pdf.svg','2e8157c21ec007ba5b8c9797c971cb45',1015,'icon-core-filetypes_application-pdf.svg',17,3,1054,1682686204,1682686204,0,0,'899133eeb8588c69cd47bb5e1c7fe720',27,''),(1030,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332/2560-1440-max.jpg','4bf91c82cc8cc0c098f472dfea5d6fbe',1026,'2560-1440-max.jpg',5,3,469369,1682686204,1682686204,0,0,'cee7b16a20e501b55e5b5f7c9ce726d5',27,''),(1031,8,'appdata_oc5ev4nda8xn/preview/3/8','298ff0cfef3ca7b55a3d27cec6b9f985',654,'8',2,1,0,1682686204,1682686204,0,0,'644bc0fc38038',31,''),(1032,8,'appdata_oc5ev4nda8xn/preview/3/8/d','58d891abfea6c7ef1ab1f0b1e5a47228',1031,'d',2,1,0,1682686204,1682686204,0,0,'644bc0fc37eca',31,''),(1033,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b','878d9bb9dc455941d188613a79f953bc',1032,'b',2,1,0,1682686204,1682686204,0,0,'644bc0fc37d60',31,''),(1034,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3','ffb726b41f127967109daf9e87fe7a28',1033,'3',2,1,0,1682686204,1682686204,0,0,'644bc0fc37bc2',31,''),(1035,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a','975dde1e7c6307f6ee9692adcb165ed8',1034,'a',2,1,0,1682686204,1682686204,0,0,'644bc0fc3791c',31,''),(1036,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e','b49e0e68ec2550937c27875b25259dc7',1035,'e',2,1,0,1682686204,1682686204,0,0,'644bc0fc377be',31,''),(1037,8,'appdata_oc5ev4nda8xn/preview/5','9c24dd2a1975c400e3cf4b5ce76aff7a',566,'5',2,1,0,1682686237,1682686237,0,0,'644bc29540ef1',31,''),(1038,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e/246','db025b6f148fab7715ff73c256943951',1036,'246',2,1,0,1682686204,1682686204,0,0,'644bc0fc374fd',31,''),(1039,8,'appdata_oc5ev4nda8xn/preview/5/3','da1ac45568fd34887978a6fde6c40bad',1037,'3',2,1,0,1682686204,1682686204,0,0,'644bc0fc3f853',31,''),(1040,8,'appdata_oc5ev4nda8xn/preview/5/3/9','2a44886a5515d03c08674308cbac152b',1039,'9',2,1,0,1682686204,1682686204,0,0,'644bc0fc3f259',31,''),(1041,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f','3dc023dbaa644c544e0c4358046cf4de',1040,'f',2,1,0,1682686204,1682686204,0,0,'644bc0fc3f0e7',31,''),(1042,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d','06f889511128b46560688c568fbb6352',1041,'d',2,1,0,1682686204,1682686204,0,0,'644bc0fc3ef81',31,''),(1043,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5','e1f445aab9b54d4ecebf21a38b5e8fb9',1042,'5',2,1,0,1682686204,1682686204,0,0,'644bc0fc3ede0',31,''),(1044,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3','014ed8092a168e0d34f2c0da3f64fe7c',1043,'3',2,1,0,1682686204,1682686204,0,0,'644bc0fc3ec05',31,''),(1045,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237','d8a5781c42643770d220b4c8f826b941',1044,'237',2,1,0,1682686231,1682686231,0,0,'644bc0fc3e9c4',31,''),(1046,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237/193-96-max.png','592cd9cc2749e3f278d95df277548cae',1045,'193-96-max.png',4,3,6148,1682686204,1682686204,0,0,'2625f5535b4e9106560d75bc026d9569',27,''),(1047,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237/64-64-crop.png','c60a27a1ad5b94346e2e0ee88e6f9625',1045,'64-64-crop.png',4,3,2669,1682686204,1682686204,0,0,'1598bd5ff26e7388706236596e2b4199',27,''),(1048,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332/64-64-crop.jpg','59a4cdbc73320dddf83d08eb68070fec',1026,'64-64-crop.jpg',5,3,2096,1682686204,1682686204,0,0,'4988e5c80afdfceba7f5014cf7daf19a',27,''),(1049,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e/246/4096-4096-max.png','f69cf7158bc1574382040ddacf2ef5f8',1038,'4096-4096-max.png',4,3,115061,1682686204,1682686204,0,0,'4bc4d9cfa21078ad2ab06e3d55cd7856',27,''),(1050,8,'appdata_oc5ev4nda8xn/preview/3/8/d/b/3/a/e/246/64-64-crop.png','44736c42c274117930196dab411f9642',1038,'64-64-crop.png',4,3,2669,1682686204,1682686204,0,0,'610e97335ce270861fe7cc9df935bc36',27,''),(1051,8,'appdata_oc5ev4nda8xn/dav-photocache/7f3e57a2397831bc58cb185e0becdc72','fc237aafd67c81663ad6b981d6eff899',663,'7f3e57a2397831bc58cb185e0becdc72',2,1,0,1682686209,1682686209,0,0,'644bc10198927',31,''),(1052,8,'appdata_oc5ev4nda8xn/dav-photocache/7641eb079d4bafa5e2024031c65e28de','ec74647052fc5f5e004864f7362e46f7',663,'7641eb079d4bafa5e2024031c65e28de',2,1,0,1682686209,1682686209,0,0,'644bc1019bab1',31,''),(1053,8,'appdata_oc5ev4nda8xn/dav-photocache/502776188a7bf675bc6b6065c729f8dd','129aea9fe6f1bbdc51d07358eb50564b',663,'502776188a7bf675bc6b6065c729f8dd',2,1,0,1682686209,1682686209,0,0,'644bc1019e09f',31,''),(1054,8,'appdata_oc5ev4nda8xn/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f','2db23e2341726b0382775803be1a4305',663,'7a6e71f6fabbda1da4a96ba84be4329f',2,1,0,1682686209,1682686209,0,0,'644bc1019f709',31,''),(1055,8,'appdata_oc5ev4nda8xn/dav-photocache/274857af206af2cd22deae7c56f80766','e4b47b95d2e7b6acdfaff0f40daae7a0',663,'274857af206af2cd22deae7c56f80766',2,1,0,1682686209,1682686209,0,0,'644bc101a1631',31,''),(1056,8,'appdata_oc5ev4nda8xn/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.png','d965e898cac10d6d8810594bdd427372',1051,'photo.png',4,3,14490,1682686209,1682686209,0,0,'0d0c820e1f223bc7aa88ea4b181b6bba',27,''),(1057,8,'appdata_oc5ev4nda8xn/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.png','11c58085e0a16b59c801bc6923b3dbbf',1052,'photo.png',4,3,6662,1682686209,1682686209,0,0,'96e8650b1197301bfa2609703b21ccce',27,''),(1058,8,'appdata_oc5ev4nda8xn/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.png','72c054cf1419df9d11e2c142a6208977',1054,'photo.png',4,3,6661,1682686209,1682686209,0,0,'9d782c2ba2842e0b3b3201c2aac02479',27,''),(1059,8,'appdata_oc5ev4nda8xn/dav-photocache/502776188a7bf675bc6b6065c729f8dd/photo.png','b4e73f3a5aebd7b9148ac32e98464833',1053,'photo.png',4,3,3958,1682686209,1682686209,0,0,'52510e88d02c9452dd34575040846efb',27,''),(1060,8,'appdata_oc5ev4nda8xn/dav-photocache/274857af206af2cd22deae7c56f80766/photo.png','0d3a83c8d2e7c4f2bdbc33b095594d40',1055,'photo.png',4,3,6658,1682686209,1682686209,0,0,'83035e8b68dad588fac7a65a597f9fd0',27,''),(1061,8,'appdata_oc5ev4nda8xn/dav-photocache/502776188a7bf675bc6b6065c729f8dd/photo.32.png','1e35a9baed5ec643bc56362710459d24',1053,'photo.32.png',4,3,1095,1682686209,1682686209,0,0,'301a1c871de6a99caf192a00934ff2bb',27,''),(1062,8,'appdata_oc5ev4nda8xn/dav-photocache/7f3e57a2397831bc58cb185e0becdc72/photo.32.png','2c0c12f3d079b5666a95800395b15e24',1051,'photo.32.png',4,3,426,1682686209,1682686209,0,0,'430e342cf1948a449e9c00163342da24',27,''),(1063,8,'appdata_oc5ev4nda8xn/dav-photocache/7641eb079d4bafa5e2024031c65e28de/photo.32.png','9ad2bad26b1da2a7e469a3858f98ab20',1052,'photo.32.png',4,3,268,1682686209,1682686209,0,0,'77f08d50a0028ba97fca8a05aab1ab35',27,''),(1064,8,'appdata_oc5ev4nda8xn/dav-photocache/7a6e71f6fabbda1da4a96ba84be4329f/photo.32.png','c6c986582dea25f932244eb51535a317',1054,'photo.32.png',4,3,266,1682686209,1682686209,0,0,'5d9d3c6ecb1b5d283177d392d34746b7',27,''),(1065,8,'appdata_oc5ev4nda8xn/dav-photocache/274857af206af2cd22deae7c56f80766/photo.32.png','157aa2d746bf32141b456cd0bccc741f',1055,'photo.32.png',4,3,264,1682686209,1682686209,0,0,'1a99c13d8b274b1b4ce890e557d8ce00',27,''),(1066,8,'appdata_oc5ev4nda8xn/css/settings','0d634086313f09160cd61697d7a97093',613,'settings',2,1,0,1682686210,1682686210,0,0,'644bc10232b3c',31,''),(1067,8,'appdata_oc5ev4nda8xn/css/settings/62ab-88d1-settings.css','2da95c2bac9fa977a1cda68000a980db',1066,'62ab-88d1-settings.css',16,11,33068,1682686210,1682686210,0,0,'1e53c4543768c9c2b837d252fde00108',27,''),(1068,8,'appdata_oc5ev4nda8xn/css/settings/62ab-88d1-settings.css.deps','5cd2b3110390fd74dc12dfdca9ccb918',1066,'62ab-88d1-settings.css.deps',14,8,229,1682686210,1682686210,0,0,'00c3ff33f5318eb0ffd75deda14c9146',27,''),(1069,8,'appdata_oc5ev4nda8xn/css/settings/62ab-88d1-settings.css.gzip','b877eb60896e0c1ca5d01d9ce19c8e72',1066,'62ab-88d1-settings.css.gzip',15,8,6051,1682686210,1682686210,0,0,'7865cd8b21322d1f73df34c59a07f453',27,''),(1070,8,'appdata_oc5ev4nda8xn/theming/1/icon-core-filetypes_folder.svg','a51135983e90dd53f6c50e43436efb8e',1015,'icon-core-filetypes_folder.svg',17,3,255,1682686210,1682686210,0,0,'9d485030eaad135fcfe04596f4772cab',27,''),(1071,8,'appdata_oc5ev4nda8xn/avatar/vok/avatar.145.png','73a48939a70e3186ada0d922c9eefca0',997,'avatar.145.png',4,3,1593,1682686211,1682686211,0,0,'ef27fac5245808c80fea1be7ebbe26c8',27,''),(1072,8,'appdata_oc5ev4nda8xn/js/files/merged-index.js','8d016b31518709a093420277a31f1c1b',578,'merged-index.js',13,8,410613,1682686230,1682686230,0,0,'e7c661510790e6a528f2ea5887725cec',27,''),(1073,8,'appdata_oc5ev4nda8xn/js/files/merged-index.js.deps','6dcded04e2c2f2e488c24d3ce04e9c4a',578,'merged-index.js.deps',14,8,2520,1682686230,1682686230,0,0,'aed573c0a6a124612827fdc87bf32a0d',27,''),(1074,8,'appdata_oc5ev4nda8xn/js/files/merged-index.js.gzip','47d73058475b371232f077bdbf420e3e',578,'merged-index.js.gzip',15,8,92688,1682686230,1682686230,0,0,'0b62558d1e1ae88a7f04ad3c22efc7c9',27,''),(1075,8,'appdata_oc5ev4nda8xn/css/files/d71e-88d1-merged.css','fc809cc86dd5d628730f7d1452a0b7d9',627,'d71e-88d1-merged.css',16,11,29015,1682686230,1682686230,0,0,'f80a0932039cad3b6ae8172e23ca2614',27,''),(1076,8,'appdata_oc5ev4nda8xn/css/files/d71e-88d1-merged.css.deps','6f9f10278d8c447ec3674b385c6d84e0',627,'d71e-88d1-merged.css.deps',14,8,608,1682686230,1682686230,0,0,'c038360631b146c04dd2082588b8859c',27,''),(1077,8,'appdata_oc5ev4nda8xn/css/files/d71e-88d1-merged.css.gzip','e70964f3634422a1e5ff98c80bb751eb',627,'d71e-88d1-merged.css.gzip',15,8,5676,1682686230,1682686230,0,0,'d6b77032b095259c2f08f2fdc913014a',27,''),(1078,8,'appdata_oc5ev4nda8xn/css/files_sharing/4472-88d1-icons.css','60605a4078fb40f85b4acce437349332',639,'4472-88d1-icons.css',16,11,174,1682686230,1682686230,0,0,'4cda81f42c874e49507df4a97c32bc4e',27,''),(1079,8,'appdata_oc5ev4nda8xn/css/files_sharing/4472-88d1-icons.css.deps','3da0212633e69bb74411632f3e60b5e5',639,'4472-88d1-icons.css.deps',14,8,231,1682686230,1682686230,0,0,'e5cc9e38038d06f92abfd79f54e9b943',27,''),(1080,8,'appdata_oc5ev4nda8xn/css/files_sharing/4472-88d1-icons.css.gzip','19c07666b4707784a6481cc794adfa01',639,'4472-88d1-icons.css.gzip',15,8,102,1682686230,1682686230,0,0,'61dfc992be0f732656f4494d32c36e4b',27,''),(1081,8,'appdata_oc5ev4nda8xn/preview/5/3/9/f/d/5/3/237/96-96-crop.png','39e2051293f791055b590db88ce1ef6f',1045,'96-96-crop.png',4,3,3679,1682686231,1682686231,0,0,'a974063662b72d7d41b1f6b75fb27549',27,''),(1082,8,'appdata_oc5ev4nda8xn/css/photos','cb477a0f514211f49499a8a8053fafe4',613,'photos',2,1,0,1682686235,1682686235,0,0,'644bc11ae9d2f',31,''),(1083,8,'appdata_oc5ev4nda8xn/css/photos/b0e8-88d1-icons.css','ee81fde7b112637d5a5fe9337050ee6c',1082,'b0e8-88d1-icons.css',16,11,383,1682686235,1682686235,0,0,'4d63f40df1e061edb286e54e06f03509',27,''),(1084,8,'appdata_oc5ev4nda8xn/css/photos/b0e8-88d1-icons.css.deps','242f760c53f8489edf0423b25450bffe',1082,'b0e8-88d1-icons.css.deps',14,8,224,1682686235,1682686235,0,0,'b7d67b5ab86ad78c952518c48d7fee73',27,''),(1085,8,'appdata_oc5ev4nda8xn/css/photos/b0e8-88d1-icons.css.gzip','555dd648e7284fd19eddb32e27263dca',1082,'b0e8-88d1-icons.css.gzip',15,8,130,1682686235,1682686235,0,0,'905917da08e4e60c8957c15fdc330d54',27,''),(1086,8,'appdata_oc5ev4nda8xn/preview/e','05bac600b86ca7cd4c478e7ce45d9e33',566,'e',2,1,0,1682686237,1682686237,0,0,'644bc11d3f127',31,''),(1087,8,'appdata_oc5ev4nda8xn/preview/e/4','7314d9bff20ccd03c045054841fbf0de',1086,'4',2,1,0,1682686237,1682686237,0,0,'644bc11d3efc7',31,''),(1088,8,'appdata_oc5ev4nda8xn/preview/e/4/a','619bddfeda8304f44c050cb91c3ecdcc',1087,'a',2,1,0,1682686237,1682686237,0,0,'644bc11d3ee68',31,''),(1089,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6','4f0a2264af2e04dde3bcf079ade7ee42',1088,'6',2,1,0,1682686237,1682686237,0,0,'644bc11d3ec3e',31,''),(1090,8,'appdata_oc5ev4nda8xn/preview/3/3','c137c93e40ac52264d3886199d504a9f',654,'3',2,1,0,1682686237,1682686237,0,0,'644bc11d453aa',31,''),(1091,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2','5fc916fc3840729706bb7f2bf2d420b6',1089,'2',2,1,0,1682686237,1682686237,0,0,'644bc11d3ea1a',31,''),(1092,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2/2','ada38adc00c417eda15dad2d95f26cd6',1091,'2',2,1,0,1682686237,1682686237,0,0,'644bc11d3e8c3',31,''),(1093,8,'appdata_oc5ev4nda8xn/preview/5/5','906f5d5fc6e3ec85fef21bf67768b840',1037,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d47916',31,''),(1094,8,'appdata_oc5ev4nda8xn/preview/f','2a24336e7910030a4aa20a735af123cd',566,'f',2,1,0,1682686237,1682686237,0,0,'644bc11d46acb',31,''),(1095,8,'appdata_oc5ev4nda8xn/preview/3/3/5','c11292a509166e36d8a6644074335b4b',1090,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d451d0',31,''),(1096,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2/2/2','e39be89b81c0a9fbfa2d1d7c8b4636a2',1092,'2',2,1,0,1682686237,1682686237,0,0,'644bc11d3e766',31,''),(1097,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f','c8ff62d488e0d9e45a4fdc631cb755ca',1095,'f',2,1,0,1682686237,1682686237,0,0,'644bc11d44841',31,''),(1098,8,'appdata_oc5ev4nda8xn/preview/5/5/5','93ed0a17a835769a163a45266f8bc9b8',1093,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d477b3',31,''),(1099,8,'appdata_oc5ev4nda8xn/preview/f/3','b36cd38e2d7a3bda4b13100cf9c54f89',1094,'3',2,1,0,1682686237,1682686237,0,0,'644bc11d4637a',31,''),(1100,8,'appdata_oc5ev4nda8xn/preview/e/4/a/6/2/2/2/242','a8aa5fb579b46e32794ca1bae7403b74',1096,'242',2,1,0,1682686237,1682686237,0,0,'644bc11d3e58e',31,''),(1101,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d','d709785b9a83d28884868649846439e9',1098,'d',2,1,0,1682686237,1682686237,0,0,'644bc11d4764e',31,''),(1102,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5','3e80e5896bf2e9f52a651939e4713837',1097,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d446d5',31,''),(1103,8,'appdata_oc5ev4nda8xn/preview/f/3/4','0a3f4616c0bf712a7a72eae91fa8c42c',1099,'4',2,1,0,1682686237,1682686237,0,0,'644bc11d46119',31,''),(1104,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6','a3605101d55776df21a0c0d8414fd6a8',1101,'6',2,1,0,1682686237,1682686237,0,0,'644bc11d474c5',31,''),(1105,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3','93ac55058f67cfd58ef84f593a100470',1102,'3',2,1,0,1682686237,1682686237,0,0,'644bc11d44561',31,''),(1106,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0','3e6c7ecfb42490efe28240f8b83c4d8e',1103,'0',2,1,0,1682686237,1682686237,0,0,'644bc11d45f8c',31,''),(1107,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7','6568d2500636056bfde908006448d7c2',1104,'7',2,1,0,1682686237,1682686237,0,0,'644bc11d4735a',31,''),(1108,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5','6615529d6e1a5fd5100d633491e581e8',1105,'5',2,1,0,1682686237,1682686237,0,0,'644bc11d443dd',31,''),(1109,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0','122c6131b915bd9c104fa60b2e086213',1107,'0',2,1,0,1682686237,1682686237,0,0,'644bc11d471d6',31,''),(1110,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f','13eb43950e987c8783f908745c7c5836',1106,'f',2,1,0,1682686237,1682686237,0,0,'644bc11d45e21',31,''),(1111,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5/240','1ecbd12aa3477318f398b3a71baaf74d',1108,'240',2,1,0,1682686237,1682686237,0,0,'644bc11d4417d',31,''),(1112,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0/239','3ba598cccb300add52f7fb49f4f4b6a4',1109,'239',2,1,0,1682686237,1682686237,0,0,'644bc11d46fab',31,''),(1113,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1','59bd20912676287131598bb0ef9d68c3',1110,'1',2,1,0,1682686237,1682686237,0,0,'644bc11d45be8',31,''),(1114,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b','5ed9eeb876065da45d70d3eb9f36ff40',1113,'b',2,1,0,1682686237,1682686237,0,0,'644bc11d45a5d',31,''),(1115,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b/241','33abe9706ae05f3fd5f5ad9d3858306f',1114,'241',2,1,0,1682686237,1682686237,0,0,'644bc11d4588a',31,''),(1116,8,'appdata_oc5ev4nda8xn/preview/c/0/4/2/f/4/d/332/256-144.jpg','502848f994e015d78197eefae79af3cb',1026,'256-144.jpg',5,3,9045,1682686237,1682686237,0,0,'9b7656961a6ac7416cc22857802f6d49',27,''),(1117,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0/239/1100-734-max.jpg','2e2b69be3a01817e8edded2733e7660c',1112,'1100-734-max.jpg',5,3,272443,1682686237,1682686237,0,0,'95cc361e535f8c5c754fe262ac915fab',27,''),(1118,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5/240/2000-1333-max.jpg','8a95e2f606c99d2f17198072eaa32b99',1111,'2000-1333-max.jpg',5,3,461107,1682686237,1682686237,0,0,'6629266739a223a7ff34ebd6741a9dce',27,''),(1119,8,'appdata_oc5ev4nda8xn/preview/5/5/5/d/6/7/0/239/256-171.jpg','178f586c937c5de85d67867ae30ebad5',1112,'256-171.jpg',5,3,16279,1682686237,1682686237,0,0,'5b2ef2363fd60abd7ccef82f76179cef',27,''),(1120,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b/241/2000-1333-max.jpg','5d117a2ed3eb4a92b84ae7e429bf6d3b',1115,'2000-1333-max.jpg',5,3,665863,1682686237,1682686237,0,0,'a99b651e5c919465efb83e52efa82427',27,''),(1121,8,'appdata_oc5ev4nda8xn/preview/3/3/5/f/5/3/5/240/256-171.jpg','367988441c72d5c7dcac61ed104aa309',1111,'256-171.jpg',5,3,12711,1682686237,1682686237,0,0,'c1a31bcfb93d41d5baa464b944edf8d6',27,''),(1122,8,'appdata_oc5ev4nda8xn/preview/f/3/4/0/f/1/b/241/256-171.jpg','fc746e7a26c6acd044b5e9ab3c4c4ae2',1115,'256-171.jpg',5,3,13404,1682686237,1682686237,0,0,'92d5b204608d27aa2f777a7435d6c1c8',27,'');
/*!40000 ALTER TABLE `oc_filecache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_filecache_extended`
--

DROP TABLE IF EXISTS `oc_filecache_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_filecache_extended` (
  `fileid` bigint unsigned NOT NULL,
  `metadata_etag` varchar(40) COLLATE utf8mb4_bin DEFAULT NULL,
  `creation_time` bigint NOT NULL DEFAULT '0',
  `upload_time` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`fileid`),
  KEY `fce_ctime_idx` (`creation_time`),
  KEY `fce_utime_idx` (`upload_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_filecache_extended`
--

LOCK TABLES `oc_filecache_extended` WRITE;
/*!40000 ALTER TABLE `oc_filecache_extended` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_filecache_extended` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_files_trash`
--

DROP TABLE IF EXISTS `oc_files_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_files_trash` (
  `auto_id` bigint NOT NULL AUTO_INCREMENT,
  `id` varchar(250) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `user` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `timestamp` varchar(12) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `location` varchar(512) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` varchar(4) COLLATE utf8mb4_bin DEFAULT NULL,
  `mime` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`auto_id`),
  KEY `id_index` (`id`),
  KEY `timestamp_index` (`timestamp`),
  KEY `user_index` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_files_trash`
--

LOCK TABLES `oc_files_trash` WRITE;
/*!40000 ALTER TABLE `oc_files_trash` DISABLE KEYS */;
INSERT INTO `oc_files_trash` VALUES (1,'deletedfile.txt','admin','1678356431','.',NULL,NULL);
/*!40000 ALTER TABLE `oc_files_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_flow_checks`
--

DROP TABLE IF EXISTS `oc_flow_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_flow_checks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `operator` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `value` longtext COLLATE utf8mb4_bin,
  `hash` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flow_unique_hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_flow_checks`
--

LOCK TABLES `oc_flow_checks` WRITE;
/*!40000 ALTER TABLE `oc_flow_checks` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_flow_checks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_flow_operations`
--

DROP TABLE IF EXISTS `oc_flow_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_flow_operations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `class` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `checks` longtext COLLATE utf8mb4_bin,
  `operation` longtext COLLATE utf8mb4_bin,
  `entity` varchar(256) COLLATE utf8mb4_bin NOT NULL DEFAULT 'OCAWorkflowEngineEntityFile',
  `events` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_flow_operations`
--

LOCK TABLES `oc_flow_operations` WRITE;
/*!40000 ALTER TABLE `oc_flow_operations` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_flow_operations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_flow_operations_scope`
--

DROP TABLE IF EXISTS `oc_flow_operations_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_flow_operations_scope` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `operation_id` int NOT NULL DEFAULT '0',
  `type` int NOT NULL DEFAULT '0',
  `value` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `flow_unique_scope` (`operation_id`,`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_flow_operations_scope`
--

LOCK TABLES `oc_flow_operations_scope` WRITE;
/*!40000 ALTER TABLE `oc_flow_operations_scope` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_flow_operations_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_group_admin`
--

DROP TABLE IF EXISTS `oc_group_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_group_admin` (
  `gid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`gid`,`uid`),
  KEY `group_admin_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_group_admin`
--

LOCK TABLES `oc_group_admin` WRITE;
/*!40000 ALTER TABLE `oc_group_admin` DISABLE KEYS */;
INSERT INTO `oc_group_admin` VALUES ('testgroup','testuser');
/*!40000 ALTER TABLE `oc_group_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_group_user`
--

DROP TABLE IF EXISTS `oc_group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_group_user` (
  `gid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`gid`,`uid`),
  KEY `gu_uid_index` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_group_user`
--

LOCK TABLES `oc_group_user` WRITE;
/*!40000 ALTER TABLE `oc_group_user` DISABLE KEYS */;
INSERT INTO `oc_group_user` VALUES ('admin','admin'),('testgroup','disableduser'),('testgroup','testuser'),('testgroup2','testuser2'),('testgroup','testuser3'),('testgroup2','testuser3'),('admin','vok'),('testgroup','vok');
/*!40000 ALTER TABLE `oc_group_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_groups`
--

DROP TABLE IF EXISTS `oc_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_groups` (
  `gid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `displayname` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT 'name',
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_groups`
--

LOCK TABLES `oc_groups` WRITE;
/*!40000 ALTER TABLE `oc_groups` DISABLE KEYS */;
INSERT INTO `oc_groups` VALUES ('admin','admin'),('emptygroup','emptygroup'),('testgroup','testgroup'),('testgroup2','testgroup2');
/*!40000 ALTER TABLE `oc_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_jobs`
--

DROP TABLE IF EXISTS `oc_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `argument` varchar(4000) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `last_run` int DEFAULT '0',
  `last_checked` int DEFAULT '0',
  `reserved_at` int DEFAULT '0',
  `execution_duration` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_class_index` (`class`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_jobs`
--

LOCK TABLES `oc_jobs` WRITE;
/*!40000 ALTER TABLE `oc_jobs` DISABLE KEYS */;
INSERT INTO `oc_jobs` VALUES (2,'OCA\\Files_Sharing\\DeleteOrphanedSharesJob','null',1682685732,1682686613,0,0),(3,'OCA\\Files_Sharing\\ExpireSharesJob','null',1682677618,1682686614,0,0),(4,'OCA\\Files_Versions\\BackgroundJob\\ExpireVersions','null',1682685732,1682686612,0,0),(5,'OCA\\Activity\\BackgroundJob\\EmailNotification','null',1682686612,1682686612,0,0),(6,'OCA\\Activity\\BackgroundJob\\ExpireActivities','null',1682676716,1682686612,0,0),(7,'OCA\\Files_Trashbin\\BackgroundJob\\ExpireTrash','null',1682684831,1682686612,0,0),(8,'OCA\\NextcloudAnnouncements\\Cron\\Crawler','null',1682603703,1682686612,0,0),(9,'OCA\\Federation\\SyncJob','null',1682677618,1682686612,0,0),(10,'OCA\\UpdateNotification\\Notification\\BackgroundJob','null',1678457922,1682720819,0,1),(11,'OCA\\Files\\BackgroundJob\\ScanFiles','null',1682686612,1682686612,0,1),(12,'OCA\\Files\\BackgroundJob\\DeleteOrphanedItems','null',1682684831,1682686613,0,0),(13,'OCA\\Files\\BackgroundJob\\CleanupFileLocks','null',1682686613,1682686613,0,0),(14,'OC\\Authentication\\Token\\DefaultTokenCleanupJob','null',1682686613,1682686613,0,0),(15,'OC\\Log\\Rotate','null',1682686613,1682686613,0,0),(36,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.ne\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956844}',0,1682720819,0,0),(37,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.net\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956849}',0,1682720819,0,0),(39,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.ne\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956869}',0,1682720819,0,0),(41,'OC\\Settings\\BackgroundJobs\\VerifyUserData','{\"verificationCode\":\"\",\"data\":\"kevin.vo@sunrise.net\",\"type\":\"email\",\"uid\":\"vok\",\"try\":1,\"lastRun\":1678956888}',0,1682720819,0,0),(48,'OC\\Settings\\RemoveOrphaned','null',0,1682720819,0,0),(49,'OC\\Repair\\NC11\\MoveAvatarsBackgroundJob','null',0,1682720819,0,0),(50,'OC\\Repair\\NC11\\CleanPreviewsBackgroundJob','{\"uid\":\"admin\"}',0,1682720819,0,0),(51,'OC\\Repair\\NC11\\CleanPreviewsBackgroundJob','{\"uid\":\"testuser2\"}',0,1682720819,0,0),(52,'OC\\Repair\\NC11\\CleanPreviewsBackgroundJob','{\"uid\":\"vok\"}',0,1682720819,0,0),(53,'OC\\Preview\\BackgroundCleanupJob','null',1682684831,1682686613,0,0),(55,'OCA\\DAV\\BackgroundJob\\CleanupDirectLinksJob','null',1682677618,1682686613,0,0),(56,'OCA\\DAV\\BackgroundJob\\UpdateCalendarResourcesRoomsBackgroundJob','null',1682684831,1682686613,0,0),(57,'OCA\\DAV\\BackgroundJob\\CleanupInvitationTokenJob','null',1682677618,1682686613,0,0),(58,'OCA\\Files_Sharing\\BackgroundJob\\FederatedSharesDiscoverJob','null',1682677618,1682686613,0,0),(59,'OCA\\Support\\BackgroundJobs\\CheckSubscription','null',1682686613,1682686613,0,0),(64,'OC\\Core\\BackgroundJobs\\CleanupLoginFlowV2','null',1682684831,1682686613,0,0),(66,'OCA\\DAV\\BackgroundJob\\EventReminderJob','null',1682686614,1682686613,0,0),(67,'OCA\\Text\\Cron\\Cleanup','null',1682686614,1682686614,0,0),(68,'OCA\\Files\\BackgroundJob\\CleanupDirectEditingTokens','null',1682685733,1682686614,0,0),(69,'OCA\\WorkflowEngine\\BackgroundJobs\\Rotate','null',1682677618,1682686614,0,0),(70,'OCA\\ContactsInteraction\\BackgroundJob\\CleanupJob','null',1682677618,1682686614,0,1),(78,'OCA\\Activity\\BackgroundJob\\DigestMail','null',1682684831,1682686614,0,0),(80,'OCA\\ServerInfo\\Jobs\\UpdateStorageStats','null',1682677619,1682686614,0,0),(82,'OCA\\UserStatus\\BackgroundJob\\ClearOldStatusesBackgroundJob','null',1682686614,1682686614,0,0);
/*!40000 ALTER TABLE `oc_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_login_flow_v2`
--

DROP TABLE IF EXISTS `oc_login_flow_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_login_flow_v2` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint unsigned NOT NULL,
  `started` smallint unsigned NOT NULL DEFAULT '0',
  `poll_token` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `login_token` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `public_key` text COLLATE utf8mb4_bin NOT NULL,
  `private_key` text COLLATE utf8mb4_bin NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `login_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `server` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `app_password` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `poll_token` (`poll_token`),
  UNIQUE KEY `login_token` (`login_token`),
  KEY `timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_login_flow_v2`
--

LOCK TABLES `oc_login_flow_v2` WRITE;
/*!40000 ALTER TABLE `oc_login_flow_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_login_flow_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_migrations`
--

DROP TABLE IF EXISTS `oc_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_migrations` (
  `app` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `version` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`app`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_migrations`
--

LOCK TABLES `oc_migrations` WRITE;
/*!40000 ALTER TABLE `oc_migrations` DISABLE KEYS */;
INSERT INTO `oc_migrations` VALUES ('activity','2006Date20170808154933'),('activity','2006Date20170808155040'),('activity','2006Date20170919095939'),('activity','2007Date20181107114613'),('activity','2008Date20181011095117'),('activity','2010Date20190416112817'),('activity','2011Date20201006132544'),('activity','2011Date20201006132545'),('activity','2011Date20201006132546'),('activity','2011Date20201006132547'),('activity','2011Date20201207091915'),('contactsinteraction','010000Date20200304152605'),('core','13000Date20170705121758'),('core','13000Date20170718121200'),('core','13000Date20170814074715'),('core','13000Date20170919121250'),('core','13000Date20170926101637'),('core','13000Date20180516101403'),('core','14000Date20180129121024'),('core','14000Date20180404140050'),('core','14000Date20180516101403'),('core','14000Date20180518120534'),('core','14000Date20180522074438'),('core','14000Date20180626223656'),('core','14000Date20180710092004'),('core','14000Date20180712153140'),('core','15000Date20180926101451'),('core','15000Date20181015062942'),('core','15000Date20181029084625'),('core','16000Date20190207141427'),('core','16000Date20190212081545'),('core','16000Date20190427105638'),('core','16000Date20190428150708'),('core','17000Date20190514105811'),('core','18000Date20190920085628'),('core','18000Date20191014105105'),('core','18000Date20191204114856'),('core','19000Date20200211083441'),('core','20000Date20201109081915'),('core','20000Date20201109081918'),('core','20000Date20201109081919'),('core','20000Date20201111081915'),('core','21000Date20201120141228'),('core','23000Date20210906132259'),('dav','1004Date20170825134824'),('dav','1004Date20170919104507'),('dav','1004Date20170924124212'),('dav','1004Date20170926103422'),('dav','1005Date20180413093149'),('dav','1005Date20180530124431'),('dav','1006Date20180619154313'),('dav','1006Date20180628111625'),('dav','1008Date20181030113700'),('dav','1008Date20181105104826'),('dav','1008Date20181105104833'),('dav','1008Date20181105110300'),('dav','1008Date20181105112049'),('dav','1008Date20181114084440'),('dav','1011Date20190725113607'),('dav','1011Date20190806104428'),('dav','1012Date20190808122342'),('dav','1016Date20201109085907'),('federatedfilesharing','1010Date20200630191755'),('federatedfilesharing','1011Date20201120125158'),('federation','1010Date20200630191302'),('files','11301Date20191205150729'),('files_sharing','11300Date20201120141438'),('files_sharing','21000Date20201223143245'),('files_trashbin','1010Date20200630192639'),('notifications','2004Date20190107135757'),('oauth2','010401Date20181207190718'),('oauth2','010402Date20190107124745'),('privacy','100Date20190217131943'),('text','010000Date20190617184535'),('text','030001Date20200402075029'),('text','030201Date20201116110353'),('text','030201Date20201116123153'),('twofactor_backupcodes','1002Date20170607104347'),('twofactor_backupcodes','1002Date20170607113030'),('twofactor_backupcodes','1002Date20170919123342'),('twofactor_backupcodes','1002Date20170926101419'),('twofactor_backupcodes','1002Date20180821043638'),('user_status','0001Date20200602134824'),('user_status','0002Date20200902144824'),('user_status','1000Date20201111130204'),('workflowengine','2000Date20190808074233'),('workflowengine','2200Date20210805101925');
/*!40000 ALTER TABLE `oc_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_mimetypes`
--

DROP TABLE IF EXISTS `oc_mimetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_mimetypes` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `mimetype` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mimetype_id_index` (`mimetype`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_mimetypes`
--

LOCK TABLES `oc_mimetypes` WRITE;
/*!40000 ALTER TABLE `oc_mimetypes` DISABLE KEYS */;
INSERT INTO `oc_mimetypes` VALUES (8,'application'),(23,'application/comicbook+7z'),(24,'application/comicbook+ace'),(25,'application/comicbook+rar'),(26,'application/comicbook+tar'),(27,'application/comicbook+truecrypt'),(28,'application/comicbook+zip'),(13,'application/javascript'),(20,'application/json'),(14,'application/octet-stream'),(9,'application/pdf'),(33,'application/vnd.oasis.opendocument.graphics-template'),(32,'application/vnd.oasis.opendocument.presentation-template'),(31,'application/vnd.oasis.opendocument.spreadsheet-template'),(10,'application/vnd.oasis.opendocument.text'),(30,'application/vnd.oasis.opendocument.text-template'),(21,'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),(18,'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),(22,'application/vnd.visio'),(15,'application/x-gzip'),(19,'application/yaml'),(1,'httpd'),(2,'httpd/unix-directory'),(3,'image'),(34,'image/heic'),(5,'image/jpeg'),(4,'image/png'),(17,'image/svg+xml'),(11,'text'),(16,'text/css'),(29,'text/html'),(12,'text/plain'),(6,'video'),(7,'video/mp4'),(35,'video/quicktime');
/*!40000 ALTER TABLE `oc_mimetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_mounts`
--

DROP TABLE IF EXISTS `oc_mounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_mounts` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `storage_id` bigint NOT NULL,
  `root_id` bigint NOT NULL,
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `mount_point` varchar(4000) COLLATE utf8mb4_bin NOT NULL,
  `mount_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mounts_user_root_index` (`user_id`,`root_id`),
  KEY `mounts_user_index` (`user_id`),
  KEY `mounts_storage_index` (`storage_id`),
  KEY `mounts_root_index` (`root_id`),
  KEY `mounts_mount_id_index` (`mount_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_mounts`
--

LOCK TABLES `oc_mounts` WRITE;
/*!40000 ALTER TABLE `oc_mounts` DISABLE KEYS */;
INSERT INTO `oc_mounts` VALUES (1,1,1,'admin','/admin/',NULL),(2,7,234,'vok','/vok/',NULL),(3,5,340,'testuser2','/testuser2/',NULL),(4,3,955,'disableduser','/disableduser/',NULL),(5,4,956,'testuser','/testuser/',NULL),(6,6,957,'testuser3','/testuser3/',NULL);
/*!40000 ALTER TABLE `oc_mounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_notifications`
--

DROP TABLE IF EXISTS `oc_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_notifications` (
  `notification_id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `user` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `timestamp` int NOT NULL DEFAULT '0',
  `object_type` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `object_id` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `subject` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `subject_parameters` longtext COLLATE utf8mb4_bin,
  `message` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `message_parameters` longtext COLLATE utf8mb4_bin,
  `link` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `icon` varchar(4000) COLLATE utf8mb4_bin DEFAULT NULL,
  `actions` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`notification_id`),
  KEY `oc_notifications_app` (`app`),
  KEY `oc_notifications_user` (`user`),
  KEY `oc_notifications_timestamp` (`timestamp`),
  KEY `oc_notifications_object` (`object_type`,`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_notifications`
--

LOCK TABLES `oc_notifications` WRITE;
/*!40000 ALTER TABLE `oc_notifications` DISABLE KEYS */;
INSERT INTO `oc_notifications` VALUES (1,'firstrunwizard','admin',1678356375,'user','admin','profile','[]','','[]','','','[]'),(2,'firstrunwizard','vok',1678358784,'user','vok','profile','[]','','[]','','','[]'),(3,'admin_notifications','admin',1681984784,'admin_notifications','64410d10','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 16.0.11snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(4,'admin_notifications','vok',1681984785,'admin_notifications','64410d11','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 16.0.11snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(5,'admin_notifications','admin',1681984939,'admin_notifications','64410dab','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 17.0.10snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(6,'admin_notifications','vok',1681984941,'admin_notifications','64410dad','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 17.0.10snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(7,'admin_notifications','admin',1681985075,'admin_notifications','64410e33','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 18.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(8,'admin_notifications','vok',1681985077,'admin_notifications','64410e35','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 18.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(9,'firstrunwizard','testuser2',1681985164,'user','testuser2','profile','[]','','[]','','','[]'),(10,'survey_client','admin',1681985166,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(11,'survey_client','vok',1681985166,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(12,'admin_notifications','admin',1681985170,'admin_notifications','64410e92','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 19.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(13,'admin_notifications','vok',1681985173,'admin_notifications','64410e95','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 19.0.12snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(14,'survey_client','admin',1681985341,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(15,'survey_client','vok',1681985341,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(16,'admin_notifications','admin',1681985349,'admin_notifications','64410f45','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 20.0.14snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(17,'admin_notifications','vok',1681985351,'admin_notifications','64410f47','cli','[\"Nextcloud updated\"]','cli','[\"The Nextcloud snap updated itself to version 20.0.14snap1. We are dedicated to ensuring these updates work amazingly well, but in the unlikely event something broke, remember you can revert the update with a single command:\\n\\n    $ sudo snap revert nextcloud\\n\\n Please also don\'t forget to log an issue: https:\\/\\/github.com\\/nextcloud\\/nextcloud-snap\"]','','','[]'),(18,'survey_client','admin',1681986242,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(19,'survey_client','vok',1681986242,'dummy','23','updated','[]','','[]','','','[{\"label\":\"enable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"POST\",\"primary\":true},{\"label\":\"disable\",\"link\":\"http:\\/\\/square-liger.maas\\/ocs\\/v2.php\\/apps\\/survey_client\\/api\\/v1\\/monthly\",\"type\":\"DELETE\",\"primary\":false}]'),(20,'firstrunwizard','vok',1682686195,'app','groupfolders','apphint-groupfolders','[]','','[]','','','[]'),(21,'firstrunwizard','vok',1682686195,'app','social','apphint-social','[]','','[]','','','[]'),(22,'firstrunwizard','vok',1682686195,'app','notes','apphint-notes','[]','','[]','','','[]'),(23,'firstrunwizard','vok',1682686195,'app','deck','apphint-deck','[]','','[]','','','[]'),(24,'firstrunwizard','vok',1682686195,'app','tasks','apphint-tasks','[]','','[]','','','[]');
/*!40000 ALTER TABLE `oc_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_notifications_pushtokens`
--

DROP TABLE IF EXISTS `oc_notifications_pushtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_notifications_pushtokens` (
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `token` int NOT NULL DEFAULT '0',
  `deviceidentifier` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `devicepublickey` varchar(512) COLLATE utf8mb4_bin NOT NULL,
  `devicepublickeyhash` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `pushtokenhash` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `proxyserver` varchar(256) COLLATE utf8mb4_bin NOT NULL,
  `apptype` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT 'unknown',
  UNIQUE KEY `oc_notifpushtoken` (`uid`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_notifications_pushtokens`
--

LOCK TABLES `oc_notifications_pushtokens` WRITE;
/*!40000 ALTER TABLE `oc_notifications_pushtokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_notifications_pushtokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_oauth2_access_tokens`
--

DROP TABLE IF EXISTS `oc_oauth2_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_oauth2_access_tokens` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `token_id` int NOT NULL,
  `client_id` int NOT NULL,
  `hashed_code` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `encrypted_token` varchar(786) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_access_hash_idx` (`hashed_code`),
  KEY `oauth2_access_client_id_idx` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_oauth2_access_tokens`
--

LOCK TABLES `oc_oauth2_access_tokens` WRITE;
/*!40000 ALTER TABLE `oc_oauth2_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_oauth2_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_oauth2_clients`
--

DROP TABLE IF EXISTS `oc_oauth2_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_oauth2_clients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `redirect_uri` varchar(2000) COLLATE utf8mb4_bin NOT NULL,
  `client_identifier` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `secret` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_client_id_idx` (`client_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_oauth2_clients`
--

LOCK TABLES `oc_oauth2_clients` WRITE;
/*!40000 ALTER TABLE `oc_oauth2_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_oauth2_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_preferences`
--

DROP TABLE IF EXISTS `oc_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_preferences` (
  `userid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `appid` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configkey` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `configvalue` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`userid`,`appid`,`configkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_preferences`
--

LOCK TABLES `oc_preferences` WRITE;
/*!40000 ALTER TABLE `oc_preferences` DISABLE KEYS */;
INSERT INTO `oc_preferences` VALUES ('admin','avatar','generated','false'),('admin','avatar','version','2'),('admin','core','lang','en'),('admin','core','timezone','Europe/Berlin'),('admin','files','file_sorting','name'),('admin','files','file_sorting_direction','asc'),('admin','firstrunwizard','show','0'),('admin','login','lastLogin','1681984278'),('admin','lookup_server_connector','dataSend','1'),('disableduser','avatar','generated','false'),('disableduser','core','enabled','false'),('testuser','avatar','generated','false'),('testuser','files','quota','1 GB'),('testuser2','avatar','generated','false'),('testuser2','core','lang','en'),('testuser2','core','timezone','Europe/Berlin'),('testuser2','files','quota','5 GB'),('testuser2','firstrunwizard','show','0'),('testuser2','login','lastLogin','1678956925'),('testuser2','lookup_server_connector','dataSend','1'),('testuser3','avatar','generated','false'),('testuser3','files','quota','395 MB'),('vok','activity','configured','yes'),('vok','avatar','generated','true'),('vok','core','lang','en'),('vok','core','timezone','Europe/Zurich'),('vok','dashboard','firstRun','0'),('vok','firstrunwizard','apphint','18'),('vok','firstrunwizard','show','0'),('vok','login','lastLogin','1682686195'),('vok','login_token','y2IX25Np5GfRhc+Ag1sBtGxZWATKRcWq','1682686195'),('vok','lookup_server_connector','dataSend','1'),('vok','password_policy','failedLoginAttempts','0'),('vok','settings','email','kevin.vo@sunrise.net');
/*!40000 ALTER TABLE `oc_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_privacy_admins`
--

DROP TABLE IF EXISTS `oc_privacy_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_privacy_admins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `displayname` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_privacy_admins`
--

LOCK TABLES `oc_privacy_admins` WRITE;
/*!40000 ALTER TABLE `oc_privacy_admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_privacy_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_properties`
--

DROP TABLE IF EXISTS `oc_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_properties` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `userid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `propertypath` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `propertyname` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `propertyvalue` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_index` (`userid`),
  KEY `properties_path_index` (`userid`,`propertypath`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_properties`
--

LOCK TABLES `oc_properties` WRITE;
/*!40000 ALTER TABLE `oc_properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_ratelimit_entries`
--

DROP TABLE IF EXISTS `oc_ratelimit_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_ratelimit_entries` (
  `hash` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `delete_after` datetime NOT NULL,
  KEY `ratelimit_hash` (`hash`),
  KEY `ratelimit_delete_after` (`delete_after`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_ratelimit_entries`
--

LOCK TABLES `oc_ratelimit_entries` WRITE;
/*!40000 ALTER TABLE `oc_ratelimit_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_ratelimit_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_recent_contact`
--

DROP TABLE IF EXISTS `oc_recent_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_recent_contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `actor_uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `uid` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `federated_cloud_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `card` longblob NOT NULL,
  `last_contact` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recent_contact_actor_uid` (`actor_uid`),
  KEY `recent_contact_id_uid` (`id`,`actor_uid`),
  KEY `recent_contact_uid` (`uid`),
  KEY `recent_contact_email` (`email`),
  KEY `recent_contact_fed_id` (`federated_cloud_id`),
  KEY `recent_contact_last_contact` (`last_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_recent_contact`
--

LOCK TABLES `oc_recent_contact` WRITE;
/*!40000 ALTER TABLE `oc_recent_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_recent_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_schedulingobjects`
--

DROP TABLE IF EXISTS `oc_schedulingobjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_schedulingobjects` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `principaluri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `calendardata` longblob,
  `uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastmodified` int unsigned DEFAULT NULL,
  `etag` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `size` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `schedulobj_principuri_index` (`principaluri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_schedulingobjects`
--

LOCK TABLES `oc_schedulingobjects` WRITE;
/*!40000 ALTER TABLE `oc_schedulingobjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_schedulingobjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_share`
--

DROP TABLE IF EXISTS `oc_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_share` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `share_type` smallint NOT NULL DEFAULT '0',
  `share_with` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uid_owner` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid_initiator` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `parent` bigint DEFAULT NULL,
  `item_type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `item_source` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `item_target` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `file_source` bigint DEFAULT NULL,
  `file_target` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `permissions` smallint NOT NULL DEFAULT '0',
  `stime` bigint NOT NULL DEFAULT '0',
  `accepted` smallint NOT NULL DEFAULT '0',
  `expiration` datetime DEFAULT NULL,
  `token` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `mail_send` smallint NOT NULL DEFAULT '0',
  `share_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `password_by_talk` tinyint(1) DEFAULT '0',
  `note` longtext COLLATE utf8mb4_bin,
  `hide_download` smallint DEFAULT '0',
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_share_type_index` (`item_type`,`share_type`),
  KEY `file_source_index` (`file_source`),
  KEY `token_index` (`token`),
  KEY `share_with_index` (`share_with`),
  KEY `parent_index` (`parent`),
  KEY `owner_index` (`uid_owner`),
  KEY `initiator_index` (`uid_initiator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_share`
--

LOCK TABLES `oc_share` WRITE;
/*!40000 ALTER TABLE `oc_share` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_share_external`
--

DROP TABLE IF EXISTS `oc_share_external`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_share_external` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `remote` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT 'Url of the remove owncloud instance',
  `remote_id` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `share_token` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'Public share token',
  `password` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Optional password for the public share',
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'Original name on the remote server',
  `owner` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'User that owns the public share on the remote server',
  `user` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'Local user which added the external share',
  `mountpoint` varchar(4000) COLLATE utf8mb4_bin NOT NULL COMMENT 'Full path where the share is mounted',
  `mountpoint_hash` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'md5 hash of the mountpoint',
  `accepted` int NOT NULL DEFAULT '0',
  `parent` bigint DEFAULT '-1',
  `share_type` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sh_external_mp` (`user`,`mountpoint_hash`),
  KEY `sh_external_user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_share_external`
--

LOCK TABLES `oc_share_external` WRITE;
/*!40000 ALTER TABLE `oc_share_external` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_share_external` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_storages`
--

DROP TABLE IF EXISTS `oc_storages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_storages` (
  `numeric_id` bigint NOT NULL AUTO_INCREMENT,
  `id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `available` int NOT NULL DEFAULT '1',
  `last_checked` int DEFAULT NULL,
  PRIMARY KEY (`numeric_id`),
  UNIQUE KEY `storages_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_storages`
--

LOCK TABLES `oc_storages` WRITE;
/*!40000 ALTER TABLE `oc_storages` DISABLE KEYS */;
INSERT INTO `oc_storages` VALUES (1,'home::admin',1,NULL),(2,'local::/var/www/nextcloud/data/',1,NULL),(3,'home::disableduser',1,NULL),(4,'home::testuser',1,NULL),(5,'home::testuser2',1,NULL),(6,'home::testuser3',1,NULL),(7,'home::vok',1,NULL),(8,'local::/var/snap/nextcloud/common/nextcloud/data/',1,NULL);
/*!40000 ALTER TABLE `oc_storages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_storages_credentials`
--

DROP TABLE IF EXISTS `oc_storages_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_storages_credentials` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `identifier` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `credentials` longtext COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stocred_ui` (`user`,`identifier`),
  KEY `stocred_user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_storages_credentials`
--

LOCK TABLES `oc_storages_credentials` WRITE;
/*!40000 ALTER TABLE `oc_storages_credentials` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_storages_credentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_systemtag`
--

DROP TABLE IF EXISTS `oc_systemtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_systemtag` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `visibility` smallint NOT NULL DEFAULT '1',
  `editable` smallint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_ident` (`name`,`visibility`,`editable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_systemtag`
--

LOCK TABLES `oc_systemtag` WRITE;
/*!40000 ALTER TABLE `oc_systemtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_systemtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_systemtag_group`
--

DROP TABLE IF EXISTS `oc_systemtag_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_systemtag_group` (
  `gid` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `systemtagid` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gid`,`systemtagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_systemtag_group`
--

LOCK TABLES `oc_systemtag_group` WRITE;
/*!40000 ALTER TABLE `oc_systemtag_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_systemtag_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_systemtag_object_mapping`
--

DROP TABLE IF EXISTS `oc_systemtag_object_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_systemtag_object_mapping` (
  `objectid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `objecttype` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `systemtagid` bigint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`objecttype`,`objectid`,`systemtagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_systemtag_object_mapping`
--

LOCK TABLES `oc_systemtag_object_mapping` WRITE;
/*!40000 ALTER TABLE `oc_systemtag_object_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_systemtag_object_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_text_documents`
--

DROP TABLE IF EXISTS `oc_text_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_text_documents` (
  `id` bigint unsigned NOT NULL,
  `current_version` bigint unsigned DEFAULT '0',
  `last_saved_version` bigint unsigned DEFAULT '0',
  `last_saved_version_time` bigint unsigned NOT NULL,
  `last_saved_version_etag` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  `base_version_etag` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_text_documents`
--

LOCK TABLES `oc_text_documents` WRITE;
/*!40000 ALTER TABLE `oc_text_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_text_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_text_sessions`
--

DROP TABLE IF EXISTS `oc_text_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_text_sessions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `guest_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `color` varchar(7) COLLATE utf8mb4_bin DEFAULT NULL,
  `token` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `document_id` bigint NOT NULL,
  `last_contact` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rd_session_token_idx` (`token`),
  KEY `ts_docid_lastcontact` (`document_id`,`last_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_text_sessions`
--

LOCK TABLES `oc_text_sessions` WRITE;
/*!40000 ALTER TABLE `oc_text_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_text_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_text_steps`
--

DROP TABLE IF EXISTS `oc_text_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_text_steps` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `document_id` bigint unsigned NOT NULL,
  `session_id` bigint unsigned NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  `version` bigint unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rd_steps_did_idx` (`document_id`),
  KEY `rd_steps_version_idx` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_text_steps`
--

LOCK TABLES `oc_text_steps` WRITE;
/*!40000 ALTER TABLE `oc_text_steps` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_text_steps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_trusted_servers`
--

DROP TABLE IF EXISTS `oc_trusted_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_trusted_servers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT 'Url of trusted server',
  `url_hash` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'sha1 hash of the url without the protocol',
  `token` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'token used to exchange the shared secret',
  `shared_secret` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'shared secret used to authenticate',
  `status` int NOT NULL DEFAULT '2' COMMENT 'current status of the connection',
  `sync_token` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'cardDav sync token',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_hash` (`url_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_trusted_servers`
--

LOCK TABLES `oc_trusted_servers` WRITE;
/*!40000 ALTER TABLE `oc_trusted_servers` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_trusted_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_twofactor_backupcodes`
--

DROP TABLE IF EXISTS `oc_twofactor_backupcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_twofactor_backupcodes` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `code` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `used` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `twofactor_backupcodes_uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_twofactor_backupcodes`
--

LOCK TABLES `oc_twofactor_backupcodes` WRITE;
/*!40000 ALTER TABLE `oc_twofactor_backupcodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_twofactor_backupcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_twofactor_providers`
--

DROP TABLE IF EXISTS `oc_twofactor_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_twofactor_providers` (
  `provider_id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `enabled` smallint NOT NULL,
  PRIMARY KEY (`provider_id`,`uid`),
  KEY `twofactor_providers_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_twofactor_providers`
--

LOCK TABLES `oc_twofactor_providers` WRITE;
/*!40000 ALTER TABLE `oc_twofactor_providers` DISABLE KEYS */;
INSERT INTO `oc_twofactor_providers` VALUES ('backup_codes','admin',0),('backup_codes','testuser2',0),('backup_codes','vok',0);
/*!40000 ALTER TABLE `oc_twofactor_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_user_status`
--

DROP TABLE IF EXISTS `oc_user_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_user_status` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `status_timestamp` int unsigned NOT NULL,
  `is_user_defined` tinyint(1) DEFAULT NULL,
  `message_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `custom_icon` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `custom_message` longtext COLLATE utf8mb4_bin,
  `clear_at` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_status_uid_ix` (`user_id`),
  KEY `user_status_clr_ix` (`clear_at`),
  KEY `user_status_tstmp_ix` (`status_timestamp`),
  KEY `user_status_iud_ix` (`is_user_defined`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_user_status`
--

LOCK TABLES `oc_user_status` WRITE;
/*!40000 ALTER TABLE `oc_user_status` DISABLE KEYS */;
INSERT INTO `oc_user_status` VALUES (1,'vok','online',1682686203,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `oc_user_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_user_transfer_owner`
--

DROP TABLE IF EXISTS `oc_user_transfer_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_user_transfer_owner` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `source_user` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `target_user` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `file_id` bigint NOT NULL,
  `node_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_user_transfer_owner`
--

LOCK TABLES `oc_user_transfer_owner` WRITE;
/*!40000 ALTER TABLE `oc_user_transfer_owner` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_user_transfer_owner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_users`
--

DROP TABLE IF EXISTS `oc_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_users` (
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `displayname` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `uid_lower` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `user_uid_lower` (`uid_lower`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_users`
--

LOCK TABLES `oc_users` WRITE;
/*!40000 ALTER TABLE `oc_users` DISABLE KEYS */;
INSERT INTO `oc_users` VALUES ('admin',NULL,'1|$2y$10$pwuAfa9lvVEXM6c.3XvodetHQI5yb7crJsuiaXwpis05lPvP0Frnm','admin'),('disableduser',NULL,'1|$2y$10$HIAL/GP0YIT7bvVC/EbcNuqntJ8dIyvVGJJgVVQL6sDLUgrCyZkDO','disableduser'),('testuser',NULL,'1|$2y$10$94cGGZtHKjsL5w9zkOrjl.61/2r81sX5fb4XJqrBrxC/XMvTLXt3a','testuser'),('testuser2',NULL,'1|$2y$10$tZz1S34E5EqSMJ95bLuoheqe7PJ/vo1DDipJPnoGedm2ywP4Ag1je','testuser2'),('testuser3',NULL,'1|$2y$10$WmvFmouMzydpIzM/lpzqzexRzg92PRyklEtlYevF1Jk.AerHZvdVa','testuser3'),('vok',NULL,'3|$argon2id$v=19$m=65536,t=4,p=1$di9GbHc5cm0yVXdBaDNqQg$BS3ngT8dGdrraMVSZNMCRV3mwUAfEHc+cUi1cYpBWTw','vok');
/*!40000 ALTER TABLE `oc_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_vcategory`
--

DROP TABLE IF EXISTS `oc_vcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_vcategory` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `category` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `uid_index` (`uid`),
  KEY `type_index` (`type`),
  KEY `category_index` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_vcategory`
--

LOCK TABLES `oc_vcategory` WRITE;
/*!40000 ALTER TABLE `oc_vcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_vcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_vcategory_to_object`
--

DROP TABLE IF EXISTS `oc_vcategory_to_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_vcategory_to_object` (
  `categoryid` bigint unsigned NOT NULL DEFAULT '0',
  `objid` bigint unsigned NOT NULL DEFAULT '0',
  `type` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`categoryid`,`objid`,`type`),
  KEY `vcategory_objectd_index` (`objid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_vcategory_to_object`
--

LOCK TABLES `oc_vcategory_to_object` WRITE;
/*!40000 ALTER TABLE `oc_vcategory_to_object` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_vcategory_to_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_webauthn`
--

DROP TABLE IF EXISTS `oc_webauthn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_webauthn` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `public_key_credential_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `webauthn_uid` (`uid`),
  KEY `webauthn_publicKeyCredentialId` (`public_key_credential_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_webauthn`
--

LOCK TABLES `oc_webauthn` WRITE;
/*!40000 ALTER TABLE `oc_webauthn` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_webauthn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oc_whats_new`
--

DROP TABLE IF EXISTS `oc_whats_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oc_whats_new` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '11',
  `etag` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `last_check` int unsigned NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `version` (`version`),
  KEY `version_etag_idx` (`version`,`etag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_whats_new`
--

LOCK TABLES `oc_whats_new` WRITE;
/*!40000 ALTER TABLE `oc_whats_new` DISABLE KEYS */;
/*!40000 ALTER TABLE `oc_whats_new` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-28 12:59:48
